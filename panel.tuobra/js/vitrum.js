/*********************************************/
/* TU OBRA JS 								 */
/*********************************************/
var url = '';
var titulo = '';
var idobjeto = 0;
var ejercicio = (new Date).getFullYear();
var recurso = 'contrato';

$(document).ready(function(e) {




	/*todos los campos en mayusculas*/
	$('body').on('keyup','input:not(input[type=url],input[type=email], .notUpper), textarea, #url-cliente', function(){
		this.value = this.value.toLocaleUpperCase();
	});



	$("#tipo-opcion-new").change(function(){
		if($(this).val() == 1){
			/*titulo = 'Contratos';
			url = "include/contratos";*/
			window.location.hash = "";
			}else{
				/*titulo = 'Licitaciones';
				url = "include/licitaciones";*/
				window.location.hash = "licitaciones";
				recurso = 'licitacion';
				}
	//changeUrl();
	});
	/*obs*/
	cantidadObs();
	/*nueva opcion*/
	var new_opcion = $('#tipo-opcion-new').val();
	/*loading*/
	$("#loading-screen").show();
	/*seleccionar el ejercicio actual*/
    $("#" + ejercicio).addClass('selected');
	/*obtener hash y partirlo*/
	var getHash = window.location.hash.split('-');
	/*obtener la opcion seleccionada*/
	var opcion_seleccionada = getHash[0].replace(/^#/,'');
	/*obtener el id del contrato*/
	var contratoid = getHash[1];
	/*detectar si biene un hash*/
	if(getHash[0]){
		/*si biene entonces mandar al opcion*/
		window.location = window.location.href.split('#')[0];
		}else{
			/*detectar la nueva opcion*/
			if(new_opcion == 1){
				/*si no biene entonces cargar la lista de contratos*/
				$("#contenedor-datos").load('include/contratos.php',{eje: ejercicio}, function(){ $("#loading-screen").hide(); });
				}else{
					$("#contenedor-datos").load('include/licitaciones.php',{eje: ejercicio}, function(){ $("#loading-screen").hide(); });
					}
			}
});
/*detectar cambios en el hash*/
$(window).bind('hashchange', function() {
	/*split hash*/
	var getHash = window.location.hash.split('-');
	/*obtener la opcion seleccionada*/
	var opcion_seleccionada = getHash[0].replace(/^#/,'');
	/*comprobar que venga el hash*/
	if(getHash[0]){
		/*si bien el hash poner la opciones seleccionada al borrar*/
		$("#borrar-recursos").attr('rel', opcion_seleccionada);
		}else{
			/*si no bien el hash entonces poner contratos*/
			$("#borrar-recursos").attr('rel', 'contratos');
			}
	/*set el numero de contrato*/
	var contratoid = getHash[1];
	/*borrar la cantidad a borrar*/
	$(".cantidad-borrar").text('0');
	/*comprobar que el boton editar tiene la clase no-editar */
	if($("#editar-recurso").hasClass('no-editar')){
		/*opciones del avance fotografico*/
		$('#nuevo-recurso, #editar-recurso, #borrar-recursos').removeClass('no-editar');
		/*modificar la opcion nuevo*/
		$('#nuevo-recurso').attr('href', 'home/#nuevo');
		/*modificar la opcion editar*/
		$("#editar-recurso").addClass('editar').attr('href', 'home/#editar');
		/*modificar la opcion borrar*/
		$("#borrar-recursos").attr('href', 'javascript:;');
		}
	/*checar si es licitacion*/



	if(opcion_seleccionada == 'licitaciones'){
		/*entoces agregar licitacion*/
		recurso = 'licitacion';

		}
	/*detectar URL*/
	detectarUrl(opcion_seleccionada, contratoid);

	//alert(opcion_seleccionada+' '+recurso);

});
/*detectar el cambio de hash*/
function detectarUrl(opcion_seleccionada, contrato){

	//console.log(opcion_seleccionada);


	/*var tipo de opcion nuevo*/
	var opcion_new = $('#tipo-opcion-new').val();
	/*comprobar si la accion es editar*/
	if(opcion_seleccionada === 'editar'){
		/*si editar entonces checar si se ha seleccionado un elemento*/
		if($('.check_opciones:input:checkbox:checked').length !== 1){
			alert('Debe seleccionar un elemento.');
			window.history.back();
			return false;
			}
		}
	/*comprobar si hay elementos para borrar*/
	if(opcion_seleccionada === 'borrar'){
		/*comprobar si hay elementos seleccionados*/
		if($('.check_opciones:input:checkbox:checked').length <= 0){
			alert('Debe seleccionar un elemento.');
			window.history.back();
			return false;
			}
		}
	/*si biene el contrato*/
	if(contrato){
		/*entonces id del objeto es igual al contrato*/
		idobjeto = contrato;
		}
	/*ocultar el buscador*/
	$("#buscador-form").hide();$(".return-contratos").show();
	/*determinar la opcion de nuevo con nueva opcion*/
	/*determinar la accion*/
	switch(opcion_seleccionada){
		case 'nuevo':
			switch(recurso){
				case 'contrato':
					url = 'forms/add-contratos';
					titulo = 'Agregar nuevo contrato';
				break;
				case 'estudios':
					url = 'forms/add-estudios';
					titulo = 'Agregar nuevo estudio y proyecto';
				break;
				case 'avances':
					url = 'forms/add-avance';
					titulo = 'Agregar nuevo avance';
				break;
				case 'fotos':
					url = 'forms/add-fotos';
					titulo = 'Agregar nueva foto';
				break;
				case 'convenios':
					url = 'forms/add-convenios-modificatorios';
					titulo = 'Agregar nuevo convenio';
				break;
				case 'expediente':
					url = 'forms/add-expediente';
					titulo = 'Agregar nuevo documento al expediente';
				break;
				case 'contratistas':
					url = 'forms/add-contratista';
					titulo = 'Agregar nuevo contratista';
				break;
				case 'unidades':
					url = 'forms/add-unidad-administrativa';
					titulo = 'Agregar nueva unidad administrativa';
				break;
				case 'supervision':
					url = 'forms/add-supervision-externa';
					titulo = 'Agregar nueva supervision externa';
				break;
				case 'residentes':
					url = 'forms/add-residente';
					titulo = 'Agregar nuevo residente';
				break;
				case 'usuarios':
					url = 'forms/add-usuario';
					titulo = 'Agregar nuevo usuario';
				break;
				case 'licitacion':
					url = 'forms/add-licitacion';
					titulo = 'Agregar nueva licitación';
				break;

				case 'licitaciones':
					url = 'forms/add-licitacion';
					titulo = 'Agregar nueva licitación';
				break;


				case 'eventos':
					url = "forms/add-eventos";
					titulo = "Agregar eventos de la licitación";
				break;
				}
		break;
		case 'editar':
			switch(recurso){
				case 'contrato':
					url = 'forms/add-contratos';
					titulo = 'Editar contrato';
				break;
				case 'estudios':
					url = 'forms/add-estudios';
					titulo = 'Agregar nuevo estudio y proyecto';
				break;
				case 'avances':
					url = 'forms/add-avance';
					titulo = 'Agregar nuevo avance';
				break;
				case 'fotos':
					url = 'forms/add-fotos';
					titulo = 'Agregar nueva foto';
				break;
				case 'convenios':
					url = 'forms/add-convenios-modificatorios';
					titulo = 'Agregar nuevo convenio';
				break;
				case 'expediente':
					url = 'forms/add-expediente';
					titulo = 'Agregar nuevo documento al expediente';
				break;
				case 'contratistas':
					url = 'forms/add-contratista';
					titulo = 'Agregar nuevo contratista';
				break;
				case 'unidades':
					url = 'forms/add-unidad-administrativa';
					titulo = 'Agregar nueva unidad administrativa';
				break;
				case 'supervision':
					url = 'forms/add-supervision-externa';
					titulo = 'Agregar nueva supervision externa';
				break;
				case 'residentes':
					url = 'forms/add-residente';
					titulo = 'Agregar nuevo residente';
				break;
				case 'usuarios':
					url = 'forms/add-usuario';
					titulo = 'Agregar nuevo usuario';
				break;
				case 'licitacion':
					url = 'forms/add-licitacion';
					titulo = 'Editar licitación';
				break;
				case 'eventos':
					url = 'forms/add-eventos';
					titulo = 'Editar evento';
				break;
				}
		break;
		case 'estudios':
			url = 'include/estudios-proyectos';
			titulo = 'Estudios y proyectos';
		break;
		case 'avances':
			url = 'include/avances';
			titulo = 'Avances';
		break;
		case 'fotos':
			url = 'include/album-fotos';
			titulo = 'Fotos';
			/*opciones avanzadas del avance fotografico*/
			$('#nuevo-recurso, #editar-recurso, #borrar-recursos').addClass('no-editar').attr('href', 'javascript:void(0)');
		break;
		case 'convenios':
			url = 'include/convenios';
			titulo = 'Convenios';
		break;
		case 'expediente':
			url = 'include/expediente';
			titulo = 'Expediente';
		break;
		case 'parametros':
			url = 'include/parametros';
			titulo = 'Parámetros';
		break;

		case 'administrador':
			url = 'admin/admin';
			titulo = 'Adminstracion';
			break;

		case 'reporteAvances':
			url = 'include/reporte-avance';
			titulo = 'Reporte de Avances';
			break;




		case 'licitaciones':
			url = 'include/licitaciones';
			titulo = 'Licitaciones';
			$(".return-contratos").hide();$("#buscador-form").show();$('.filtro-licitaciones').show(); $('.filtro-contratos').hide();
		break;
		case 'eventos':
			url = 'include/eventos';
			titulo = 'Eventos de la licitación';
			recurso = 'eventos';
		break;
		case 'observaciones':
			url = 'include/observaciones';
			titulo = 'Observaciones/Comentarios Ciudadanos';
		break;
		case 'bitacora':
			url = 'include/bitacora';
			titulo = 'Bitácora de Obra';
		break;
		default:
			url = 'include/contratos';
			titulo = 'Contratos';
			$(".return-contratos").hide();$("#buscador-form").show(); $('.filtro-contratos').show(); $('.filtro-licitaciones').show(); recurso = 'contrato';
		break;
		}
		
		if(opcion_seleccionada === ''){
			url = 'include/contratos';
			titulo = 'Contratos';
			$(".return-contratos").hide();$("#buscador-form").show(); $('.filtro-contratos').show(); $('.filtro-licitaciones').show(); recurso = 'contrato';
			}
	/*cambiar los datos*/		
	changeUrl();
	
}

/*validar numeros*/
function validarNumeros(e){
	var datos = e.value;
	var newdatos = datos.replace(/[^\d.]/g, "");
	e.value = newdatos;
	}

function convertirCurrency(e){
	var valor = parseFloat(e.value);
	var nuevoValor = valor.formatoDinero(2, ',', '.');
	return e.value = nuevoValor;
	}
	
/*convertir numero a currency*/
Number.prototype.formatoDinero = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

/*animacion del buscador*/
$(document).on("focus", "#dato-buscar", function(){
	$(this).animate({width:320}, 400, function(){
		$('#contenedor-advance-search').show();
		});
	});
	
$(document).on("focusout", "#dato-buscar", function(e){
	if(!$(this).val() && !$('#contenedor-advance-search').is(':visible')){
		$('#contenedor-advance-search').hide();
		$(this).animate({width:200}, 400);
		}
	});
$(document).click(function(){
	if($('#contenedor-advance-search').is(':visible')){
		$('#contenedor-advance-search').hide();
		if(!$('#dato-buscar').val()){
			$('#dato-buscar').animate({width:200}, 400);
			}
		}
	});
$(document).on('click','#contenedor-advance-search, #enviar-form-buscar', function(event){
	event.stopPropagation();
	});
/*seleccionar el ejercicio*/	
$(document).on("click", ".select-eje", function(){
	if($("#lista-contratos").is(":visible")){
		var new_opcion = $('#tipo-opcion-new').val();
		ejercicio = $(this).attr('id');
		if(new_opcion == 1){
			titulo = 'Contratos';
			url = "include/contratos";
			}else{
				titulo = 'Licitaciones';
				url = "include/licitaciones";
				}
		$(".select-eje").removeClass('selected');
		$(this).addClass('selected');
		changeUrl();
		}
	});

/*check opciones*/	
$(document).on("click", ".check_opciones", function(){
	var padre = $(this).parent().parent();
	var opciones_seleccionada = $('.check_opciones:input:checkbox:checked').length;
	$(".cantidad-borrar").text(opciones_seleccionada);
	
	if($(this).is(":checked")){
		padre.addClass('selected-tr');
		}else{
			if(padre.hasClass('selected-tr')){
				padre.removeClass('selected-tr');
				}
			}
	if(opciones_seleccionada > 1){
		$("#editar-recurso").removeClass('editar').addClass('no-editar').attr('href', 'javascript:;');
		}else if(opciones_seleccionada == 1){
			idobjeto = $('.check_opciones:input:checkbox:checked').val();
			$("#editar-recurso").removeClass('no-editar').addClass('editar').attr('href', 'home/#editar');
			}
	});

/*cambiar del url*/
function changeUrl(){
	/*serilizar los datos*/
	var datos = $('.check_opciones').serialize();
	var search_string = $("#buscador-form").serialize();
	
	if(url != ""){
		$.ajax({
			beforeSend: function(){
				$("#contenedor-datos").empty();
				$("#loading-screen").show();
				$("#titulos h1").text(titulo);
				},
			url: url + '.php?rand=' + Math.random() * 9999999999,
			cache: false,
			type: "POST",
			data: 'id=' + idobjeto + '&eje=' + ejercicio + '&' + datos + '&' + search_string,
			error: function(jqXHR, textStatus, errorThrown){
				$("#contenedor-datos").html(jqXHR.responseText);
				},
			success: function(resultado){
				$("#contenedor-datos").html(resultado);
				},
			complete: function(){
				$("#loading-screen").hide();
				},
			});
		}
	}
/*borrar la imagen*/
$(document).on("click", ".borrar-imagen-click", function(){
	var fotoid = $(this).attr('id');
	var titulo_objeto = $("#" + $(this).attr('date'));
	var texto = $(this).text();
	var objeto = $(this);
	var cantidad_objetos = parseInt($(this).parent().parent().parent().parent().find('figure').length) - 1;
	var contenedor_imagenes = $(this).parent().parent().parent().parent().parent();
	
		if(fotoid != ""){
			if(texto != "Borrar"){
				$(this).text("Borrar");
				return false;
				}
			if(texto == "Borrar"){
				/*entonces ajax*/
				$.ajax({beforeSend: function(){objeto.parent().parent().parent().find('figcaption:first').text('Borrando...').css('line-height', '175px').css('text-align', 'center').show();},url: "sources/delete/delete-foto.php", type: "POST", data: {id: fotoid}, dataType:"json", error: function(jqXHR, textStatus, errorThrown){alert(jqXHR.responseText);}, success: function(res){ if(res.affected == true) $("#foto_" + res.idfoto).remove();},});
				}
			}
	});
/*function para determinar el recurso*/
function getRecursoSelect($rec){
	if($rec != ""){
		recurso = $rec;
		}
	}
/*search*/
$(document).on("submit", "#buscador-form", function(){
	var tipo_fil = $('#tipo-opcion-new').val();
	if(tipo_fil == 1){
		url = 'include/contratos';
		titulo = 'Contratos';
		recurso = 'contrato';
		}else if(tipo_fil == 2){
			url = 'include/licitaciones';
			titulo = 'Licitaciones';
			recurso = 'licitaciones';
			}
	
	$(".return-contratos").hide();$("#buscador-form").show();
	changeUrl();
	return false;
	});
/**/
$(document).on("click", ".documento-expediente", function(e){
	e.preventDefault();
	var documento = $(this).attr("href").substring(1) + "#view=Fit";
	var id_documento = "file_" + $(this).attr("id");
	var nombreArchivo = documento.replace(/^.*[\\\/]/, '').split("#");
	var id_documento_previo = $(".documentoPreview").attr("id");
	
		if(documento != "" && id_documento != ""){
			if(id_documento !== id_documento_previo){
				$(".documentoPreview").remove();
				$("#titulo-vista-previa").text("Vista previa del documento" + " - " + nombreArchivo[0]);
				$("#contenedor-vista-previa").append("<embed src='" + documento + "#view=Fit' width='100%' height='700' id='" + id_documento + "' class='documentoPreview'>");
				}
			}
	});
/*nuevo catalogo*/
$(document).on("click", ".agregar-catalogo", function(){
	$("#loading-screen").show();
	var url_catalogo = "";
	var catalogo = $(this).children().attr("id");
	switch(catalogo){
		case "add-contratista":
			url_catalogo = "add-contratista";
			titulo = "Agregar un nuevo contratista";
		break;
		case "add-residente":
			url_catalogo = "add-residente";
			titulo = "Agregar nuevo residente";
		break;
		case "add-supervision":
			url_catalogo = "add-supervision-externa";
			titulo = "Agregar supervisión externa";
		break;
		case "add-unidad-admin":
			url_catalogo = "add-unidad-administrativa";
			titulo = "Agregar nueva unidad administrativa";
		break;
		}
	$('<div id="contenedor-datos-catalogo"><span class="cancelar-add-catalogo" data-tooltip="Cancelar">&#10006;</span><div id="datos-formulario-catalogo"></div></div>').appendTo($("#contenedor-datos")).slideDown('slow');
	$("#datos-formulario-catalogo").load("forms/" + url_catalogo + ".php?rand=" + Math.random() * 999999, function(){
		$("#loading-screen").hide();
		$("#titulos h1").text(titulo);
		});
	});
/*cancelar catalogo*/
$(document).on("click", ".cancelar-catalogo, .cancelar-add-catalogo", function(){
	var pregunta = confirm("¿Seguro que desea cancelar?");
		if(pregunta){
			$("#contenedor-datos-catalogo").remove();
			$("#titulos h1").text('Agregar nuevo contrato');
			}
	});
	
/*cargar catalogo*/
function cargarCatalogos(tipo_catalogo, detalle){
	var catalogo, name_obj;
	detalle = typeof detalle !== 'undefined' ? detalle : false;
	if(tipo_catalogo){
		switch(tipo_catalogo){
			case 'CT':
				catalogo = $("#contratista");
				name_obj = 'contratista';
			break;
			case 'RS':
				catalogo = $("#residente");
				name_obj = 'residente';
			break;
			case 'SE':
				catalogo = $("#supervision-externa");
				name_obj = 'supervision-externa';
			break;
			case 'UA':
				catalogo = $(".unidades-administrativas");
				name_obj = 'unidades-administrativas';
			break;
			}
		
		/*cargar los datos*/
		catalogo.load('sources/catalogos-load.php?rand=' + Math.random() * 999999,{tipo: tipo_catalogo}, function(){
			$('#' + name_obj + ' option').filter(function() {
				  return $(this).text() == detalle;
			  }).prop("selected", true);
			});
		}
	}
/*borrar*/
$(document).on('click', '#borrar-recursos', function(){
	if(!$(this).hasClass('no-editar')){
		var url_borrar = "";
		var tipoBorrar = $(this).prop('rel');
		var cant = $('.check_opciones:input:checkbox:checked').length;
		if(cant <= 0){
			alert('Debe de seleccionar un registro');
			return false;
			}
		var datosSeleccionados = $('.check_opciones:input:checkbox:checked').serialize();
		var pregunta = confirm('¿Seguro que desea borrar los elemento(s) seleccionados?');
		
		/*comprobar que venga el tipo borrar && tipoBorrar != undefined*/
		if(tipoBorrar){
			/*confirmar si desea borrar*/
			if(pregunta){
				/*mandar los datos*/
				$.ajax({
					beforeSend: function(){
						$("#loading-screen").show();
						},
					url: 'sources/delete/delete-elements.php?rand=' + Math.random() * 99999,
					cache: false,
					type: 'POST',
					dataType: 'json',
					data: datosSeleccionados + '&tipo=' + tipoBorrar,
					error: function(jqXHR, textStatus, errorThrown){
						$("#contenedor-datos").html(jqXHR.responseText);
						},
					success: function(resultado){
						if(resultado.ok == true){
							alert("La información se ha borrado correctamente.");
							$(window).bind('hashchange', function(){}).trigger('hashchange');
							}
						},
					complete: function(){
						$("#loading-screen").hide();
						},
					});
				}else{
					return false;
					}
			}
		return false;
	   }
	});
/*perfil del usuario*/
$(document).on("click", "#perfile-de-usuario", function(){
	$(this).children().html('&#9650;');
	$(this).addClass('sub-menu-seleccionado');
	$(this).nextAll().show();
	return false;
	});
$(document).on("click", "html:not(.sub-menu)", function(){
	if($(".sub-menu").is(":visible")){
		$(".sub-menu").hide().parent().find("a:first").removeClass("sub-menu-seleccionado");
		$("#perfile-de-usuario").children().html('&#9660;');
		}
	});
/*publicar*/
$(document).on('click','.publicar-contrato', function(){
	var ob = $(this);
	var id = $(this).attr('data-id');
	var pregunta_string = "";
	if($(this).hasClass('despublicado')){
			pregunta_string = '¿Seguro que desea publicar este contrato?';
	}else{
			pregunta_string = '¿Seguro que desea despublicar este contrato?';
	}
	var pregunta = confirm(pregunta_string);
	
	if(pregunta){
		$.ajax({
			beforeSend: function(){},
			url: 'sources/insert/crear-contrato-publicar.php',
			cache: false,
			type: 'POST',
			dataType: 'json',
			data: 'id=' + id,
			error: function(jqXHR, textStatus, errorThrown){
				alert(jqXHR.responseText);
				},
			success: function(resultado){
					if(resultado.estatus == true){
						ob.html('<i class="fa fa-cloud" aria-hidden="true"></i>').addClass('publicado').removeClass('despublicado');
					}else{
						ob.html('<i class="fa fa-cloud-upload" aria-hidden="true"></i>').removeClass('publicado').addClass('despublicado');
					}
				},
			});
		}
	return false;
	});
function cantidadObs(){
	$.ajax({
	  beforeSend: function(){
		  //alert('asasdasd');
	  },
	  url: 'sources/cantidad-observaciones.php',
	  cache: false,
	  type: 'POST',
	  dataType: 'json',
	  error: function(jqXHR, textStatus, errorThrown){
		 // alert(jqXHR.responseText);
		  },
	  success: function(res){
		  $('#cantidad-observaciones').text(res.obs);

		  },
	  });
	}
$(document).on('click','.publica-obs', function(){
	var id_obs = $(this).attr('id');
	var tipo = $(this).attr('data-type');
	var ob = $(this);
	
	switch(tipo){
		case '0':
			var pregunta = confirm('¿Seguro que desea publicar Observación/Comentario?');
		break;
		case '1':
			var pregunta = confirm('¿Seguro que desea des publicar Observación/Comentario?');
		break;
		}
	if(pregunta){
		$.ajax({
		  beforeSend: function(){},
		  url: 'sources/publicar-des-obs.php',
		  cache: false,
		  type: 'POST',
		  dataType: 'json',
		  data: 'id=' + id_obs + '&tipo=' + tipo,
		  error: function(jqXHR, textStatus, errorThrown){
			  alert(jqXHR.responseText);
			  },
		  success: function(res){
			  if(tipo == 0){
				  if(res.estatus == true){
					  ob.attr('data-type', 1);
					  ob.find('img:first').attr('src', res.img_on);
					  }
				  }
				if(tipo == 1){
				  if(res.estatus == true){
					  ob.attr('data-type', 0);
					  ob.find('img:first').attr('src', res.img_off);
					  }
				  }
			  },
		  });
		}
	});
$(document).on('change', '#filtro-comentarios', function(){
	var valor = $(this).val();
	
	window.location.href = '#observaciones-' + valor;
	});
/*date picker*/
 $(function() {
$( ".datepicker" ).datepicker();
});


function PopupCenter(url, title, w, h) {
	// Fixes dual-screen position                         Most browsers      Firefox
	var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	var top = ((height / 2) - (h / 2)) + dualScreenTop;
	var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	// Puts focus on the newWindow
	if (window.focus) {
		newWindow.focus();
	}
}
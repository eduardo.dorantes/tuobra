<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Mapa</title>
<link type="text/css" rel="stylesheet" href="../css/newestilo.css">
<style type="text/css">
html, body, #map-canvas { height: 100%; margin: 0px; padding: 0px;}
#datos-click{position:absolute; bottom:0; left:0; width:530px; height:170px; padding:10px; background-color:#fff; z-index:99; -moz-box-shadow: 0 0 5px #888;
-webkit-box-shadow: 0 0 5px#888; box-shadow: 0 0 5px #888;}
</style>
<script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAB4Mdh2n0AKYk-uRPpbXP93VuboQTgy8s"></script>
<script type="text/javascript">
  var map;
  var geocoder;
  var mapOptions = { center: new google.maps.LatLng(0.0, 0.0), zoom: 2,
    mapTypeId: google.maps.MapTypeId.ROADMAP };

  function initialize() {
	  var latd = '<?php echo !empty($_GET['lat']) ? $_GET['lat'] : ''; ?>';
	  var logt = '<?php echo !empty($_GET['log']) ? $_GET['log'] : ''; ?>';
      var myOptions = {
            center: new google.maps.LatLng(20.117, -98.7333),
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        geocoder = new google.maps.Geocoder();
        var map = new google.maps.Map(document.getElementById("map-canvas"),
        myOptions);
		/*posicion anterior*/
		if(latd !== '' && logt !== ''){
			var my_pos = new google.maps.Marker({
			  position: new google.maps.LatLng(latd, logt),
			  map: map,
			  title: 'Posición anterior',
			});
		}
        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        var marker;
        function placeMarker(location) {
            if(marker){ //on vérifie si le marqueur existe
                marker.setPosition(location); //on change sa position
            }else{
                marker = new google.maps.Marker({ //on créé le marqueur
                    position: location, 
                    map: map
                });
            }
            //alert( + '///////' + );
			document.getElementById('latitud').value = location.lat();
			document.getElementById('longitud').value = location.lng();
            getAddress(location);
        }

  function getAddress(latLng) {
    geocoder.geocode( {'latLng': latLng},
      function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          if(results[0]) {
			  document.getElementById('location').value = results[0].formatted_address.toUpperCase();
          }
          else {
            alert("No results");
          }
        }
        else {
          alert(status);
        }
      });
    }
  }
 google.maps.event.addDomListener(window, 'load', initialize);
</script>

</head>

<body>
<div id="map-canvas"></div>
<div id="datos-click">
	<table border="0" cellpadding="5" cellspacing="5" width="90%">
    	<tr>
        	<td>Latitud:</td>
            <td><input type="text" name="latitud" id="latitud" readonly></td>
            <td>Longitud:</td>
            <td><input type="text" name="longitud" id="longitud" readonly></td>
        </tr>
        <tr valign="top">
        	<td>Dirección:</td>
            <td colspan="3"><textarea name="location" id="location" cols="51"></textarea></td>
        </tr>
        <tr>
        	<td colspan="4" align="right"><input type="button" name="enviar-datos-locacion" id="enviar-datos-locacion" value="Aceptar" class="btn"></td>
        </tr>
    </table>
</div>
<script type="text/javascript">
$(document).on("click", "#enviar-datos-locacion", function(){
	var getLatitud = $("#latitud").val();
	var getLongitud = $("#longitud").val();
	var getLocalidad = $("#location").val();
	var usarInfo = false;
	
	/*pregunta*/
	var pregunta = confirm("¿Desea usar la información de la dirección como parte de la locación?");
	if(pregunta){
		usarInfo = true;
		}
	
	/*enviar la informacion*/
	$("#latitud-obra", window.opener.document).val(getLatitud);
	$("#longitud-obra", window.opener.document).val(getLongitud);
	/*checar usar locacion*/
	if(usarInfo){
		$("#localidad", window.opener.document).val(getLocalidad);
		}
	
	window.close();
	});
</script>
</body>
</html>
<?php
require_once('../requires/Mobile_Detect.php');
require('../librerias/query.class.inc.php');
require('../librerias/config.php');
require('../include/functions.php');
/*crear objeto query*/
$query = new querys();
/*crear objeto detectar*/
$detectar = new Mobile_Detect();
/*comprobar si es table*/
if($detectar->isTablet()){
	/*hacer algo*/
	}
/*ejecutar y comprrobar query detalles de licitacion*/
if(!$row = $query->traerSoloResultado('SELECT l.id, l.num_licitacion, l.tipo, l.descripcion, l.fecha_publiacion, l.url_compranet, l.normatividad, l.foto_proyecto, l.desierta, c.NOMBRE FROM `licitaciones` l INNER JOIN `clientes` c ON l.id_dependencia = c.ID WHERE l.num_licitacion = :num', $array_bind = array(':num' => $_GET['num']))){
	}
	
/*comprobar si existe la licitacion*/
if(!is_array($row)){
	echo "Licitacion no existe";
	exit;
	}



$meta = '<meta property="og:site_name" content="tuObra" />
<meta property="og:site" content="tuobra.sonora.gob.mx" />
<meta property="og:title" content="Licitación - '.$row['num_licitacion'].'" />
<meta property="og:description" content="'.$row['descripcion'].'" />
<meta property="og:image" content="http://pruebastuobra.sonora.gob.mx/assets/img/share-tu-obra-01.png" />
<meta property="og:url" content="http://tuobra.sonora.gob.mx/licitacion/'.$row['num_licitacion'].'" />
<meta property="og:type" content="article" />
<meta name="twitter:card" content=\'summary\' />
<meta name="twitter:image" content="http://pruebastuobra.sonora.gob.mx/assets/img/share-tu-obra-01.png" />
<meta name="twitter:site" content="@gobiernosonora" />
<meta name="twitter:url" content="http://tuobra.sonora.gob.mx/licitacion/'.$row['num_licitacion'].'" />
<meta name="twitter:description" content="'.$row['descripcion'].'" />
<meta name="twitter:title" content=" Contrato - '.$row['num_licitacion'].'" />
<title>'.$row['num_licitacion'].' - '.$row['descripcion'].'</title>
<meta name="description" content="'.$row['num_licitacion'].' - '.$row['descripcion'].'">';




?>





<?php include('../include/header.php'); ?>




<script type="text/javascript" src="assets/js/jquery.sharrre.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {


		$('.share').sharrre({
			share: {
				twitter: true,
				facebook: true
			},
			template: '<li><a href="#" class="btn btn-primary btn-circle btn-facebook facebook"><i class="fa fa-facebook"></i></a></li><li><a href="#" class="btn btn-primary btn-circle btn-twitter twitter"><i class="fa fa-twitter"></i></a></li>',
			enableHover: false,
			enableTracking: false,
			enableCounter: true,
			render: function(api, options){
				$(api.element).on('click', '.twitter', function() {
					api.openPopup('twitter');
				});
				$(api.element).on('click', '.facebook', function() {
					api.openPopup('facebook');
				});
			}
		});

	});
</script>








<section id="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Licitaciones</h2>
				<p>Inicio | Licitaciones de Obra Pública | Licitación  <?php echo $row['num_licitacion']?></p>
			</div>
		</div>
	</div>

</section>


<section id="contentForm">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">


				<div class="form-horizontal">


					<div class="row">
						<div class="col-sm-6">
							<h1><?php echo $row['num_licitacion']?>


							</h1>

							<?php
							if($row['desierta']){
								echo '<span class="label label-warning">Atención: Esta licitación ha sido declarada desierta</span>';
							}
							?>

						</div>
						<div class="col-sm-4">
							<p class="text-right m-t">
								<strong>Compartir en Redes Sociales</strong>
							</p>
						</div>
						<div class="col-sm-2">
							<div class=" m-t-sm">
								<ul class="social-icons share" data-title="Portal TuObra.mx / <?php echo $row['num_licitacion'] ?>" data-url="http://tuobra.sonora.gob.mx/licitacion/<?php echo $row['num_licitacion']; ?>"></ul>

							</div>
						</div>
					</div>

					<hr />


					<div class="form-group">
						<label class="col-sm-2 control-label text-left">Número</label>
						<div class="col-sm-5">
							<p class="form-control-static"><?php echo $row['num_licitacion']; ?></p>
						</div>

						<label class="col-sm-2 control-label  text-left">Tipo</label>
						<div class="col-sm-3">
							<p class="form-control-static"><?php echo $array_tipo_licitacion[$row['tipo']]; ?></p>
						</div>
					</div>

					<hr />


					<div class="form-group">
						<label class="col-sm-2 control-label text-left">Dependencia / Organismo</label>
						<div class="col-sm-5">
							<p class="form-control-static"><?php echo $dependencia = $row['NOMBRE']; ?></p>
						</div>

						<label class="col-sm-2 control-label  text-left">Fecha publicación</label>
						<div class="col-sm-3">
							<p class="form-control-static"><?php echo $row['fecha_publiacion']; ?></p>
						</div>
					</div>

					<hr />


					<div class="form-group">
						<label class="col-sm-2 control-label text-left">Objeto</label>
						<div class="col-sm-5">
							<p class="form-control-static"><?php echo $row['descripcion']; ?></p>
						</div>

						<label class="col-sm-2 control-label  text-left">Normatividad</label>
						<div class="col-sm-3">
							<p class="form-control-static"><?php echo $array_normatividad[$row['normatividad']]; ?></p>
						</div>
					</div>
				</div>

				<?php include('testigo-ciudadano.php'); ?>


			</div>
		</div>
	</div>
</section>


<section id="contentResults">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">





				<h3 class="subtitle text-underline text-black text-center">Eventos de la licitación</h3>

				<?php
				if($row['desierta']){
					echo '<span class="label label-warning">Atención: Esta licitación ha sido declarada desierta</span>';
				}
				?>



				<?php
				/*eventos de la licitacion*/
				/**************************************/
				/*query string*/
				$query_string = "SELECT `tipo`, `fecha`, `hora`, `url_trasmision`, `documento` FROM `licitaciones_eventos` WHERE `id_licitacion` = :id_licitacion";
				$array_bind = array(':id_licitacion' => $row['id']);
				/*ejecutar y comprobar query*/
				if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
					/*tabla*/
					echo "<table class=\"table table-bordered table-hover table-striped m-t-b\" border=\"0\" width=\"100%\">";
					echo "<thead><tr>";
					echo "<th>Evento</th>";
					echo "<th>Fecha</th>";
					echo "<th>Hora</th>";
					echo "<th width=\"2%\"></th>";
					echo "<th width=\"2%\"></th>";
					echo "</tr></thead><tbody valing=\"top\">";
					/*loop*/
					foreach($rows as $raw){
						echo "<tr>";
						echo "<td>".$array_eventos[$raw['tipo']]."</td>";
						echo "<td align=\"center\">".$raw['fecha']."</td>";
						echo "<td align=\"center\">".$raw['hora']."</td>";
						echo "<td align=\"center\">";
						if(!empty($raw['documento'])){
							echo "<a href=\"http://tuzobra.cf/panel.tuobra/".$raw['documento']."\" class=\"\" target=\"_blank\"> <i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i></a>";
						}
						echo "</td>";
						echo "<td align=\"center\">";
						/********EVENTO****************/
						if(!empty($raw['url_trasmision'])){

							$parameters = array('youtube'=>youtube_id_from_url($raw['url_trasmision']),'evento'=>$array_eventos[$raw['tipo']],'fecha'=>$raw['fecha'].' '.$raw['hora'],'dependencia'=>$dependencia);
							$parameters = json_encode($parameters);

							echo "<a href='javascript:void(0);' data-parameters='".$parameters."'' class='youtube-play'><i class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i></a>";
						}
						echo "</td>";
						echo "</tr>";
					}
					/*cerrar tabla*/
					echo "</tbody></table>";
				}
				?>






			</div>
		</div>
	</div>
</section>

<div id="eventoModal" class="modal fade licitacionesModal">

	<div class="modal-dialog">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>

				<h4 class="modal-title">Evento de Licitación</h4>

			</div>

			<div class="modal-body">


			</div>

		</div>

	</div>

</div>


<script type="text/javascript" src="assets/js/licitacion.js"></script>


<?php include('../include/footer.php'); ?>

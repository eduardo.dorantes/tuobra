<?php
class borrar{
	var $db;//conexion a la base de datos
	
	/*comstructor*/
	function __construct($conn){
		/*conexcion a la base de datos*/
		$this->db = $conn;
		}
	
	public function borrarElementos($qe, $id_elemento){
		/*comprobar que venga el query y el elemento*/
		if(!empty($id_elemento) and strlen($id_elemento) > 0 and !empty($qe) and strlen($qe) > 0){
			/*query*/
			$sql_query = $qe;
			/*preparar query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $id_elemento);
			/*ejecutar query*/
			$query->execute();
			/*comprobar que se ejecutar bien el query*/
			if($query->affected_rows > 0){
				return true;
				}else{
					return false;
					}
			}
		}
	}
?>
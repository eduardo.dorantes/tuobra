<?php
require('../../librerias/query.class.inc.php');
require('../../librerias/config.php');





// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=obras-'.date("Y-m-d-H-i-s").'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');



// fetch the data




$q = "SELECT NUM_CONTRATO,ID_OBRA_COMPLEMENTARIA,OC_NUMERO_CONTRATO,NUM_LICITACION,OBJETO,MODALIDAD,TIPO_CONTRATO,TIPO_OBRA,TOTAL_CONTRATO,AVANCE,FECHA_AVANCE,DOCUMENTOS,FOTOGRAFIAS,MUNICIPIO,LOCALIDAD,LATITUD,LONGITUD,BENEFICIADOS,ORGANISMO,UA_SOLICITANTE,UA_RESPONSABLE,CONTRATISTA,RESIDENTE,SUPERVISOR_EXTERNO,FECHA_CONTRATO,FECHA_INICIO,FECHA_TERMINO,FECHA_ENTREGA,EJERCICIO FROM vcontratosod";


$a = $t= array();
$query = new querys();
$r = $query->traerMultiplesResultados($q, $a);


function encode_items(&$item, $key)
{
    $item = utf8_decode($item);
}

array_walk_recursive($r, 'encode_items');

$t = array_keys($r[0]);

// output the column headings
fputcsv($output, $t);


foreach($r as $v){
    fputcsv($output, $v);
}


?>
<?php
session_start();
header("cache-control: no-cache");
include("../../config/conn.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");
require("../../funciones/parametros.class.inc.php");

if(!empty($_POST) and $_SESSION['login'] == true){
	/*crear objeto parametro*/
	$parametro = new parametros($conn, $_SESSION['id-cliente']);
	
	if($parametro->saveParametros($_POST['parametro-iva'])){
		echo "<div class=\"success\">".$mensaje['1002']."</div>";
		}
	}
?>
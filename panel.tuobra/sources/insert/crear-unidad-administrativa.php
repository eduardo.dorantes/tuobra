<?php
session_start();
require("../../config/conn.php");
require("../../funciones/unidad.administrativa.class.inc.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");

/*comprobar que se aya hecho el post*/
if(!empty($_POST)){
	/*crear un un nuevo objeto unidad*/
	$unidad = new unidadAdminstraiva($conn, $_POST['id-unidad']);
	/*varibles para checar se creo o se actulizo el objeto*/
	$comprobar_objeto = false;
	
	/*comprobar que los campos requeridos no vengan vacios*/
	if(validar::notEmpty($_POST['nombre-unidad']) == true){//comprobar unidad
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
	/*comprobar el tipo*/
	if(validar::notEmpty($_POST['tipo-unidad']) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
	
	/*crear o actulizar la unidad administrativa*/
	if(empty($unidad->getNombre())){
		$comprobar_objeto = $unidad->createUnidad($_SESSION['id-cliente'], $_POST['nombre-unidad'], $_POST['tipo-unidad']);
		}else{
			$comprobar_objeto = $unidad->updateUnidad($_POST['nombre-unidad'], $_POST['tipo-unidad']);
			}
	/*comprobar que se aya guardados los datos*/
	if($comprobar_objeto == true){
		echo "<div class=\"success\">".$mensaje['1002']."</div>";
		}
	}
?>
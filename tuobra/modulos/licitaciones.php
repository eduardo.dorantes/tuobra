<?php include('../include/header.php'); ?>


<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Licitaciones</h2>
                <p>Inicio | Licitaciones de Obra Pública</p>
            </div>
        </div>
    </div>

</section>

<section id="contentForm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php include('../include/forms/buscar-licitaciones.php'); ?>

            </div>
        </div>
    </div>
</section>



    <section id="contentResults">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="open-data"></div>
                    <div id="listado-licitaciones"></div>
                </div>
            </div>
        </div>
    </section>









<?php include('../include/footer.php'); ?>
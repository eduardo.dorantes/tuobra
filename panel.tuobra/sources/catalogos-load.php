<?php
session_start();
require("../config/conn.php");
require("../funciones/forms.selects.classs.php");

/*comprobar post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*recibir el tipo de catalogo*/
	$tipo = $_POST['tipo'];
	
	/*comprobar que venga el tipo*/
	if($tipo){
		/*crear un objeto catalogo*/
		$catalogo = new Selects($conn);
		/*extraer el tipo*/
		switch($tipo){
			case 'CT':
				$catalogo->selectContratistas($_SESSION['id-cliente']);
			break;
			case 'RS':
				$catalogo->selectResidentes($_SESSION['id-cliente']);
			break;
			case 'SE':
				$catalogo->selectSupervisionExterna($_SESSION['id-cliente']);
			break;
			case 'UA':
				$catalogo->selectUnidadesAdministrativas();
			break;
			}
		}
	}
?>
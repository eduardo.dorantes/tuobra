<?php
session_start();
require("../config/conn.php");
require("../funciones/contratistas.class.inc.php");
if($_SESSION['login'] != true){
	exit;
	}

/*crear un un nuevo objeto unidad*/
$contratista = new contratista($conn, $_GET['id']);
?>
<div id="resultado_catalogo"></div>
<form name="agregar-contratista" id="agregar-contratista" method="post">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" id="table-form-add-contratista">
    	<caption class="form-caption" style="display:none;"><span>Agregar un nuevo contratista</span></caption>
    	<tbody valign="top">
        	<tr>
            	<td><label for="nombre-razon">Nombre/Razón social</label></td>
                <td></td>
            </tr>
            <tr>
            	<td><input type="text" name="nombre-razon" id="nombre-razon" size="55" required value="<?php echo $contratista->getNombreContratista(); ?>"></td>
                <td></td>
            </tr>
            <tr>
            	<td><label for="rfc">RFC</label></td>
                <td><label for="direccion">Dirección</label></td>
            </tr>
            <tr>
           	  <td><input type="text" name="rfc" id="rfc" required value="<?php echo $contratista->getRfcContratista(); ?>"></td>
                <td><textarea name="direccion" cols="40" rows="2" id="direccion" required><?php echo $contratista->getDireccionContratista(); ?></textarea></td>
            </tr>
            <tr>
            	<td><label for="telefono">Teléfono</label></td>
                <td><label for="email">Correo electrónico</label></td>
            </tr>
            <tr>
            	<td><input type="tel" name="telefono" id="telefono" required value="<?php echo $contratista->getTelefonoContratista(); ?>"></td>
                <td><input type="email" name="email" id="email" value="<?php echo $contratista->getEmailContratista(); ?>"></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn cancelar-catalogo">
                </td>
                <td><input type="hidden" name="id-contratista" id="id-contratista" value="<?php echo $_GET['id']; ?>"></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$('#agregar-contratista').submit(function(){
	var datos = $(this).serialize();
	var nombre = $('#nombre-razon').val();
	$.ajax({
		beforeSend: function(){
			$("#resultado_catalogo").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url:"sources/insert/crear-contratista.php?rand=" + Math.random() * 9999999,
		type:"POST",
		data: datos,
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado_catalogo").html(jqXHR.responseText);
			},
		success: function(resultados){
			$("#resultado_catalogo").html(resultados);
			},
		complete: function(){
			$("#enviar-formulario").prop("disabled", false);
			cargarCatalogos('CT', nombre);
			},
		});
	
	return false;
	});
</script>
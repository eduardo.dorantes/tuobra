<?php
session_start();
if($_SESSION['login'] != true){
	exit;
	}
?>
<form name="agregar-avance-fotografico" id="agregar-avance-fotografico" method="post" enctype="multipart/form-data">
	<table cellpadding="5" cellspacing="0" border="0" id="table-form-add-foto">
    	<tbody valign="top">
            <tr>
            	<td><label for="documento">Imagen:</label></td>
                <td valign="bottom"><input type="file" name="documento" id="documento" required></td>
            </tr>
          <tr>
            	<td><label for="pie-imagen">Pie Imagen:</label></td>
                <td><textarea name="pie-imagen" id="pie-imagen" rows="1" cols="40" required></textarea></td>
            </tr>
            <tr>
            	<td>&nbsp;</td>
                <td><input type="hidden" name="id-usuario" id="id-usuario" value="<?php echo $_SESSION['id-usuario']; ?>"><input type="hidden" name="id-contrato" id="id-contrato" value="<?php echo $_POST['id']; ?>"><input type="hidden" name="id-frente" id="id-frente" value="<?php echo $_POST['id_frente']; ?>">
                <input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();"></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$("#agregar-avance-fotografico").submit(function(e){
	e.preventDefault();
	if(window.FormData == undefined){
		alert('Necesita un navegador que soporte HTML5');
		return false;
		}
	var dataForm = new FormData($("#agregar-avance-fotografico")[0]);
	
	$.ajax({
		beforeSend: function(){},
			url:"sources/insert/crear-foto.php?rand=" + Math.random() * 9999999,
			type: "POST",
			cache: false,
            contentType: false,
            processData: false,
			data: dataForm,
			xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('progress',function(ev) {
						if (ev.lengthComputable) {
							var percentUploaded = Math.floor(ev.loaded * 100 / ev.total);
							$("#progreso").css('width', percentUploaded + '%');
							$("#porcentaje").text(percentUploaded + '%');
						}
				   }, false);
				}
				return myXhr;
            },
			error: function(jqXHR, textStatus, errorThrown){
				$("#resultado").html(jqXHR.responseText);
				},
			success: function(resultado){
				$("#resultado").html(resultado);
				},
			complete: function(){
				$("#agregar-avance-fotografico")[0].reset();
				},
		});
	return false;
	});
</script>
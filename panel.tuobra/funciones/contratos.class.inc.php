<?php
class contratos{
	
	private $db;//conexion a la base de datos
	private $id_contrato;//id del contrato
	private $id_contratista;//id del contratista
	private $id_municipio;//id del municipio
	private $id_unidad_sol;//id de la unidad solicitante
	private $id_unidad_rep;//id de la unidad responsable
	private $id_residente;//id del residente de la obra
	private $id_suprv_ex;//id de la supervision externa
	private $tipo_contrato;//tipo de contrato
	private $tipo_obra;
	private $obra_complementaria;//si la obra es complementaria
	private $id_obra_cabecera;//id de la obra cabecera a la que pertence la obra complementaria
	private $num_cont_obra_cabecera;//numero de contrato de la obra cabcera
	private $modalidad;//modalidad del contrato
	private $numero_contrato;//numero del contrato
	private $contratista;// nombre del contratista
	private $unidad_solicitante;//unidad administrativa solicitante
	private $unidad_responsable;//unidad administrativa responsable
	private $supervision_externa;//nombre del supervision externa
	private $numero_licitacion;//numero de licitacion
	private $objeto;//objeto del contrato
	private $importe;//importe del contrato
	private $iva_contrato;//porcentaje de iva del contrato
	private $total_contrato;//total contratado incluyendo iva
	private $localidad;//localidad donde se llevo acabo la obra
	private $municipio;//nombre del municipio
	private $municipiosVarios;//nombre de los municipios

	private $recursos_inifed;//nombre de los municipios



	private $beneficiados;//numero de habitantes beneficiados
	private $fecha_contrato;//fecha firma del contrato
	private $fecha_inicio;//fecha de inicio del contrato
	private $fecha_termino;//fecha de termino del contrato
	private $fecha_modificacion;//fecha de la ultima modificacion del contrato
	private $observaciones;//observaciones del contrato
	private $fecha_real_termino;//fecha real de terminacion del contrato
	private $latitud;//latitud donde se encuentra la obra
	private $longitud;//longitud donde se encuentra la obra
	private $online;// estatus del contrato si esta online o no
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		$this->db = $conn;//conexion a la base de datos
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			$this->id_contrato = $id;//set el id de la obra
			$this->datosContrato();//datos del contrato
			}
		}
		
	/*crear contrato*/	
	public function crearContrato($id_cliente, $id_usuario, $modalidad_, $num_contrato_, $num_licitacion_, $importe_, $por_iva_, $total_contrato_, $objeto_, $municipio_, $beneficiados_, $localidad_, $fecha_contrato_, $fecha_inicio_, $fecha_termino_, $contratista_, $residente_, $supervision_externa_, $unidad_sol_, $unidad_resp_, $fecha_real_terminacion, $observaciones, $tipo_contrato, $obra_complementaria, $id_obra_cabecera, $latitud, $longitud, $tipo_obra, $recursos_inifed){
		$fecha = date("Y-m-d H:i:s");
		/*query*/
		$sql_query = "INSERT INTO `contratos` (ID_CLIENTE, ID_USUARIO, ID_CONTRATISTA, ID_UNIDAD_UAS, ID_UNIDAD_UAR, ID_RESIDENTE, ID_SUPERV_EXTERNA, NUM_CONTRATO,";
		$sql_query .= "NUM_LICITACION, OBJETO, IMPORTE_CONTRATO, IVA_CONTRATO, TOTAL_CONTRATO, LOCALIDAD, MUNICIPIO, BENIFICIADOS, FECHA_CONTRATO, FECHA_INICIO, FECHA_TERMINO, ";
		$sql_query .= " TIPO_MODALIDAD, FECHA_ALTA, FECHA_ULT_MOD, FECHA_TERMINO_REAL, COMENTARIOS, TIPO_CONTRATO, OBRA_COMPLEMENTARIA, ID_OBRA_CABECERA, LATITUD, LONGITUD, TIPO_OBRA, INIFED) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
		}
		/*bind los resultados*/
		$query->bind_param('iiiiiiisssdddsiisssissssiiissii', $id_cliente, $id_usuario, $contratista_, $unidad_sol_, $unidad_resp_, $residente_, $supervision_externa_, $num_contrato_, $num_licitacion_, $objeto_, $importe_, $por_iva_, $total_contrato_, $localidad_, $municipio_, $beneficiados_, $fecha_contrato_, $fecha_inicio_, $fecha_termino_, $modalidad_, $fecha, $fecha, $fecha_real_terminacion, $observaciones, $tipo_contrato, $obra_complementaria, $id_obra_cabecera, $latitud, $longitud, $tipo_obra, $recursos_inifed);
		/*ejecutar query*/
		$query->execute();

		$lasId = $query->insert_id;

		//print_r($lasId);


		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return $lasId;
		}else{
			return false;
		}
	}
	
	/*actulizar el contrato*/
	public function updateContrato($modalidad_, $num_contrato_, $num_licitacion_, $importe_, $por_iva_, $total_contrato_, $objeto_, $municipio_, $beneficiados_, $localidad_, $fecha_contrato_, $fecha_inicio_, $fecha_termino_, $contratista_, $residente_, $supervision_externa_, $unidad_sol_, $unidad_resp_, $fecha_real_terminacion, $observaciones, $tipo_contrato, $obra_complementaria, $id_obra_cabecera, $latitud, $longitud, $tipo_obra, $recursos_inifed){
		if(!empty($this->id_contrato) and strlen($this->id_contrato) > 0){
				$fecha = date("Y-m-d H:i:s");
				/*query*/
				$sql_query = "UPDATE `contratos` SET ID_CONTRATISTA = ?, ID_UNIDAD_UAS = ?, ID_UNIDAD_UAR = ?, ID_RESIDENTE = ?, ID_SUPERV_EXTERNA = ?, NUM_CONTRATO = ?,";
				$sql_query .= "NUM_LICITACION = ?, OBJETO = ?, IMPORTE_CONTRATO = ?, IVA_CONTRATO = ?, TOTAL_CONTRATO = ?, LOCALIDAD = ?, MUNICIPIO = ?, FECHA_CONTRATO =?, FECHA_INICIO = ?,";
				$sql_query .= "FECHA_TERMINO = ?, TIPO_MODALIDAD = ?, FECHA_ULT_MOD = ?, BENIFICIADOS = ?, FECHA_TERMINO_REAL = ?, COMENTARIOS = ?, TIPO_CONTRATO = ?, OBRA_COMPLEMENTARIA = ?, ID_OBRA_CABECERA = ?, LATITUD = ?, LONGITUD = ?, TIPO_OBRA = ? , INIFED = ? WHERE ID = ?;";
				/*preparar queery*/
				$query = $this->db->prepare($sql_query);
				/*comprobar query*/
				if($query === false){
					trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
					}
				/*bind los resultados*/
				$query->bind_param('iiiiisssdddsisssissssiiissiii', $contratista_, $unidad_sol_, $unidad_resp_, $residente_, $supervision_externa_, $num_contrato_, $num_licitacion_, $objeto_, $importe_, $por_iva_, $total_contrato_, $localidad_, $municipio_, $fecha_contrato_, $fecha_inicio_, $fecha_termino_, $modalidad_, $fecha, $beneficiados_, $fecha_real_terminacion, $observaciones, $tipo_contrato, $obra_complementaria, $id_obra_cabecera, $latitud, $longitud, $tipo_obra,$recursos_inifed, $this->id_contrato);
				/*ejecutar query*/
				$query->execute();
				/*comprobar que se ejecutar bien el query*/
				if($query->affected_rows > 0){
					return true;
				}else{
					return false;
				}
			}
		}

	public function insertMunicipiosVarios($id_contrato, $municipiosVarios){

		self::deleteMunicipios($id_contrato);
			foreach($municipiosVarios as $v){
				/*query*/
				$sql_query = "INSERT INTO `municipiosContratos` (idContrato, idMunicipio) values (?,?)";
				/*preparar query*/
				$query = $this->db->prepare($sql_query);
				/*comprobar query*/
				if ($query === false) {
					trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
				/*bind los resultados*/
				$query->bind_param('ii',$id_contrato,$v);
				/*ejecutar query*/
				$query->execute();
			}
			return true;
	}



	public function deleteContrato(){
		if(!empty($this->id_contrato) and strlen($this->id_contrato) > 0){
			if(self::checkTieneAvances() == false){
				self::deleteMunicipios($this->id_contrato);

				/*query*/
				$sql_query = "DELETE FROM `contratos` WHERE ID = ?;";
				/*preparar query*/
				$query = $this->db->prepare($sql_query);
				/*comprobar query*/
				if($query === false){
					trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
				/*bind los resultados*/
				$query->bind_param('i', $this->id_contrato);
				/*ejecutar query*/
				$query->execute();
				/*comprobar que se ejecutar bien el query*/
				if($query->affected_rows > 0){
					return true;
				}else{
					return false;
				}
			}
		}
	}

	/*funcion para publicar el contrato en linea*/
	public function publicarContrato($estatus){
		if(!empty($this->id_contrato) and strlen($this->id_contrato) > 0){
				/*query*/
				$sql_query = "UPDATE `contratos` SET ONLINE = ? WHERE ID = ?;";
				/*preparar query*/
				$query = $this->db->prepare($sql_query);
				/*comprobar query*/
				if($query === false){
					trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
				/*bind los resultados*/
				$query->bind_param('ii', $estatus, $this->id_contrato);
				/*ejecutar query*/
				$query->execute();
				/*comprobar que se ejecutar bien el query*/
				if($query->affected_rows > 0){
					return true;
				}else{
					return false;
				}
			}
	}






	/*borrar contrato*/
	public function deleteMunicipios($id_contrato){



		if(!empty($id_contrato) and strlen($id_contrato) > 0){

			/*query*/
			$sql_query = "DELETE FROM `municipiosContratos` WHERE idContrato = ?;";
			/*preparar query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);

			}
			/*bind los resultados*/
			$query->bind_param('i',$id_contrato);
			/*ejecutar query*/
			$query->execute();
			/*comprobar que se ejecutar bien el query*/



			if($query->affected_rows > 0){
				return true;
			}else{
				return false;
			}

		}
	}

	public function municipiosContrato(){

		if(!empty($this->id_contrato) and strlen($this->id_contrato) > 0){

		$sql_query = "SELECT idContrato, idMunicipio FROM municipiosContratos WHERE idContrato = ?;";
		/*preparar el query*/
		$query_ = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query_ === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
		}


		/*bind los resultados*/
		$query_->bind_param('i', $this->id_contrato);
		/*ejecutar query*/
		$query_->execute();



		/*store resultados*/
		$query_->store_result();
		/*bind los resultados*/
		$query_->bind_result($idContrato, $idMunicipio);
		/*fetch datos*/
		//$query_->fetch();
		$a = array();
		$i = 0;
			while($query_->fetch()){
				$a[$i] = $idMunicipio;
				$i++;
			}

		$query_->close();

		return $a;


		}

	}









	/*datos del comtrato*/
	/**
	 *
     */
	private function datosContrato(){
		if(!empty($this->id_contrato) and strlen($this->id_contrato) > 0){
			/*query*/
			$sql_query .= "SELECT C.ID_CONTRATISTA, C.ID_RESIDENTE, C.ID_SUPERV_EXTERNA, C.ID_UNIDAD_UAR, C.ID_UNIDAD_UAS, C.MUNICIPIO, C.TIPO_MODALIDAD, CON.RAZON_SOCIAL, U.NOMBRE AS UNIDADSOLICITANTE, UU.NOMBRE AS UNIDADRESPONSABLE, SE.NOMBRE AS SUPERVISIONEXTERNA, C.NUM_CONTRATO, C.NUM_LICITACION, C.OBJETO, C.IMPORTE_CONTRATO, C.IVA_CONTRATO, C.TOTAL_CONTRATO, C.LOCALIDAD, M.nombre_municipio, C.FECHA_CONTRATO, C.FECHA_INICIO, C.FECHA_TERMINO, C.FECHA_ULT_MOD, C.BENIFICIADOS, C.COMENTARIOS, C.FECHA_TERMINO_REAL, C.TIPO_CONTRATO, C.OBRA_COMPLEMENTARIA, C.ID_OBRA_CABECERA, C.LATITUD, C.LONGITUD, (SELECT CC.NUM_CONTRATO FROM `contratos` CC WHERE CC.ID_OBRA_CABECERA = C.ID) AS CONTCABECERA, C.ONLINE, C.TIPO_OBRA, C.INIFED FROM `contratos` C INNER JOIN `contratistas` CON ON C.ID_CONTRATISTA = CON.ID LEFT JOIN `unidades_administrativas` U ON U.ID = C.ID_UNIDAD_UAS LEFT JOIN `unidades_administrativas` UU ON UU.ID = C.ID_UNIDAD_UAR LEFT JOIN `supervision_externa` SE ON SE.ID = C.ID_SUPERV_EXTERNA INNER JOIN `municipios` M ON M.id = C.MUNICIPIO WHERE C.ID = ?;";
			/*preparar el query*/
			$query_ = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query_ === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query_->bind_param('i', $this->id_contrato);
			/*ejecutar query*/
			$query_->execute();
			/*store resultados*/
			$query_->store_result();
			/*bind los resultados*/
			$query_->bind_result($id_contratista_, $id_residente_, $id_suprv_, $id_uni_sol, $id_uni_rep, $id_mun, $tipo_modalidad, $razon, $unidad_solicitante, $unidad_responsable, $supervision_externa, $contrato, $licitacion, $objeto, $importe, $iva, $total, $localidad_cont, $municipio_cont, $fecha_cont, $fecha_inicio_cont, $fecha_termino_cont, $fecha_mod, $beneficiados_cont, $observaciones, $fecha_terminacion_real, $tipo_contrato, $obra_complementaria, $id_obra_cabecera, $latitud, $longitud, $num_cont_cabecera, $online, $tipo_obra, $recursos_inifed);
			/*fetch datos*/
			$query_->fetch();
			/*datos de la unidad*/
			$this->id_contratista = $id_contratista_;
			//$this->_municipiosVarios = self::municipiosContrato();
			$this->id_residente = $id_residente_;
			$this->id_suprv_ex = $id_suprv_;
			$this->id_unidad_sol = $id_uni_sol;
			$this->id_unidad_rep = $id_uni_rep;
			$this->id_municipio = $id_mun;
			$this->modalidad = $tipo_modalidad;
			$this->numero_contrato = $contrato;
			$this->contratista = $razon;
			$this->unidad_solicitante = $unidad_solicitante;
			$this->unidad_solicitante = $unidad_responsable;
			$this->supervision_externa = $supervision_externa;
			$this->numero_licitacion = $licitacion;
			$this->objeto = $objeto;
			$this->importe = $importe;
			$this->iva_contrato = $iva;
			$this->total_contrato = $total;
			$this->localidad = $localidad_cont;
			$this->municipio = $municipio_cont;
			$this->beneficiados = $beneficiados_cont;
			$this->fecha_contrato = $fecha_cont;
			$this->fecha_inicio = $fecha_inicio_cont;
			$this->fecha_termino = $fecha_termino_cont;
			$this->fecha_modificacion = $fecha_mod;
			$this->observaciones = $observaciones;
			$this->fecha_real_termino = $fecha_terminacion_real;
			/*nuevo campos*/
			$this->tipo_contrato = $tipo_contrato;
			$this->obra_complementaria = $obra_complementaria;
			$this->id_obra_cabecera = $id_obra_cabecera;
			$this->num_cont_obra_cabecera = $num_cont_cabecera;
			$this->latitud = $latitud;
			$this->longitud = $longitud;
			$this->online = $online;
			$this->tipo_obra = $tipo_obra;
			$this->recursos_inifed = $recursos_inifed;
			
			/*cerrar conexiones*/
			$query_->close();
			}
		}
		
	/*metodo para checar si el contrato tiene avances*/
	private static function checkTieneAvances(){
		/*total de registros*/
		$total_registros = 0;
		/*querys*/
		$sql_query_uno = "SELECT A.ID_CONTRATO FROM `avances` A WHERE A.ID_CONTRATO = ?;";
		$sql_query_dos = "SELECT AF.ID_CONTRATO FROM `avance_fotografico` AF WHERE AF.ID_CONTRATO = ?;";
		/*preparar querys*/
		$query_uno = $this->db->prepare($sql_query_uno);
		$query_dos = $this->db->prepare($sql_query_dos);
		/*comprobar los querys*/
		if($query_uno === false or $query_dos === false){
			trigger_error('Ocurrio un problema con el query: ' .  $sql_query_uno.$sql_query_dos . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind querys*/
		$query_uno->bind_param('i', $this->id_contrato);
		$query_dos->bind_param('i', $this->id_contrato);
		
		/*sumar los registros regresados*/
		$total = ($query_uno->num_rows + $query_dos->num_rows);
		/*cerrar los querys*/
		$query_uno->close();
		$query_dos->close();
		
		/*comprobar si tiene avances*/
		if($total_registros > 0){
			return true;
			}else{
				return false;
				}
		}
	
	/*ejercicios de los contratos y cantidades*/
	function generarEjercicios(){
		/*set array de datos*/
		$ejercicios = array();
		/*query*/
		$sql_query = "SELECT EJERCICIO, COUNT(*) AS CANTIDADCONTRATOS FROM `vistacontratos` GROUP BY EJERCICIO ORDER BY EJERCICIO DESC;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*ejecutar query*/
		$query->execute();
		/*store resultados*/
		$query->store_result();
		/*bind los resultados*/
		$query->bind_result($eje, $cantidad);
		/*fetch de los resultados*/
			while($query->fetch()){
				$ejercicios[$eje] = $cantidad;
			}
		/*cerrar conexiones*/
		$query->close();
		/*regresar resultados*/
		return $ejercicios;
		}
	
	/*get modalidad del contrato*/
	public function getModalidad(){
		return $this->modalidad;
		}
	/*get numero de contrato*/
	public function getNumeroContrato(){
		return $this->numero_contrato;
		}
	/*get el numero de licitacion*/
	public function getNumeroLicitacion(){
		return $this->numero_licitacion;	
		}
	/*get id del contratista*/
	public function getIdContratista(){
		return $this->id_contratista;
		}
	/*get id del residente*/
	public function getIdResidente(){
		return $this->id_residente;
		}
	/*get id del supervision externa*/
	public function getIdSupervisionExterna(){
		return $this->id_suprv_ex;
		}
	/*get id del unidad solicitante*/
	public function getIdUnidadSolicitante(){
		return $this->id_unidad_sol;
		}
	/*get id de la unidad reponsable*/
	public function getIdUnidadResponsable(){
		return $this->id_unidad_rep;
		}
	/*get id del municipio*/
	public function getIdMunicipio(){
		return $this->id_municipio;
		}
	/*get el numero de beneficiados*/
	public function getBeneficiados(){
		return $this->beneficiados;
		}
	/*get contratista*/
	public function getContratista(){
		return $this->contratista;
		}
	/*get unidad solicitante*/
	public function getUnidadSolicitante(){
		return $this->unidad_solicitante;
		}
	/*get unidad administrativa responsable*/
	public function getUnidadResponsable(){
		return $this->unidad_responsable;
		}
	/*get objeto del contrato*/
	public function getObjeto(){
		return $this->objeto;
		}
	/*get importe del contrato*/
	public function getImporteContrato(){
		return $this->importe;
		}
	/*get el iva del contrato*/
	public function getIvaContrato(){
		return $this->iva_contrato;
		}
	/*get total del contrato*/
	public function getTotalContratado(){
		return $this->total_contrato;
		}
	/*get localidad*/
	public function getLocalidad(){
		return $this->localidad;
		}
	/*get el municipios*/
	public function getMunicipio(){
		return $this->municipio;
		}
	/*get fecha del contrato*/
	public function getFechaContrato(){
		return $this->fecha_contrato;		
		}
	/*get fecha de incicio*/
	public function getFechaInicio(){
		return $this->fecha_inicio;
		}
	/*get fecha determino del contrato*/
	public function getFechaTermino(){
		return $this->fecha_termino;
		}
	/*get fecha ultima modificacion del contrato*/
	public function getFechaUltimaMod(){
		return $this->fecha_modificacion;
		}
	/*get observaciones del contraro*/
	public function getObservaciones(){
		return $this->observaciones;
		}
	/*get fecha real de terminacion del contrato*/
	public function getFechaRealTerminacion(){
		return $this->fecha_real_termino;
		}
	/*get tipo de obra*/
	public function getTipoObra(){
		/*array tipos de obra*/
		$tipos = array(1 => "Publica", 2 => "Equipamiento", 3 => "Servicios");
		/*resultado a regresar*/
		$resultado = array();
		
		/*set resultados*/
		$resultado[0] = $tipos[$this->tipo_contrato];
		$resultado[1] = $this->tipo_contrato;
		
		/*regresar resultados*/
		return $resultado;
		}
	/*tipo de contrato*/
	public function getTipoObraNew(){
		return $this->tipo_obra;
		}

	public function getRecursosINIFED(){
		return $this->recursos_inifed;
	}



	/*get si la obra es cabecera*/
	public function getObraComplementaria(){
		return $this->obra_complementaria;
		}
	/*get si la obra es complementaria*/
	public function getIdObraCabecera(){
		return $this->id_obra_cabecera;
		}
	/*get numero de contrato de la obra cabecera*/
	public function getNumContratoCabcera(){
		return $this->num_cont_obra_cabecera;
		}
	/*get latitud de la obra*/
	public function getLatitud(){
		return $this->latitud;
		}
	/*get longitud de la obra*/
	public function getLongitud(){
		return $this->longitud;
		}
	/*get estatus del contrato*/
	public function getEstatus(){
		return $this->online;
		}

	public function _municipiosVarios(){
		return $this->_municipiosVarios;
	}



	}





?>
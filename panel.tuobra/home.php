<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (empty($_SESSION['id-usuario']) || !isset($_SESSION['login']) || $_SESSION['login'] != true) {
    session_destroy();
    header("LOCATION: ../index.php?session=false");
}

require("config/conn.php");
require('funciones/config.php');
require('funciones/query.class.inc.php');
require("funciones/clientes.class.inc.php");
require("funciones/validar.formularios.class.inc.php");

/*DEFINIR EL ROOT*/
define('URL', 'home/');
/*DATOS DEL CLIENTE*/
$cliente = new clientes($conn, $_SESSION['id-usuario']);
/*nuevo objeto query*/
$query = new querys();
/*tipos de obra*/
$tipos_obra = $query->traerMultiplesResultados('SELECT id, descripcion FROM `tipo_obra` GROUP BY id, descripcion ORDER BY descripcion', NULL);
/*municipios*/
$municipios = $query->traerMultiplesResultados('SELECT M.id, M.nombre_municipio FROM `municipios` M');

$domain = "http://tuzobra.cf/panel.tuobra/";

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?php echo $domain; ?>">
    <title>Panel TuObra</title>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">


    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;subset=latin-ext" rel="stylesheet">


    <link type="text/css" rel="stylesheet" href="css/fuentes.css">
    <link type="text/css" rel="stylesheet" href="css/tooltips.css">
    <link type="text/css" rel="stylesheet" href="css/jquery-ui.css">


    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="css/newestilo.css">

    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>

    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>




    <script type="text/javascript" src="js/vitrum.js"></script>
</head>

<body>
<!-- contenedor -->
<div class="contenedor">
    <!-- inicio del header -->
    <header>
        <div class="logo-span">
            <a href="home/"><img src="http://tuobra.sonora.gob.mx/assets/img/logo_tu-obra.png" id="logo-tu-obra" title="Obra de tu gobierno a tu alcance" alt="logo"></a>
        </div>
        <h2 style="display:inline-block;">Panel de Captura</h2>
        <!--<div class="divicion-icons"></div>
        <div class="panel-icon">
        	<span style="font-size: 14px"><a href="http://tuobra.mx/<?php echo $cliente->getCustomUrl(); ?>" class="icon-fontawesome-webfont-4" target="_blank" title="Portal <?php echo $cliente->getNombre(); ?>"><span style="font-size:14px; vertical-align:top;">Visitar portal</span></a></span>
        </div>-->
        <!-- opciones del usuario -->
        <div class="header-opciones-usuario">
            <ul id="menu-opciones">
                <li><a href="#" class="icon-torso" id="perfile-de-usuario"><?php echo $_SESSION['nombre-usuario']; ?>
                        <span class="tick-down-up">&#9660;</span></a>
                    <ul class="sub-menu">
                        <!--<li><a href="<?php echo URL; ?>#">Perfil</a></li> -->
                        <li><a href="<?php echo URL; ?>#parametros">Parametros</a></li>
                        <li><a href="<?php echo URL; ?>#reporteAvances">Reporte de Avances</a></li>


                        <?php
                        //Valido que sea un administrador
                        if ($_SESSION['tipo'] == 1) {
                            ?>
                            <li><a href="<?php echo URL; ?>#administrador">Administrador</a></li>

                        <?php } ?>
                        <!--<li><a href="<?php echo URL; ?>#">Mensaje</a></li>-->
                    </ul>
                </li>
                <li><a href="#" class="icon-help-circled">Ayuda</a></li>
                <li><a href="logout.php" class="icon-fontawesome-webfont-9">Salir</a></li>
            </ul>
        </div>
    </header>
    <!-- fin del header -->
    <!-- contendor de opciones -->
    <div class="contenedor-opciones">
        <!-- comntenedor opciones inner -->
        <div class="contenedor-opciones-inner">
            <div class="content-op">
                <!-- home -->
                <ul class="opciones-listas opciones">
                    <li><a href="<?php echo URL; ?>" class="icon-home">Inicio</a></li>
                </ul>
                <hr class="guide-separator">
                <!-- opciones -->
                <ul class="opciones-listas opciones">
                    <li><select name="tipo-opcion" id="tipo-opcion-new" style="margin-bottom:10px; width:100%;">
                            <option value="1">Obra</option>
                            <option value="2">Licitaciones</option>
                        </select></li>
                    <li><a href="<?php echo URL; ?>#nuevo" class="icon-fontawesome-webfont-8" id="nuevo-recurso"><span>Nuevo</span></a>
                    </li>
                    <li><a href="<?php echo URL; ?>#editar" class="icon-editar editar"
                           id="editar-recurso"><span>Editar</span></a></li>
                    <li><a href="javascript:;" class="icon-delete-uno" id="borrar-recursos"
                           rel="contratos"><span>Borrar</span><span class="cantidad-borrar">0</span></a></li>
                </ul>
                <hr class="guide-separator">
                <div class="contenedor-ejercicios">
                    <ul class="opciones-listas ejercicios">
                        <?php include("include/ejercicios.php"); ?>
                    </ul>
                </div>
                <hr class="guide-separator">
                <!-- opciones del usuario -->
                <div class="opciones-usuario">
                    <ul class="lista-opciones-usuario">
                        <li data-tooltip="Mi Perfil"><a href="<?php echo URL; ?>#" class="icon-torso"></a></li>
                        <li data-tooltip="Mensajes"><a href="<?php echo URL; ?>#observaciones"
                                                       class="icon-fontawesome-webfont-3"><span class="msg-text"
                                                                                                id="cantidad-observaciones">0</span></a>
                        </li>
                        <li data-tooltip="Parametros"><a href="<?php echo URL; ?>#parametros" class="icon-widget"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- footer -->
            <footer>
                <div class="footer-content">
                    <a href="https://www.facebook.com/GobiernoSonora" class="icon-fontawesome-webfont-6"></a>
                    <a href="https://twitter.com/gobiernosonora" class="icon-fontawesome-webfont-7"></a>
                    <hr class="guide-separator">
                    <strong>Dudas o aclaraciones</strong> favor de comunicarse a los teléfonos de la SDAT 2175198 o
                    2175193.
                    <hr class="guide-separator">
                    © <?php echo date("Y"); ?> TuObra.mx Todo los derechos reservados.


                </div>
                <div class="fix">
                </div>
            </footer>
        </div>
    </div>
    <!-- fin del contenedor  de opciones -->
    <!-- inicio del contenedor de datos -->
    <div class="contenedor-datos">
        <!-- loding -->
        <div id="loading-screen"><img src="images/loading.gif"></div>
        <!-- inner contenedor datos -->
        <div class="contenedor-datos-inner">
            <!-- titulos -->
            <div class="contenedor-datos-titulos" id="titulos">
                <h1>Contratos</h1>
                <div class="content-buscar">
                    <form name="buscador-form" id="buscador-form" method="post" style="float:left; display:block;">
                        <input type="text" name="dato-buscar" id="dato-buscar" placeholder="Buscar">
                        <button type="submit" name="enviar-formulario" id="enviar-form-buscar"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <div id="contenedor-advance-search">
                            <table width="100%;">
                                <caption>
                                    <center>Búsqueda Avanzada</center>
                                </caption>
                                <tr class="filtro-contratos">
                                    <td><label>Modalidad:</label></td>
                                    <td><select name="modalidad" id="modalidad" style="width:100%;">
                                            <option value="">--Seleccione--</option>
                                            <?php
                                            foreach ($array_modalidad as $index => $mod) {
                                                echo "<option value=\"" . $index . "\">" . $mod . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                </tr>
                                <tr class="filtro-licitaciones" style="display:none;">
                                    <td><label>Tipo:</label></td>
                                    <td><select name="tipo_licitacion" id="tipo_licitacion" style="width:100%;">
                                            <option value="">--Seleccione--</option>
                                            <?php
                                            foreach ($array_tipo_licitacion as $index => $tipo) {
                                                echo "<option value=\"" . $index . "\">" . $tipo . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="filtro-contratos">
                                    <td><label>Tipo Obra:</label></td>
                                    <td><select name="tipo-obra" id="tipo-obra">
                                            <option value="">--Seleccione--</option>
                                            <?php
                                            foreach ($tipos_obra as $tipo) {
                                                echo "<option value=\"" . $tipo['id'] . "\">" . $tipo['descripcion'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                </tr>

                                <tr class="filtro-contratos">
                                    <td><label>Tipo de Contrato:</label></td>
                                    <td><select name="tipoContrato" id="tipoContratoƒ">
                                            <option value="">--Seleccione--</option>
                                            <?php
                                            foreach ($array_contratos as $k => $v) {
                                                echo "<option value=\"" . $k . "\">" . $v . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                </tr>

                                <tr class="filtro-contratos">
                                    <td><label>Municipio:</label></td>
                                    <td><select name="municipio" id="municipio" style="width:100%;">
                                            <option value="">--Seleccione--</option>
                                            <?php
                                            foreach ($municipios as $mun) {
                                                echo "<option value=\"" . $mun['id'] . "\">" . $mun['nombre_municipio'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td><label>Estatus</label></td>
                                    <td><select name="estatus" id="estatus">
                                            <option value="all">Todos</option>
                                            <option value="1">Proceso</option>
                                            <option value="2">Finalizadas</option>
                                        </select></td>
                                </tr>
                            </table>
                        </div>
                    </form>
                    <div class="return-contratos" style="display:none; line-height:40px; font-size:14px;">
                        <!--<span data-tooltip="Inicio" class="tooltip-left"><a href="<?php echo URL; ?>#" class="icon-home" style="color:#888; font-size:20px; border-right:1px solid #eee; padding-right:3px; margin-right:5px;"></a></span>-->
                        <a href="javascript:;" class="icon-level-up" onClick="window.history.back();">Regresar</a>
                    </div>
                </div>
            </div>
            <!-- opciones de contratos -->
            <div class="detalles-contratos off toggle-content" id="detalles-contratos">
            </div>
            <!-- datos -->
            <div class="contenedor-datos-datos toggle-content" id="contenedor-datos">
            </div>
        </div>
    </div>
</div>
<!-- fin del contenedor -->
</body>
</html>
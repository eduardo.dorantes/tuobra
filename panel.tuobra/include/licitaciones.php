<?php
session_start();
require('../funciones/query.class.inc.php');
require('../funciones/config.php');
require("../sources/msg-file.php");

if ($_SESSION['login'] != true) {
    exit;
}
/*crear objeto query*/
$query = new querys();
/*ini sql aux*/
$sql_aux = "";
/*ini array bind*/
$array_bind = array();
/*recibir el ejercicio*/
$ejercicio = $_POST['eje'];
/*search string*/
$search = '%' . $_POST['dato-buscar'] . '%';
/*tipo*/
$tipo_licitacion = $_POST['tipo_licitacion'];
/*estatus*/
$estatus = $_POST['estatus'];
/**/
$array_bind[':id_cliente'] = $_SESSION['id-cliente'];
$array_bind[':ejercicio'] = $ejercicio;
$array_bind[':search'] = $search;

if (!empty($tipo_licitacion)) {
    $sql_aux .= " AND l.tipo = :tipo";
    $array_bind[':tipo'] = $tipo_licitacion;
}


function  elYoutube($d){
    if($d){
        $r = '<span class="activo"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>';
    }else{
        $r = '<span class="inactivo"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>';

    }

    return $r;
}

function  elDocumento($d){
    if($d){
        $r = '<span class="activo"><i class="fa fa-file-text" aria-hidden="true"></i></span>';
    }else{
        $r = '<span class="inactivo"><i class="fa fa-file-text" aria-hidden="true"></i></span>';

    }
    return $r;
}


/*fecha de hoy*/
$hoy = date('Y-m-d');
/*determinar estatus*/
if (isset($_POST['estatus']) and !empty($_POST['estatus'])) {
    switch ($estatus) {
        case 1:
            $sql_aux .= " AND (str_to_date(le4.fecha, '%d/%m/%Y') IS NULL OR str_to_date(le4.fecha, '%d/%m/%Y') >= '" . $hoy . "')";
            break;
        case 2:
            $sql_aux .= " AND str_to_date(le4.fecha, '%d/%m/%Y') < '$hoy'";
            break;
        case 'all':
            $sql_aux .= "";
            break;
    }
}
/*sql query*/
$sql_query = "SELECT l.id, l.num_licitacion, l.tipo, l.normatividad, l.descripcion, l.fecha_publiacion, l.desierta,

IF(le1.tipo IS NOT NULL, 1, 0) AS visita,
IF((le1.url_trasmision IS NOT NULL) AND (TRIM(le1.url_trasmision)!='') AND (TRIM(le1.url_trasmision)!='0'), 1, 0) AS visita1,
IF((le1.documento IS NOT NULL) AND (TRIM(le1.documento)!='') AND (TRIM(le1.documento)!='0'), 1, 0) AS visita2,

IF(le2.tipo IS NOT NULL, 1, 0) AS junta,
IF((le2.url_trasmision IS NOT NULL) AND (TRIM(le2.url_trasmision)!='') AND (TRIM(le2.url_trasmision)!='0'), 1, 0) AS junta1,
IF((le2.documento IS NOT NULL) AND (TRIM(le2.documento)!='') AND (TRIM(le2.documento)!='0'), 1, 0) AS junta2,

IF(le3.tipo IS NOT NULL, 1, 0) AS apertura,
IF((le3.url_trasmision IS NOT NULL) AND (TRIM(le3.url_trasmision)!='') AND (TRIM(le3.url_trasmision)!='0'), 1, 0) AS apertura1,
IF((le3.documento IS NOT NULL) AND (TRIM(le3.documento)!='') AND (TRIM(le3.documento)!='0'), 1, 0) AS apertura2,

IF(le4.tipo IS NOT NULL, 1, 0) AS fallo,
IF((le4.url_trasmision IS NOT NULL) AND (TRIM(le4.url_trasmision)!='') AND (TRIM(le4.url_trasmision)!='0'), 1, 0) AS fallo1,
IF((le4.documento IS NOT NULL) AND (TRIM(le4.documento)!='') AND (TRIM(le4.documento)!='0'), 1, 0) AS fallo2

FROM `licitaciones` l 
INNER JOIN `clientes` c ON l.id_dependencia = c.ID 
LEFT JOIN `licitaciones_eventos` le1 ON le1.id_licitacion = l.id AND le1.tipo = 1
LEFT JOIN `licitaciones_eventos` le2 ON le2.id_licitacion = l.id AND le2.tipo = 2
LEFT JOIN `licitaciones_eventos` le3 ON le3.id_licitacion = l.id AND le3.tipo = 3
LEFT JOIN `licitaciones_eventos` le4 ON le4.id_licitacion = l.id AND le4.tipo = 4
WHERE c.ID = :id_cliente AND YEAR(str_to_date(l.fecha_publiacion, '%d/%m/%Y')) = :ejercicio 
AND (l.num_licitacion LIKE :search OR l.descripcion LIKE :search) " . $sql_aux . " order by l.num_licitacion asc";


echo "<div class=\"lista-ventana\">";
/*ejecutar query y comprobar*/
if ($rows = $query->traerMultiplesResultados($sql_query, $array_bind)) {
    /*empezar la tabla*/
    echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" id=\"lista-contratos\">";
    echo "<thead>";
    echo "<tr>";
    echo "<th width=\"1%\"></th>";
    echo "<th width=\"1%\">&nbsp;</th>";
    echo "<th align=\"center\" width=\"5%\">Numero licitación</th>";
    echo "<th align=\"left\">Tipo</th>";
    echo "<th align=\"left\">Normatividad</th>";
    echo "<th align=\"left\" width=\"30%\">Descripción</th>";
    echo "<th width=\"5%\">Fecha publicación</th>";
    echo "<th align=\"center\">V</th>";
    echo "<th align=\"center\">J</th>";
    echo "<th align=\"center\">A</th>";
    echo "<th align=\"center\">F</th>";
    echo "<th align=\"center\">Desierta</th>";
    echo "<th width=\"12%\"></th>";
    echo "</tr>";
    echo "</thead><tbody>";
    /*loop*/



    function desierta($a){
        $b = 'No';
        if($a){
            $b = 'Si';
        }
        return $b;
    }


    foreach ($rows as $row) {
        $i++;
        echo "<tr>";
        echo "<td>" . $i . "</td>";
        echo "<td><input type=\"checkbox\" name=\"selected-item[]\" id=\"check_" . $row['id'] . "\" class=\"check_opciones\" value=\"" . $row['id'] . "\"></td>";
        echo "<td>" . $row['num_licitacion'] . "</td>";
        echo "<td>" . $array_tipo_licitacion[$row['tipo']] . "</td>";
        echo "<td>" . $array_normatividad[$row['normatividad']] . "</td>";
        echo "<td width=\"40%\">" . $row['descripcion'] . "</td>";
        echo "<td align=\"center\">" . $row['fecha_publiacion'] . "</td>";
        echo "<td align=\"center\">";
        echo elYoutube($row['visita1']) .' '. elDocumento($row['visita2']);
        echo $row['visita'] > 0 ? '<span class="circule-green"></span>' : '<span class="circule-yellow"></span>';
        echo "</td>";
        echo "<td align=\"center\">";
        //echo $row['junta1'] .'---'. $row['junta2'];
        echo elYoutube($row['junta1']) .' '. elDocumento($row['junta2']);
        echo $row['junta'] > 0 ? '<span class="circule-green"></span>' : '<span class="circule-yellow"></span>';
        echo "</td>";
        echo "<td align=\"center\">";
        //echo $row['apertura1'] .'---'. $row['apertura2'];
        echo elYoutube($row['apertura1']) .' '. elDocumento($row['apertura2']);
        echo $row['apertura'] > 0 ? '<span class="circule-green"></span>' : '<span class="circule-yellow"></span>';
        echo "</td>";
        echo "<td align=\"center\">";
        //echo $row['fallo1'] .'---'. $row['fallo2'];
        echo elYoutube($row['fallo1']) .' '. elDocumento($row['fallo2']);
        echo $row['fallo'] > 0 ? '<span class="circule-green"></span>' : '<span class="circule-yellow"></span>';
        echo "</td>";
        echo "<td align='center'><strong>".desierta($row['desierta'])."</strong></td>";
        echo "<td class=\"over-flow-td\">";
        echo "<ul class=\"opciones-contrato\">";
        echo "<li data-tooltip=\"Agregar eventos de licitación\"><a href=\"home/#eventos-" . $row['id'] . "\">Eventos</a></li>";
        echo "</ul>";
        echo "</td>";
        echo "</tr>";
    }
    /*cerrar tabla*/
    echo "</tbody></table></div>";
} else {
    echo "<div class=\"info\" id=\"lista-contratos\">" . $mensaje['1003'] . "</div>";
}
?>
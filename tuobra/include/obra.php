<?php
require_once('../requires/Mobile_Detect.php');
require('../librerias/query.class.inc.php');
require('../librerias/config.php');
require('../librerias/upload.files.class.inc.php');
/*crear objeto query*/
$query = new querys();
/*crear objeto detectar*/
$detectar = new Mobile_Detect();
/*comprobar si es table*/
if($detectar->isTablet()){
	/*hacer algo*/
	}
/*ejecutar y comprrobar query detalles de licitacion*/


$sql = "SELECT c.* FROM vcontratosod as c where TRIM(c.NUM_CONTRATO) = :num_contrato";


if(!$row = $query->traerSoloResultado($sql, $array_bind = array(':num_contrato' => $_GET['num']))){

}

/*
 * SELECT V.ID, V.ID_MUNICIPIO as MUNICIPIO, V.INIFED, V.TIPO_OBRA AS TIPO, V.NUM_CONTRATO, V.NUM_LICITACION, V.OBJETO, V.nombre_municipio, V.LOCALIDAD, V.FECHA_INICIO, V.FECHA_TERMINO, V.TOTAL_CONTRATO, V.RAZON_SOCIAL, C.NOMBRE, V.por_avance, CN.LATITUD, CN.LONGITUD, V.RESIDENTE, V.SUPERVEX, (SELECT AV.FECHA FROM `avances` AV WHERE AV.ID_CONTRATO = V.ID ORDER BY AV.ID DESC LIMIT 1) AS fechaAvance, CN.FECHA_TERMINO_REAL FROM `vistacontratos` V INNER JOIN `clientes` C ON C.ID = V.ID_CLIENTE INNER JOIN `contratos` CN ON CN.ID = V.ID WHERE TRIM(V.NUM_CONTRATO) = :num_contrato
 *
 * */








/*comprobar si existe la licitacion*/
if(!is_array($row)){
	echo "El contrato no existe";
	exit;
	}


	$meta = '<meta property="og:site_name" content="tuObra" />
<meta property="og:site" content="tuobra.sonora.gob.mx" />
<meta property="og:title" content="Contrato - '.$row['NUM_CONTRATO'].'" />
<meta property="og:description" content="'.$row['OBJETO'].'" />
<meta property="og:image" content="http://pruebastuobra.sonora.gob.mx/assets/img/share-tu-obra-01.png" />
<meta property="og:url" content="http://tuobra.sonora.gob.mx/obra/'.$row['NUM_CONTRATO'].'" />
<meta property="og:type" content="article" />
<meta name="twitter:card" content=\'summary\' />
<meta name="twitter:image" content="http://pruebastuobra.sonora.gob.mx/assets/img/share-tu-obra-01.png" />
<meta name="twitter:site" content="@gobiernosonora" />
<meta name="twitter:url" content="http://tuobra.sonora.gob.mx/obra/'.$row['NUM_CONTRATO'].'" />
<meta name="twitter:description" content="'.$row['OBJETO'].'" />
<meta name="twitter:title" content=" Contrato - '.$row['NUM_CONTRATO'].'" />
<title>TuObra - Contrato:'.$row['NUM_CONTRATO'].'</title>
<meta name="description" content="'.$row['NUM_CONTRATO'].' - '.$row['OBJETO'].'">';


$tipoObra = ['','educacion','infraestructura','salud','carreteras'];
$tipoTituloObra = ['','Educación','Infraestructura','Salud','Caminos y Carreteras'];
$tipoColorObra = ['','#fb3448','#34C08E','#7fcccc','#F4B12A']


?>

<?php include('../include/header.php'); ?>




<link href="assets/css/jquery.circliful.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="assets/js/jquery.circliful.min.js"></script>



<link href="assets/css/jquery.fancybox.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="assets/js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>








<script type="text/javascript">
$(document).ready(function() {
	$('#progreso-obra').circliful();

});
</script>




<script type="text/javascript" src="assets/js/jquery.sharrre.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {


		$('.share').sharrre({
			share: {
				twitter: true,
				facebook: true
			},
			template: '<li><a href="#" class="btn btn-primary btn-circle btn-facebook facebook"><i class="fa fa-facebook"></i></a></li><li><a href="#" class="btn btn-primary btn-circle btn-twitter twitter"><i class="fa fa-twitter"></i></a></li>',
			enableHover: false,
			enableTracking: false,
			enableCounter: true,
			render: function(api, options){
				$(api.element).on('click', '.twitter', function() {
					api.openPopup('twitter');
				});
				$(api.element).on('click', '.facebook', function() {
					api.openPopup('facebook');
				});
			}
		});

	});
</script>






<section id="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Obra Pública</h2>
				<p>Inicio | Obra Pública | Expediente Digital</p>
			</div>
		</div>
	</div>

</section>



<section id="contentForm">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">


				<div class="form-horizontal">


					<div class="form-group">
						<div class="col-sm-6 text-uppercase">
							<h1><?php echo $row['NUM_CONTRATO'] ?></h1>
						</div>
						<div class="col-sm-4">
							<p class="text-right m-t">
								<strong class="social-title">Compartir en Redes Sociales</strong>
							</p>
						</div>
						<div class="col-sm-2">
							<div class=" m-t-sm">
								<ul class="social-icons share" data-title="Portal TuObra.mx / <?php echo $row['NUM_CONTRATO'] ?>" data-url="http://tuobra.sonora.gob.mx/obra/<?php echo $row['NUM_CONTRATO']; ?>">

								</ul>
							</div>
						</div>
					</div>



					<hr />

					<?php

					if($row['INIFED']==1){ ?>

					<div class="form-group">
						<div class="col-sm-12">
							<img src="assets/img/banner-inifed.png" style="display:block; width:100%" />
						</div>
					</div>

					<hr />

					<?php } ?>





                    <?php


                    $sql2 = "SELECT C.TOTAL_CONTRATO, C.NUM_CONTRATO, C.OBJETO, C.MODALIDAD, C.TIPO_CONTRATO, C.TIPO_OBRA, C.AVANCE, C.CONTRATISTA FROM vcontratosod as C where C.OC_NUMERO_CONTRATO = :NUM_CONTRATO";
                    $row2 = $query->traerMultiplesResultados($sql2, $array_bind = array(':NUM_CONTRATO' => $row['NUM_CONTRATO']));

					//print_r($row2);


					$t = $is_oc =  '';
					$oc = 0;

					if(count($row2) > 0){

						//echo count($row2);

						$t .='<table class="table table-details">';
						$t .='<thead><tr> <th>No. Contrato</th> <th>Objeto</th> <th>Modalidad</th> <th>Tipo de Contrato</th> <th>Contratista</th>  <th>Total Contrato</th></tr></thead><tbody>';

						$sm = $row['TOTAL_CONTRATO'];


						$oc = 1;


						foreach($row2 as $k => $v){

							$sm = $sm + $v['TOTAL_CONTRATO'];

							$t .='<tr>';
							$t .='<td class="col-sm-1"><a target="_blank" href="obra/'.$v['NUM_CONTRATO'].'" title="Ver Obra">'.$v['NUM_CONTRATO'].'</a></td>';
							$t .='<td class="col-sm-4"><a target="_blank" href="obra/'.$v['NUM_CONTRATO'].'" title="Ver Obra">'.$v['OBJETO'].'</a></td>';
							$t .='<td class="col-sm-2"><a target="_blank" href="obra/'.$v['NUM_CONTRATO'].'" title="Ver Obra">'.$v['MODALIDAD'].'</a></td>';
							$t .='<td class="col-sm-2"><a target="_blank" href="obra/'.$v['NUM_CONTRATO'].'" title="Ver Obra">'.$v['TIPO_CONTRATO'].'</a></td>';
							$t .='<td class="col-sm-2"><a target="_blank" href="obra/'.$v['NUM_CONTRATO'].'" title="Ver Obra">'.$v['CONTRATISTA'].'</a></td>';
							//$t .='<td>'.$v['AVANCE'].' %</td>'; <th>Avance</th>
							$t .='<td class="col-sm-2"><a target="_blank" href="obra/'.$v['NUM_CONTRATO'].'" title="Ver Obra">$ '.number_format($v['TOTAL_CONTRATO'], 2).'</a></td>';
							$t .='</tr>';

						}




						//$t .='<tfoot><tr><th colspan="4" class="text-right"><strong>Costo total</strong></th><th>$ '.number_format($sm,2).'</th></tr></tfoot>';


						$t .='</tbody></table>';

						//echo $t;


					}

					$arrayTipoOC = ['OBRA PÚBLICA'=>'trabajo de obra', 'SERVICIOS'=>'servicio', 'EQUIPAMIENTO'=>'equipamiento'];

					if(!empty($row['OC_NUMERO_CONTRATO'])){

						$is_oc = '<p class="alert alert-warning"><strong> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
Aviso Importante: </strong> Esta obra es un '.$arrayTipoOC[$row['TIPO_CONTRATO']].' complementario a la obra <a href="obra/'.$row['OC_NUMERO_CONTRATO'].'" title="Ver Obra">'.$row['OC_NUMERO_CONTRATO'].'</a>.</p>';


						echo $is_oc;
					}



                    ?>







					<div class="form-group">

						<div class="<?php echo !empty($row['AVANCE']) ? 'col-sm-9 ' : 'col-sm-12';  ?>">
							<div class="box-inversion" style="background: <?php echo $tipoColorObra[$row['ID_TIPO_OBRA']]; ?> ">
								<h2>$
									<?php

										if($oc){
											echo number_format($sm,2). '<span>*</span>';
										}else{
											echo number_format($row['TOTAL_CONTRATO'], 2);
										}
									?>
								</h2>
								<p class="form-control-static">Monto de la inversión</p>
                                <?php

                                if(trim($row['BENEFICIADOS'])!="" && trim($row['BENEFICIADOS'])>0){ ?>

                                <p class="text-uppercase"><small style="font-size: 70%"> <?php echo number_format($row['BENEFICIADOS'],0); ?> beneficiados</small></p>


                                <?PHP } ?>



							</div>



						</div>

						<div class="col-sm-3 text-center <?php echo !empty($row['AVANCE']) ? '' : 'hidden';  ?>">
								<div id="progreso-obra" data-dimension="220" data-text="<?php echo !empty($row['AVANCE']) ? $row['AVANCE']: 0;  ?>%" data-info="al <?php echo $row['FECHA_AVANCE']; ?>" data-width="30" data-fontsize="40" data-percent="<?php echo !empty($row['AVANCE']) ? $row['AVANCE']: 0; ?>" data-fgcolor="<?php echo $tipoColorObra[$row['ID_TIPO_OBRA']]; ?>" data-bgcolor="#ddd" data-fill="#fff">
							</div>
								<p class="form-control-static clear">Avance F&iacute;sico</p>
						</div>
					</div>


					<?php

					if($oc){
						echo '<p><strong>* El monto de la inversión incluye servicios y equipamiento complementarios. A continuación se detallan cuales son:</strong></p>';
					}
					?>



					<?php echo $t; ?>



					<hr />


					<div class="form-group">
						<label class="col-sm-2 control-label text-left">Tipo</label>
						<div class="col-sm-5">
							<p class="form-control-static"><?php echo $tipoTituloObra[$row['ID_TIPO_OBRA']]; ?></p>
						</div>

						<label class="col-sm-2 control-label  text-left">Licitación</label>
						<div class="col-sm-3">
							<p class="form-control-static">
                                <?php

                                if(trim($row['NUM_LICITACION'])!=""){ ?>

                                <a href="licitacion/<?php echo $row['NUM_LICITACION']; ?>"><?php echo $row['NUM_LICITACION']; ?></a>

                                <?PHP } ?>
                            </p>
						</div>
					</div>

					<hr />


					<div class="form-group">
						<label class="col-sm-2 control-label text-left">Dependencia / Organismo</label>
						<div class="col-sm-5">
							<p class="form-control-static"><?php echo $dependencia = strtoupper($row['ORGANISMO']); ?></p>
						</div>

						<label class="col-sm-2 control-label  text-left">Contratista</label>
						<div class="col-sm-3">
							<p class="form-control-static"><?php echo strtoupper($row['CONTRATISTA']); ?></p>
						</div>
					</div>




					<hr />


					<div class="form-group">
						<label class="col-sm-2 control-label text-left">Municipio</label>
						<div class="col-sm-5">


								<?php

								if($row['ID_MUNICIPIO']==103){

									$q = new querys();

									$array = array(':num_contrato' => $_GET['num']);
									$m = $q->traerMultiplesResultados('SELECT m.nombre_municipio AS municipio FROM municipios m LEFT JOIN municipiosContratos mc ON m.id = mc.idMunicipio LEFT JOIN contratos o ON o.ID = mc.idContrato where TRIM(o.NUM_CONTRATO) = :num_contrato  order by municipio asc ',$array);

									echo '<ul style="margin:0; padding:0">';
									foreach($m as $k =>$v){
										echo '<li>'.$v['municipio'].'</li>';
									}
									echo '</ul>';
								}else{

									echo '<p class="form-control-static">'.$row['MUNICIPIO'].'</p>';

								}



								?>


						</div>

						<label class="col-sm-2 control-label  text-left">Ubicación</label>
						<div class="col-sm-3">
							<p class="form-control-static"><?php echo $row['LOCALIDAD']; ?></p>
						</div>
					</div>


				<hr />

				<div class="form-group">
					<label class="col-sm-2 control-label text-left">Objeto</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?php echo  strtoupper($row['OBJETO']); ?></p>
					</div>


				</div>

				<hr />

				<div class="form-group">
					<label class="col-sm-2 control-label text-left">Residente / Supervisor</label>
					<div class="col-sm-5">
						<p class="form-control-static"><?php echo $row['RESIDENTE']; ?></p>
					</div>

					<label class="col-sm-2 control-label  text-left">Supervisión Externa</label>
					<div class="col-sm-3">
						<p class="form-control-static"><?php echo $row['SUPERVISOR_EXTERNO']; ?></p>
					</div>
				</div>

				<hr />


					<div class="form-group">
						<label class="col-sm-2 control-label text-left">Periodo de Ejecución</label>
						<div class="col-sm-5">
							<p class="form-control-static"><?php echo $row['FECHA_INICIO']; ?> - <?php echo $row['FECHA_TERMINO']; ?></p>
						</div>

						<label class="col-sm-2 control-label  text-left">Esta obra estará concluida en</label>
						<div class="col-sm-3">
							<p class="form-control-static"><?php echo $row['FECHA_ENTREGA']; ?></p>
						</div>
					</div>

					<hr />



				<div class="form-group">
					<label class="col-sm-2 control-label text-left">Documentación</label>
					<div class="col-sm-10">
						<?php
						if($docs = $query->traerMultiplesResultados('SELECT e.ID, e.ARCHIVO, e.NOMBRE, p.DOCUMENTO FROM `expediente` e LEFT JOIN `plantillas` p ON e.ID_PLANTILLA = p.ID WHERE e.ID_CONTRATO = :id_contrato', $array_bind = array(':id_contrato' => $row['ID_OBRA']))){
							/*si traer resultados entonces mostrar los archivos*/
							echo "<ul class=\"nav navbar-nav\">";
							/*loop para sacar los documentos*/
							foreach($docs as $doc){
								/*comprobar que venga la url*/
								if(!empty($doc['ARCHIVO'])){
									echo "<li><a href=\"http://paneltuobra.sonora.gob.mx/".$doc['ARCHIVO']."\" target=\"_blank\"><img src=\"images/".$array_tipo_documento_img[upload::getExtencionArchivo($doc['ARCHIVO'])]."\"> <span>"; echo empty($doc['NOMBRE']) ? $doc['DOCUMENTO'] : $doc['NOMBRE']; echo "</span></a></li>";
								}
							}
							echo "</ul>";
						}
						?>
					</div>
				</div>



				<hr />

				<div class="form-group">
					<label class="col-sm-2 control-label text-left">Geolocalización</label>
					<div class="col-sm-10">
						<div id="contenedor-detalles-mapa">
							<iframe frameborder="0" style="width:100%; height:100%; height: 300px; margin: 0px; padding: 0px" allowfullscreen  src="include/obra-mapa.php?lat=<?php echo $row['LATITUD']; ?>&long=<?php echo $row['LONGITUD']; ?>&tipo=<?php echo $row['ID_TIPO_OBRA']; ?>"></iframe>
						</div>
						<p class="text-right"><strong>Latitud</strong> <?php echo $row['LATITUD']; ?>  - <strong>Longitud</strong> <?php echo $row['LONGITUD']; ?></p>

					</div>
				</div>

					<?php include('testigo-ciudadano.php'); ?>


				<div class="well">


					<h4 class="subtitle subtitle-black text-center text-uppercase">Avance Fotográfico</h4>
					<?php
					/******avance fotografico***********/
					/*query string*/
					$query_string = "SELECT AFF.ID, 0 AS IDFOTO, AFF.DESCRIPCION, '' AS THUMB, '' AS IMAGEN, '' AS PIE FROM `avance_fotografico_frentes` AFF WHERE AFF.ID_CONTRATO = :id_contrato UNION ALL SELECT AF.ID_FRENTE, AF.ID, '', AF.THUMB, AF.THUMB_LARGE as IMAGEN, AF.PIE_FOTO FROM `avance_fotografico` AF WHERE AF.ID_CONTRATO = :id_contrato ORDER BY ID, IDFOTO";
					/*ejecutar y comprobar query*/
					if($rows = $query->traerMultiplesResultados($query_string, $array_bind = array(':id_contrato' => $row['ID_OBRA']))){
						/*loop*/
						$es_frente = false;
						foreach($rows as $raw){
							if($raw['IDFOTO'] == 0){
								if($es_frente){
									echo "</div>\t\n";
								}
								echo "<h4>".$raw['DESCRIPCION']."</h4> <div class=\"row\">\t\n";
							}
							if($raw['IDFOTO'] > 0){
								$es_frente = true;
								echo "<div class='col-sm-2 col-xs-6'><a class='thumbnail' href=\"http://tuzobra.cf/panel.tuobra/".$raw['IMAGEN']."\"  data-fancybox-group=\"main_".$raw['ID']."\"><img src=\"http://tuzobra.cf/panel.tuobra/".$raw['THUMB']."\"><figcaption>".$raw['PIE']."</figcaption></a></div>\t\n";
							}
						}
						/*cerrar el ultimo div*/
						echo "</div>\t\n";
					}else{
						echo "No se encontró ninguna imagen.";
					}
					?>
				</div>


				</div>






				</div>



			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(document).ready(function() {
		$(".thumbnail").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none',
			padding: 0
		});
	});
</script>


<?php include('../include/footer.php'); ?>


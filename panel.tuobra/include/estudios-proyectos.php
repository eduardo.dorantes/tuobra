<?php
/************************/
/* ESTUDIOS Y PROYECTOS */
/************************/
session_start();
require("../config/conn.php");
require("../sources/msg-file.php");

/*get el contrato*/
$id_contrato = !empty($_POST['id']) ? $_POST['id'] : NULL;

	/*comprobar que no venga vacio el id del contrato*/
	if(!empty($id_contrato) and $id_contrato != NULL and $_SESSION['login'] == true){
		/*query*/
		$sql_query = "SELECT E.ID, E.DESCRIPCION, E.ARCHIVO_FTP FROM `estudios` E WHERE E.ID_CONTRATO = ?;";
		/*preparar el query*/
		$query = $conn->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $query->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('i', $id_contrato);
		/*executar query*/
		$query->execute();
		/*store resultados*/
		$query->store_result();
		/*resultados*/
		$query->bind_result($id, $descripcion, $archivo);
		/*comprobar que aya resultados*/
			if($query->num_rows > 0){
				/*empezar la tabla*/
				echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\">";
				echo "<thead>";
				echo "<tr>";
				echo "<th></th>";
				echo "<th>Descripción</th>";
				echo "<th>Documento</th>";
				echo "</tr>";
				echo "</thead><tbody>";
				/*extraer los resultados*/
				while($query->fetch()){
					echo "<tr>";
					echo "<td align=\"center\"><input type=\"checkbox\" name=\"selected-item[]\" id=\"check_".$id."\" class=\"check_opciones\" value=\"".$id."\"></td>";
					echo "<td align=\"left\">".$descripcion."</td>";
					echo "<td align=\"center\"><a href=\"".$archivo."\" target=\"_blank\"><img src=\"images/icons/pdf_big.png\"></a></td>";
					echo "</tr>";
					}
				/*cerrar la tabla*/
				echo "</tbody></table>";
				}else{
					echo $mensaje['1003'];
					}
		/*cerrar el query*/
		$query->close();
		/*cerrar la conexcion*/
		$conn->close();
			}
?>
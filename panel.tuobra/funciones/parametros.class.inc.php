<?php
class parametros{
	
	var $db;//conexion a la base de datos
	private $exite = false;//regresa true o false si existe parametros para ese cliente
	private $id_cliente;//id del cliente
	private $iva;//porcentaje de iva
	private $municipio;//id del municipo del cliente
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		$this->db = $conn;//conexion a la base de datos
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			$this->id_cliente = $id;//set el id de la obra
			$this->datosParametros();
			}
		}
	
	/*guardar los parametros*/
	public function saveParametros($iva){
		/*determinar el query*/
		if($this->exite == false){
			$query_string = "INSERT INTO parametros_cliente (IVA, ID_CLIENTE) VALUES (?, ?);";
			}else{
				$query_string = "UPDATE parametros_cliente SET IVA = ? WHERE ID_CLIENTE = ?;";
				}
		/*preparar query*/
		$query = $this->db->prepare($query_string);
		/*comprobar query*/
		if($query === false){
			echo trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: '. $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('di',$iva, $this->id_cliente);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	
	/*datos parametros*/
	public function datosParametros(){
		if(!empty($this->id_cliente) and strlen($this->id_cliente) > 0){
			/*query*/
			$sql_query = "SELECT IVA, C.MUNICIPIO FROM `parametros_cliente` PC LEFT JOIN `clientes` C ON C.ID = PC.ID_CLIENTE WHERE PC.ID_CLIENTE = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				echo trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_cliente);
			/*ejecutar query*/
			$query->execute();
			/*almacenar los resultados*/
			$query->store_result();
			/*bind los resultados*/
			$query->bind_result($iva, $municipio);
			/*fetch datos*/
			if($query->num_rows > 0){
				$this->exite = true;
				}
			$query->fetch();
			/*datos*/
			$this->iva = $iva;
			$this->municipio = $municipio;
			}
		}
	
	/*traer resultados*/
	public function getIva(){
		return $this->iva;
		}
	/*traer dato del municipio*/
	public function getMunicipio(){
		return $this->municipio;
		}
	}
?>
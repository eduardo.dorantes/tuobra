$(document).ready(function() {

    // page is now ready, initialize the calendar...
    function renderCalendar() {
        $('#comingSoonEvents').fullCalendar({
            header: {
                left: 'title',
                center: '',
                right: 'month'
                //,agendaWeek,agendaDay
            },
            lang: "es",
            buttonIcons: false, // show the prev/next text
            weekNumbers: false,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            eventSources: [

                // your event source
                {
                    url: 'ajax/get_cs_events.php',
                    type: 'POST',
                    data: {
                        opcion: 'get_month_events'
                    },
                    error: function() {
                        alert('there was an error while fetching events!');
                    }
                }

                // any other sources...

            ],

            contentHeight: 800,
            eventClick: function(calEvent, jsEvent, view) {




                //alert('Event: ' + calEvent.title +' Descripción'+ calEvent.descripcion);
                //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                //alert('View: ' + view.name);

                // change the border color just for fun
                $(this).css('border-color', 'red');


                var fechaActual = new Date();
                var fechaEvento = new Date(calEvent.fechaEvento);
                var highlightYoutube = "highlightYoutube";
                var hiddenYoutube = "";

                if(fechaActual.getTime() < fechaEvento.getTime()){
                    highlightYoutube ="";
                    hiddenYoutube = "hidden"
                }


                var tipoEvento = ['Evento','Visita a la obra','Junta de Aclaraciones','Apertura de Paquetes','Fallo'];

                //console.log(calEvent.youtube);

                var $html = $('<div class="event"></div>');
                    $html.append('<div class="event-title"><i class="fa fa-calendar"></i> Evento del día '+calEvent.fechaEvento+' </div>');
                    $html.append('<div class="event-content"></div>');
                    $html.find('.event-content').append('<div id="fechaActual" class="pull-right col-sm-2 text-center '+highlightYoutube+'"><div><i class="fa fa-play-circle-o fa-3x" aria-hidden="true"></i></div> '+calEvent.horaEvento+'</div>');
                    $html.find('.event-content').append('<h3>'+tipoEvento[calEvent.tipoEvento]+' - '+calEvent.title+'</h3>');
                    $html.find('.event-content').append('<h5>'+calEvent.dependencia+'</h5>');
                    $html.find('.event-content').append('<p>'+calEvent.descripcion+'</p>');
                    if(calEvent.youtube != false){
                        $html.find('.event-content').append('<p id="frameYoutube" class="'+hiddenYoutube+'"> <iframe width="560" height="315" src="https://www.youtube.com/embed/'+calEvent.youtube+'" frameborder="0" allowfullscreen></iframe></p>');
                    }


                $("#eventoModal").find('.modal-body').html($html);
                $("#eventoModal").modal('show');
            },


        })
    }

    renderCalendar();



    $.ajax({
        type: "POST",
        url: 'ajax/get_cs_events.php',
        data: {'opcion':'get_lastest_events'},
        success: function(data){

            var tipoEvento = ['Evento','Visita a la obra','Junta de Aclaraciones','Apertura de Paquetes','Fallo'];

            $(data).each(function(k,v){


                var $html = $('<div>').addClass('item');


                $html.append($('<a>').attr('href','licitacion/'+ v.licitacion).attr('title','Ver Licitación: '+ v.licitacion +': '+ v.descripcion).addClass('link'));
                $html.find('.link').append($('<h5>').html(v.licitacion));
                $html.find('.link').append($('<p>').addClass('meta'));
                $html.find('.meta').append($('<span>').html(tipoEvento[v.tipoEvento]).addClass('type'));
                $html.find('.meta').append(' | ');
                $html.find('.meta').append($('<span>').html(v.fechaEvento+' '+ v.horaEvento).addClass('date'));

  /*
                $html.find('.caption').append($('<h3>').html(v.contrato));
                $html.find('.caption').append($('<p>').addClass('municipio').html(v.municipio+', SONORA'));
                $html.find('.caption').append.html('<i class="fa fa-chevron-right" aria-hidden="true"></i>'));
                $html.find('.caption').append($('<p>').addClass('objeto').html(v.objeto));
                $html.find('.caption').append($('<p>').addClass('monto').html('Inversión: '+accounting.formatMoney(v.monto, "$", 0, ",", ".")));

*/


                $('#lastestEvents ul').append($('<li>').addClass('list-item').html($html));

            });


        },
        dataType: 'json'
    });





    $('#eventoModal').on('hidden.bs.modal', function (e) {
        $(this).find('.modal-body').html('');
    })





});



<?php
require('../../funciones/query.class.inc.php');
require('../../funciones/config.php');

session_start();
if(empty($_SESSION['id-usuario']) || !isset($_SESSION['login']) || $_SESSION['login'] != true ){
    header("LOCATION:home/");
}








// fetch the data

$cSql ="";
/*
if(!empty($_POST['organismos2']) && $_POST['organismo2s']!='all'){
    $organismo = $_POST['organismos2'];
    $organismo = intval($organismo);

    if($organismo > 0){
        $cSql = "AND c.ID='".$organismo."' ";
    }
}*/


if($_SESSION['tipo']==1) {

    if (!empty($_POST['organismos2']) && $_POST['organismos2'] != 'all') {
        $organismo = $_POST['organismos2'];
        $organismo = intval($organismo);

        if ($organismo > 0) {
            $cSql = "AND c.ID='" . $organismo . "' ";
        }
    }
}else{
    $cSql = "AND c.ID='" . $_SESSION['id-cliente'] . "' ";
}





// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte-de-Avance-de-Licitaciones-'.$organismo.'-'.date("Y-m-d-H-i-s").'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');





$q = "SELECT
	LICITACION,
	ORGANISMO,
	IF(COALESCE(TIPO,'')='','NO','SI') AS TIPO,	
	IF(COALESCE(NORMATIVIDAD,'')='','NO','SI') AS NORMATIVIDAD,
	IF(COALESCE(descripcion,'')='','NO','SI') AS DESCRIPCION,
	IF(COALESCE(FECHA_PUBLICACION,'')='','NO','SI') AS FECHA_PUBLICACION,
	IF(COALESCE(FECHA_VISITA_OBRA,'')='','NO','SI') AS FECHA_VO,
	IF(COALESCE(HORA_VO,'')='','NO','SI') AS HORA_VO,
	IF(COALESCE(DOC_VO,'')='','NO','SI') AS DOC_VO,
	IF(COALESCE(URL_VO,'')='','NO','SI') AS URL_VO,

	IF(COALESCE(FECHA_JUNTA_ACLARACIONES,'')='','NO','SI') AS FECHA_JA,
	IF(COALESCE(HORA_JA,'')='','NO','SI') AS HORA_JA,
	IF(COALESCE(DOC_JA,'')='','NO','SI') AS DOC_JA,
	IF(COALESCE(URL_JA,'')='','NO','SI') AS URL_JA,

	IF(COALESCE(FECHA_APERTURA_PROPUESTAS,'')='','NO','SI') AS FECHA_AP,
	IF(COALESCE(HORA_AP,'')='','NO','SI') AS HORA_AP,
	IF(COALESCE(DOC_AP,'')='','NO','SI') AS DOC_AP,
	IF(COALESCE(URL_AP,'')='','NO','SI') AS URL_AP,

	IF(COALESCE(FECHA_FALLO,'')='','NO','SI') AS FECHA_F,
	IF(COALESCE(HORA_F,'')='','NO','SI') AS HORA_F,
	IF(COALESCE(DOC_F,'')='','NO','SI') AS DOC_F,
	IF(COALESCE(URL_F,'')='','NO','SI') AS URL_F
FROM
	vlicitaciones
LEFT JOIN clientes AS c ON (c.NOMBRE=vlicitaciones.ORGANISMO) 
WHERE (
	COALESCE(TIPO,'')='' OR
	COALESCE(NORMATIVIDAD,'')='' OR
	COALESCE(descripcion,'')='' OR
	COALESCE(FECHA_PUBLICACION,'')='' or 
	 
	 ( desierta = 0 and ( 
	
            /*
            COALESCE(FECHA_VISITA_OBRA,'')='' OR
            COALESCE(HORA_VO,'')='' OR
            COALESCE(DOC_VO,'')='' OR
            COALESCE(URL_VO,'')='' OR
            
            */
            
            
            COALESCE(FECHA_JUNTA_ACLARACIONES,'')='' OR
            COALESCE(HORA_JA,'')='' OR
            COALESCE(DOC_JA,'')='' OR
            
            /*
            COALESCE(URL_JA,'')='' OR
            */
            
            COALESCE(FECHA_APERTURA_PROPUESTAS,'')='' OR
            COALESCE(HORA_AP,'')='' OR
            COALESCE(DOC_AP,'')='' OR
            COALESCE(URL_AP,'')='' OR
            
            COALESCE(FECHA_FALLO,'')='' OR
            COALESCE(HORA_F,'')='' OR
            COALESCE(DOC_F,'')='' OR
            COALESCE(URL_F,'')=''
        )
	)
	
)

".$cSql."

order by ORGANISMO asc";

$a = $t= array();
$query = new querys();
$r = $query->traerMultiplesResultados($q, $a);


function encode_items(&$item, $key)
{
    $item = utf8_decode($item);
}

array_walk_recursive($r, 'encode_items');

$t = array_keys($r[0]);

// output the column headings
fputcsv($output, $t);


foreach($r as $v){
    fputcsv($output, $v);
}


?>
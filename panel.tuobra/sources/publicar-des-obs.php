<?php
session_start();
require('../funciones/query.class.inc.php');
require('../funciones/config.php');
require("../sources/msg-file.php");

/*crear nuevo objeto query*/
$query = new querys();
/*recibir el id de la obs*/
$id_obs = $_POST['id'];
/*tipo de publicacion*/
$tipo = $_POST['tipo'];
/*ini resultado*/
$resultado = array();
/*ini set estatus false*/
$resultado['estatus'] = false;
/*ini set img publicado*/
$resultado['img_on'] = "images/icons/1444781395_cloud_cloud-upload-online.png";
/*ini set img no publicado*/
$resultado['img_off'] = "images/icons/1444781395_cloud_cloud-upload.png";

/*comprobar que venga el id del contrato*/
if(!empty($id_obs) and $_SESSION['login'] == true){
	/*checar el tipo de query*/
	switch($tipo){
		case 0:
			if($query->ejecutarQuery("UPDATE `observaciones` SET activo = 1 WHERE id = :id_obs", $array_bind = array(':id_obs' => $id_obs))){
				$resultado['estatus'] = true;
				}
		break;
		case 1:
			if($query->ejecutarQuery("UPDATE `observaciones` SET activo = 0 WHERE id = :id_obs", $array_bind = array(':id_obs' => $id_obs))){
				$resultado['estatus'] = true;
				}
		break;
		}
	}
/*convetir a json*/
echo json_encode($resultado);
?>
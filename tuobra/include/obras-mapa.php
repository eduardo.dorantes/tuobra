

<?php
require('../librerias/query.class.inc.php');
require('../librerias/config.php');
/*nuevo objeto query*/
$query = new querys();
/*query municipios*/
$municipios = $query->traerMultiplesResultados('SELECT M.id, M.nombre_municipio FROM `municipios` M');
$clientes = $query->traerMultiplesResultados('SELECT `ID`, `NOMBRE` FROM `clientes` ORDER BY `NOMBRE`', NULL);
$tipos_obra = $query->traerMultiplesResultados('SELECT id, descripcion FROM `tipo_obra` GROUP BY id, descripcion ORDER BY descripcion', NULL);
?>



<section id="main-map">

    <!-- Controles -->




    <!-- Google Map -->
    <div id="googleMap">
        <div id="contenedor-mapa">
            <div id="map-canvas">
                <div id="mapLoader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
            </div>
        </div>
    </div>



    <div id="filterBar">
        <form>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="filtrosMapa">
                            <label><input type="checkbox" checked name="filterMapCategroy[]" value="1"/> Educación</label>
                            <label><input type="checkbox" checked name="filterMapCategroy[]" value="2"/> Infraestructura</label>
                            <label><input type="checkbox" checked name="filterMapCategroy[]" value="3"/> Salud</label>
                            <label><input type="checkbox" checked name="filterMapCategroy[]" value="4"/> Carreteras y Caminos</label>
                            <div id="btnMoreFiltersMap" class="btnMoreFiltersMap right"><a href="#top" class="btn btn-success btn-circle"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a> </div>
                        </div>
                    </div>
                </div>
                <div id="moreFilterMap" style="">




                    <div id="buscar-obras" class="form-horizontal m-t-sm">

                        <div class="form-group">


                            <div class="col-sm-3">
                                <select name="organismo" id="organismo" class="form-control">
                                    <option value="">Dependencia / Organismo</option>
                                    <?php
                                    foreach($clientes as $value){
                                        echo "<option value=\"".$value['ID']."\">".$value['NOMBRE']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="modalidad" id="modalidad" class="form-control">
                                    <option value="">Modalidad</option>
                                    <?php
                                    foreach($array_modalidad as $index => $mod){
                                        echo "<option value=\"".$index."\">".$mod."</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <select name="municipio" id="municipio" class="form-control">
                                    <option value="">Municipio</option>
                                    <?php
                                    foreach($municipios as $mun){
                                        echo "<option value=\"".$mun['id']."\">".$mun['nombre_municipio']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-3 text-right">
                                <button id="btnFiltrarMapaTodos" type="reset" class="btn btn-default">Todos</button>
                                <button id="btnFiltrarMapa" type="button" class="btn btn-success">Buscar</button>
                            </div>



                        </div>






                    </div>

                </div>
            </div>
        </form>
    </div>



    <!---Marcadores -->

    <div class="markerImage">
        <svg width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
            <g>
                <path fill="#FFFFFF" d="M19.101,15.195l-0.135-0.078l-0.028,0.029c0.495-0.696,0.789-1.544,0.789-2.461
		c0-2.35-1.912-4.262-4.261-4.262s-4.261,1.912-4.261,4.261c0,0.92,0.296,1.77,0.794,2.467l-0.033-0.034l-0.135,0.078
		c-1.897,1.095-3.143,3.011-3.509,5.396L8.309,20.68l0.057,0.069c1.894,2.272,4.416,3.524,7.101,3.524
		c2.685,0,5.207-1.252,7.101-3.524l0.057-0.069l-0.014-0.089C22.245,18.207,20.998,16.29,19.101,15.195z"/>
                <path fill="#34C08E" d="M16,0C7.163,0,0,7.163,0,16c0,8.837,7.164,16,16,16c8.837,0,16-7.163,16-16C32,7.163,24.837,0,16,0z
		 M22.567,20.749c-1.893,2.272-4.416,3.524-7.101,3.524c-2.685,0-5.207-1.251-7.101-3.524L8.309,20.68l0.013-0.089
		c0.366-2.385,1.612-4.301,3.509-5.396l0.135-0.078l0.033,0.034c-0.498-0.697-0.794-1.548-0.794-2.467
		c0-2.349,1.912-4.261,4.261-4.261s4.261,1.912,4.261,4.262c0,0.917-0.294,1.766-0.789,2.461l0.028-0.029l0.135,0.078
		c1.897,1.095,3.143,3.011,3.509,5.396l0.014,0.089L22.567,20.749z"/>
            </g>
        </svg>
    </div>

    <div class="markerImage2">
        <svg width="34" height="26" viewBox="0 0 34.911 26" xmlns="http://www.w3.org/2000/svg">
            <g opacity="0.25">

                <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="15.6815" y1="47.3321" x2="27.0614" y2="38.7109" gradientTransform="matrix(0.9691 -0.2468 0.2468 0.9691 -7.7202 -18.2274)">
                    <stop  offset="0" style="stop-color:#000000"/>
                    <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
                </linearGradient>
                <path fill="url(#SVGID_2_)" d="M17.392,26l11.438-4.727c2.055-0.846,3.716-2.018,4.809-3.391c2.656-3.336,1.018-6.792-3.651-7.704
		c-4.669-0.912-10.628,1.06-13.284,4.396c-1.093,1.372-1.493,2.79-1.158,4.1L17.392,26z M26.607,14.425
		c1.392,0.272,1.88,1.302,1.088,2.297s-2.568,1.582-3.96,1.31c-1.392-0.272-1.88-1.302-1.088-2.297S25.215,14.153,26.607,14.425z"/>
            </g>
            <g opacity="0">

                <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-171.8311" y1="-0.4306" x2="-160.4512" y2="-9.0517" gradientTransform="matrix(-0.9691 -0.2468 -0.2468 0.9691 -150.8687 -18.2274)">
                    <stop  offset="0" style="stop-color:#000000"/>
                    <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
                </linearGradient>
                <path fill="url(#SVGID_3_)" d="M19.366,18.675c0.335-1.31-0.066-2.728-1.158-4.1c-2.656-3.336-8.615-5.308-13.284-4.396
		c-4.669,0.912-6.307,4.368-3.651,7.704c1.093,1.372,2.754,2.544,4.809,3.391L17.519,26L19.366,18.675z M12.264,15.735
		c0.792,0.994,0.304,2.025-1.088,2.297s-3.168-0.316-3.96-1.31s-0.304-2.025,1.088-2.297C9.696,14.153,11.472,14.741,12.264,15.735z
		"/>
            </g>
            <g>
                <g>
                    <path fill="{{colorPin}}" d="M17.448,25.988l7.795-9.896c1.403-1.774,2.142-3.9,2.142-6.155c0-5.48-4.458-9.937-9.937-9.937
			S7.511,4.458,7.511,9.937c0,2.254,0.74,4.381,2.139,6.15L17.448,25.988z M17.448,6.975c1.634,0,2.962,1.329,2.962,2.962
			S19.081,12.9,17.448,12.9s-2.962-1.329-2.962-2.962C14.486,8.304,15.814,6.975,17.448,6.975z"/>
                </g>
            </g>
        </svg>
    </div>



</section>

<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<link href="assets/css/skins/all.css?v=1.0.2" rel="stylesheet">
<script src="assets/js/icheck.min.js?v=1.0.2"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAB4Mdh2n0AKYk-uRPpbXP93VuboQTgy8s"></script>
<script type="text/javascript" src="assets/js/indexGoogleMap.js"></script>








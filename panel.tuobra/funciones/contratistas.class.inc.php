<?php

class contratista{
	
	var $db;//conexcion a la base de datos
	private $id_contratista;//id del contratista
	private $nombre_contratista;//nombre o razon social del contratista
	private $rfc_contratista;//rfc del contratista
	private $direccion_contratista;//direccion del contratista
	private $telefono_contratista;//telefono del contratista
	private $email_contratista;//email del contratista
	
	/*constructor*/
	function __construct($conn, $id){
		$this->db = $conn;//conexion a la base de datos
		$this->id_contratista = $id;//id del contratista
		/*comprobar que no venga vacio el id del contratista*/
		if(!empty($this->id_contratista)){
			/*si biene el id del contratista entonces creamos el contratiosta*/
			$this->datosContratista();
			}
		}
		
	/*crear contratista*/
	public function crearContratista($id_cliente, $nombre_, $rfc_, $direccion_, $telefono_, $email_){
		/*query*/
		$sql_query = "INSERT INTO `contratistas` (ID_CLIENTE, RAZON_SOCIAL, RFC, DOMICILIO_FISCAL, TELEFONO, EMAIL) VALUES (?, ?, ?, ?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('isssss',$id_cliente, $nombre_, $rfc_, $direccion_,$telefono_,$email_);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*actulizaar el contratista*/
	public function updateContratista($nombre_, $rfc_, $direccion_, $telefono_, $email_){
		/*query*/
		$sql_query = "UPDATE `contratistas` SET RAZON_SOCIAL = ?, RFC = ?, DOMICILIO_FISCAL = ?, TELEFONO = ?, EMAIL = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('sssssi',$nombre_, $rfc_, $direccion_, $telefono_, $email_, $this->id_contratista);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*datos del contratista*/
	private function datosContratista(){
		if(!empty($this->id_contratista) and strlen($this->id_contratista) > 0){
			/*query*/
			$sql_query = "SELECT RAZON_SOCIAL, RFC, DOMICILIO_FISCAL, TELEFONO, EMAIL FROM `contratistas` WHERE ID = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_contratista);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($nombre, $rfc, $direccion, $telefono, $email);
			/*fetch datos*/
			$query->fetch();
			/*datos de la unidad*/
			$this->nombre_contratista = $nombre;
			$this->rfc_contratista = $rfc;
			$this->direccion_contratista = $direccion;
			$this->telefono_contratista = $telefono;
			$this->email_contratista = $email;
			
			/*cerrar conexiones*/
			$query->close();
			$this->db->close();
			}
		}
	/*get nombre del contratista*/
	public function getNombreContratista(){
		return $this->nombre_contratista;
		}
	/*get rfc del contratista*/
	public function getRfcContratista(){
		return $this->rfc_contratista;
		}
	/*get direccion del contratista*/
	public function getDireccionContratista(){
		return $this->direccion_contratista;
		}
	/*get telefono del contratista*/
	public function getTelefonoContratista(){
		return $this->telefono_contratista;
		}
	/*get email del contratista*/
	public function getEmailContratista(){
		return $this->email_contratista;
		}
	}
?>
<?php
$array_tipo_licitacion = array(1 => mb_strtoupper('Licitación pública', "UTF-8"), 2 => mb_strtoupper('Licitación Simplificada/Invitación Restringida', "UTF-8"));
$array_normatividad = array(1 => mb_strtoupper('Estatal', "UTF-8"), 2 => mb_strtoupper('Federal', "UTF-8"));
$array_eventos = array(1 => mb_strtoupper('Visita a la Obra', "UTF-8"), 2 => mb_strtoupper('Junta de Aclaraciones', "UTF-8"), 3 => mb_strtoupper( 'Apertura de Propuestas', "UTF-8"), 4 => mb_strtoupper('Fallo', "UTF-8"));
$array_modalidad = array(1 => mb_strtoupper('Licitación Pública', "UTF-8"), 2 => mb_strtoupper('Licitación Simplificada/Invitación Restringida', "UTF-8"), 3 => mb_strtoupper('Adjudicación Directa basada en normatividad', "UTF-8"), 4 => mb_strtoupper('Administración Directa', "UTF-8"));
$array_tipo_documento_img = array('doc' => 'word_big.png', 'pdf' => 'pdf_big.png', 'rar' => 'zip_big.png', 'zip' =>'zip_big.png');
$array_tipoContratos = array(1=>'Obra',2=>'Equipamiento',3=>'Servicios');
//$array_tipoContratos = array(1=>'Obra',2=>'Equipamiento',3=>'Servicios');

?>

<?php 
class login{
	
	var $db;//conexcion a la base de datos
	private $usuario;//nombre del usuario
	private $email;//email del usuario (que tambien va a ser el usuario de login)
	private $id_usuario;//id asignado al usuario
	private $id_cliente;//id del cliente
	private $nombre_usuario;//nombre del usuario real nombre
	private $tipo;//tipo del usuario real nombre
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		$this->db = $conn;//conexion a la base de datos
		}
		
	/*metodo de login*/
	public function loging($usuario, $password){
		/*query*/
		$sql_query = "SELECT U.ID, U.ID_CLIENTE, U.USUARIO, U.NOMBRE, U.PASSWORD, U.SALT, U.EMAIL, U.TIPO FROM `usuarios` U WHERE U.EMAIL = ?;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('s', $usuario);
		/*ejecutar query*/
		$query->execute();
		/*guardar los datos*/
		$query->store_result();
		/*bind los resultados*/
		$query->bind_result($id_usuario, $id_cliente, $name_usuario, $nombre_usuario, $pass, $salt, $email, $tipo);
		/*fetch datos*/
		$query->fetch();
			/*comprobar que haya arrojado resultados*/
			if($query->num_rows == 1){
				/*comprobar fuerza bruta*/
				if($this->checkFuerzaBruta($id_usuario) == false){
					/*hash el password*/
					$password = hash('sha512', $password.$salt);
					/*comprobar que el password de la db coicida con el introducido*/
					if($password == $pass){
						/*varibles del usuario*/
						$this->id_usuario = $id_usuario;
						$this->id_cliente = $id_cliente;
						$this->nombre_usuario = $nombre_usuario;
						$this->email = $email;
						$this->tipo = $tipo;
						
						/*regresar true*/
						return true;
						}else{
							/*guardar los datos pata contrarestar la fuerza bruta*/
							$now = time();//set el timpo (hora y fecha) timestamp
							/*insertar los datos*/
							$this->db->query("INSERT INTO `secure_login` (ID_USUARIO, TIME) VALUES ($id_usuario, $now);");
							/*regresar false*/
							return false;
							}
					}else{
						return false;
						}
				}else{
					return false;
					}
		/*cerrar el query*/
		$query->close();
		}
	
	/*comprobar fuerza bruta*/
	private function checkFuerzaBruta($id_usuario){
		/*obtener el tiempo timestamp*/
		$time = time();
		/*timpo valido 2 hrs*/
		$intentos_validos = $time - (2 * 60 * 60);
		/*sql query*/
		$sql_query = "SELECT TIME FROM `secure_login` WHERE ID_USUARIO = ? AND TIME > ?;";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('ii', $id_usuario, $intentos_validos);
		/*ejecutar el query*/
		$query->execute();
		/*guardar los resultados*/
		$query->store_result();
			/*comprobar los intentos realizados las pasadas dos horas*/
			if($query->num_rows > 5){
				return true;
				}else{
					return false;
					}
		}
	/*get el id del usuario*/
	public function getIdUsuario(){
		return $this->id_usuario;
		}
	/*get el id del cliente*/
	public function getIdCliente(){
		return $this->id_cliente;
		}
	/*get el nombre del usuario*/
	public function getNombreUsuario(){
		return $this->nombre_usuario;
		}
	/*get email del usuario*/
	public function getEmail(){
		return $this->email;
		}
	public function getTipo(){
		return $this->tipo;
		}
	}
?>
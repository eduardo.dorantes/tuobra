<?php
//require('../librerias/query.class.inc.php');
/*crear nuevo objeto query*/
$query = new querys();

/*query string*/
$query_string = "SELECT `ID`, `NOMBRE`, `NOMBRE_CORTO`, `CUSTOMURLMD5` FROM `clientes`";

/*ejecutar query y comprobar*/
if($rows = $query->traerMultiplesResultados($query_string, NULL)){
	/*ini set contador*/
	$cont = 0;
	/*si existen resultados entonces loop para extraerlos*/
	foreach($rows as $raw){
?>
			<!-- eventos por dependencia -->
            <div id="contenedor-<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" data-id="<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" class="contenedor-transmisiones-eventos">
            	<!-- titulo de la dependencia -->
                <div class="contenedor-transmisiones-titulo-dependencia">
                	<h1 class="<?php if($cont == 0){ echo 'tu-obra-minus-circled'; }else{ echo 'tu-obra-plus-circled'; } ?>"><?php echo $raw['NOMBRE']; ?>
                    	<span class="contenedor-icono-live-event">
                        	<span class="tu-obra-play"></span>
                        </span>
                    </h1>
                </div>
                <!-- contenedor inside acoordion -->
                <div class="contenedor-transmision-inside-acoordion <?php if($cont == 0){ echo 'show-acoordion'; } ?>" id="acoordion-sidur">
                  <!-- proximos eventos -->
                  <div class="contenedor-transmisiones-proximos-eventos">
                      <h2>Próximos Eventos</h2>
                      <?php include('../include/proximos-eventos-transimision.php'); ?>
                  </div>
                  <!-- embed video live -->
                  <div class="contenedor-transmisiones-video">
                  	<h3 id="titulo-video_<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" class="titulo-video"></h3>
                    <iframe style="width:100%;" height="400" id="video_cliente_<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" class="video_cliente" name="" src="https://www.youtube.com/embed/4ie1baU_RcM" frameborder="0" allowfullscreen></iframe>
                      <!-- contenedor redes sociales -->
                      <div class="contenedor-transmision-redes-sociales">
                      	<!-- <ul>
                        	<li><a href="#" class="tu-obra-twitter-circled"></a></li>
                            <li><a href="#" class="tu-obra-facebook-circled"></a></li>
                        </ul> -->
                      </div>
                  </div>
                <!-- fin contenedor acoordion -->
                </div>
            </div>

<?php
	/*comntador ++*/
	$cont++;
	}
}
?>
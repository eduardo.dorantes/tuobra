<?php

class supervisionExterna{
	
	var $db;//conexion a la base de datos
	private $id_supervision_externa;//id de ls supervision externa
	private $nombre_supervision_externa;//nombre de la sueprvsion externa
	private $datos_supervisoon_externa;//datos de la supervsion externa
	
	/*constructor*/
	function __construct($conn, $id){
		$this->db = $conn;//conexion a la base de datos
		$this->id_supervision_externa = $id;//id de la supervicion externa
		/*comprobar que venga el id de la supervision externa*/
		if(!empty($this->id_supervision_externa)){
			/*si biene el id de la supervision entonces crear al supervisor*/
			$this->datosSupervisionExterna();
			}
		}
		
	/*crear supervsion externa*/
	public function crearSupervisionExterna($id_cliente, $nombre, $datos){
		/*query*/
		$sql_query = "INSERT INTO `supervision_externa` (ID_CLIENTE, NOMBRE, DATOS) VALUES (?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('iss',$id_cliente, $nombre, $datos);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*actulizar supervsion externa*/
	public function updateSupervisionExterna($nombre, $datos){
		/*query*/
		$sql_query = "UPDATE `supervision_externa` SET NOMBRE = ?, DATOS = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('ssi', $nombre, $datos, $this->id_supervision_externa);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*datos de la supervision externa*/
	private function datosSupervisionExterna(){
		if(!empty($this->id_supervision_externa) and strlen($this->id_supervision_externa) > 0){
			/*query*/
			$sql_query = "SELECT NOMBRE, DATOS FROM `supervision_externa` WHERE ID = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_supervision_externa);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($nombre_, $datos_);
			/*fetch datos*/
			$query->fetch();
			/*datos de la unidad*/
			$this->nombre_supervision_externa = $nombre_;
			$this->datos_supervisoon_externa = $datos_;
			
			/*cerrar conexiones*/
			$query->close();
			$this->db->close();
			}
		}
	/*traer el nombre de la supervision externa*/
	public function getNameSupervision(){
		return $this->nombre_supervision_externa;
		}
	/*traer los datos de la supervision externa*/
	public function getDatosSupervision(){
		return $this->datos_supervisoon_externa;
		}
	}
?>
<?php
session_start();
if(empty($_SESSION['id-usuario']) and !isset($_SESSION['login']) and $_SESSION['login'] != true){
	header("LOCATION: home/");
	}
?>
<div class="contenedor-opciones-parametros">
	<!-- opciones de parametros -->
    <ul>
    	<li><a href="javascript:;" id="parametros-generales" data-id="generales" class="current-tab tab-parametros">Generales</a></li>
        <?php
			if($_SESSION['tipo']==1){
		?>

		<li><a href="javascript:;" id="tipos-obras" data-id="tipos-obra" class="tab-parametros">Tipos Obra</a></li>
			<?php } ?>
		<li><a href="javascript:;" id="parametros-configuracion" data-id="config" class="tab-parametros">Configuración</a></li>
    </ul>
</div>
<div id="contenido-tabs"><?php include('../forms/add-parametros-generales.php'); ?></div>
<script type="text/javascript">
var op_tab;
$(".tab-parametros").click(function(){
	if(op_tab !== $(this).attr('data-id') || op_tab !== ''){
		var url;
		$(".tab-parametros").removeClass('current-tab');
		$(this).addClass('current-tab');
		switch($(this).attr('data-id')){
			case 'generales':
				url = 'add-parametros-generales';
			break;
			case 'config':
				url = 'add-parametros-configuracion';
			break;
			case 'tipos-obra':
				url = 'add-tipo-obra';
			break;
			}
		$.post('forms/' + url + '.php', function(contenido){
			$("#contenido-tabs").html(contenido);
			});
	}
	op_tab = $(this).attr('data-id');
	});
</script>
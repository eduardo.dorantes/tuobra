<?php
session_start();
include("../config/conn.php");
require("../funciones/parametros.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}
/*crear objeto parametros*/
$parametro = new parametros($conn, $_SESSION['id-cliente']);
?>
<div id="resultado"></div>
<div class="parametros-config-contenedor">
	<h2 style="margin-bottom:15px;">Unidades administrativas</h2>
    <form name="agregar-parametros" id="agregar-parametros" method="post">
    <table cellpadding="5" cellspacing="0" border="0" id="table-form-add-unidad-administrativa">
        <tr>
            <td><label for="tipo-unidad">Tipo</label></td>
            
        </tr>
        <tr>
            <td><select name="tipo-unidad" required id="tipo-unidad"><option value="">--Seleccione--</option><option value="1">Unidad administrativa solicitante</option><option value="2">Unidad administrativa responsable</option></select></td>
          
        </tr>


        <tr>

            <td><label for="nombre-unidad">Nombre de la Unidad Administrativa</label></td>
            
        </tr>
        <tr>

          <td><input name="nombre-unidad" type="text" required="required" id="nombre-unidad" size="45"></td>
            
        </tr>



        <tr>
        	<td colspan="3"><input type="hidden" name="id-unidad" id="id-unidad" value=""><input type="submit" name="enviar-formulario" id="enviar-formulario" class="btn-green" value="Guardar"> <input type="reset" name="cancelar" id="cancelar" value="Cancelar" class="btn"></td>
        </tr>
    </table>
    </form>
    <!-- listado de unidaes -->
    <div id="lista-unidades-administrativas">
    </div>
</div>
<div class="parametros-config-contenedor parametros-config-contenedor-separador">
	<h2 style="margin-bottom:15px;">Configuración general</h2>
    <form name="generales-parametros-form" id="generales-parametros-form" method="post">
        <table cellpadding="5" cellspacing="0" border="0">
            <tr>
                <td><label for="parametro-iva">IVA %: </label></td>
                <td><input type="number" name="parametro-iva" id="parametro-iva" size="5" value="<?php echo $parametro->getIva(); ?>" placeholder="%"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td><input type="submit" name="enviar-formulario-generales" id="enviar-formulario-generales" class="btn-green" value="Guardar"> <input type="reset" name="cancelar-generales" id="cancelar-generales" class="btn" value="Cancelar"></td>
                <td></td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
    listaUnidades();
});
$('#agregar-parametros').submit(function(){
	$.post('sources/insert/crear-unidad-administrativa.php', $(this).serialize(), function(resultado){
		$('#resultado').html(resultado);
		}).done(function(){
			$('#agregar-parametros')[0].reset();
			listaUnidades();
			});
	return false;
	});
$('#generales-parametros-form').submit(function(){
	$.post('sources/insert/crear-parametros-generales.php', $(this).serialize(), function(resultado){
		$('#resultado').html(resultado);
		});
	return false;
	});
$(document).on('click','.editar-unidad', function(){
	var unidad = $(this).attr('data-id');
	$.post('sources/datos-unidad-json.php',{unidad: unidad},function(res){
		$('#id-unidad').val(unidad);
		$('#nombre-unidad').val(res.name);
		$('#tipo-unidad').val(res.tipo);
		}, 'json').done(function(){
			$('#id-unidad').val();
			listaUnidades();
			});
	});
function listaUnidades(){
	$.post('sources/listas/lista-unidad-administrativas.php',{va_r: 'dato'}, function(contenido){
		$('#lista-unidades-administrativas').html(contenido);
		});
	}
</script>
<?php include('../include/header.php'); ?>

<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Contacto</h2>
                <p>Inicio | Formulario de Contacto</p>
            </div>
        </div>
    </div>

</section>

<section id="contentForm">
    <div class="container">
        <div class="row">
            <div id="contenedor-clientes-eventos" class="col-sm-12">
                <iframe src="https://docs.google.com/forms/d/1f8I5HbbSyNnsHjXNZkHt5qxAHyLlo4JzFBIcjCwbaeA/viewform?embedded=true" width="100%" height="1300" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
            </div>
        </div>
    </div>
</section>

<?php include('../include/footer.php'); ?>


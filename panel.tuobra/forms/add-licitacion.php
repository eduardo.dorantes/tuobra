<?php
session_start();
require('../funciones/query.class.inc.php');

if($_SESSION['login'] != true){
	exit;
	}
/**/
if(!empty($_POST['selected-item'][0])){
	/*recibir el id de la licitacion*/
	$id_licitacion = $_POST['selected-item'][0];
	/*crear objeto query*/
	$query = new querys();
	/*ejecutar query*/
	$row = $query->traerSoloResultado('SELECT `num_licitacion`, `tipo`, `normatividad`, `descripcion`, `fecha_publiacion`, `url_compranet`, `foto_proyecto`, desierta FROM `licitaciones` WHERE `id` = :id', $array_bind = array(':id' => $id_licitacion));
	}
?>
<div id="resultado"></div>
<form name="add-nueva-licitacion" id="add-nueva-licitacion" method="post">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-form-add-obras" class="table-forms">
    	<tbody valign="top">
        	<tr>
            	<td><label>Numero licitación</label></td>
                <td><label>Tipo</label></td>
                <td><label>Normatividad</label></td>
            </tr>
            <tr>
            	<td><input type="text" name="num_licitacion" id="num_licitacion" required value="<?php echo !empty($id_licitacion) ? $row['num_licitacion'] : ''; ?>"></td>
                <td><select name="tipo" id="tipo" required><option value="">--Seleccione--</option><option value="1">Licitación Pública</option><option value="2">Licitación Simplificada/Invitación Restringida</option></select></td>
                <td><select name="normatividad" id="normatividad" required><option value="">--Seleccione--</option><option value="1">Estatal</option><option value="2">Federal</option></select></td>
            </tr>
            <tr>
            	<td><label>Descripción/Objeto</label></td>
                <td><label>Fecha publicación</label></td>
                <td><label>URL CompraNet</label></td>
            </tr>
            <tr>
            	<td><textarea name="descripcion" id="descripcion" rows="3" cols="45" required><?php echo !empty($id_licitacion) ? $row['descripcion'] : ''; ?></textarea></td>
                <td><input type="text" name="fecha-publicacion" id="fecha-publicacion" class="campo-fecha" required readonly size="10" value="<?php echo !empty($id_licitacion) ? $row['fecha_publiacion'] : ''; ?>"></td>
                <td><input type="url" name="url-compranet" id="url-compranet" value="<?php echo !empty($id_licitacion) ? $row['url_compranet'] : ''; ?>"></td>
            </tr>
            <tr>
            	<td><label>Imagen/Foto proyecto</label></td>
                <td><label>Licitación Desierta</label></td>
                <td></td>
            </tr>
            <tr>

				<?php

					function isChecked($c){
						$cc= '';
						if($c){
							$cc = "checked='checked'";
						}
						return $cc;
					}

				?>

            	<td><input type="file" name="archivo" id="archivo" accept="image/jpeg"><label><?php echo !empty($id_licitacion) ? "<img src=\"".$row['foto_proyecto']."\" width=\"50\" height=\"50\">" : ''; ?></label></td>
                <td><input type="checkbox" name="desierta" <?php echo !empty($id_licitacion) ? isChecked($row['desierta']) : ''; ?> id="desierta" value="1"> </td>
                <td></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn-green">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                </td>
                <td><input type="hidden" name="id-licitacion" id="id-licitacion" value="<?php echo $_POST['selected-item'][0]; ?>"></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$(document).ready(function(e) {
	<?php
	if(!empty($id_licitacion)){
		echo "$('#tipo').val(".$row['tipo'].");";
		echo "$('#normatividad').val(".$row['normatividad'].");";
		}
	?>
	});
$('#add-nueva-licitacion').submit(function(){
	var formData = new FormData($('#add-nueva-licitacion')[0]);
	$.ajax({
		beforeSend: function(){
			$("#resultado").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url: 'sources/insert/crear-licitacion.php',
		data: formData,
		dataType:"json",
		async: false,
		contentType: false,
		processData: false,
		cache: false,
		type:"POST",
		success: function(res){
			if(res.status == true){
				$("#resultado").html('<div class="alert alert-success">Los datos se han guardado correctamente.</div>');
				$("#add-nueva-licitacion")[0].reset();
				setTimeout(function(){
					window.history.back();
					}, 2000);
				}
			},
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			}
		});
	return false;
	});
$(function() {
    $(".campo-fecha").datepicker({dayNames: ['Domingo','Lunes','Martes','Miercoles','Juevez','Viernes','Sabado'], dayNamesMin: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],nextText:'Siguiente',prevText: 'Atras', dateFormat: 'dd/mm/yy', autoSize: true, changeYear: true, monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']});
});
</script>
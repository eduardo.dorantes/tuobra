<?php
header('Content-Type: application/json');
require('../librerias/query.class.inc.php');

/*nuevo objeto query*/
$query = new querys();
/*set array*/
$json_array  = array();
/*ejecutar y comprobar query*/
if($rows = $query->traerMultiplesResultados('SELECT `ID`, `NUM_CONTRATO`, `OBJETO`, `LATITUD`, `LONGITUD`, `TIPO_OBRA`, ID_CLIENTE AS ORGANISMO, TIPO_MODALIDAD AS MODALIDAD, MUNICIPIO, YEAR(str_to_date(FECHA_CONTRATO, \'%d/%m/%Y\')) as ANIO FROM `contratos` ', NULL)){
	/*loop*/
	foreach($rows as $row){
		$json_array[$row['ID']]['titulo'] = $row['OBJETO'];
		$json_array[$row['ID']]['num'] = $row['NUM_CONTRATO'];
		$json_array[$row['ID']]['latitud'] = $row['LATITUD'];
		$json_array[$row['ID']]['longitud'] = $row['LONGITUD'];
		$json_array[$row['ID']]['tipo'] = $row['TIPO_OBRA'];
		$json_array[$row['ID']]['modalidad'] = $row['MODALIDAD'];
		$json_array[$row['ID']]['organismo'] = $row['ORGANISMO'];
		$json_array[$row['ID']]['municipio'] = $row['MUNICIPIO'];
        $json_array[$row['ID']]['anio'] = $row['ANIO'];
		}
	}

echo json_encode($json_array, JSON_PRETTY_PRINT);
?>
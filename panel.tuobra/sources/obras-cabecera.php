<?php
session_start();
require("../config/conn.php");
require("../funciones/clientes.class.inc.php");
if (empty($_SESSION['id-usuario']) or !isset($_SESSION['login']) or $_SESSION['login'] != true) {
    session_destroy();
    echo "<script>window.close();</script>";
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Obras cabecera</title>
    <link type="text/css" rel="stylesheet" href="../css/popup.css">
    <link type="text/css" rel="stylesheet" href="../css/newestilo.css">
    <script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

</head>

<body>
<div class="header">
    <h3>Obras Cabecera</h3>
</div>
<div class="contenedor">

    <p>Busque y seleccione la obra cabecera a la que pertenece la obra que está capturando.</p>
    <div id="contenedor-obras">
        <!--    	<div id="buscador-obras"><span style="float:right;"><input type="text" name="buscar-obra" id="buscar-obra" placeholder="Buscar obra..." size="45"></span></div>-->
        <div id="contenedor-resultados">
            <?php
            /*QUERY*/
            $sql_query = "SELECT C.ID, C.NUM_CONTRATO, C.OBJETO, C.TOTAL_CONTRATO FROM `contratos` C WHERE C.ID_CLIENTE = ? and C.ID != ? AND (C.OBRA_COMPLEMENTARIA = 0);";
            /*PREPARAR EL QUERY*/
            $query = $conn->prepare($sql_query);
            /*comprobar el query*/
            if ($query === false) {
                trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $query->error, E_USER_ERROR);
            }
            /*bind parametros*/


            if (is_int(intval($_GET['c'])) && intval($_GET['c']) > 0) {
                $nocontrato = intval($_GET['c']);
            } else {
                $nocontrato = 0;
            }

            $query->bind_param('ii', $_SESSION['id-cliente'], $nocontrato);
            /*executar query*/
            $query->execute();
            /*store resultados*/
            $query->store_result();
            /*resultados*/
            $query->bind_result($id, $num_contrato, $objeto, $monto);

            $i = 0;
            echo "<table id=\"tabla-obras-cabecera\" class='zebra'>";
            echo "<thead><tr>";
            echo "<th></th>";
            echo "<th></th>";
            echo "<th>Numero de contrato</th>";
            echo "<th>Objeto del contrato</th>";
            echo "<th>Monto del contrato</th>";
            echo "</tr></thead><tbody>";
            while ($query->fetch()) {
                $i++;
                echo "<tr>";
                echo "<td>" . $i . "</td>";
                echo "<td><input type=\"checkbox\" name=\"obra-a-seleccionar\" id=\"obra-a-seleccionar\" value=\"" . $id . "\"></td>";
                echo "<td>" . $num_contrato . "</td>";
                echo "<td>" . $objeto . "</td>";
                echo "<td>" . number_format($monto, 2) . "</td>";
                echo "</tr>";
            }
            echo "</tbody></table>";
            /*cerrar el query*/
            $query->close();
            /*cerrar la conexcion*/
            $conn->close();
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on("click", "input[type=checkbox]:checked", function () {
        var id_contrato = $(this).val();
        var num_contrato = $(this).parent().parent().find("td:eq(2)").text();
        /*comprobar que este checked*/
        if ($(this).is(":checked")) {
            var pregunta = confirm("¿Seguro que desea seleccionar esta obra?");
            /*comprobar pregunta*/
            if (pregunta) {
                $("#obra-cabecera", window.opener.document).append("<option value='" + id_contrato + "'>" + num_contrato + "</option>").val(id_contrato);
                window.close();
            } else {
                $("input[type=checkbox]").prop('checked', false);
            }
        }
    });


    $(document).ready(function () {
        $('.zebra').DataTable({
            "lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "Todos"]],
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json"
            }
        } );
    });
</script>
</body>
</html>
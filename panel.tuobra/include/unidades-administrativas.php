<?php
/***********************************/
/* 	UNIDADES ADMINISTRATIVAS  */
session_start();
require("../config/conn.php");

if($_SESSION['login'] != true){
	exit;
	}
?>
<div class="acciones-ventana">
	<a href="../forms/add-unidad-administrativa.php" class="btn icon-agregar">Agregar</a>
    <a href="#" class="btn icon-delete-uno">Eliminar</a>
</div>
<div class="lista-ventana">
<?php
	/*query*/
 	$sql_query = "SELECT UA.ID, UA.NOMBRE, UA.NOMBRE_RESPONSABLE, UA.PUESTO_RESPONSABLE FROM `unidades_administrativas` UA WHERE UA.ID_CLIENTE = ?;";
	/*preparar el query*/
	$query = $conn->prepare($sql_query);
	/*comprobar el query*/
	if($query === false){
		trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $query->error, E_USER_ERROR);
		}
	/*bind parametros*/
	$query->bind_param('i', $_SESSION['id-cliente']);
	/*executar query*/
	$query->execute();
	/*store resultados*/
	$query->store_result();
	/*resultados*/
	$query->bind_result($id, $nombre, $nombre_responsable, $puesto);
	/*comprobar que aya resultados*/
		if($query->num_rows > 0){
			/*empezar la tabla*/
			echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\">";
			echo "<thead>";
			echo "<tr>";
			echo "<th>&nbsp;</th>";
			echo "<th>Unidad administrativa</th>";
			echo "<th>Nombre responsable</th>";
			echo "<th>Puesto responsable</th>";
			echo "<th>&nbsp;</th>";
			echo "</tr>";
			echo "</thead><tbody>";
			/*extraer los resultados*/
			while($query->fetch()){
				echo "<tr>";
				echo "<td><input type=\"checkbox\" name=\"selected-item\" id=\"check_".$id."\" value=\"".$id."\"></td>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$nombre_responsable."</td>";
				echo "<td>".$puesto."</td>";
				echo "<td></td>";
				echo "</tr>";
				}
			/*cerrar la tabla*/
			echo "</tbody></table>";
			}
	/*cerrar el query*/
	$query->close();
	/*cerrar la conexcion*/
	$conn->close();
?>
</div>
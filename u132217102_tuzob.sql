-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 15, 2018 at 08:11 PM
-- Server version: 10.2.17-MariaDB
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u132217102_tuzob`
--

-- --------------------------------------------------------

--
-- Table structure for table `avances`
--

CREATE TABLE `avances` (
  `ID` int(11) NOT NULL,
  `ID_CONTRATO` int(11) NOT NULL COMMENT 'ID DEL CONTRATO',
  `ID_USUARIO` int(11) NOT NULL COMMENT 'ID DEL USUARIO QUE DIO DE ALTA LA INFORMACION',
  `FECHA` varchar(25) NOT NULL COMMENT 'FECHA DEL AVANCE',
  `AVANCE` double NOT NULL COMMENT 'PORCENTAJE DE AVANCE A LA FECHA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `avance_fotografico`
--

CREATE TABLE `avance_fotografico` (
  `ID` int(11) NOT NULL,
  `ID_CONTRATO` int(11) NOT NULL COMMENT 'ID DEL CONTRATO',
  `ID_FRENTE` int(11) NOT NULL COMMENT 'ID DEL FRENTE DE LA OBRA A LA QUE PERTENCE LA IMAGEN',
  `ID_USUARIO` int(11) NOT NULL COMMENT 'ID DEL USUARIO QUE DIO DE ALTA LA INFORMACION',
  `FECHA` varchar(12) NOT NULL COMMENT 'FECHA DE LA FOTO',
  `PIE_FOTO` mediumtext NOT NULL COMMENT 'PIE DE LA FOTO',
  `IMAGEN` varchar(255) NOT NULL COMMENT 'RUTA AL ARCHIVO DE IMAGEN',
  `THUMB` varchar(255) NOT NULL COMMENT 'PATH HACIA EL THUMB DE LA IMAGNE',
  `THUMB_LARGE` varchar(255) NOT NULL COMMENT 'PATH HACIA EL THUMB NAIL DE LA IMAGEN LARGE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `avance_fotografico_frentes`
--

CREATE TABLE `avance_fotografico_frentes` (
  `ID` int(11) NOT NULL COMMENT 'ID DEL FRENTE FOTOGRAFICO',
  `ID_CONTRATO` int(11) NOT NULL COMMENT 'ID DEL CONTRATO AL QUE SE LE ESTA ABRIENDO FRENTE',
  `DESCRIPCION` varchar(255) NOT NULL COMMENT 'NOMBRE O DESCRIPCIUON DEL FRENTE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `bitacora_obra`
--

CREATE TABLE `bitacora_obra` (
  `id` int(11) NOT NULL,
  `id_obra` int(11) NOT NULL,
  `fecha` varchar(25) NOT NULL,
  `descripcion` longtext NOT NULL,
  `id_creador` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `ID` int(11) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL COMMENT 'NOMBRE DE LA DEPENDENCIA',
  `NOMBRE_CORTO` varchar(50) DEFAULT NULL,
  `DIRECCCION` varchar(255) NOT NULL COMMENT 'DIRECCION DE LA DEPENDENCIA',
  `TELEFONO` varchar(255) NOT NULL COMMENT 'TELEFONO O CELULAR O ALGUN TIPO DE CONTACTO DE LA DEPENDENCIA',
  `MUNICIPIO` int(11) NOT NULL COMMENT 'MUNICIPIO DONDE SE ENCUENTRA LA DEPENDENCIA',
  `RESPONSABLE` varchar(255) NOT NULL COMMENT 'NOMBRE DEL RESPONSABLE DE LA APLICACION',
  `EMAIL` varchar(255) NOT NULL COMMENT 'CORREO ELECTRONICO DEL RESPONSABLE',
  `CUSTOMURL` varchar(255) NOT NULL COMMENT 'NOMBRE DEL CLIENTE QUE APARECERA EN LA URL DOMINIO.COM/CLIENTE',
  `CUSTOMURLMD5` varchar(255) NOT NULL COMMENT 'CUSTOM URL CON CODIFICACION MD5',
  `MENSAJE` mediumtext NOT NULL COMMENT 'MENSAJE PERSONALISADO PARA CADA UNO DE LOS CLIENTES',
  `LOGO` varchar(255) NOT NULL COMMENT 'PATH HACIA EL LOGO DEL CLIENTE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `contratistas`
--

CREATE TABLE `contratistas` (
  `ID` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL COMMENT 'ID DEL CLIENTE INSCRITO EN APLICACION',
  `RAZON_SOCIAL` varchar(255) NOT NULL COMMENT 'NOMBRE O RAZON SOCIAL DEL CONTRATISTA',
  `RFC` varchar(13) NOT NULL COMMENT 'REGISTRO FEDERAL DE CONTRATIBULLENTES DEL CONTRATISTA',
  `DOMICILIO_FISCAL` varchar(255) NOT NULL COMMENT 'DOMICILIO FISCAL DEL CONTRATISTA',
  `TELEFONO` varchar(255) NOT NULL COMMENT 'TELEFONO O CONTACTO DEL CONTRATISTA',
  `EMAIL` varchar(50) NOT NULL COMMENT 'CORREO ELECTRONICO DEL CONTRATISTA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `contratos`
--

CREATE TABLE `contratos` (
  `ID` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL COMMENT 'ID DEL CLIENTE INSCRITO EN LA APLICACION',
  `ID_USUARIO` int(11) NOT NULL COMMENT 'ID DEL USUARIO QUE DIO DE ALTA LA INFORMACION',
  `ID_CONTRATISTA` int(11) NOT NULL COMMENT 'ID DEL CONTRATISTA QUE EJECUTO EL CONTRATO',
  `ID_UNIDAD_UAS` int(11) NOT NULL COMMENT 'ID DE LA UNIDAD ADMINISTRATIVA SOLICITANTE',
  `ID_UNIDAD_UAR` int(11) NOT NULL COMMENT 'ID DE LA UNIDAD ADMINISTRATIVA RESPONSABLE',
  `ID_RESIDENTE` int(11) NOT NULL COMMENT 'ID DEL RESIDENTE',
  `ID_SUPERV_EXTERNA` int(11) NOT NULL COMMENT 'ID DE LA EMPRESA QUE EJECUTO LA SUPERVISION EXTERNA',
  `TIPO_CONTRATO` int(11) NOT NULL COMMENT 'TIPO DE CONTRAT',
  `TIPO_OBRA` int(11) NOT NULL COMMENT 'TIPO DE OBRA A LA QUE PERTENCE EL CONTRATO',
  `OBRA_COMPLEMENTARIA` int(11) NOT NULL COMMENT 'ESTA OBRA ES PARTE DE UNA OBRA CABECERA',
  `ID_OBRA_CABECERA` int(11) NOT NULL COMMENT 'ID DE LA OBRA CABECERA',
  `NUM_CONTRATO` varchar(255) NOT NULL COMMENT 'NUMERO DE CONTRATO',
  `NUM_LICITACION` varchar(255) NOT NULL COMMENT 'NUMERO DE LICITACION DEL CONTRATO',
  `OBJETO` longtext NOT NULL COMMENT 'OBJETO DEL CONTRATO',
  `COMENTARIOS` longtext NOT NULL COMMENT 'COMENTARIOS SOBRE EL CONTRARTO',
  `IMPORTE_CONTRATO` double NOT NULL COMMENT 'IMPORTE TOTAL DEL CONTRATO',
  `IVA_CONTRATO` double NOT NULL COMMENT 'PORCENTAJE DE IVA DEL CONTRATO',
  `TOTAL_CONTRATO` double NOT NULL COMMENT 'TOTAL DEL CONTRATO',
  `LOCALIDAD` varchar(255) NOT NULL COMMENT 'LOCALDIDAD DONDE SE LLEVO ACABO LA OBRA',
  `LATITUD` char(50) NOT NULL COMMENT 'LATITUD DONDE SE ENCUENTRA LA OBRA',
  `LONGITUD` char(50) NOT NULL COMMENT 'LONGITUD DONDE SE ENCUENTRA LA OBRA',
  `MUNICIPIO` int(11) NOT NULL COMMENT 'ID DEL MUNICIPIO DONDE SE EJECUTO LA OBRA',
  `BENIFICIADOS` int(11) NOT NULL COMMENT 'NUMERO DE HABITANTES BENIFICIADOS POR EL CONTRATO',
  `FECHA_CONTRATO` varchar(22) NOT NULL COMMENT 'FECHA DE FIRMA DEL CONTRATO',
  `FECHA_INICIO` varchar(22) NOT NULL COMMENT 'FECHA DE INCIIO DE LOS TRABAJOS',
  `FECHA_TERMINO` varchar(22) NOT NULL COMMENT 'FECHA DE TERMINO DE LOS TRABAJOS',
  `FECHA_TERMINO_REAL` char(22) NOT NULL COMMENT 'FECHA REAL DE TERMINACION DEL CONTRATO',
  `NOMBRE_REP_UAS` varchar(255) NOT NULL COMMENT 'NOMBRE DEL RESPONSABLE DE LA UNIDAD ADMINISTRATIVA SOLICITANTE',
  `PUESTO_REP_UAS` varchar(255) NOT NULL COMMENT 'PUESTO DEL RESPONABLE DE LA UNIDAD ADMINISTRATIVA SOLICITANTE',
  `NOMBRE_REP_UAR` varchar(255) NOT NULL COMMENT 'NOMBRE DEL RESPONSABLE DE LA UNIDAD ADMINISTRATIVA RESPONSABLE',
  `PUESTO_REP_UAR` varchar(255) NOT NULL COMMENT 'PUESTO DEL RESPONSABLE DE LA UNIDAD ADMINISTRATIVA RESPONABLE',
  `TIPO_MODALIDAD` int(11) NOT NULL COMMENT 'MODALIDAD DEL CONTRATO',
  `ONLINE` int(11) NOT NULL COMMENT 'CONTRATO LISTO PARA SER PUBLICADO EN LINEA',
  `FECHA_ALTA` datetime NOT NULL COMMENT 'FECHA DE ALTA DE LA OBRA',
  `FECHA_ULT_MOD` datetime NOT NULL COMMENT 'FECHA DE LA ULTIMA MODIFICACION DE LA OBRA',
  `INIFED` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `convenios_modificatorios`
--

CREATE TABLE `convenios_modificatorios` (
  `ID` int(11) NOT NULL,
  `ID_CONTRATO` int(11) NOT NULL COMMENT 'ID DEL CONTRATO',
  `ID_USUARIO` int(11) NOT NULL COMMENT 'ID DEL USUARIO QUE DIO DE ALTA LA INFORMACION',
  `FECHA` varchar(22) NOT NULL COMMENT 'FECHA DEL CONVENIO MODIFICATORIO',
  `TIPO` int(11) NOT NULL COMMENT 'TIPO DE CONVENIO MODIFICATORIO',
  `OBJETO` mediumtext DEFAULT NULL COMMENT 'NUEVO OBJETO DEL CONTRATO',
  `MONTO` double DEFAULT NULL COMMENT 'MONTO DE REDUCCION O AMPLIACION DEL CONTRATO',
  `FECHA_INICIO` varchar(22) DEFAULT NULL COMMENT 'NUEVA FECHA DE INICIO DEL TRABAJOS',
  `FECHA_TERMINO` varchar(22) DEFAULT NULL COMMENT 'NUEVA FECHA DE TERMINA DE LOS TRABAJOS',
  `DOCUMENTO` varchar(255) NOT NULL COMMENT 'PATH HACIA EL DOCUNENTO PDF DEL CONVENIO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `estudios`
--

CREATE TABLE `estudios` (
  `ID` int(11) NOT NULL,
  `ID_CONTRATO` int(11) NOT NULL COMMENT 'ID DEL CONTRATO',
  `ID_USUARIO` int(11) NOT NULL COMMENT 'ID DEL CLIENTE QUE DIO DE ALTA EL ESTUDIO',
  `DESCRIPCION` mediumtext NOT NULL COMMENT 'DESCRIPCION DEL ESTUDIO',
  `ARCHIVO_FTP` varchar(255) NOT NULL COMMENT 'RUTA AL ARCHIVO DEL ESTUDIO SOLO FTPS'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `expediente`
--

CREATE TABLE `expediente` (
  `ID` int(11) NOT NULL,
  `ID_CONTRATO` int(11) NOT NULL COMMENT 'ID DEL CONTRATO',
  `ID_USUARIO` int(11) NOT NULL COMMENT 'ID DEL USUARIO QUE DIO DE ALTA LA INFORMACION',
  `ID_PLANTILLA` int(11) NOT NULL COMMENT 'ID DE LA PLANTILLA DEL DOCUMENTO',
  `NOMBRE` mediumtext DEFAULT NULL COMMENT 'NOMBRE DEL DOCUMENTO EN CASO DE QUE NO ESTE DADO DE ALTA EN LA PLANTILLAS',
  `ARCHIVO` varchar(255) NOT NULL COMMENT 'RUTA DEL ARHCIVO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `licitaciones`
--

CREATE TABLE `licitaciones` (
  `id` int(11) NOT NULL COMMENT 'ID DEL REGISTRO',
  `id_dependencia` int(11) NOT NULL COMMENT 'ID DE LA DEPENDENCIA',
  `num_licitacion` varchar(255) NOT NULL COMMENT 'NUMERO DE LICITACION',
  `tipo` int(11) NOT NULL COMMENT 'TIPO DE LICITACION',
  `normatividad` int(11) NOT NULL COMMENT 'NORMATIVIDAD',
  `descripcion` longtext NOT NULL COMMENT 'DESCRIPCION/OBJETO DE LA LICITACION',
  `fecha_publiacion` varchar(255) NOT NULL COMMENT 'FECHA DE PUBLICACION',
  `url_compranet` varchar(255) NOT NULL COMMENT 'URL PARA LOS DETALLES EN COMPRANET',
  `foto_proyecto` varchar(255) NOT NULL COMMENT 'FOTO IMAGEN DEL PROYECTO',
  `fecha_creacion` datetime NOT NULL COMMENT 'FECHA DE CREACION EN EL SISTEMA',
  `desierta` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `licitaciones_eventos`
--

CREATE TABLE `licitaciones_eventos` (
  `id` int(11) NOT NULL COMMENT 'ID DEL REGISTRO',
  `id_licitacion` int(11) NOT NULL COMMENT 'ID DE LA LICITACION',
  `tipo` int(11) NOT NULL COMMENT 'TIPO DE EVENTO',
  `fecha` varchar(50) NOT NULL COMMENT 'FECHA DEL EVENTO',
  `hora` varchar(50) NOT NULL COMMENT 'HORA DEL EVENTO',
  `url_trasmision` varchar(255) NOT NULL COMMENT 'URL PARA TRASMISION EN VIVO',
  `documento` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `municipios`
--

CREATE TABLE `municipios` (
  `id` int(11) NOT NULL,
  `id_entidad` int(11) NOT NULL,
  `nombre_municipio` varchar(255) NOT NULL,
  `clave_municipio` varchar(255) NOT NULL,
  `fecha` varchar(11) NOT NULL,
  `fecha_modificacion` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `municipiosContratos`
--

CREATE TABLE `municipiosContratos` (
  `id` int(10) NOT NULL,
  `idContrato` int(6) NOT NULL,
  `idMunicipio` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `observaciones`
--

CREATE TABLE `observaciones` (
  `id` int(11) NOT NULL COMMENT 'ID DEL REGISTRO',
  `tipo` varchar(50) NOT NULL COMMENT 'TIPO DE EVENTO',
  `referencia` varchar(50) NOT NULL COMMENT 'REFERENCIA DEL EVENTO',
  `nombre` varchar(255) NOT NULL COMMENT 'NOMBRE DEL CIUDADANO QUE LO ENVIO',
  `email` varchar(255) DEFAULT NULL COMMENT 'CORREO ELECTRONICO DEL CIUDADANO',
  `comentario` longtext DEFAULT NULL COMMENT 'COMENTARIO U OBSERVACION QUE ENCONTRO',
  `path_file` varchar(255) DEFAULT NULL COMMENT 'PATH HACIA LA FOTO QUE MANDO EL CIUDADANO',
  `visto` int(11) NOT NULL,
  `activo` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL COMMENT 'FECHA DE CREACION DE LA REPORTE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `parametros_cliente`
--

CREATE TABLE `parametros_cliente` (
  `ID` int(11) NOT NULL COMMENT 'ID CONSECUTIVO',
  `ID_CLIENTE` int(11) NOT NULL COMMENT 'ID DEL CLIENTE',
  `IVA` float NOT NULL COMMENT 'IVA PARAMETRIZADO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `plantillas`
--

CREATE TABLE `plantillas` (
  `ID` int(11) NOT NULL COMMENT 'ID DE LA PLANTILLA',
  `DOCUMENTO` varchar(255) NOT NULL COMMENT 'NOMBRE DEL DOCUMENTO',
  `ORDEN` int(11) NOT NULL COMMENT 'ORDEN'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `residentes`
--

CREATE TABLE `residentes` (
  `ID` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL COMMENT 'ID DEL CLIENTE INSCRITO EN APLICACION',
  `NOMBRE` varchar(255) NOT NULL COMMENT 'NOMBRE DEL REISENTE',
  `DATOS` longtext NOT NULL COMMENT 'DATOS PARTICULARES DEL RESIDENTE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `respuesta_observaciones`
--

CREATE TABLE `respuesta_observaciones` (
  `respuestaid` int(10) NOT NULL,
  `observacionid` int(10) NOT NULL,
  `usuarioid` int(11) NOT NULL,
  `respuesta` text NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `secure_login`
--

CREATE TABLE `secure_login` (
  `ID` int(11) NOT NULL,
  `ID_USUARIO` int(11) NOT NULL COMMENT 'ID DEL USUARIO',
  `TIME` datetime NOT NULL COMMENT 'TIEMPO Y FECHA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `supervision_externa`
--

CREATE TABLE `supervision_externa` (
  `ID` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL COMMENT 'ID DEL CLIENTE INSCRITO EN APLICACION',
  `NOMBRE` varchar(255) NOT NULL COMMENT 'NOMBRE DE LA EMPRESA O PERSONA COMO SUPERVISION EXTERNA',
  `DATOS` longtext NOT NULL COMMENT 'DATOS PARTICULARES DE LA EMPRESA SUPERVISION EXTERNA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_obra`
--

CREATE TABLE `tipo_obra` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `unidades_administrativas`
--

CREATE TABLE `unidades_administrativas` (
  `ID` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL COMMENT 'ID DEL CLIENTE INSCRITO EN APLICACION',
  `NOMBRE` varchar(255) NOT NULL COMMENT 'NOMBRE DE UNIDAD ADMINISTRATIVA',
  `TIPO` int(11) NOT NULL,
  `NOMBRE_RESPONSABLE` varchar(255) NOT NULL COMMENT 'NOMBRE DEL ENCARGADO DE LA UNIDAD',
  `PUESTO_RESPONSABLE` longtext NOT NULL COMMENT 'PUESTO DEL ENCARGADO DE LA UNIDAD'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `ID` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL COMMENT 'ID DEL CLIENTE INSCRITO EN APLICACION',
  `USUARIO` varchar(12) NOT NULL COMMENT 'NOMBRE DE USUARIO',
  `PASSWORD` char(128) NOT NULL,
  `SALT` varchar(128) NOT NULL COMMENT 'SALT DEL PASSWORD',
  `NOMBRE` varchar(255) NOT NULL COMMENT 'NOMBRE DEL USUARIO',
  `EMAIL` varchar(255) NOT NULL COMMENT 'EMAIL DEL USUARIO',
  `TIPO` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`ID`, `ID_CLIENTE`, `USUARIO`, `PASSWORD`, `SALT`, `NOMBRE`, `EMAIL`, `TIPO`) VALUES
(1, 1, 'hidalgo', '67654096105808fccd9e194eac394dd22634112c37a8540050af60df9757cf238590a03688d437ea7730bba0eaea14732542bd5ba929a2751b33d2d67c2a9f48', '[F#W2b>hg$QtHf]MAy8<MuvB0%kSSl%E5naClqU$OrB82e27T', 'Administrador del portal TuObra Hidalgo', 'tuobra@hidalgo.gob.mx', 1),
(62, 1, 'jorge.mendoz', 'bb38d3b48b26e70b297a7ddf112a478b7e02733074c434f272a5b165b022a742ac6c648753427a3b6b703034064a8083bfa8f47f253c98a5921abd1aaf73e470', '@lFe{CG\\y[cWaeaC*m08]kAoEvgTr}<lct5nx2byzz$EyfJEo2', 'Jorge Lara Mendoza', 'jorge.lara@hidalgo.gob.mx', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vcontratosod`
-- (See below for the actual view)
--
CREATE TABLE `vcontratosod` (
`NUM_CONTRATO` varchar(255)
,`ID_OBRA_COMPLEMENTARIA` int(11)
,`OC_NUMERO_CONTRATO` varchar(255)
,`NUM_LICITACION` varchar(255)
,`OBJETO` longtext
,`MODALIDAD` varchar(23)
,`TIPO_CONTRATO` varchar(12)
,`TIPO_OBRA` varchar(20)
,`TOTAL_CONTRATO` double
,`AVANCE` double
,`FECHA_AVANCE` varchar(25)
,`DOCUMENTOS` varbinary(21)
,`FOTOGRAFIAS` varbinary(21)
,`MUNICIPIO` varchar(255)
,`LOCALIDAD` varchar(255)
,`LATITUD` char(50)
,`LONGITUD` char(50)
,`BENEFICIADOS` int(11)
,`ORGANISMO` varchar(255)
,`UA_SOLICITANTE` varchar(255)
,`UA_RESPONSABLE` varchar(255)
,`CONTRATISTA` varchar(255)
,`RESIDENTE` varchar(255)
,`SUPERVISOR_EXTERNO` varchar(255)
,`FECHA_CONTRATO` varchar(22)
,`FECHA_INICIO` varchar(22)
,`FECHA_TERMINO` varchar(22)
,`FECHA_ENTREGA` char(22)
,`EJERCICIO` int(4)
,`ID_OBRA` int(11)
,`ID_ORGANISMO` int(11)
,`ID_CONTRATISTA` int(11)
,`ID_RESIDENTE` int(11)
,`ID_SUPERV_EXTERNA` int(11)
,`ID_UNIDAD_UAR` int(11)
,`ID_UNIDAD_UAS` int(11)
,`ID_MUNICIPIO` int(11)
,`ONLINE` int(11)
,`ID_TIPO_MODALIDAD` int(11)
,`ID_TIPO_OBRA` int(11)
,`ID_TIPO_CONTRATO` int(11)
,`INIFED` varchar(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vistacontratos`
-- (See below for the actual view)
--
CREATE TABLE `vistacontratos` (
`ID` int(11)
,`NUM_CONTRATO` varchar(255)
,`NUM_LICITACION` varchar(255)
,`OBJETO` longtext
,`IMPORTE_CONTRATO` double
,`IVA_CONTRATO` double
,`TOTAL_CONTRATO` double
,`nombre_municipio` varchar(255)
,`LOCALIDAD` varchar(255)
,`RAZON_SOCIAL` varchar(255)
,`RESIDENTE` varchar(255)
,`SUPERVEX` varchar(255)
,`FECHA_CONTRATO` varchar(22)
,`FECHA_INICIO` varchar(22)
,`FECHA_TERMINO` varchar(22)
,`EJERCICIO` int(4)
,`ID_CLIENTE` int(11)
,`ID_CONTRATISTA` int(11)
,`ID_RESIDENTE` int(11)
,`ID_SUPERV_EXTERNA` int(11)
,`ID_UNIDAD_UAR` int(11)
,`ID_UNIDAD_UAS` int(11)
,`ID_MUNICIPIO` int(11)
,`ONLINE` int(11)
,`por_avance` double
,`TIPO_MODALIDAD` int(11)
,`TIPO_OBRA` int(11)
,`INIFED` tinyint(4)
,`TIPO_CONTRATO` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vlicitaciones`
-- (See below for the actual view)
--
CREATE TABLE `vlicitaciones` (
`LICITACION` varchar(255)
,`ORGANISMO` varchar(255)
,`TIPO` varchar(46)
,`NORMATIVIDAD` varchar(7)
,`descripcion` longtext
,`FECHA_PUBLICACION` varchar(255)
,`desierta` tinyint(1)
,`FECHA_VISITA_OBRA` varchar(50)
,`HORA_VO` varchar(50)
,`DOC_VO` varchar(255)
,`URL_VO` varchar(255)
,`FECHA_JUNTA_ACLARACIONES` varchar(50)
,`HORA_JA` varchar(50)
,`DOC_JA` varchar(255)
,`URL_JA` varchar(255)
,`FECHA_APERTURA_PROPUESTAS` varchar(50)
,`HORA_AP` varchar(50)
,`DOC_AP` varchar(255)
,`URL_AP` varchar(255)
,`FECHA_FALLO` varchar(50)
,`HORA_F` varchar(50)
,`DOC_F` varchar(255)
,`URL_F` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `vcontratosod`
--
DROP TABLE IF EXISTS `vcontratosod`;

CREATE ALGORITHM=UNDEFINED DEFINER=`u132217102_tuzob`@`127.0.0.1` SQL SECURITY DEFINER VIEW `vcontratosod`  AS  select `c`.`NUM_CONTRATO` AS `NUM_CONTRATO`,`c`.`OBRA_COMPLEMENTARIA` AS `ID_OBRA_COMPLEMENTARIA`,`OC`.`NUM_CONTRATO` AS `OC_NUMERO_CONTRATO`,`c`.`NUM_LICITACION` AS `NUM_LICITACION`,`c`.`OBJETO` AS `OBJETO`,case when `c`.`TIPO_MODALIDAD` = 1 then 'LICITACIÓN PÚBLICA' when `c`.`TIPO_MODALIDAD` = 2 then 'LICITACIÓN SIMPLIFICADA' when `c`.`TIPO_MODALIDAD` = 3 then 'ADJUDICACIÓN DIRECTA' when `c`.`TIPO_MODALIDAD` = 4 then 'ADMINISTRACIÓN DIRECTA' end AS `MODALIDAD`,case when `c`.`TIPO_CONTRATO` = 1 then 'OBRA PÚBLICA' when `c`.`TIPO_CONTRATO` = 2 then 'EQUIPAMIENTO' when `c`.`TIPO_CONTRATO` = 3 then 'SERVICIOS' end AS `TIPO_CONTRATO`,case when `c`.`TIPO_OBRA` = 4 then 'CAMINOS Y CARRETERAS' when `c`.`TIPO_OBRA` = 1 then 'EDUCACIÓN' when `c`.`TIPO_OBRA` = 3 then 'SECTOR SALUD' when `c`.`TIPO_OBRA` = 2 then 'INFRAESTRUCTURA' end AS `TIPO_OBRA`,`c`.`TOTAL_CONTRATO` AS `TOTAL_CONTRATO`,(select `av`.`AVANCE` from `avances` `av` where `av`.`ID_CONTRATO` = `c`.`ID` order by `av`.`ID` desc limit 1) AS `AVANCE`,(select `av`.`FECHA` from `avances` `av` where `av`.`ID_CONTRATO` = `c`.`ID` order by `av`.`ID` desc limit 1) AS `FECHA_AVANCE`,(select if(count(`ex`.`ID_CONTRATO`) > 0,count(`ex`.`ID_CONTRATO`),'NO') from `expediente` `ex` where `ex`.`ID_CONTRATO` = `c`.`ID`) AS `DOCUMENTOS`,(select if(count(`af`.`ID_CONTRATO`) > 0,count(`af`.`ID_CONTRATO`),'NO') from `avance_fotografico` `af` where `af`.`ID_CONTRATO` = `c`.`ID`) AS `FOTOGRAFIAS`,`m`.`nombre_municipio` AS `MUNICIPIO`,`c`.`LOCALIDAD` AS `LOCALIDAD`,`c`.`LATITUD` AS `LATITUD`,`c`.`LONGITUD` AS `LONGITUD`,`c`.`BENIFICIADOS` AS `BENEFICIADOS`,`o`.`NOMBRE` AS `ORGANISMO`,`u`.`NOMBRE` AS `UA_SOLICITANTE`,`uu`.`NOMBRE` AS `UA_RESPONSABLE`,`ct`.`RAZON_SOCIAL` AS `CONTRATISTA`,ucase(`r`.`NOMBRE`) AS `RESIDENTE`,`sp`.`NOMBRE` AS `SUPERVISOR_EXTERNO`,`c`.`FECHA_CONTRATO` AS `FECHA_CONTRATO`,`c`.`FECHA_INICIO` AS `FECHA_INICIO`,`c`.`FECHA_TERMINO` AS `FECHA_TERMINO`,`c`.`FECHA_TERMINO_REAL` AS `FECHA_ENTREGA`,year(str_to_date(`c`.`FECHA_CONTRATO`,'%d/%m/%Y')) AS `EJERCICIO`,`c`.`ID` AS `ID_OBRA`,`c`.`ID_CLIENTE` AS `ID_ORGANISMO`,`c`.`ID_CONTRATISTA` AS `ID_CONTRATISTA`,`c`.`ID_RESIDENTE` AS `ID_RESIDENTE`,`c`.`ID_SUPERV_EXTERNA` AS `ID_SUPERV_EXTERNA`,`c`.`ID_UNIDAD_UAR` AS `ID_UNIDAD_UAR`,`c`.`ID_UNIDAD_UAS` AS `ID_UNIDAD_UAS`,`c`.`MUNICIPIO` AS `ID_MUNICIPIO`,`c`.`ONLINE` AS `ONLINE`,`c`.`TIPO_MODALIDAD` AS `ID_TIPO_MODALIDAD`,`c`.`TIPO_OBRA` AS `ID_TIPO_OBRA`,`c`.`TIPO_CONTRATO` AS `ID_TIPO_CONTRATO`,if(`c`.`INIFED` = 1,'SI','NO') AS `INIFED` from ((((((((`contratos` `c` left join `municipios` `m` on(`m`.`id` = `c`.`MUNICIPIO`)) left join `unidades_administrativas` `u` on(`u`.`ID` = `c`.`ID_UNIDAD_UAS`)) left join `unidades_administrativas` `uu` on(`uu`.`ID` = `c`.`ID_UNIDAD_UAR`)) left join `contratistas` `ct` on(`ct`.`ID` = `c`.`ID_CONTRATISTA`)) left join `residentes` `r` on(`r`.`ID` = `c`.`ID_RESIDENTE`)) left join `supervision_externa` `sp` on(`sp`.`ID` = `c`.`ID_SUPERV_EXTERNA`)) left join `contratos` `OC` on(`OC`.`ID` = `c`.`ID_OBRA_CABECERA`)) left join `clientes` `o` on(`o`.`ID` = `c`.`ID_CLIENTE`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vistacontratos`
--
DROP TABLE IF EXISTS `vistacontratos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`u132217102_tuzob`@`127.0.0.1` SQL SECURITY DEFINER VIEW `vistacontratos`  AS  select `c`.`ID` AS `ID`,`c`.`NUM_CONTRATO` AS `NUM_CONTRATO`,`c`.`NUM_LICITACION` AS `NUM_LICITACION`,`c`.`OBJETO` AS `OBJETO`,`c`.`IMPORTE_CONTRATO` AS `IMPORTE_CONTRATO`,`c`.`IVA_CONTRATO` AS `IVA_CONTRATO`,`c`.`TOTAL_CONTRATO` AS `TOTAL_CONTRATO`,`m`.`nombre_municipio` AS `nombre_municipio`,`c`.`LOCALIDAD` AS `LOCALIDAD`,`ct`.`RAZON_SOCIAL` AS `RAZON_SOCIAL`,`r`.`NOMBRE` AS `RESIDENTE`,`sp`.`NOMBRE` AS `SUPERVEX`,`c`.`FECHA_CONTRATO` AS `FECHA_CONTRATO`,`c`.`FECHA_INICIO` AS `FECHA_INICIO`,`c`.`FECHA_TERMINO` AS `FECHA_TERMINO`,year(str_to_date(`c`.`FECHA_CONTRATO`,'%d/%m/%Y')) AS `EJERCICIO`,`c`.`ID_CLIENTE` AS `ID_CLIENTE`,`c`.`ID_CONTRATISTA` AS `ID_CONTRATISTA`,`c`.`ID_RESIDENTE` AS `ID_RESIDENTE`,`c`.`ID_SUPERV_EXTERNA` AS `ID_SUPERV_EXTERNA`,`c`.`ID_UNIDAD_UAR` AS `ID_UNIDAD_UAR`,`c`.`ID_UNIDAD_UAS` AS `ID_UNIDAD_UAS`,`c`.`MUNICIPIO` AS `ID_MUNICIPIO`,`c`.`ONLINE` AS `ONLINE`,(select `av`.`AVANCE` from `avances` `av` where `av`.`ID_CONTRATO` = `c`.`ID` order by `av`.`ID` desc limit 1) AS `por_avance`,`c`.`TIPO_MODALIDAD` AS `TIPO_MODALIDAD`,`c`.`TIPO_OBRA` AS `TIPO_OBRA`,`c`.`INIFED` AS `INIFED`,`c`.`TIPO_CONTRATO` AS `TIPO_CONTRATO` from ((((((`contratos` `c` join `municipios` `m` on(`m`.`id` = `c`.`MUNICIPIO`)) left join `unidades_administrativas` `u` on(`u`.`ID` = `c`.`ID_UNIDAD_UAS`)) left join `unidades_administrativas` `uu` on(`uu`.`ID` = `c`.`ID_UNIDAD_UAR`)) join `contratistas` `ct` on(`ct`.`ID` = `c`.`ID_CONTRATISTA`)) left join `residentes` `r` on(`r`.`ID` = `c`.`ID_RESIDENTE`)) left join `supervision_externa` `sp` on(`sp`.`ID` = `c`.`ID_SUPERV_EXTERNA`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vlicitaciones`
--
DROP TABLE IF EXISTS `vlicitaciones`;

CREATE ALGORITHM=UNDEFINED DEFINER=`u132217102_tuzob`@`127.0.0.1` SQL SECURITY DEFINER VIEW `vlicitaciones`  AS  select `L`.`num_licitacion` AS `LICITACION`,`O`.`NOMBRE` AS `ORGANISMO`,case when `L`.`tipo` = 1 then 'LICITACIÓN PÚBLICA' when `L`.`tipo` = 2 then 'LICITACIÓN SIMPLIFICADA/INVITACIÓN RESTRINGIDA' end AS `TIPO`,case when `L`.`normatividad` = 1 then 'ESTATAL' when `L`.`normatividad` = 2 then 'FEDERAL' end AS `NORMATIVIDAD`,`L`.`descripcion` AS `descripcion`,`L`.`fecha_publiacion` AS `FECHA_PUBLICACION`,`L`.`desierta` AS `desierta`,`E1`.`fecha` AS `FECHA_VISITA_OBRA`,`E1`.`hora` AS `HORA_VO`,`E1`.`documento` AS `DOC_VO`,`E1`.`url_trasmision` AS `URL_VO`,`E2`.`fecha` AS `FECHA_JUNTA_ACLARACIONES`,`E2`.`hora` AS `HORA_JA`,`E2`.`documento` AS `DOC_JA`,`E2`.`url_trasmision` AS `URL_JA`,`E3`.`fecha` AS `FECHA_APERTURA_PROPUESTAS`,`E3`.`hora` AS `HORA_AP`,`E3`.`documento` AS `DOC_AP`,`E3`.`url_trasmision` AS `URL_AP`,`E4`.`fecha` AS `FECHA_FALLO`,`E4`.`hora` AS `HORA_F`,`E4`.`documento` AS `DOC_F`,`E4`.`url_trasmision` AS `URL_F` from (((((`clientes` `O` left join `licitaciones` `L` on(`O`.`ID` = `L`.`id_dependencia`)) left join `licitaciones_eventos` `E1` on(`E1`.`id_licitacion` = `L`.`id` and `E1`.`tipo` = 1)) left join `licitaciones_eventos` `E2` on(`E2`.`id_licitacion` = `L`.`id` and `E2`.`tipo` = 2)) left join `licitaciones_eventos` `E3` on(`E3`.`id_licitacion` = `L`.`id` and `E3`.`tipo` = 3)) left join `licitaciones_eventos` `E4` on(`E4`.`id_licitacion` = `L`.`id` and `E4`.`tipo` = 4)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avances`
--
ALTER TABLE `avances`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CONTRATO` (`ID_CONTRATO`),
  ADD KEY `ID_CONTRATO_2` (`ID_CONTRATO`);

--
-- Indexes for table `avance_fotografico`
--
ALTER TABLE `avance_fotografico`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CONTRATO` (`ID_CONTRATO`);

--
-- Indexes for table `avance_fotografico_frentes`
--
ALTER TABLE `avance_fotografico_frentes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CONTRATO` (`ID_CONTRATO`);

--
-- Indexes for table `bitacora_obra`
--
ALTER TABLE `bitacora_obra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `contratistas`
--
ALTER TABLE `contratistas`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`),
  ADD KEY `ID_CLIENTE_2` (`ID_CLIENTE`);

--
-- Indexes for table `contratos`
--
ALTER TABLE `contratos`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `NUM_CONTRATO` (`NUM_CONTRATO`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`);

--
-- Indexes for table `convenios_modificatorios`
--
ALTER TABLE `convenios_modificatorios`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CONTRATO` (`ID_CONTRATO`);

--
-- Indexes for table `estudios`
--
ALTER TABLE `estudios`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CONTRATO` (`ID_CONTRATO`);

--
-- Indexes for table `expediente`
--
ALTER TABLE `expediente`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CONTRATO` (`ID_CONTRATO`);

--
-- Indexes for table `licitaciones`
--
ALTER TABLE `licitaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `num_licitacion` (`num_licitacion`),
  ADD KEY `id_dependencia` (`id_dependencia`);

--
-- Indexes for table `licitaciones_eventos`
--
ALTER TABLE `licitaciones_eventos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_licitacion` (`id_licitacion`);

--
-- Indexes for table `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `municipiosContratos`
--
ALTER TABLE `municipiosContratos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `observaciones`
--
ALTER TABLE `observaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parametros_cliente`
--
ALTER TABLE `parametros_cliente`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID_CLIENTE` (`ID_CLIENTE`),
  ADD KEY `ID_CLIENTE_2` (`ID_CLIENTE`);

--
-- Indexes for table `plantillas`
--
ALTER TABLE `plantillas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `residentes`
--
ALTER TABLE `residentes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`);

--
-- Indexes for table `respuesta_observaciones`
--
ALTER TABLE `respuesta_observaciones`
  ADD PRIMARY KEY (`respuestaid`);

--
-- Indexes for table `secure_login`
--
ALTER TABLE `secure_login`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `supervision_externa`
--
ALTER TABLE `supervision_externa`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`);

--
-- Indexes for table `tipo_obra`
--
ALTER TABLE `tipo_obra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `unidades_administrativas`
--
ALTER TABLE `unidades_administrativas`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `EMAIL` (`EMAIL`),
  ADD KEY `ID_CLIENTE` (`ID_CLIENTE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avances`
--
ALTER TABLE `avances`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `avance_fotografico`
--
ALTER TABLE `avance_fotografico`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `avance_fotografico_frentes`
--
ALTER TABLE `avance_fotografico_frentes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID DEL FRENTE FOTOGRAFICO', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bitacora_obra`
--
ALTER TABLE `bitacora_obra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `contratistas`
--
ALTER TABLE `contratistas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contratos`
--
ALTER TABLE `contratos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `convenios_modificatorios`
--
ALTER TABLE `convenios_modificatorios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estudios`
--
ALTER TABLE `estudios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expediente`
--
ALTER TABLE `expediente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `licitaciones`
--
ALTER TABLE `licitaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID DEL REGISTRO', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `licitaciones_eventos`
--
ALTER TABLE `licitaciones_eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID DEL REGISTRO', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `municipios`
--
ALTER TABLE `municipios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `municipiosContratos`
--
ALTER TABLE `municipiosContratos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `observaciones`
--
ALTER TABLE `observaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID DEL REGISTRO', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `parametros_cliente`
--
ALTER TABLE `parametros_cliente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID CONSECUTIVO', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `plantillas`
--
ALTER TABLE `plantillas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID DE LA PLANTILLA', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `residentes`
--
ALTER TABLE `residentes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `respuesta_observaciones`
--
ALTER TABLE `respuesta_observaciones`
  MODIFY `respuestaid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `secure_login`
--
ALTER TABLE `secure_login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supervision_externa`
--
ALTER TABLE `supervision_externa`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipo_obra`
--
ALTER TABLE `tipo_obra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `unidades_administrativas`
--
ALTER TABLE `unidades_administrativas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

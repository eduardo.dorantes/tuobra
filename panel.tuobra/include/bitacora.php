<div id="resultado"></div>
<form name="add-bitacora" id="add-bitacora" method="post">
	<table>
    	<tr>
        	<td><input type="text" name="fecha" id="fecha" class="campo-fecha" required size="10"></td>
        </tr>
    	<tr>
        	<td><textarea name="descripcion" id="descripcion" rows="6" cols="55" required></textarea></td>
        </tr>
        <tr>
        	<td class="content-form-buttons">
            	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn-green">
                <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                <input type="hidden" name="obra" id="obra" value="<?php echo $_POST['id']; ?>" required>
            </td>
        </tr>
    </table>
</form>
<!-- contenedor de bitacora -->
<div class="contenedor-bitacora" id="contenedor-bitacora">
</div>
<script type="text/javascript">
$(document).ready(function(e) {
    listaBitacora();
});
$('#add-bitacora').submit(function(){
	
	var datos = $(this).serialize();
	
	$.ajax({
		beforeSend: function(){
			$("#resultado").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url:"sources/insert/crear-bitacora-registro.php?rand=" + Math.random() * 9999999,
		type:"POST",
		data: datos,
		dataType:"json",
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			},
		success: function(res){
			$('#resultado').html(res.msg);
			if(res.status == true){
				$('#add-bitacora')[0].reset();
				listaBitacora();
				}
			$("#enviar-formulario").prop("disabled", false);
			}
		});
	return false;
	});
function listaBitacora(){
	$.ajax({
		beforeSend: function(){
			$("#contenedor-bitacora").html("Cargado....");
			},
		url:"sources/listas/lista-bitacora-obra.php?rand=" + Math.random() * 9999999,
		type:"POST",
		data: 'id=<?php echo $_POST['id']; ?>',
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			},
		success: function(res){
			$('#contenedor-bitacora').html(res);
			}
		});
	}
$(function() {
    $(".campo-fecha").datepicker({dayNames: ['Domingo','Lunes','Martes','Miercoles','Juevez','Viernes','Sabado'], dayNamesMin: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],nextText:'Siguiente',prevText: 'Atras', dateFormat: 'dd/mm/yy', autoSize: true, changeYear: true, monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']}); });
</script>
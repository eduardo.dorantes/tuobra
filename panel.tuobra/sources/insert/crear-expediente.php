<?php
session_start();
require("../../config/conn.php");
require("../../funciones/expediente.class.inc.php");
require("../../funciones/upload.files.inc.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");

/*comprobar post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*path*/
	$path = "../../files/";
	/*recibir las varibles*/
	$id_contarto = $_POST['id-contrato'];
	$id_usuario = $_POST['id-usuario'];
	$id_plantilla = $_POST['expediente'];
	$descripcion = $_POST['nombre-otro-documento'];
	$tmp_archivo = $_FILES['documento']['tmp_name'];
	$archivo = $_FILES['documento']['name'];
	
	/*crear un obejto de expediente*/
	$expediente = new expediente($conn);
	
	/*comprobar los campos requerdiso*/
	if(validar::notEmpty($id_plantilla) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit;
		}
	if(validar::notEmpty($archivo) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit;
		}
	/*comprobar la extencion del archivo sea PDF*/
	if(upload::getExtencionArchivo($archivo) != "pdf"){
		echo "<div class=\"warning\">Extencion del archivo no es valido solo PDF.</div>";
		exit;
		}
	/*convinar el path*/
	$path .= $id_contarto."/EXPEDIENTE/".$id_plantilla."/";
	
	/*crear objeto subir archivos*/
	$upload = new upload();
	$archivo_upload = $upload->uploadFile($path, $archivo, $tmp_archivo);//subir el archivo
	
	/*comprobar que se aya subido el archivo*/
	if($archivo_upload !== false){
		/*crear el expediente*/
		$objeto = $expediente->crearExpediente($id_contarto, $id_usuario, $id_plantilla, $descripcion, substr($archivo_upload, 6));
		/*comprobar que se aya creado el obejto*/
		if($objeto == true){
			echo "<script>window.history.back();</script>";
			}
		}
	}
?>
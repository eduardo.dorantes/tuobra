<?php
session_start();
header("cache-control: no-cache");
require("../../config/conn.php");
require("../../funciones/upload.files.inc.php");
require("../../funciones/fotos.class.inc.php");
require("../../require/Thumb/ThumbLib.inc.php");
require("../../sources/msg-file.php");

/*path donde se guardaran los archivos*/
$path = "../../files/";
$seg_path = "../files/";
/*resivir las varibles*/
$fecha = date("d/m/Y");//fecha de la imagen
$pie_imagen = "";//pie/descripcion de la imagen
$id_contrato = $_POST['id-contrato'];//id del contrato
$id_usuario = $_POST['id-usuario'];//id del cliente
$id_frente = $_POST['id-frente'];
$tmp_file = $_FILES['documento']['tmp_name'];//recibir el archivo temporal del documento
$file = $_FILES['documento']['name'];//recibir el archivo del documento

if(!empty($_POST) and !empty($id_contrato) and $_SESSION['login'] == true){
	/*extraer el nombre y la extecion del archivo*/
	$nombre = upload::getNameFile($file);//nombre original del archivo
	$extencion = upload::getExtencionArchivo($file);//extecion del archivo original
	
	/*comprobar que sea un archivo imagen*/
	if($extencion == "jpg" or $extencion == "png" or $extencion == "gif" or $extencion == 'jpeg'){
		/*combinar el path*/
		$path .= $id_contrato."/FOTOS/";
		$seg_path .= $id_contrato."/FOTOS/";
		
		/*crear los nombres del thubnail*/
		$thumb_file = $nombre."_".(rand() * time())."_thumb240x175.".$extencion;//thumbnail del archivo
		/*crear el nombre del archivo thumb mas grande*/
		$thumb_file_large = $nombre."_".(rand() * time())."_thumb600x400.".$extencion;
		
		/*crear el objeto subir*/
		/*luego subir la imagen original*/
		$subir = new upload();
		$subir->uploadFile($path, $file, $tmp_file);
		
		/*comrpobar que se aya subido el archivo*/
		if($subir !== false){
			/*si se subio bien el archivo entonces creamos su thumbnail*/
			try{
				/*crear el thumbnail del documento*/
				$thumb = PhpThumbFactory::create($subir->getPathToFile());
				/*crear el thumbnail del documento large*/
				$thumb_large = PhpThumbFactory::create($subir->getPathToFile());
				}catch(Exception $e){
					/*mostrar error al crear el thumb*/
					echo $e."<br />".$path;
					}
			/*rize la imgen del thumb a 175x175 */
			$thumb->adaptiveResize(240, 175);
			/*rize la imagen del thumb a 600x400*/
			$thumb_large->adaptiveResize(600, 400);
			/*subir el archivo del thumb*/
			$thumb->save($path.$thumb_file, "jpg");
			/*subir el archivo mthumb large*/
			$thumb_large->save($path.$thumb_file_large, "jpg");
			
			/*crar el objeto foto*/
			$foto = new fotos($conn);
			$foto->crearFoto($id_contrato, $id_frente, $id_usuario, $fecha, $pie_imagen);
			
			/*comprobar que se aya crear el objeto*/
			if($foto == true){
				/*si se creo el bojeto actulizar con los archivos de la imagne y del thumnail*/
				$foto->actulizarFoto("IMAGEN", substr($subir->getPathToFile(), 6));//imagen original
				$foto->actulizarFoto("THUMB", substr($path.$thumb_file, 6));//thumbnail de la foto
				$foto->actulizarFoto("THUMB_LARGE", substr($path.$thumb_file_large, 6));
				
				/*si todo sale bien regresar*/
				echo "";
				}
			}
		}else{
			echo "<div class=\"warning\">Archivo invalido - ".$extencion."</div>";
			}
	}
?>
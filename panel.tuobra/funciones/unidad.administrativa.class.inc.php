<?php

class unidadAdminstraiva{
	/* Bind parameters. TYpes: s = string, i = integer, d = double,  b = blob */
	/*varibles*/
	var $db; //conexcion a la base de datos
	private $id_unidad;//id de la unidad 
	private $nombre;//nombre la unidad
	private $tipo;//tipo de unidad administrativa
	//private $nombre_encargado;//nombre del encargado
	//private $puesto_encargado;//puesto del encargado
	
	/*constructor*/
	function __construct($conn, $id){
		$this->db = $conn;//conexion a la base de datos
		$this->id_unidad = $id;//is de la unidad
		/*comprobar que no este vacio el id de la unidad*/
		if(!empty($this->id_unidad)){
			/*si bien el id de la unidad entonces construimos la unidad*/
			$this->datosUnidadAdministrativa();
			}
		}
		
	/*crear unidad*/	
	public function createUnidad($id_cliente, $nombre_, $tipo){
		/*query*/
		$sql_query = "INSERT INTO `unidades_administrativas` (ID_CLIENTE, NOMBRE, tipo) VALUES (?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('isi',$id_cliente, $nombre_, $tipo);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*actuliar unidad*/
	public function updateUnidad($nombre_, $tipo_){
		/*query*/
		$sql_query = "UPDATE `unidades_administrativas` SET NOMBRE = ?, TIPO = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('sii',$nombre_, $tipo_, $this->id_unidad);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*datos generales de la unidad*/
	private function datosUnidadAdministrativa(){
		/*comprobar que venga el id de la unidad*/
		if(!empty($this->id_unidad) and strlen($this->id_unidad) > 0){
			/*query*/
			$sql_query = "SELECT NOMBRE, TIPO FROM `unidades_administrativas` WHERE ID = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_unidad);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($nombre_unidad, $tipo);
			/*fetch datos*/
			$query->fetch();
			/*datos de la unidad*/
			$this->nombre = $nombre_unidad;
			$this->tipo = $tipo;
			
			}
		}
	/*nombre de la unidad*/
	public function getNombre(){
		return $this->nombre;
		}
	/*nombre del encargado*/
	public function getNombreEncargado(){
		return $this->nombre_encargado;
		}
	/*puesto del encargado*/
	public function getPuestoEncargado(){
		return $this->puesto_encargado;
		}
	/*traer el tipo de unidad administrativa*/
	public function getTipoUnidad(){
		return $this->tipo;
		}
	}
?>
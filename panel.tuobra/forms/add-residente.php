<?php
session_start();
require("../config/conn.php");
require("../funciones/residente.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}
/*crear un un nuevo objeto unidad*/
$residente = new residentes($conn, $_GET['id']);
?>
<div id="resultado"></div>
<form name="agregar-residente" id="agregar-residente" method="post">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-form-add-rfesidnete">
    	<caption class="form-caption" style="display:none;">Agregar nuevo residente</caption>
    	<tbody valign="top">
        	<tr>
            	<td><label for="nombre-residente">Nombre del residente</label></td>
                <td><input type="text" name="nombre-residente" id="nombre-residente" size="45" value="<?php echo $residente->getNameResidente(); ?>"></td>
            </tr>
            <tr>
            	<td><label for="datos-residente">Datos particulares</label></td>
                <td><textarea name="datos-residente" id="datos-residente" rows="3" cols="40"><?php echo $residente->getDatosResidente(); ?></textarea></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn cancelar-catalogo">
                </td>
                <td><input type="hidden" name="id-residente" id="id-residente" value="<?php echo $_GET['id']; ?>"></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$('#agregar-residente').submit(function(){
	var datos = $(this).serialize();
	var nombre = $('#nombre-residente').val();
	$.ajax({
		beforeSend: function(){
			$("#resultado").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url:"sources/insert/crear-residente.php?rand=" + Math.random() * 9999999,
		type:"POST",
		data: datos,
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			},
		success: function(resultados){
			$("#resultado").html(resultados);
			},
		complete: function(){
			$("#enviar-formulario").prop("disabled", false);
			cargarCatalogos('RS', nombre);
			},
		});
	
	return false;
	});
</script>
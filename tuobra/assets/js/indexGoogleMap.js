

var map;
var service;
var geocoder;


var gmarkers1 = [];
var markers1 = [];



function getMakers(callback) {

    $.ajax({
        type: 'POST',
        url: 'ajax/map-locations.php',
        success: function (data, status, xhr) {
                callback(data);
        },
        dataType: 'json'
    });

}



function initialize(ltd_, lgt_) {
    //var pyrmont = new google.maps.LatLng(-110.967, 29.0667);
    var ltd = 20.117;
    var lgt = -98.7333;

    if(ltd_ != ''){
        ltd = ltd_;
        lgt = lgt_;
    }
    /*cordenada*/
    var coords = new google.maps.LatLng(ltd, lgt);

    // opciones generales del mapa
    var mapOptions = {
        zoom: 13,
        scrollwheel: false,
        center: coords,
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        mapTypeControl: false,

        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        streetViewControl: false,
        styles: [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}]
    };

    /*geocoder*/
    geocoder = new google.maps.Geocoder();
    // creando el mapa
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);




    map.addListener('zoom_changed', function() {
        //console.log('Zoom: ' + map.getZoom());
    });





    /*info de mi posicion*/
    var infowindow = new google.maps.InfoWindow({
        content: "Tu ubicación"
    });

    /*mi posicion*/
    if(ltd_ != ''){

        var $markerImage = document.querySelector('.markerImage'),
            markerImageSvg = $markerImage.innerHTML || '';


        var my_pos = new google.maps.Marker({
            position: coords,
            map: map,
            title: 'Tu',
            icon: {
                anchor: new google.maps.Point(16, 16),
                url: 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(markerImageSvg)
            },
            zIndex: 9999999

        });

        /*mostrar info*/
        google.maps.event.addListener(my_pos, 'click', function() {
            infowindow.open(map, my_pos);
        });
    }


    getMakers(function(result){

        $.each(result, function(key, datos) {
            var latLng = new google.maps.LatLng(datos.latitud, datos.longitud);
            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                title: datos.titulo,
                icon: getTipoIcono(datos.tipo),
                id: key,
                contrato: datos.num,
                category: datos.tipo,
                organismo: datos.organismo,
                municipio: datos.municipio,
                modalidad: datos.modalidad,
                anio: datos.anio
            });

            var arr = Object.keys(datos).map(function (k) {
                return datos[k]
            });

            markers1.push(arr);
            gmarkers1.push(marker);

            /*click el marcador */
            google.maps.event.addListener(marker, 'click', function () {
                window.top.location.href = 'http://' + window.location.host + '/tuobra/obra/' + this.contrato;
            });


            var nPins = parseInt($('#countPins').text());
            nPins ++;

            $('#countPins').text(nPins);

        });



    });


    /*mostrar tu direccion*/
    getAddress(coords);
}
function getAddress(latLng) {
    geocoder.geocode({ 'latLng': latLng }, function(results, status){
        if(status == google.maps.GeocoderStatus.OK){
            if(results[0]){
                //alert(results[0].address_components[0].neighborhood.toUpperCase());
            }
        }
    });
}
function getTipoIcono(tipo_){
    var $markerImage = document.querySelector('.markerImage2'),
        markerImageSvg = $markerImage.innerHTML || '';


    var color ="";


    switch(tipo_){

        case '1':
            //Educación
            color = "#fb3448";
            //var image = new google.maps.MarkerImage('images/flag_03.png', null, null, null, new google.maps.Size(42, 42));
            break;

        case '2':
            //infraestructura
            //var image = new google.maps.MarkerImage('images/flag_02.png', null, null, null, new google.maps.Size(42, 42));
            color ="#34C08E";
            break;

         case '3':
            // Salud
            //var image = new google.maps.MarkerImage('images/flag_02.png', null, null, null, new google.maps.Size(42, 42));
            color ="#7fcccc";
            break;   
                
        case '4':
            //Carreteras
            color = "#F4B12A";
            //var image = new google.maps.MarkerImage('images/flag_04.png', null, null, null, new google.maps.Size(42, 42));
            break;
        

        


        default:
            //var image = new google.maps.MarkerImage('images/flag_05.png', null, null, null, new google.maps.Size(42, 42));
            color ="#b5263b";
            break;
    }


    var image = {
        anchor: new google.maps.Point(36, 26),
        url: 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(markerImageSvg.replace('{{colorPin}}', color))
    }



    return image;
}
function displayPosition(position) {
    initialize(position.coords.latitude, position.coords.longitude);
}
function displayError(error) {
    google.maps.event.addDomListener(window, 'load', initialize('', ''));

    if(error.message.indexOf("Only secure origins are allowed") == 0) {
        //alert(00);
    }


}

if (navigator.geolocation) {
    var timeoutVal = 10 * 1000 * 1000;
    navigator.geolocation.getCurrentPosition(
        displayPosition,
        displayError,
        { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
    );
}
else {
    alert("Geolocation is not supported by this browser");
}

/*
filterMarkers = function (category) {
    for (i = 0; i < markers1.length; i++) {
        marker = gmarkers1[i];
        // If is same category or category not picked
        if (marker.category != category) {
            marker.setVisible(true);
        }
        // Categories don't match
        else {
            marker.setVisible(false);
        }
    }
}*/


function filterMakers2(){
    var modalidad;
    var organismo;
    var municipio;
    var anio;
    var tipo = [];
    var position = new Array();

    $('#countPins').text('0');

    var datos = $("#filterBar form").serializeArray();

    $(datos).each(function(x,y){
        if(y.name=='filterMapCategroy[]'){
            tipo.push(y.value);
        }

        if(y.name=='modalidad'){
            modalidad = y.value;
        }
        if(y.name=='organismo'){
            organismo = y.value;
        }
        if(y.name=='municipio'){
            municipio = y.value;
        }

        if(y.name=='anio'){
            anio = y.value;
        }

    });



    for (i = 0; i < markers1.length; i++) {
        marker = gmarkers1[i];

        var v1 = true, v2 = true,  v3 = true,  v4 = true, v5 = true;

        if (tipo.length > 0) {
            if ($.inArray(marker.category, tipo) == -1) {
                v1 = false;

            }
        }else{
            v1 = false;
        }

        if (modalidad!="") {

            if (marker.modalidad != modalidad) {
                v2 = false;
            }
        }

        if (organismo!="") {
            //console.log(marker.organismo+'-'+organismo);
            if (marker.organismo != organismo) {
                v3 = false;
            }
        }
        if (municipio!="") {
            if (marker.municipio != municipio) {
                v4 = false;
            }
        }

        if (anio!="") {
            if (marker.anio != anio) {
                v5 = false;
            }
        }



        if(v1 && v2 && v3 && v4 && v5){
            marker.setVisible(true);
            //console.log(marker.position.lat());
            if(!isNaN(marker.position.lat()) && !isNaN(marker.position.lng())){
                position['lat'] = marker.position.lat();
                position['lng'] = marker.position.lng();
            }
            var nPins = parseInt($('#countPins').text());
            nPins ++;
            $('#countPins').text(nPins);

        }else{
            marker.setVisible(false);
            //nPins --;
        }



    }

    if(municipio>=1){
        if (typeof position['lat'] !="undefined") {
                map.setCenter(new google.maps.LatLng(position['lat'], position['lng']));
                map.setZoom(10);
        }else{
            alert("No hay puntos para este municipio");
            //map.setCenter(new google.maps.LatLng(ltd, lgt));
            //map.setZoom(zoom);
            $("#btnFiltrarMapaTodos").trigger("click");
        }
    }


}


$($('input[name="filterMapCategroy[]"]')).each(function(){
    if(!$(this).is(':checked')) {
        //alert($(this).val());
        $(this).prop("checked", true);
    }
});



$('input[name="filterMapCategroy[]"][value = 1]').iCheck({
    checkboxClass: 'icheckbox_square-red',
    increaseArea: '20%' // optional
});
$('input[name="filterMapCategroy[]"][value = 2]').iCheck({
    checkboxClass: 'icheckbox_square-green',
    increaseArea: '20%' // optional
});
$('input[name="filterMapCategroy[]"][value = 3]').iCheck({
    checkboxClass: 'icheckbox_square-aero',
    increaseArea: '20%' // optional
});
$('input[name="filterMapCategroy[]"][value = 4]').iCheck({
    checkboxClass: 'icheckbox_square-yellow',
    increaseArea: '20%' // optional
});



$('input[name="filterMapCategroy[]"]').on('ifChanged', function(event){
    filterMakers2();
});


$('#btnFiltrarMapa').click(function(){
    filterMakers2();

});


$("#btnFiltrarMapaTodos").click(function(){
    $('#countPins').text('0');
    for (i = 0; i < markers1.length; i++) {
        marker = gmarkers1[i];
        marker.setVisible(true);

        var nPins = parseInt($('#countPins').text());
        nPins ++;
        $('#countPins').text(nPins);

    }

    /*
    var ltd = 29.3200782;
    var lgt = -116.2321095;
    var zoom = 7;

    map.setCenter(new google.maps.LatLng(ltd, lgt));
    map.setZoom(zoom);*/



    $('input[name="filterMapCategroy[]"]').prop("checked",true);
    $('input[name="filterMapCategroy[]"]').iCheck('update');

});

$('#btnMoreFiltersMap').click(function(e){
    e.preventDefault();
    $('#filterBar').toggleClass('moreFilters');
    $('#btnMoreFiltersMap .btn .fa').toggleClass('fa-rotate-180');
});

$("#btnFiltrarMapaTodos").trigger("click");

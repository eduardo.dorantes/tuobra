<?php
session_start();
require("../../config/conn.php");
require("../../funciones/contratos.class.inc.php");


if(!empty($_POST) and !empty($_POST['id']) and $_SESSION['login'] == true){
	/*array resultado*/
	$resultado = array();
	/*crear objeto contrato*/
	$contrato =  new contratos($conn, $_POST['id']);
	/*comprobar el estatus del contrato*/
	if($contrato->getEstatus()){
		$estatus = $contrato->publicarContrato(0);
		$resultado['estatus'] = false;
		}else{
			$contrato->publicarContrato(1);
			$resultado['estatus'] = true;
			}
	
	/*regresar el estatus del contrato*/
	echo json_encode($resultado);
	}
?>
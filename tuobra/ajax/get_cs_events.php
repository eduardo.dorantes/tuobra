<?php


ini_set('display_errors',1);
error_reporting(E_ALL|E_STRICT);
ini_set('error_log','script_errors.log');
ini_set('log_errors','On');


require('../librerias/query.class.inc.php');
require('../include/functions.php');




$opcion = isset($_POST['opcion']) ? $_POST['opcion'] : '';
$ejercicio_del = isset($_POST['ejercicio-del']) ? $_POST['ejercicio-del'] : '';
$ejercicio_al = isset($_POST['ejercicio-al']) ? $_POST['ejercicio-al'] : '';
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
$dependencia = isset($_POST['dependencia']) ? $_POST['dependencia'] : '';
$estatus = isset($_POST['estatus']) ? $_POST['estatus'] : '';
$q = isset($_POST['q']) ? $_POST['q'] : '';
$search = '%'.$q.'%';



if(empty($_POST) || empty($opcion)){
    die();
}









switch($opcion){

    /*case 'get_month_events';
        $sql_aux = "";
        $array_bind = array();
        $query = new querys();
        $query_string = "SELECT l.id, l.num_licitacion as licitacion, l.descripcion as descripcion, l.fecha_publiacion as fecha,  c.NOMBRE as dependencia, c.NOMBRE_CORTO as acronimo, le.tipo as tipoEvento, str_to_date(le.fecha, '%d/%m/%Y') as fechaEvento, date_format(str_to_date(le.fecha, '%d/%m/%Y'), '%W, %d \d\e %M \d\e %Y') as fechaEventoString, str_to_date(le.hora, '%H:%i:%s') as horaEvento, le.url_trasmision as youtube FROM `licitaciones` l INNER JOIN `clientes` c ON l.id_dependencia = c.ID LEFT JOIN `licitaciones_eventos` le ON le.id_licitacion = l.id AND le.tipo IN (3,4) WHERE MONTH(str_to_date(le.fecha, '%d/%m/%Y')) = MONTH (NOW())  ORDER BY str_to_date(l.fecha_publiacion, '%d/%m/%Y') ASC";
        $data = array();

       if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
              foreach ($rows as $key => $value){
                    $data[$key]['id'] = $value['id'];
                    $data[$key]['title'] = $value['licitacion'];
                    $data[$key]['start'] = $value['fechaEvento'].' '.$value['horaEvento'];
                    $data[$key]['dependencia'] = $value['dependencia'];
                    $data[$key]['descripcion'] = $value['descripcion'];
                    $data[$key]['tipoEvento'] = $value['tipoEvento'];
                    $data[$key]['fechaEvento'] = $value['fechaEvento'];
                    $data[$key]['horaEvento'] = $value['horaEvento'];
                    $data[$key]['youtube'] = youtube_id_from_url($value['youtube']);
                    $data[$key]['fechaEventoString'] = $value['fechaEventoString'];
             }

         }

         echo json_encode($data, JSON_PRETTY_PRINT);
         break;
    */

    case 'get_month_events';
        $sql_aux = "";
        $array_bind = array();
        $query = new querys();
        $query_string = "SELECT l.id, l.num_licitacion as licitacion, l.descripcion as descripcion, l.fecha_publiacion as fecha,  c.NOMBRE as dependencia, c.NOMBRE_CORTO as acronimo, le.tipo as tipoEvento, str_to_date(le.fecha, '%d/%m/%Y') as fechaEvento, date_format(str_to_date(le.fecha, '%d/%m/%Y'), '%W, %d \d\e %M \d\e %Y') as fechaEventoString, str_to_date(le.hora, '%H:%i:%s') as horaEvento, le.url_trasmision as youtube FROM `licitaciones` l INNER JOIN `clientes` c ON l.id_dependencia = c.ID LEFT JOIN `licitaciones_eventos` le ON le.id_licitacion = l.id AND le.tipo IN (3,4) WHERE MONTH(str_to_date(le.fecha, '%d/%m/%Y')) = MONTH (NOW()) and le.url_trasmision !=''  ORDER BY str_to_date(l.fecha_publiacion, '%d/%m/%Y') ASC";
        $data = array();

        if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
            foreach ($rows as $key => $value){
                $data[$key]['id'] = $value['id'];
                $data[$key]['title'] = $value['licitacion'];
                $data[$key]['start'] = $value['fechaEvento'].' '.$value['horaEvento'];
                $data[$key]['dependencia'] = $value['dependencia'];
                $data[$key]['descripcion'] = $value['descripcion'];
                $data[$key]['tipoEvento'] = $value['tipoEvento'];
                $data[$key]['fechaEvento'] = $value['fechaEvento'];
                $data[$key]['horaEvento'] = $value['horaEvento'];
                $data[$key]['youtube'] = youtube_id_from_url($value['youtube']);
                $data[$key]['fechaEventoString'] = $value['fechaEventoString'];
            }

        }

        echo json_encode($data, JSON_PRETTY_PRINT);
        break;


    case 'get_lastest_events';
        $sql_aux = "";
        $array_bind = array();
        $query = new querys();
        $query_string = "SELECT l.id, l.num_licitacion AS licitacion, l.descripcion AS descripcion, l.fecha_publiacion AS fecha, c.NOMBRE AS dependencia, c.NOMBRE_CORTO AS acronimo, le.tipo AS tipoEvento, str_to_date(le.fecha, '%d/%m/%Y') AS fechaEvento, date_format( str_to_date(le.fecha, '%d/%m/%Y'), '%W, %d \d\e %M \d\e %Y' ) AS fechaEventoString, str_to_date(le.hora, '%H:%i:%s') AS horaEvento, le.url_trasmision AS youtube FROM `licitaciones` l LEFT JOIN `clientes` c ON l.id_dependencia = c.ID LEFT JOIN `licitaciones_eventos` le ON le.id_licitacion = l.id AND le.tipo IN (3, 4) WHERE str_to_date( concat(le.fecha, ' ', le.hora), '%d/%m/%Y %H:%i:%s' ) <= NOW() and le.url_trasmision !='' ORDER BY str_to_date( concat(le.fecha, ' ', le.hora), '%d/%m/%Y %H:%i:%s' ) DESC LIMIT 10";
        $data = array();

        if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
            /*
             * foreach ($rows as $key => $value){
                $data[$key]['id'] = $value['id'];
                $data[$key]['title'] = $value['licitacion'];
                $data[$key]['start'] = $value['fechaEvento'].' '.$value['horaEvento'];
                $data[$key]['dependencia'] = $value['dependencia'];
                $data[$key]['descripcion'] = $value['descripcion'];
                $data[$key]['tipoEvento'] = $value['tipoEvento'];
                $data[$key]['fechaEvento'] = $value['fechaEvento'];
                $data[$key]['horaEvento'] = $value['horaEvento'];
                $data[$key]['youtube'] = youtube_id_from_url($value['youtube']);
                $data[$key]['fechaEventoString'] = $value['fechaEventoString'];
            }

            */

            $data = $rows;

        }

        echo json_encode($data, JSON_PRETTY_PRINT);
        break;



}

?>
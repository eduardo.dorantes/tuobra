<?php
session_start();
if(empty($_SESSION['id-usuario']) || !isset($_SESSION['login']) || $_SESSION['login'] != true || $_SESSION['tipo']==0){
	header("LOCATION: home/");
}


?>

<div class="contenedor-opciones-parametros">
	<!-- opciones de parametros -->
	<ul>
		<li><a href="javascript:;" id="cliente" data-id="cliente" class="current-tab tab-parametros">Organismos</a></li>
		<li><a href="javascript:;" id="usuario" data-id="usuario" class="tab-parametros">Usuarios</a></li>
		<!--li><a href="javascript:;" id="reporte" data-id="reporte" class="tab-parametros">Reporte de Avance</a></li-->
	</ul>
</div>
<div id="contenido-tabs"><?php include('../forms/add-cliente.php'); ?></div>


	<script type="text/javascript">
		var op_tab;
		$(".tab-parametros").click(function(){
			if(op_tab !== $(this).attr('data-id') || op_tab !== ''){

				var url;

				$(".tab-parametros").removeClass('current-tab');

				$(this).addClass('current-tab');

				switch($(this).attr('data-id')){
					case 'cliente':
						url = 'add-cliente';
						break;
					case 'usuario':
						url = 'add-usuario';
						break;

				}

				$.post('forms/' + url + '.php',
					function(contenido){
						$("#contenido-tabs").html(contenido);
				});
			}
			op_tab = $(this).attr('data-id');
		});
	</script>
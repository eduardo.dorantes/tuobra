<?php
session_start();
require('../../funciones/query.class.inc.php');


/*checar si es post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*crear objeto query*/
	$query = new querys();
	/*get id de la obra*/
	$id_obra = $_POST['id'];
	
	/*comprobar que el id de la obra no venga vacio*/
	if(!empty($id_obra)){
		/*ejecutar y comprobar queery*/
		if($rows = $query->traerMultiplesResultados("SELECT `id`, `fecha`, `descripcion`, `fecha_hora` FROM `bitacora_obra` WHERE `id_obra` = :id_obra", $array_bind = array(':id_obra' => $id_obra))){
			
			/*comprobar que venga regitors*/
			if(is_array($rows)){
				/*set ini contador*/
				$cont = 1;
				/*table*/
				echo "<table class=\"zebra\" width=\"\100%\"><thead><tr><th>#</th><th>Fecha</th><th>Descripción</th><th></th></tr></thead><tbody>";
				
				/*loop registros*/
				foreach($rows as $row){
					echo "<tr>";
						echo "<td align=\"center\">".$cont."</td>";
						echo "<td align=\"center\">".$row['fecha']."</td>";
						echo "<td>".$row['descripcion']."</td>";
						echo "<td></td>";
					echo "</tr>";
					
					$cont++;
					}
				
				/*cerrar tabla*/
				echo "</tbody></table>";
				}
			}
		}
	}
?>
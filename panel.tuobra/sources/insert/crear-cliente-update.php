<?php
session_start();
include("../../config/conn.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");
require("../../funciones/clientes.class.inc.php");

/*comprobar que sea post*/
if(!empty($_POST) and !empty($_POST['id-cliente']) and $_SESSION['login'] == true){
	/*crear un nuevo objeto cliente*/
	$cliente = new clientes($conn, $_SESSION['id-cliente']);
	/*reicibr las varibles*/
	$nombre = $_POST['nombre'];
	$nombre_corto = $_POST['nombre_corto'];
	$direccion = $_POST['direccion'];
	$municipio = $_POST['municipio'];
	$telefono = $_POST['telefono'];
	$email = $_POST['email'];
	$responsable = $_POST['nombre-responsable'];
	$custom_url = $_POST['url-cliente'];
	
	/*comprobar los campos requeridos*/
	/*validar que venga el nombre*/
	if(validar::notEmpty($nombre) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."1</div>";
		exit;
		}
	/*validar el municipio
	if(validar::notEmpty($municipio) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."2</div>";
		exit;
		}*/
	/*validar el responsable*/
	if(validar::notEmpty($responsable) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."3</div>";
		exit;
		}
	/*validar el correo electronico*/
	if(validar::notEmpty($email) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."4</div>";
		exit;
		}
	/*validar la custom url*/
	if(validar::notEmpty($custom_url) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."5</div>";
		exit;
		}
	/*actulizar objeto cliente*/
	$update_cliente = $cliente->updateCliente($nombre, $nombre_corto, $direccion, $telefono, $municipio, $responsable, $email, $custom_url, '', '');
	
	if($update_cliente == true){
		echo "<div class=\"success\">".$mensaje['1002']."</div>";
		}else{
			echo $update_cliente;
			}
	}
?>
<?php
class conveniosModificatorios{
	
	var $db;//conexio a la base de datos
	private $id_convenio;//id del conveios modificatorio
	private $id_contrato;//id del contrato del convenio
	private $fecha;//fecha del convenio modificatorio
	private $tipo;//tipo del convenios modiificatorio
	private $tipoLetra;//tipo de convenio modificatorio en letra
	private $fechaInicio;//fecha nueva de incio del contrato
	private $fechaTermino;//fecha nueva de termino del contrato
	private $monto;//monto de amplicacion del contrato
	private $objeto;//nuevo objeto del contrato
	private $documento;//path hacia el documento pdf del convenio
	private static $tipoConvenio = array(1 => 'Plazo', 3 => 'Monto', 4 => 'Plazo/Monto', 2 => 'Objeto');//array de tipos de convenios modificatorios por letra
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		//conexion a la base de datos
		$this->db = $conn;
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			//set el id del estudio
			$this->id_convenio = $id;
			//cargar los datos del convenio
			$this->datosConvenios();
			}
		}
	/*crear el objeto convejo modificatorio*/
	public function crearConvenio($id_contrato, $id_usuario, $fecha_, $tipo_, $fecha_inicio, $fecha_termino, $monto_, $objeto_){
		/*query*/
		$sql_query = "INSERT INTO `convenios_modificatorios` (ID_CONTRATO, ID_USUARIO, FECHA, TIPO, OBJETO, MONTO, FECHA_INICIO, FECHA_TERMINO) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			echo trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('iisisdss',$id_contrato, $id_usuario, $fecha_, $tipo_, $objeto_, $monto_, $fecha_inicio, $fecha_termino);
		/*ejecutar query*/
		$query->execute();
		/*el ultimo id registrado*/
		$this->id_convenio = $query->insert_id;
		/*el numero de contrato*/
		$this->id_contrato = $id_contrato;
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*actulizar el objeto convenio modificatorio*/
	public function actulizarConvenio($fecha_, $tipo_, $fecha_inicio, $fecha_termino, $monto_, $objeto_){
		/*query*/
		$sql_query = "UPDATE `convenios_modificatorios` SET FECHA = ?, TIPO = ?, OBJETO = ?, MONTO = ?, FECHA_INICIO = ?, FECHA_TERMINO = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			echo trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('sisdssi',$fecha_, $tipo_, $objeto_, $monto_, $fecha_inicio, $fecha_termino, $this->id_convenio);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query){
			return true;
			}else{
				return false;
				}
		}
	/*actulizar documento*/
	public function actulizarDocumentoConvenio($documento){
		/*query*/
		$sql_query = "UPDATE `convenios_modificatorios` SET DOCUMENTO = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			echo trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('si', $documento, $this->id_convenio);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query){
			return true;
			}else{
				return false;
				}
		}
	/*borrar el objeto convenio modificatorios*/
	public function borrarConvenio(){
		/*query*/
		$sql_query = "DELETE FROM `convenios_modificatorios` WHERE ID = ?;";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			echo trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('i', $this->id_convenio);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se aya borrado el registor*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*cargar la informacion del convenio*/
	private function datosConvenios(){
		if(!empty($this->id_convenio) and strlen($this->id_convenio) > 0){
			/*query*/
			$sql_query = "SELECT ID_CONTRATO, FECHA, TIPO, OBJETO, MONTO, FECHA_INICIO, FECHA_TERMINO, DOCUMENTO FROM `convenios_modificatorios` WHERE ID = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				echo trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_convenio);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($contrato, $fecha_, $tipo_, $objeto_, $monto_, $fecha_incio, $fecha_termino, $doc);
			/*fetch datos*/
			$query->fetch();
			/*datos de la unidad*/
			$this->fecha = $fecha_;
			$this->tipo = $tipo_;
			$this->tipoLetra = self::tipoLetra($tipo_);
			$this->monto = $monto_;
			$this->objeto = $objeto_;
			$this->fechaInicio = $fecha_incio;
			$this->fechaTermino = $fecha_termino;
			$this->documento = $doc;
			$this->id_contrato = $contrato;
			
			/*cerrar conexiones*/
			$query->close();
			}
		}
	/*tipo de convejo modificatorio en letra*/
	private static function tipoLetra($tipo){
		return self::$tipoConvenio[$tipo];
		}
	/*get fecha del convenio*/
	public function getFecha(){
		return $this->fecha;
		}
	/*get tipo de convenio*/
	public function getTipo(){
		return $this->tipo;
		}
	/*get tipo de convenio letra*/
	public function getTipoLetra(){
		return $this->tipoLetra;
		}
	/*get nuevo objeto del contrato*/
	public function getObejto(){
		return $this->objeto;
		}
	/*get el nuevo monto del contrato*/
	public function getMonto(){
		return $this->monto;
		}
	/*get nueva fecha de inicio*/
	public function getFechaInicio(){
		return $this->fechaInicio;
		}
	/*get nueva fecha de termino*/
	public function getFechaTermino(){
		return $this->fechaTermino;
		}
	/*get el documento convenio*/
	public function getDocumentoConvenio(){
		return $this->documento;
		}
	/*get el id del contrato*/
	public function getIdContrato(){
		return $this->id_contrato;
		}
	}
?>
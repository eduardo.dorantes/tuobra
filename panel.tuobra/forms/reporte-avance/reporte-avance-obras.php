<?php
require('../../funciones/query.class.inc.php');
require('../../funciones/config.php');

session_start();
if(empty($_SESSION['id-usuario']) || !isset($_SESSION['login']) || $_SESSION['login'] != true){
    header("LOCATION:home/");
}




function encode_items(&$item, $key)
{
    $item = utf8_decode($item);
}



// fetch the data

$cSql ="";

if($_SESSION['tipo']==1) {

    if (!empty($_POST['organismos']) && $_POST['organismos'] != 'all') {
        $organismo = $_POST['organismos'];
        $organismo = intval($organismo);

        if ($organismo > 0) {
            $cSql = "AND ID_ORGANISMO='" . $organismo . "' ";
        }
    }
}else{
    $cSql = "AND ID_ORGANISMO='" . $_SESSION['id-cliente'] . "' ";
}


// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte-de-Avance-de-Obras-'.$organismo.'-'.date("Y-m-d-H-i-s").'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');




$q = "SELECT
	ORGANISMO,
	NUM_CONTRATO,
	EJERCICIO,
	IF(ID_OBRA_COMPLEMENTARIA=0,'NO','SI') AS OBRA_COMPLEMENTARIA,
	IF(COALESCE(OC_NUMERO_CONTRATO,'')='','N/A','SI') AS NUM_OBRA_COMPLEMENTARIA,
	IF(COALESCE(NUM_LICITACION,'')='',IF( ID_TIPO_MODALIDAD = 3,'N/A','NO'),'SI') AS NUM_LICITACION,
    IF(COALESCE(OBJETO,'')='','NO','SI') AS OBJETO,
	IF(COALESCE(MODALIDAD,'')='','NO','SI') AS MODALIDAD,
	IF(COALESCE(TIPO_CONTRATO,'')='','NO','SI') AS TIPO_CONTRATO,
	IF(COALESCE(TIPO_OBRA,'')='','NO','SI') AS TIPO_OBRA,
	IF(COALESCE(TOTAL_CONTRATO,'')='','NO','SI') AS TOTAL_CONTRATO,
	IF(COALESCE(AVANCE,'')='','NO','SI') AS AVANCE,
	IF(COALESCE(FECHA_AVANCE,'')='','NO','SI') AS FECHA_AVANCE,	
	IF(DOCUMENTOS='NO','NO','SI') AS DOCUMENTOS,
	IF(FOTOGRAFIAS='NO','NO','SI') AS FOTOGRAFIAS,
	IF(COALESCE(MUNICIPIO,'')='','NO','SI') AS MUNICIPIO,
	IF(COALESCE(LOCALIDAD,'')='','NO','SI') AS LOCALIDAD,
	IF(COALESCE(LATITUD,'')='','NO','SI') AS LATITUD,
	IF(COALESCE(LONGITUD,'')='','NO','SI') AS LONGITUD,
	IF(COALESCE(BENEFICIADOS,'')='','NO','SI') AS BENEFICIADOS,
	IF(COALESCE(UA_SOLICITANTE,'')='','NO','SI') AS UA_SOLICITANTE,
	IF(COALESCE(UA_RESPONSABLE,'')='','NO','SI') AS UA_RESPONSABLE,
	IF(COALESCE(CONTRATISTA,'')='','NO','SI') AS CONTRATISTA,
	IF(COALESCE(RESIDENTE,'')='','NO','SI') AS RESIDENTE,
	IF(COALESCE(SUPERVISOR_EXTERNO,'')='','NO','SI') AS SUPERVISOR_EXTERNO,
	IF(COALESCE(FECHA_CONTRATO,'')='','NO','SI') AS FECHA_CONTRATO,
	IF(COALESCE(FECHA_INICIO,'')='','NO','SI') AS FECHA_INICIO,
	IF(COALESCE(FECHA_TERMINO,'')='','NO','SI') AS FECHA_TERMINO,
	IF(COALESCE(FECHA_ENTREGA,'')='','NO','SI') AS FECHA_ENTREGA
FROM
	vcontratosod
WHERE 

(
documentos = 'NO' 
OR fotografias = 'NO'
OR (ID_OBRA_COMPLEMENTARIA=1 AND COALESCE(OC_NUMERO_CONTRATO,'')='')
OR (ID_TIPO_MODALIDAD != 3 AND COALESCE(NUM_LICITACION,'')='')
OR COALESCE(OBJETO,'')=''
OR COALESCE(MODALIDAD,'')=''
OR COALESCE(TIPO_CONTRATO,'')=''
OR COALESCE(AVANCE,'')=''
OR COALESCE(FECHA_AVANCE,'')=''
OR COALESCE(MUNICIPIO,'')=''
OR COALESCE(LOCALIDAD,'')=''
OR COALESCE(LATITUD,'')=''
OR COALESCE(LONGITUD,'')=''
OR COALESCE(BENEFICIADOS,'')=''
OR COALESCE(UA_SOLICITANTE,'')=''
OR COALESCE(UA_RESPONSABLE,'')=''
OR COALESCE(CONTRATISTA,'')=''
OR COALESCE(RESIDENTE,'')=''
/*OR COALESCE(SUPERVISOR_EXTERNO,'')=''*/
OR COALESCE(FECHA_CONTRATO,'')=''
OR COALESCE(FECHA_TERMINO,'')=''
OR COALESCE(FECHA_INICIO,'')=''
OR COALESCE(FECHA_ENTREGA,'')='' 

) 

".$cSql."
order by ORGANISMO asc";

$a = $t= array();
$query = new querys();
$r = $query->traerMultiplesResultados($q, $a);

if(count($r)>0){


array_walk_recursive($r, 'encode_items');

$t = array_keys($r[0]);

// output the column headings
fputcsv($output, $t);


foreach($r as $v){
    fputcsv($output, $v);
}

}else{
	echo 'No se encontraron resultados.';
}





?>
<?php
require('../librerias/query.class.inc.php');
require('../librerias/config.php');
/*crear objeto query*/







$query = new querys();


$tipos_obra = $query->traerMultiplesResultados('SELECT id, descripcion FROM `tipo_obra` GROUP BY id, descripcion ORDER BY descripcion', NULL);

$array_tipoObras = array();
foreach ($tipos_obra as $k => $v){
    $array_tipoObras[$v['id']] = $v['descripcion'];
}



/*deterctar post*/
if(!empty($_POST)){
	/*set sql aux*/
	$sql_aux = "";
	/*set array bind*/
	$array_bind = array();
	/*recibir las vairbles*/
	$ejercicio_del = $_POST['ejercicio-del'];
	$ejercicio_al = $_POST['ejercicio-al'];
	$municipio = $_POST['municipio'];
	$modalidad = $_POST['modalidad'];
	$dependencia = $_POST['ejecutora'];
	$tipo_obra = $_POST['tipo'];
	$estatus = $_POST['estatus'];
	$localidad = '%'.$_POST['localidad-colonia'].'%';
	$search = '%'.$_POST['descripcion'].'%';


    $tipo_contrato = $_POST['contrato-al'];

	
	/*set ejercicio array bind*/
	$array_bind[':eje_del'] = $ejercicio_del;
	$array_bind[':eje_al'] = $ejercicio_al;
	$array_bind[':search'] = $search;
	$array_bind[':localidad'] = $localidad;
	
	/*municipi*/
	if(!empty($municipio)){
		$sql_aux .= " AND V.ID_MUNICIPIO = :municipio";
		$array_bind[':municipio'] = $municipio;
		}
	/*modalidad*/
	if(!empty($modalidad)){
		$sql_aux .= " AND V.TIPO_MODALIDAD = :modalidad";
		$array_bind[':modalidad'] = $modalidad;
		}
	/*dependencia*/
	if(!empty($dependencia)){
		$sql_aux .= " AND V.ID_CLIENTE = :dependencia";
		$array_bind[':dependencia'] = $dependencia;
		}
	/*tipo de obra*/
	if(!empty($tipo_obra)){
		$sql_aux .= " AND V.TIPO_OBRA = :tipo_obra";
		$array_bind[':tipo_obra'] = $tipo_obra;
		}
    if(!empty($tipo_contrato)){
        $sql_aux .= " AND V.TIPO_CONTRATO = :tipo_contrato";
        $array_bind[':tipo_contrato'] = $tipo_contrato;
    }




	/*estatus*/
	if(!empty($estatus)){
		switch($estatus){
			case 1:
				$sql_aux .= " AND (V.por_avance IS NULL OR V.por_avance < 100)";
			break;
			case 2:
				$sql_aux .= " AND V.por_avance >= 100";
			break;
			}
		}
	
	/*query sgtring*/
	$query_string = "SELECT V.ID, V.TIPO_MODALIDAD, V.TIPO_CONTRATO, V.TIPO_OBRA, V.NUM_CONTRATO, V.OBJETO, V.TOTAL_CONTRATO, V.TIPO_OBRA, V.nombre_municipio as MUNICIPIO, str_to_date(V.FECHA_CONTRATO, '%d/%m/%Y') as FECHA_CONTRATO, V.RAZON_SOCIAL, C.NOMBRE FROM `vistacontratos` V left join clientes C ON (C.ID = V.ID_CLIENTE) WHERE V.EJERCICIO BETWEEN :eje_del AND :eje_al AND (V.NUM_CONTRATO LIKE :search OR V.OBJETO LIKE :search OR V.RAZON_SOCIAL LIKE :search) AND (V.LOCALIDAD LIKE :localidad) $sql_aux ORDER BY str_to_date(V.FECHA_CONTRATO, '%d/%m/%Y') DESC";

	/*ejecutar y comprobar query*/
	if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
		echo "<table class=\"table table-bordered table-hover table-striped\" border=\"0\" width=\"100%\">";
		echo "<thead><tr>";
			//echo "<th></th>";
        echo "<th class='col-sm-2'>Detalles</th>";
			echo "<th class='col-xs-12 col-sm-2'>No.</th>";
			echo "<th class='col-sm-1'>Fecha</th>";
			echo "<th class='col-sm-3'>Objeto</th>";

			echo "<th class='col-sm-2'>Municipio</th>";
			echo "<th class='col-sm-2'>Dependencia / Organismo</th>";
			echo "<th class='col-sm-2'>Inversión</th>";
		echo "</thead></tr><tbody>";
		/*LOOP*/
		foreach($rows as $row){
			echo "<tr>";
				//echo "<td align=\"center\"> </a></td>";
                echo "<td><a title='Ver obra: ".$row['NUM_CONTRATO']." ' href=\"obra/".$row['NUM_CONTRATO']."\" class=\"detalles detalles-licitacion\"><label class='label label-success'>Tipo de Obra:</label><br><small>".$array_tipoObras[$row['TIPO_OBRA']]." </small> <br><label class='label label-warning'>Tipo de Modalidad:</label> <br><small> ".$array_modalidad[$row['TIPO_MODALIDAD']]."</small> <br> <label class='label label-info'>Tipo de Contrato:</label><br><small>  ".$array_tipoContratos[$row['TIPO_CONTRATO']]."</small> </a></td>";
				echo "<td><a title='Ver obra: ".$row['NUM_CONTRATO']." ' href=\"obra/".$row['NUM_CONTRATO']."\" class=\"detalles detalles-licitacion\">".$row['NUM_CONTRATO']."</a></td>";
				echo "<td><a title='Ver obra: ".$row['NUM_CONTRATO']." ' href=\"obra/".$row['NUM_CONTRATO']."\" class=\"detalles detalles-licitacion\">".$row['FECHA_CONTRATO']."</a></td>";
				echo "<td><a title='Ver obra: ".$row['NUM_CONTRATO']." ' href=\"obra/".$row['NUM_CONTRATO']."\" class=\"detalles detalles-licitacion\">".$row['OBJETO']."</a></td>";
				echo "<td><a title='Ver obra: ".$row['NUM_CONTRATO']." ' href=\"obra/".$row['NUM_CONTRATO']."\" class=\"detalles detalles-licitacion\">".$row['MUNICIPIO']."</a></td>";

				echo "<td><a title='Ver obra: ".$row['NUM_CONTRATO']." ' href=\"obra/".$row['NUM_CONTRATO']."\" class=\"detalles detalles-licitacion\">".$row['NOMBRE']."</a></td>";

			echo "<td><a title='Ver obra: ".$row['NUM_CONTRATO']." ' href=\"obra/".$row['NUM_CONTRATO']."\" class=\"detalles detalles-licitacion inversion\">$ ".number_format($row['TOTAL_CONTRATO'], 2)."</a></td>";
			echo "</tr>";
			}
		/*cerrar tabla*/
		echo "</tbody></table>";
		}
	}

	//echo json_encode($rows, JSON_PRETTY_PRINT);
?>
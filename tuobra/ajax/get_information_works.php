<?php


ini_set('display_errors',1);
error_reporting(E_ALL|E_STRICT);
ini_set('error_log','script_errors.log');
ini_set('log_errors','On');


require('../librerias/query.class.inc.php');
require('../include/functions.php');




$opcion = isset($_POST['opcion']) ? $_POST['opcion'] : '';
$year = isset($_POST['year']) ? intval($_POST['year']) : '';


/*$ejercicio_del = isset($_POST['ejercicio-del']) ? $_POST['ejercicio-del'] : '';
$ejercicio_al = isset($_POST['ejercicio-al']) ? $_POST['ejercicio-al'] : '';
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : '';
$dependencia = isset($_POST['dependencia']) ? $_POST['dependencia'] : '';
$estatus = isset($_POST['estatus']) ? $_POST['estatus'] : '';
$q = isset($_POST['q']) ? $_POST['q'] : '';
$search = '%'.$q.'%';*/



if(empty($_POST) || empty($opcion)){
    die();
}









switch($opcion){
    case 'get_inversion_works':
        $sql_aux = "";
        $array_bind = array();
        $query = new querys();



        $query_string = "SELECT SUM(contratos.TOTAL_CONTRATO) as inversion, YEAR(str_to_date(contratos.FECHA_CONTRATO, '%d/%m/%Y')) as year FROM contratos WHERE YEAR(str_to_date(contratos.FECHA_CONTRATO, '%d/%m/%Y')) = YEAR(NOW()) ";
        $data = array();

       if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
              foreach ($rows as $key => $value){
                    $data[$key]['inversion'] = $value['inversion'];
                    $data[$key]['year'] = $value['year'];
             }
         }


         echo json_encode($data, JSON_PRETTY_PRINT);
         break;



    case 'get_information_works':

        $filterYear ="";
        $filterYear2 ="";
        $filterYear3 ="";
        if($year!=""){
            $filterYear = "WHERE YEAR ( str_to_date(o.FECHA_CONTRATO, '%d/%m/%Y')) = '". $year."' GROUP BY YEAR ( str_to_date(o.FECHA_CONTRATO, '%d/%m/%Y'))";
            $filterYear2 = "AND YEAR(str_to_date(c2.FECHA_CONTRATO, '%d/%m/%Y')) = '".$year."' ";
            $filterYear3 = "WHERE YEAR(str_to_date(c3.FECHA_CONTRATO, '%d/%m/%Y')) = '".$year."' ";
        }
/*
        SELECT
	COUNT(av.ID_CONTRATO)
FROM
	avances as av
LEFT JOIN contratos as c2
ON (av.ID_CONTRATO=c2.ID)
WHERE
	av.AVANCE = 100


*/

        $sql_aux = "";
        $array_bind = array();
        $query = new querys();
        //$query_string = "SELECT count(ID) AS Obras, COUNT(DISTINCT(o.ID_CONTRATISTA)) AS Contratistas, SUM(o.BENIFICIADOS) AS Beneficiados, SUM(o.TOTAL_CONTRATO) AS totalObras, SUM(IF(o.TIPO_CONTRATO = 1, 1, 0)) AS tc_1, SUM(IF(o.TIPO_CONTRATO = 2, 1, 0)) AS tc_2, SUM(IF(o.TIPO_CONTRATO = 3, 1, 0)) AS tc_3, SUM(IF(o.TIPO_MODALIDAD = 1, 1, 0)) AS tm_1, SUM(IF(o.TIPO_MODALIDAD = 2, 1, 0)) AS tm_2, SUM(IF(o.TIPO_MODALIDAD = 3, 1, 0)) AS tm_3, SUM(IF(o.TIPO_MODALIDAD = 4, 1, 0)) AS tm_4, SUM( IF ( o.ID_CLIENTE IN (3, 7, 9), o.TOTAL_CONTRATO, 0 )) AS t_Infraestructura, SUM(IF(o.ID_CLIENTE IN(3, 7, 9), 1, 0)) AS c_Infraestructura, SUM( IF ( o.ID_CLIENTE = 2, o.TOTAL_CONTRATO, 0 )) AS t_Educacion, SUM(IF(o.ID_CLIENTE = 2, 1, 0)) AS c_Educacion, SUM( IF ( o.ID_CLIENTE IN (1, 9), o.TOTAL_CONTRATO, 0 )) AS t_Caminos, SUM(IF(o.ID_CLIENTE IN(1, 9), 1, 0)) AS c_Caminos, SUM( IF ( o.ID_CLIENTE = 4, o.TOTAL_CONTRATO, 0 )) AS t_Salud, SUM(IF(o.ID_CLIENTE = 4, 1, 0)) AS c_Salud, SUM( IF ( str_to_date(o.FECHA_TERMINO, '%d/%m/%Y') < NOW(), 1, 0 )) AS 'Finalizadas', SUM( IF ( str_to_date(o.FECHA_TERMINO, '%d/%m/%Y') >= NOW(), 1, 0 )) AS 'Proceso', YEAR ( str_to_date(o.FECHA_CONTRATO, '%d/%m/%Y')) AS Anio FROM contratos o WHERE 1=1";
        $query_string = "SELECT count(ID) AS Obras, COUNT(DISTINCT(o.ID_CONTRATISTA)) AS Contratistas, SUM(o.BENIFICIADOS) AS Beneficiados, (SELECT COUNT(DISTINCT(mc.idMunicipio))
FROM municipiosContratos mc INNER JOIN contratos c3 ON mc.idContrato = c3.ID ".$filterYear3.")AS municipios, SUM(o.TOTAL_CONTRATO) AS totalObras, SUM(IF(o.TIPO_CONTRATO = 1, 1, 0)) AS tc_1, SUM(IF(o.TIPO_CONTRATO = 2, 1, 0)) AS tc_2, SUM(IF(o.TIPO_CONTRATO = 3, 1, 0)) AS tc_3, SUM( IF ( o.TIPO_MODALIDAD = 1, o.TOTAL_CONTRATO, 0 )) AS tm_1, SUM( IF ( o.TIPO_MODALIDAD = 2, o.TOTAL_CONTRATO, 0 )) AS tm_2, SUM( IF ( o.TIPO_MODALIDAD = 3, o.TOTAL_CONTRATO, 0 )) AS tm_3, SUM( IF ( o.TIPO_MODALIDAD = 4, o.TOTAL_CONTRATO, 0 )) AS tm_4, SUM( IF ( o.TIPO_MODALIDAD NOT IN (1, 2, 3, 4), o.TOTAL_CONTRATO, 0 )) AS tm_5, SUM( IF ( o.TIPO_OBRA = 2, o.TOTAL_CONTRATO, 0 )) AS t_Infraestructura, SUM(IF(o.TIPO_OBRA = 2, 1, 0)) AS c_Infraestructura, SUM( IF ( o.TIPO_OBRA = 1, o.TOTAL_CONTRATO, 0 )) AS t_Educacion, SUM(IF(o.TIPO_OBRA = 1, 1, 0)) AS c_Educacion, SUM( IF ( o.TIPO_OBRA = 4, o.TOTAL_CONTRATO, 0 )) AS t_Caminos, SUM(IF(o.TIPO_OBRA = 4, 1, 0)) AS c_Caminos, SUM( IF ( o.TIPO_OBRA = 3, o.TOTAL_CONTRATO, 0 )) AS t_Salud, SUM(IF(o.TIPO_OBRA = 3, 1, 0)) AS c_Salud, ( SELECT COUNT(av.ID_CONTRATO) FROM avances AS av LEFT JOIN contratos AS c2 ON (av.ID_CONTRATO = c2.ID) WHERE av.AVANCE = 100 ".$filterYear2." ) AS 'Finalizadas', YEAR ( str_to_date(o.FECHA_CONTRATO, '%d/%m/%Y')) AS Anio FROM contratos o ".$filterYear;
        //YEAR ( str_to_date(o.FECHA_CONTRATO, '%d/%m/%Y')) = YEAR (NOW()) GROUP BY YEAR ( str_to_date(o.FECHA_CONTRATO, '%d/%m/%Y'))

//WHERE  YEAR ( str_to_date(o.FECHA_CONTRATO, '%d/%m/%Y')) = '".$filterYear."'



        $data = array();

        if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){




            foreach ($rows as $k => $v){

                $data[$k]['inversion'] = $v['totalObras'];


                //$data[$k]['year'] = $v['Anio'];
                //$data[$k]['year'] ='2016';
                $data[$k]['works'] = $v['Obras'];
                $data[$k]['benefited'] = $v['municipios'];
                $data[$k]['contractors'] = $v['Contratistas'];

                $data[$k]['roads'] = array('Caminos y Carreteras',$v['t_Caminos'],$v['c_Caminos']);
                $data[$k]['education'] = array('Infraestructura Educativa',$v['t_Educacion'],$v['c_Educacion']);
                $data[$k]['health'] = array('Sector Salud',$v['t_Salud'],$v['c_Salud']);
                $data[$k]['infraestructure'] =  array('Infraestructura',$v['t_Infraestructura'],$v['c_Infraestructura']);


                $data[$k]['tc'] = array();

                $data[$k]['tc'][] = array('Obra Pública',$v['tc_1']);
                $data[$k]['tc'][] = array('Equipamiento',$v['tc_2']);
                $data[$k]['tc'][] = array('Servicios',$v['tc_3']);


                $data[$k]['tm'] = array();

                $data[$k]['tm'][] = array('Licitación Pública',round($v['tm_1'],2));
                $data[$k]['tm'][] = array('Licitación Simplificada',round($v['tm_2'],2));
                $data[$k]['tm'][] = array('Adjudicación Directa',round($v['tm_3'],2));
                $data[$k]['tm'][] = array('Administración Directa',round($v['tm_4'],2));
                //$data[$k]['tm'][] = array('Otros',round($v['tm_5'],2));


                $data[$k]['s'] = array();
                $data[$k]['s'][] = array('Finalizadas',$v['Finalizadas']);
                $data[$k]['s'][] = array('En proceso',$v['Obras']-$v['Finalizadas']);

            }

            //$data = $rows;

        }


        echo json_encode($data, JSON_PRETTY_PRINT);


        break;




    case 'get_lastest_finished_works':
        $sql_aux = "";
        $array_bind = array();
        $query = new querys();
        $query_string = "SELECT o.ID AS id, o.TIPO_OBRA as idTipo, o.OBJETO AS objeto, t.descripcion AS tipo, f.THUMB_LARGE AS thumbnail, o.TOTAL_CONTRATO AS monto, m.nombre_municipio AS municipio, o.NUM_CONTRATO AS contrato, a.AVANCE AS avance, str_to_date(a.FECHA, '%d/%m/%Y') AS fecha FROM avance_fotografico f LEFT JOIN contratos o ON f.ID_CONTRATO = o.ID LEFT JOIN municipios m ON m.id = o.MUNICIPIO LEFT JOIN avances a ON a.ID_CONTRATO = o.ID LEFT JOIN tipo_obra t ON o.TIPO_OBRA = t.ID AND m.id_entidad = 26 WHERE a.AVANCE = 100 GROUP BY o.ID ORDER BY fecha DESC LIMIT 9";
        $data = array();

        if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
            /*foreach ($rows as $key => $value){
                $data[$key]['inversion'] = $value['inversion'];
                $data[$key]['year'] = $value['year'];
            }*/

            $data = $rows;



        }


        echo json_encode($data, JSON_PRETTY_PRINT);
        break;







}

?>
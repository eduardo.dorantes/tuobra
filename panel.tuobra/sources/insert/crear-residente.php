<?php
session_start();
require("../../config/conn.php");
require("../../funciones/residente.class.inc.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");

/*comprobar que se aya hecho post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*crear un nuevo objeto residente*/
	$residente = new residentes($conn, $_POST['id-residente']);
	/*crear un objeto formulario*/
	$formulario = new validar();
	/*resivir las varibles*/
	$id_residente = $_POST['id-residente'];
	$nombre = $_POST['nombre-residente'];
	$datos = $_POST['datos-residente'];
	/*objeto para determinar si se creo o modifico el objeto*/
	$comprobar_objeto = false;
	
	/*comprobar que las varbiles requeridas no vengan vacias*/
	if($formulario->notEmpty($nombre) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
		
	/*comprobar si hay que crear o actulizar el objeto residente*/
	if(empty($id_residente) and strlen($id_residente) == 0){
		$comprobar_objeto = $residente->crearResidente($_SESSION['id-cliente'], $nombre, $datos);
		}else{
			$comprobar_objeto = $residente->updateResidente($nombre, $datos);
			}
			
	/*comprobar que se aya guardados los datos*/
	if($comprobar_objeto == true){
		echo "<script type=\"text/javascript\">alert('".$mensaje['1002']."');$(\"#contenedor-datos-catalogo\").remove();</script>";
		}
	}
?>
<?php
$url = substr($_SERVER['REQUEST_URI'], 1);
$partir_url = explode("/", $url);
$evento = $partir_url[count($partir_url) - 2];
?>
<!-- testigo virtual ciudadano contenedor -->
<hr />
<div class="m-t-b row">
	<!-- texto -->

	<div class="col-sm-8">
		<p class="text-justify">

			<i class="fa fa-info-circle fa-3x pull-left" aria-hidden="true"></i>

			<?php
			switch($evento){
				case 'licitacion':
					echo "<span>Si desea hacer comentarios u observaciones sobre la licitación le invitamos a utilizar nuestra forma de contacto desde la cual puede adjuntar la documentación que usted considere necesaria.</span>";
					break;
				case 'obra':
					echo "<span>Si desea hacer comen arios u observaciones sobre la obra, le invitamos a utilizar nuestra forma de contacto desde la cual puede adjuntar la documentación que usted considere necesaria.</span>";
					break;
			}
			?>
		</p>

	</div>

	<div class="col-sm-4">
		<p class="text-center">
			<a href="testigo/<?php echo $evento.'/'.$_GET['num']; ?>" class="btn btn-success"> <i class="fa fa-comments" aria-hidden="true"></i> Comentar</a>
			<a href="javascript: history.back();" class="btn btn-default"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Regresar</a>
		</p>
	</div>

    <!-- acciones -->



</div>

<hr/>


<?php
/***********************************/
/* 	RESIDENTES  */
session_start();
require("../config/conn.php");

if($_SESSION['login'] != true){
	exit;
	}
?>
<div class="acciones-ventana">
	<a href="#" class="btn icon-agregar">Agregar</a>
    <a href="#" class="btn icon-delete-uno">Eliminar</a>
</div>
<div class="lista-ventana">
<?php
	/*query*/
 	$sql_query = "SELECT R.ID, R.NOMBRE, R.DATOS FROM `residentes` R WHERE R.ID_CLIENTE = ?;";
	/*preparar el query*/
	$query = $conn->prepare($sql_query);
	/*comprobar el query*/
	if($query === false){
		trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $query->error, E_USER_ERROR);
		}
	/*bind parametros*/
	$query->bind_param('i', $_SESSION['id-cliente']);
	/*executar query*/
	$query->execute();
	/*store resultados*/
	$query->store_result();
	/*resultados*/
	$query->bind_result($id, $nombre, $datos);
	/*comprobar que aya resultados*/
		if($query->num_rows > 0){
			/*empezar la tabla*/
			echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\">";
			echo "<thead>";
			echo "<tr>";
			echo "<th>&nbsp;</th>";
			echo "<th>Nombre</th>";
			echo "<th>Datos</th>";
			echo "<th>&nbsp;</th>";
			echo "</tr>";
			echo "</thead><tbody>";
			/*extraer los resultados*/
			while($query->fetch()){
				echo "<tr>";
				echo "<td><input type=\"checkbox\" name=\"selected-item\" id=\"check_".$id."\" value=\"".$id."\"></td>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$datos."</td>";
				echo "<td></td>";
				echo "</tr>";
				}
			/*cerrar la tabla*/
			echo "</tbody></table>";
			}
	/*cerrar el query*/
	$query->close();
	/*cerrar la conexcion*/
	$conn->close();
?>
</div>
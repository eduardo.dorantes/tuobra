<?php
require('../librerias/config.php');
/*crear nuevo objeto query*/
$query = new querys();

/*recibir el id del cliente*/
if(!isset($raw['ID'])){
	$id_cliente = $row['ID'];
	}else{
		$id_cliente = $raw['ID'];
		}

/*ejecutar query proximos eventos*/
if($rows = $query->traerMultiplesResultados("SELECT LE.id_licitacion, L.num_licitacion, LE.tipo, LE.fecha, LE.hora, LE.url_trasmision, L.descripcion, C.NOMBRE, C.`NOMBRE_CORTO` FROM `licitaciones_eventos` LE INNER JOIN `licitaciones` L ON L.id = LE.id_licitacion  INNER JOIN `clientes` C ON C.ID = L.id_dependencia WHERE str_to_date(LE.fecha, '%d/%m/%Y') >= CURDATE() AND (LE.tipo = 3 OR LE.tipo = 4) AND L.tipo = 1 AND L.id_dependencia = :id_cliente ORDER BY str_to_date(LE.fecha, '%d/%m/%Y'), LE.hora", $array_bind = array(':id_cliente' => $id_cliente))){
	

	/*dibujar tabla*/
	echo "<table class=\"table table-bordered table-hover table-striped\" border=\"0\" width=\"100%\" id=\"tabla-transimisiones-promiximos-eventos\">";
	echo "<thead><tr>";
		echo "<th>Evento</th>";
		echo "<th>Fecha</th>";
		echo "<th>Hora</th>";
		echo "<th></th>";
	echo "</tr></thead><tbody>";
	/*set daia de hoy*/
	$fecha_hoy = DateTime::createFromFormat('d/m/Y H:i:s', date('d/m/Y H:i:s'));
	/*set hora de hoy*/
	$hora = date('H');
	/*set minuto*/
	$minuto = date('i');
	/*set numero licitacion*/
	$num_licitacion = "";
	foreach($rows as $row){
		/*fecha formateada*/
		$fecha_evento = $row['fecha'].' '.$row['hora'].':00';
		$fecha_termino = $row['fecha'].' ';
		/*patir la hora*/
		$partir_hora = explode(':', $row['hora']);
		/*pegar la hora*/
		$fecha_termino .= ($partir_hora[0] + 2).':'.($partir_hora[1] > 3 ? ($partir_hora[1] - 3) : $partir_hora[1]).':00';
		/*fecha evento format*/
		$fecha_evento_format = DateTime::createFromFormat('d/m/Y H:i:s', $fecha_evento);
		$fecha_evento_format_termino = DateTime::createFromFormat('d/m/Y H:i:s',  $fecha_termino);
		/*ini id del video*/
		$id_video = "";
		if($fecha_hoy <= $fecha_evento_format_termino){
		/*add numero de licitacion*/
		if($num_licitacion != $row['num_licitacion']){
			/*set numero de licitacion a numero de licitacion*/
			$num_licitacion = $row['num_licitacion'];
			echo "<tr>";
				echo "<td colspan=\"4\" class=\"titulo-licitacion\"><h4><a href=\"http://tuobra.sonora.gob.mx/licitacion/".$row['num_licitacion']."\" class=\"tu-obra-info-circle\" target=\"blank_\">".$row['num_licitacion']."</a></h4></td>";
			echo "</tr>";
			}
			/*add registro*/
		 	echo "<tr>";
			echo "<td>".$array_eventos[$row['tipo']]."</td>";
			echo "<td class='text-center'>".$row['fecha']."</td>";
			echo "<td class='text-center'>".$row['hora']."</td>";
			
			echo "<td class='text-center'>";
			/*buscar el id del video*/
			if(!empty($row['url_trasmision'])){
				if(strpos($row['url_trasmision'], '/embed/') !== false){
				  	$partir_url_id = explode('/embed/', $row['url_trasmision']);
				  	/*set id del video*/
				  	$id_video = $partir_url_id[1];
				  }else{
					  /*patir y buscar el id normal*/
					  $partir_url = explode('v=', $row['url_trasmision']);
					  $id_video = $partir_url[1];
					  }
				}
			/*comprobar fechas y hora*/
			if($fecha_hoy >= $fecha_evento_format and $fecha_hoy <= $fecha_evento_format_termino){
				echo "<a href=\"javascript:void(0);\" class=\"current-video-trans\" data-rel=\"".strtolower($row['NOMBRE_CORTO'])."\" data-id=\"".$id_video."\" title=\"Transmisión en Vivo\"><i class=\"fa fa-play-circle-o fa-2x  lista-eventos-cliente\" aria-hidden=\"true\"></i></a>";
				}else{
					echo "<i class=\"fa fa-play-circle-o fa-2x  off-event lista-eventos-cliente\" aria-hidden=\"true\"></i>";
					}
			echo "</td>";
		  	echo "</tr>";
		}
	}
	echo "</tbody></table>";
	}else{
		/*mostrar mensaje si n hay proximos eventos*/
		echo "<center>No hay Licitaciones en Proceso.</center>";
		}
?>
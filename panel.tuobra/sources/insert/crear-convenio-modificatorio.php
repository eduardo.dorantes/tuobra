<?php
require("../../config/conn.php");
require("../../sources/msg-file.php");
require("../../funciones/upload.files.inc.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../funciones/convenios.modificatorios.class.inc.php");

/*auxiliar*/
$objeto = false;

/*comprobar el post*/
if(!empty($_POST)){
	/*crear un nuevo objeto convenio*/
	$convenio = new conveniosModificatorios($conn, $_POST['id-convenio']);
	/*recibir las varibles*/
	$id_contrato = $_POST['id-contrato'];
	$id_usuario = $_POST['id-usuario'];
	$id_convenio = $_POST['id-convenio'];
	$fecha = $_POST['fecha'];
	$tipo = $_POST['tipo'];
	$fecha_inicio = $_POST['fechaInicio'];
	$fecha_termino = $_POST['fechaTermino'];
	$monto = validar::onlyNumbers($_POST['monto']);
	$objeto = $_POST['objeto'];
	$path = "../../files/";
	
	/*valirdar los campos requeridos*/
	if(validar::notEmpty($fecha) == true){//validar que venga la fecha
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit;
		}
	if(validar::notEmpty($tipo) == true){//validar que venga el tipo
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit;
		}
	/*comprobar si biene el documento*/
	if(validar::notEmpty($_FILES['documento_convenio']['name']) == false){
		/*entonces comprobar que el documento sea pdf*/
		if(upload::getExtencionArchivo($_FILES['documento_convenio']['name']) !== 'pdf'){
			echo "<div class=\"warning\">".$mensaje['1006']."<b>PDF</b></div>";
			exit;
			}
		}
	
	/*comprobar si se crea un nuevo objeto*/
	if(validar::notEmpty($id_convenio) == true){
		/*crear nuevo convenio*/
		$objeto = $convenio->crearConvenio($id_contrato, $id_usuario, $fecha, $tipo, $fecha_inicio, $fecha_termino, $monto, $objeto);
		}else{
			/*actulizar convenio*/
			$objeto = $convenio->actulizarConvenio($fecha, $tipo, $fecha_inicio, $fecha_termino, $monto, $objeto);
			}
			
	/*comprobar el objeto*/
	if($objeto == true){
		/*si las accciones del objeto fueron correctas entonces damos el valor del id del contrato*/
		$id_contrato = $convenio->getIdContrato();
		/*si el objeto se a creado correctamente entonces comprobar si biene el documento*/
		if(validar::notEmpty($_FILES['documento_convenio']['name']) == false){
			/*si biene el docucomento entonces crear el path*/
			$path .= $id_contrato."/CONVENIOS/";
			/*subir el archivo*/
			$upload = new upload();
			$se_subio = $upload->uploadFile($path, $_FILES['documento_convenio']['name'], $_FILES['documento_convenio']['tmp_name']);
			/*comprobar que se subio el documento*/
			if($se_subio !== false){
				$convenio->actulizarDocumentoConvenio(substr($se_subio, 6));
				}
			}
		echo "<script>$(\"#agregar-convenio-modificatorio\")[0].reset(); window.history.back();</script>";
		}
	}

?>
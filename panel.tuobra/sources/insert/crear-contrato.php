<?php
session_start();
require("../../config/conn.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../funciones/contratos.class.inc.php");
require("../../funciones/query.class.inc.php");
require("../../sources/msg-file.php");

if(!empty($_POST) && $_SESSION['login'] == true){
	/*crerar el objeto contrato*/
	$conrato = new contratos($conn, $_POST['id-contrato']);
	/*ini resultados*/
	$resultado = array();
	$resultado['status'] = false;
	/*validar objeto*/
	$validar_objeto = false;
	/*recibir los valores*/
	$id_contrato = $_POST['id-contrato'];
	$modalidad = $_POST['tipo-modalidad'];
	$id_contratista = $_POST['contratista'];
	$id_unidad_solicitante = $_POST['unidad-a-solicitante'];
	$id_unidad_responsable = $_POST['unidad-a-responsable'];
	$id_residente = $_POST['residente'];
	$id_supervision_externa = $_POST['supervision-externa'];
	$num_contrato = trim($_POST['num_contrato']); // Se agrego un trim para evitar que se capturen espacios indeseados
	$num_licitacion = trim($_POST['num_licitacion']);
	$objeto = $_POST['objeto_contrato'];
	$importe =  validar::onlyNumbers($_POST['importe-contrato']);
	$por_iva = validar::onlyNumbers($_POST['porcentaje-iva']);
	$total = validar::onlyNumbers($_POST['total-contrato']);
	$localidad = $_POST['localidad'];
	$municipio = $_POST['municipio'];
	$beneficiados = $_POST['beneficiados'];
	$fecha_contrato = $_POST['fecha_contrato'];
	$fecha_inicio = $_POST['fecha_inicio'];
	$fecha_termino = $_POST['fecha_termino'];
	$observaciones = $_POST['observaciones-contrato'];
	$fecha_real_terminacion = $_POST['fecha-real-termino'];
	/*Nuevo campos*/
	$tipo_contratos = $_POST['tipo-contrato'];
	$obra_complementaria = $_POST['obra-complementaria'];
	$recursos_inifed = $_POST['recursos_inifed'];
	$id_cobra_cabecera = $_POST['obra-cabecera'];
	$latitud = $_POST['latitud-obra'];
	$longitud = $_POST['longitud-obra'];
	$tipo_obra = $_POST['tipo-obra'];

	$municipiosVarios = $_POST['municipioVarios'];
	$municipiosVariosC = count($municipiosVarios);

	if($municipiosVariosC==0){
		$municipiosVarios[0] = $municipio;
	}



	/*comprobar los campos requeridos*/
	if(validar::notEmpty($modalidad) == true){//modalidad
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."1</div>";
		}
	if(validar::notEmpty($id_contratista) == true){//contratista
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."2</div>";
		}
	if(validar::notEmpty($id_unidad_solicitante) == true){//unidad solicitante
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."3</div>";
		}
	if(validar::notEmpty($id_unidad_responsable) == true){//unidad responsable
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."4</div>";
		}
	/*if(validar::notEmpty($id_residente) == true){//residente
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."5</div>";
		}
	if(validar::notEmpty($num_contrato) == true){//numero de contrato
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."6</div>";
		}*/
	if(validar::notEmpty($objeto) == true){//objeto
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."7</div>";
		}
	if(validar::notEmpty($importe) == true){//importe del contrato
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."8</div>";
		}
	if(validar::notEmpty($por_iva) == true){//porcentaje de iva del contrato
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."9</div>";
		}
	if(validar::notEmpty($localidad) == true){//localidad del contrato
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."10</div>";
		}
	if(validar::notEmpty($tipo_contratos) == true){
		$resultado['msg'] = "<div class=\"warning\">".$mensaje['1001']."10</div>";
		}
	if(validar::notEmpty($obra_complementaria) == true){
		$obra_complementaria = 0;
		}

	if(validar::notEmpty($recursos_inifed) == true){
		$recursos_inifed = 0;
	}






	if(validar::notEmpty($id_cobra_cabecera) == true){
		$id_cobra_cabecera = 0;
		}
	
	/*comprobar si un objeto nuevo o actulizacion*/
	if(validar::notEmpty($id_contrato) == true){
		$validar_objeto = $conrato->crearContrato($_SESSION['id-cliente'], $_SESSION['id-usuario'], $modalidad, $num_contrato, $num_licitacion, $importe, $por_iva, $total, $objeto, $municipio, $beneficiados, $localidad, $fecha_contrato, $fecha_inicio, $fecha_termino, $id_contratista, $id_residente, $id_supervision_externa, $id_unidad_solicitante, $id_unidad_responsable, $fecha_real_terminacion, $observaciones, $tipo_contratos, $obra_complementaria, $id_cobra_cabecera, $latitud, $longitud, $tipo_obra, $recursos_inifed);

		if($validar_objeto != false){
			$conrato->insertMunicipiosVarios($validar_objeto,$municipiosVarios);
		}


	}else{
		$validar_objeto = $conrato->updateContrato($modalidad, $num_contrato, $num_licitacion, $importe, $por_iva, $total, $objeto, $municipio, $beneficiados, $localidad, $fecha_contrato, $fecha_inicio, $fecha_termino, $id_contratista, $id_residente, $id_supervision_externa, $id_unidad_solicitante, $id_unidad_responsable, $fecha_real_terminacion, $observaciones, $tipo_contratos, $obra_complementaria, $id_cobra_cabecera, $latitud, $longitud, $tipo_obra, $recursos_inifed);
		$conrato->insertMunicipiosVarios($id_contrato,$municipiosVarios);
	}









			
	/*comprobar si el bojeto se creo o se actulizo correctamente*/
	if($validar_objeto != false){
		$resultado['status'] = true;
		$resultado['msg'] = "<div class=\"success\">".$mensaje['1002']."</div>";
		}else{
			$resultado['msg'] = "Error: ";
			}
	echo json_encode($resultado);
	}

?>
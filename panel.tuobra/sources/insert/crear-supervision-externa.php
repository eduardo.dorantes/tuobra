<?php
session_start();
require("../../config/conn.php");
require("../../funciones/supervision.externa.class.inc.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");

/*comprobar que se aya eectuado el post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*crear objeto supervision externa*/
	$supervision = new supervisionExterna($conn, $_POST['id-supervision-externa']);
	/*resivir los valores*/
	$id_supervision_externa = $_POST['id-supervision-externa'];
	$nombre_supervision_externa = $_POST['nombre-supervision'];
	$datos_supervision_externa = $_POST['datos-supervision'];
	/*comporbar si se creo o se actulizo el objeto*/
	$comprobar_objeto = false;
	
	/*comprobar los campos requerdos*/
	if(validar::notEmpty($nombre_supervision_externa) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
		
	/*comprobar si se crea el objeto o se actuliza*/
	if(empty($id_supervision_externa) and strlen($id_supervision_externa) == 0){
		$comprobar_objeto = $supervision->crearSupervisionExterna($_SESSION['id-cliente'], $nombre_supervision_externa, $datos_supervision_externa);
		}else{
			$comprobar_objeto = $supervision->updateSupervisionExterna($nombre_supervision_externa, $datos_supervision_externa);
			}
	/*comprobar si se creo o actulizo el objeto*/
	if($comprobar_objeto == true){
		echo "<script type=\"text/javascript\">alert('".$mensaje['1002']."');$(\"#contenedor-datos-catalogo\").remove();</script>";
		}
	}
?>
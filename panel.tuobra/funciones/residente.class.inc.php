<?php

class residentes{
	
	/*varibles*/
	private $db;//conexcion a la base de datos
	private $id_residente;//id del residente
	private $nombre_residente;//nombre del residente
	private $datos_residente;//datos generales del residente
	
	/*constructor*/
	function __construct($conn, $id){
		$this->db = $conn;//conexion a la base de datos
		$this->id_residente = $id;//is de la unidad
		/*comprobar si si el id del residente biene vacio*/
		if(!empty($this->id_residente)){
			/*si biene el id del residente creamos el residente*/
			$this->datosResidente();
			}
		}
		
	/*crear al residente*/
	public function crearResidente($id_cliente, $nombre_residente_, $datos_residente_){
		/*query*/
		$sql_query = "INSERT INTO `residentes` (ID_CLIENTE, NOMBRE, DATOS) VALUES (?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('iss',$id_cliente, $nombre_residente_, $datos_residente_);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*actulizar residente*/
	public function updateResidente($nombre_residente_, $datos_residente_){
		/*query*/
		$sql_query = "UPDATE `residentes` SET NOMBRE = ?, DATOS = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('ssi', $nombre_residente_, $datos_residente_, $this->id_residente);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*traer datos del residente*/
	private function datosResidente(){
		/*comprobar que venga el id del residente*/
		if(!empty($this->id_residente) and strlen($this->id_residente) > 0){
			/*query*/
			$sql_query = "SELECT NOMBRE, DATOS FROM `residentes` WHERE ID = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_residente);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($nombre_, $datos_);
			/*fetch datos*/
			$query->fetch();
			/*datos de la unidad*/
			$this->nombre_residente = $nombre_;
			$this->datos_residente = $datos_;
			
			/*cerrar conexiones*/
			$query->close();
			$this->db->close();
			}
		}
	/*traer el nombre del residente*/
	public function getNameResidente(){
		return $this->nombre_residente;
		}
	/*traer el datos del residente*/
	public function getDatosResidente(){
		return $this->datos_residente;
		}
	}
?>
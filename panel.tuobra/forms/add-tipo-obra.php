<?php
session_start();
require('../funciones/query.class.inc.php');

if($_SESSION['login'] != true && $_SESSION['tipo']==0){
	exit;
	}
/*crear objeto query*/
$query = new querys();
?>
<div id="resultado"></div>
<form name="add-tipo-obra" id="add-tipo-obra" method="post">
	<input type="text" name="descripcion-tipo" id="descripcion-tipo" size="45" placeholder="Descripción">
    <input type="hidden" name="id-tipo" id="id-tipo" value="">
    <input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
    <input type="reset" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn">
</form>
<div style="margin-top:30px;" id="lista-tipos-obra">
</div>
<script type="text/javascript">
$(document).ready(function(e) {
    listaTiposObra();
});
$('#add-tipo-obra').submit(function(){
	$.ajax({
		url: 'sources/insert/crear-tipo-obra.php',
		type:'POST',
		data:$(this).serialize(),
		dataType:"json",
		success: function(res){
			if(res.status == true){
				$('#add-tipo-obra')[0].reset();
				$('#id-tipo').val('');
				listaTiposObra();
				}
			$('#resultado').html(res.msg).show();
			setTimeout(function(){
				$('#resultado').hide();
				}, 2000);
			
			},
		error: function(){
			
			}
		});
	return false;
	});
$('#cancelar-formulario').click(function(){
	$('#id-tipo').val('');
	});
$(document).on('click','.editar-tipo', function(){
	var id_tipo = $(this).attr('data-id');
	if(id_tipo != ''){
		$.ajax({
			url: 'sources/traer-datos-tipo.php',
			type:'POST',
			data:'id=' + id_tipo,
			dataType:"json",
			success: function(res){
				if(res.status == true){
					$('#id-tipo').val(id_tipo);
					$('#descripcion-tipo').val(res.desc);
					}
				},
			});
		}
	});
function listaTiposObra(){
	$('#lista-tipos-obra').load('sources/listas/lista-tipos-obra.php');
	}
</script>
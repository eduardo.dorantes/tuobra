<?php
session_start();
require("../config/conn.php");

/*recibir el id del contrato*/
$id_contrato = !empty($_POST['id']) ? $_POST['id'] : NULL;
$tipoConvenio = array(1 => 'Plazo', 3 => 'Monto', 4 => 'Plazo/Monto', 2 => 'Objeto');

/*comprobar el post*/
if(!empty($_POST) and !empty($id_contrato) and $_SESSION['login'] == true){
	/*query*/
	$sql_query = "SELECT CM.ID, CM.FECHA, CM.TIPO, CM.OBJETO, CM.MONTO, CM.FECHA_INICIO, CM.FECHA_TERMINO, CM.DOCUMENTO FROM `convenios_modificatorios` CM WHERE CM.ID_CONTRATO = ?;";
	/*preparar query*/
	$query = $conn->prepare($sql_query);
	/*comprobar el query*/
	if($query === false){
		trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $query->error, E_USER_ERROR);
		}
	/*bind parametros*/
	$query->bind_param('i', $id_contrato);
	/*executar query*/
	$query->execute();
	/*store resultados*/
	$query->store_result();
	/*resultados*/
	$query->bind_result($id, $fecha, $tipo, $objeto, $monto, $fechaInicio, $fechaTermino, $documento);
		/*comprobar que aya resultados*/
		if($query->num_rows > 0){
			/*empezar la tabla*/
			echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\">";
			echo "<thead>";
			echo "<tr>";
			echo "<th>&nbsp;</th>";
			echo "<th align=\"center\" width=\"5%\">Fecha</th>";
			echo "<th align=\"left\">Tipo</th>";
			echo "<th align=\"left\">Objeto</th>";
			echo "<th align=\"right\">Monto</th>";
			echo "<th>Fecha inicio</th>";
			echo "<th>Fecha termino</th>";
			echo "<th></th>";
			echo "</tr>";
			echo "</thead><tbody>";
			/*extraer los resultados*/
			while($query->fetch()){
				echo "<tr>";
				echo "<td align=\"center\"><input type=\"checkbox\" name=\"selected-item[]\" id=\"check_".$id."\" class=\"check_opciones\" value=\"".$id."\"></td>";
				echo "<td align=\"center\">".$fecha."</td>";
				echo "<td>".$tipoConvenio[$tipo]."</td>";
				echo "<td>".$objeto."</td>";
				echo "<td align=\"right\">".number_format($monto, 2)."</td>";
				echo "<td align=\"center\">".$fechaInicio."</td>";
				echo "<td align=\"center\">".$fechaTermino."</td>";
				echo "<td><a href=\"".$documento."\" target=\"_blank\"><img src=\"images/icons/pdf_big.png\"></a></td>";
				echo "</tr>";
				}
			/*cerrar la tabla*/
			echo "</tbody></table>";
			}else{
				echo "No resultados";
				}
	/*cerrar el query*/
	$query->close();
	/*cerrar conexion*/
	$conn->close();
	}
?>
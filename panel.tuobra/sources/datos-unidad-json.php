<?php
session_start();
require("../config/conn.php");
require("../funciones/unidad.administrativa.class.inc.php");
require("../funciones/validar.formularios.class.inc.php");
require("../sources/msg-file.php");

/*comprobar que se aya hecho el post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*array de resultados*/
	$resultados = array();
	/*crear un objeto*/
	$unidad = new unidadAdminstraiva($conn, $_POST['unidad']);
	
	$resultados['name'] = $unidad->getNombre();
	$resultados['tipo'] = $unidad->getTipoUnidad();
	
	echo json_encode($resultados);
	}
?>
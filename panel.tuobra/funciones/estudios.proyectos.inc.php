<?php
class estudiosProyectos{
	var $db;//conexcion a la base de datos
	private $id_estudio_proyecto;//id del estudios y proyecto
	private $descripcion;//descripcion del estudio
	private $archivo;//ruta al archivo
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		$this->db = $conn;//conexion a la base de datos
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			$this->id_estudio_proyecto = $id;//set el id del estudio
			}
		}
	/*crear un nuevo estudio y proyecto*/
	public function crearEstudioProyecto($id_contrato, $id_usuario, $descripcion){
		/*query*/
		$sql_query = "INSERT INTO `estudios` (ID_CONTRATO, ID_USUARIO, DESCRIPCION) VALUES (?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('iis',$id_contrato, $id_usuario, $descripcion);
		/*ejecutar query*/
		$query->execute();
		/*el ultimo id registrado*/
		$this->id_estudio_proyecto = $query->insert_id;
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*actulizar el estudio o proyecto*/
	public function actulizarEstudioProyecto($campo, $nuevoValor){
		/*query*/
		$sql_query = "UPDATE `estudios` SET $campo = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('si',$nuevoValor, $this->id_estudio_proyecto);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*borrar estudio o proyecto*/
	public function borrarEstudioProyecto(){
		/*comprobar que venga el id del estudio*/
		if(!empty($this->id_estudio_proyecto) and strlen($this->id_estudio_proyecto) > 0){
			/*query*/
			$sql_query = "DELETE FROM `estudios` WHERE ID = ?;";
			/*preparar query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_estudio_proyecto);
			/*ejecutar query*/
			$query->execute();
			/*comprobar que se aya borrado*/
			if($query->affected_rows > 0){
				return true;
				}else{
					return false;
					}
			}
		}
	/*get el id del estudio y proyecto*/
	public function getIdEstudioProyecto(){
		return $this->id_estudio_proyecto;
		}
	/*get descripcion del estudios y proyecto*/
	public function getDescripcion(){
		return $this->descripcion;
		}
	/*get el archivo del estudio*/
	public function getArchivo(){
		return $this->archivo;
		}
	}
?>
<?php
session_start();
require("../config/conn.php");
require("../funciones/supervision.externa.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}
/*crear un un nuevo objeto unidad*/
$supervision = new supervisionExterna($conn, $_GET['id']);
?>
<div id="resultado"></div>
<form name="agregar-supervision-externa" id="agregar-supervision-externa" method="post">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-add-supervision-externa">
    	<caption class="form-caption" style="display:none;">Agregar supervisión externa</caption>
    	<tbody valign="top">
        	<tr>
            	<td><label>Nombre/razón social</label></td>
                <td><input type="text" name="nombre-supervision" id="nombre-supervision" size="45" value="<?php echo $supervision->getNameSupervision(); ?>"></td>
            </tr>
            <tr>
            	<td><label>Datos particulares</label></td>
                <td><textarea name="datos-supervision" id="datos-supervision" rows="3" cols="40"><?php echo $supervision->getDatosSupervision(); ?></textarea></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn cancelar-catalogo">
                </td>
                <td><input type="hidden" name="id-supervision-externa" id="id-supervision-externa" value="<?php echo $_GET['id']; ?>"></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$('#agregar-supervision-externa').submit(function(){
	var datos = $(this).serialize();
	var nombre = $('#nombre-supervision').val();
	$.ajax({
		beforeSend: function(){
			$("#resultado").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url:"sources/insert/crear-supervision-externa.php?rand=" + Math.random() * 9999999,
		type:"POST",
		data: datos,
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			},
		success: function(resultados){
			$("#resultado").html(resultados);
			},
		complete: function(){
			$("#enviar-formulario").prop("disabled", false);
			cargarCatalogos('SE', nombre);
			},
		});
	
	return false;
	});
</script>
<?php
session_start();
/*******************/
/* AVANCE GRAFICA */
/*****************/

/*datos del avance*/
$fechas = array();
$avances = array();
$fechas_comma = "";
$avance_comma = "";
/*set*/
$fechas[] = "'".$contrato->getFechaInicio()."'";
$avances[] = 0;
foreach($fechas_avances as $key => $valor){
	$fechas[] = "'".$key."'";
	$avances[] = $valor;
	}
/*implode datos*/
$fechas_comma = implode(",", $fechas);
$avance_comma = implode(",", $avances);

if($_SESSION['login'] != true){
	exit;
	}
?>
<script type="text/javascript">
$(function () {
    $('#contenedor-grafica').highcharts({
        title: {
            text: 'Avance',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php echo $fechas_comma; ?>]
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '%'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0,
			x: 0.0,
			y: 100
        },
        series: [{
			name: 'Avance',
            data: [<?php echo $avance_comma; ?>]
        }]
    });
});
</script>
<div id="contenedor-grafica"></div>
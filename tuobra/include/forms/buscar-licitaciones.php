<?php
require('../librerias/query.class.inc.php');
require('../librerias/config.php');
/*crear nuevo objeto query*/
$query = new querys();
/*extraer todos los clientes ejecutores*/
$clientes = $query->traerMultiplesResultados('SELECT `ID`, `NOMBRE` FROM `clientes` ORDER BY `NOMBRE`', NULL);
?>
<form name="buscar-licitaciones" id="buscar-licitaciones" method="post" class="form-horizontal">


	<div class="form-group">

		<label for="ejercicio-del" class="col-sm-1 text-left control-label">Ejercicio</label>
		<div class="col-sm-2">
			<select name="ejercicio-del" id="ejercicio-del" class="form-control">
				<option value="2015">2015</option>
				<option value="2016">2016</option>
				<option value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
			</select>
		</div>
		<label for="ejercicio-al" class="col-sm-1 text-center control-label ">al</label>
		<div class="col-sm-2">
			<select name="ejercicio-al" id="ejercicio-al" class="form-control">
				<?php for($i=2015; $i<=date('Y'); $i++ ){
					echo '<option value="'.$i.'">'.$i.'</option>';
				} ?>
			</select>
		</div>

		<label for="estatus" class="col-sm-1 text-left control-label">Estatus</label>
		<div class="col-sm-3">
			<select name="estatus" id="estatus-licitacion" class="form-control">
				<option value="">Todas</option>
				<option value="1">En Proceso</option>
				<option value="2">Finalizadas</option>
			</select>
		</div>


	</div>

	<div class="form-group">

		<label for="dependencia-licitacion" class="col-sm-2 text-left control-label">Dependencia / Organismo</label>
		<div class="col-sm-4">
			<select name="dependencia" id="dependencia-licitacion" class="form-control">
				<option value="">Todas</option>
				<?php
				foreach($clientes as $value){
					echo "<option value=\"".$value['ID']."\">".$value['NOMBRE']."</option>";
				}
				?>
			</select>
		</div>




		<label for="tipo" class="col-sm-1 text-left control-label">Tipo</label>
		<div class="col-sm-3">
			<select name="tipo" id="tipo-licitacion" class="form-control">
				<option value="">Todas</option>
				<?php
					foreach($array_tipo_licitacion as $index => $value){
						echo "<option value=\"".$index."\">".$value."</option>";
					}
				?>
			</select>
		</div>

	</div>

	<div class="form-group">
		<div class="col-sm-10 col-xs-8">
			<input type="text" name="q" id="search-query" class="form-control" placeholder="Buscar por palabras clave">
		</div>
		<div class="col-sm-2 col-xs-4 text-right">
			<button name="enviar" id="enviar" class="btn btn-success">Buscar</button>
		</div>
	</div>



</form>


<hr />


<!--<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>-->

<!--<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.0/js/buttons.flash.min.js"></script>-->



<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.js"></script>




<script type="text/javascript">

    var dataTable = {
        "lengthMenu": [[30, 50, 100,-1], [ 30, 50, 100, "Todo"]],
        responsive: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        },
        "order": [[1, "des"]],
        dom: 'lBfrtip',
        buttons: {
            name: 'primary',
            buttons: ['copy', 'csv', 'excel', 'pdf','print']
        }

    }

$(document).ready(function() {
    $('#enviar').trigger('click');
});

$('#buscar-licitaciones').submit(function(){
	$('#listado-licitaciones').html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
	$.post('search-licitacion/',$(this).serialize(),function(res){
		$('#listado-licitaciones').html(res);
    }).done(function() {
        $('#listado-licitaciones .table').DataTable(dataTable);
    });
	return false;
});

</script>
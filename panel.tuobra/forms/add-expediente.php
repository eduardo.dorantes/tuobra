<?php 
session_start();
require("../config/conn.php");
require("../funciones/forms.selects.classs.php");
require("../funciones/contratos.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}
/*recibir el id del contrato*/
$id_contrato = !empty($_POST['id']) ? $_POST['id'] : NULL;
/*crear objeto contrato*/
$contrato = new contratos($conn, $id_contrato);
$modalidad = $contrato->getModalidad();//obtener la modalidad del contrato
/*crear un nuevo objeto select*/
$select = new Selects($conn);

?>
<div id="resultado"></div>
<div style="width:20%; height:5px; margin-top:10px; margin-bottom:10px;">
	<div id="progreso" style="height:5px; background-color:green; width:0px; text-align:center; position:relative;"><span id="porcentaje" style=" position:absolute; left:0;"></span></div>
</div>
<form name="agregar-expediente" id="agregar-expediente" method="post" action="" enctype="multipart/form-data">
	<table cellpadding="5" cellspacing="0" border="0" id="add-form-expediente" width="50%">
    	<tbody valign="top">
        	<tr>
            	<td><label>Expediente:</label></td>
                <td>
                	<select name="expediente" id="expediente" required>
                    	<option value="">--SELECCIONAR DOCUMENTO--</option>
                        <?php echo $select->selectPlantillas($modalidad); ?>
                        <option value="999">OTRO</option>
                    </select>
                </td>
            </tr>
            <tr>
            	<td><label>Nombre documento:</label></td>
                <td><textarea name="nombre-otro-documento" id="nombre-otro-documento" rows="2" cols="40" disabled required></textarea></td>
            </tr>
            <tr>
            	<td><label>Archivo:</label></td>
                <td><input type="file" name="documento" id="documento" accept="application/pdf" required></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                </td>
                <td>
                	<input type="hidden" name="id-usuario" id="id-usuario" value="<?php echo $_SESSION['id-usuario']; ?>"><input type="hidden" name="id-contrato" id="id-contrato" value="<?php echo $_POST['id']; ?>">
                </td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$("#agregar-expediente").submit(function(e){
	e.preventDefault();
	if(window.FormData == undefined){
		alert('Necesita un navegador que soporte HTML5');
		return false;
		}
	var dataForm = new FormData($("#agregar-expediente")[0]);
	
	$.ajax({
		beforeSend: function(){},
			url:"sources/insert/crear-expediente.php?rand=" + Math.random() * 9999999,
			type: "POST",
			cache: false,
            contentType: false,
            processData: false,
			data: dataForm,
			xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('progress',function(ev) {
						if (ev.lengthComputable) {
							var percentUploaded = Math.floor(ev.loaded * 100 / ev.total);
							$("#progreso").css('width', percentUploaded + '%');
							$("#porcentaje").text(percentUploaded + '%');
						}
				   }, false);
				}
				return myXhr;
            },
			error: function(jqXHR, textStatus, errorThrown){
				$("#resultado").html(jqXHR.responseText);
				},
			success: function(resultado){
				$("#resultado").html(resultado);
				},
			complete: function(){
				$("#agregar-avance-fotografico")[0].reset();
				},
		});
	return false;
	});
</script>
<script type="text/javascript">
$(document).on("change", "#expediente", function(){
	if($(this).val() == 999){
		$("#nombre-otro-documento").attr("disabled", false);
		}else{
			$("#nombre-otro-documento").attr("disabled", true);
			}
	});
</script>
<?php
session_start();
require("../config/conn.php");
require("../funciones/forms.selects.classs.php");
require("../funciones/clientes.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}
/*crear el objeto cliente*/
$cliente = new clientes($conn, $_SESSION['id-cliente']);
?>
<div id="resultado"></div>
<form name="agregar-cliente" id="agregar-cliente" method="post">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-form-add-cliente">
    	<tbody valign="top">
        	<tr>
            	<td><label for="nombre-dependencia">Nombre</label></td>
                <td><label>Nombre Corto</label></td>
                <td></td>
            </tr>
            <tr>
            	<td><input type="text" name="nombre" id="nombre-dependencia" size="45" required value="<?php echo $cliente->getNombre(); ?>"></td>
                <td><input type="text" name="nombre_corto" id="nombre_corto" required value="<?php echo $cliente->getNombreCorto(); ?>"></td>
                <td></td>
            </tr>
            <tr>
            	<td><label for="direccion">Dirección</label></td>
                <td><label for="municipio">Municipio</label></td>
            </tr>
            <tr>
            	<td><textarea name="direccion" cols="45" rows="3" id="direccion" required><?php echo $cliente->getDireccion(); ?></textarea></td>
                <td><select name="municipio" id="municipio"><option value="0">--Seleccione--</option><?php $municipio = new Selects($conn); $municipio->selectMunicipios(); ?></select></td>
          </tr>
            <tr>
            	<td><label for="telefono">Teléfono</label></td>
                <td><label for="email">Email</label></td>
            </tr>
            <tr>
            	<td><input type="text" name="telefono" id="telefono" required value="<?php echo $cliente->getTelefono(); ?>"></td>
                <td><input type="email" name="email" id="email" required value="<?php echo $cliente->getEmail(); ?>"></td>
            </tr>
            <tr>
            	<td><label>Nombre responsable</label></td>
                <td><label>Presupuesto de la Dependencia</label></td>
            </tr>
            <tr>
            	<td><input type="text" name="nombre-responsable" id="nombre-responsable" size="55" required value="<?php echo $cliente->getNombreResponsable(); ?>"></td>
                <td><input type="text" name="presupuesto" id="presupuesto" style="text-align:right;"></td>
            </tr>
            <tr>
            	<td><label>URL</label></td>
                <td></td>
            </tr>
            <tr>
            	<td width="50%"><input type="text" name="url-cliente" id="url-cliente" size="45" value="<?php echo $cliente->getCustomUrl(); ?>"> 
            	<span style="margin-left:20px;">http://tuobra.mx/<span id="custom-url" style="font-weight:700;"><?php echo $cliente->getCustomUrl(); ?></span></span></td>
                <td></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn">
                </td>
                <td>
                	<input type="hidden" name="id-cliente" id="id-cliente" value="<?php echo $_SESSION['id-usuario']; ?>">
                </td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$(document).ready(function(e) {
	<?php
	$municipio = $cliente->getNombreMunicipio();
	if(!empty($municipio)){
		echo "$(\"#municipio option:contains('".$cliente->getNombreMunicipio()."')\").attr('selected', 'selected');";
		}
	?>
});
$(document).on("keyup", "input#url-cliente", function(){
	var url_custom = $(this).val();
	$("#custom-url").text(url_custom);
	});
$("input#url-cliente").on({
  keydown: function(e) {
    if (e.which === 32)
      return false;
  },
  change: function() {
    this.value = this.value.replace(/\s/g, "");
  }
});
</script>
<script type="text/javascript">
$('#agregar-cliente').submit(function(){
	var datos = $(this).serialize();
	$.ajax({
		beforeSend: function(){
			$("#resultado_catalogo").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url:"sources/insert/crear-cliente-update.php?rand=" + Math.random() * 9999999,

		type:"POST",
		data: datos,
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			},
		success: function(resultados){
			$("#resultado").html(resultados);
			},
		complete: function(){
			$("#enviar-formulario").prop("disabled", false);
			},
		});
	
	return false;
	});
</script>
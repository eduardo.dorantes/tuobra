<?php
session_start();
require("../config/conn.php");
require("../funciones/unidad.administrativa.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}

/*crear un un nuevo objeto unidad*/
$unidad = new unidadAdminstraiva($conn, $_GET['id']);
?>
<div id="resultado"></div>
<form name="agregar-unidad-administrativa" id="agregar-unidad-administrativa" method="post">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-form-add-unidad-administrativa">
    	<caption class="form-caption" style="display:none;"><span>Agregar nueva unidad administrativa<span></caption>
    	<tbody valign="top">
        	<tr>
            	<td><label for="nombre-unidad">Nombre de la unidad administrativa</label></td>
                <td><input type="text" name="nombre-unidad" id="nombre-unidad" size="45" value="<?php echo $unidad->getNombre(); ?>"></td>
            </tr>
            <tr>
            	<td><label for="nombre-responsable">Nombre del responsable</label></td>
                <td><input type="text" name="nombre-responsable" id="nombre-responsable" size="45" value="<?php echo $unidad->getNombreEncargado(); ?>"></td>
            </tr>
            <tr>
            	<td><label for="puesto-responsable">Puesto del responsable</label></td>
                <td><textarea name="puesto-responable" id="puesto-responsable" rows="3" cols="40"><?php echo $unidad->getPuestoEncargado(); ?></textarea></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn cancelar-catalogo">
                </td>
                <td><input type="hidden" name="id-unidad" id="id-unidad" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ""; ?>"></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$(document).on("submit", "#agregar-unidad-administrativa", function(){
	var datos = $(this).serialize();
	$.ajax({
		beforeSend: function(){
			$("#resultado").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url:"sources/insert/crear-unidad-administrativa.php?rand=" + Math.random() * 9999999,
		type:"POST",
		data: datos,
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			},
		success: function(resultados){
			$("#resultado").html(resultados);
			},
		complete: function(){
			$("#enviar-formulario").prop("disabled", false);
			cargarCatalogos('UA');
			},
		});
	
	return false;
	});
</script>
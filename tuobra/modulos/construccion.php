<?php include('../include/header.php'); ?>


    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Página en construcción</h2>
                    <p>Inicio | Página en construcción</p>
                </div>
            </div>
        </div>

    </section>

    <section id="contentForm">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="inConstruction">
                        <h1>Página en construcción</h1>
                        <p>Estamos generando la información para esta sección. Disculpe las molestias.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section id="contentResults">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="open-data"></div>
                    <div id="listado-licitaciones"></div>
                </div>
            </div>
        </div>
    </section>









<?php include('../include/footer.php'); ?>
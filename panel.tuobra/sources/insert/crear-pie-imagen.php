<?php
session_start();
header("cache-control: no-cache");
require("../../config/conn.php");
require("../../funciones/fotos.class.inc.php");
require("../../sources/msg-file.php");


if(!empty($_POST) and !empty($_POST['id_imagen']) and $_SESSION['login'] == true){
	/*recibir el id de la imagen*/
	$id_imagen = $_POST['id_imagen'];
	/*descripcion o pie de imagen*/
	$descripcion = $_POST['desc'];
	/*fecha*/
	$fecha = $_POST['fecha'];
	
	/*comprobar que venga una descripcion*/
	if(!empty($descripcion)){
		/*crear objeto foto*/
		$foto = new fotos($conn, $id_imagen);
		
		/*comprobar la actulizacion*/
		if($foto->actulizarFoto('PIE_FOTO', $descripcion)){
			$foto->actulizarFoto('FECHA', $fecha);
			echo "<p>".$fecha." - ".$descripcion."</p>";
			}
		}
	}
?>
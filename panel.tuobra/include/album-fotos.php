<?php
/************************/
/* AVANCE FOTOGRAFICO   */
/************************/
session_start();
require("../config/conn.php");
require("../sources/msg-file.php");

/*query*/
	$sql_query = "SELECT 0 AS ORDEN, AF.ID, AF.FECHA, '' AS IMAGEN,'' AS THUMB, str_to_date(AF.FECHA, '%d/%m/%Y') AS FECHAORDEN, '' AS PIE FROM `avance_fotografico` AF WHERE AF.ID_CONTRATO = ? GROUP BY AF.FECHA UNION ALL SELECT 1, AF.ID, AF.FECHA, AF.IMAGEN, AF.THUMB, str_to_date(AF.FECHA, '%d/%m/%Y'), AF.PIE_FOTO FROM `avance_fotografico` AF WHERE AF.ID_CONTRATO = ? ORDER BY FECHAORDEN DESC;";

/*get el numero de contrato*/
$id_contrato = !empty($_POST['id']) ? $_POST['id'] : NULL;

/*comprobar el post*/
if(!empty($_POST) and !empty($id_contrato) and $_SESSION['login'] == true){
	/*contenedor de frentes*/
	echo "<div id=\"contenedor-frentes\"></div>";
	/*inpu file para subir la imagen*/
	echo "<input type=\"file\" name=\"documento\" id=\"documento_upload\" accept=\".png, .jpg, .jpeg\">";
	/*contenedor de form para agregar frente*/
	echo "<div class=\"contenedor-add-frente-form\"><form name=\"add-new-frente-form\" id=\"add-new-frente-form\"> <input type=\"text\" name=\"descripcion\" id=\"descripcion\" placeholder=\"Nombre del frente\" required><input type=\"hidden\" name=\"id-contrato\" id=\"id-contrato\" value=\"".$id_contrato."\"><input type=\"submit\" name=\"enviar-frente\" id=\"enviar-frente\" class=\"btn-green\" value=\"Crear Frente\"></form></div>";
	/*contenedor para agregar frentes*/
	echo "<div class=\"contenedor-add-frente\"><a href=\"javascript:;\" id=\"add-new-frente\" class=\"icon-fontawesome-webfont-8\" data-id=\"".$id_contrato."\">Agregar nuevo frente</a></div>";
	}
?>
<script type="text/javascript">
var id_frente_obra;
var id_contrato;
var ob;
$(document).ready(function(e) {
    cargarFrentes($("#id-contrato").val());
});
$('#add-new-frente').click(function(){
	if(!$(".contenedor-add-frente-form").is(':visible')){
		$(".contenedor-add-frente-form").show();
		}else{
			$(".contenedor-add-frente-form").hide();
			}
	});
$('#add-new-frente-form').submit(function(){
	var valores = $(this).serialize();
	$.post('sources/insert/crear-frente.php', valores, function(datos){
		if(datos['status'] == true){
			cargarFrentes($("#id-contrato").val());
			$("#descripcion").val('');
			}
		}, 'json');
	return false;
	});
$(document).on('click', '.contenedor-add-frente-imagen-nueva', function(e){
	e.preventDefault();
	ob = $(this);
	id_frente_obra = $(this).attr('data-rel');
	id_contrato = $(this).attr('data-id');
	$("#documento_upload:hidden").trigger('click');
	});
$("#documento_upload").change(function(e){
	e.preventDefault();
	if(window.FormData == undefined){
		alert('Necesita un navegador que soporte HTML5');
		return false;
		}
	
	var dataForm =  new FormData();
	dataForm.append('documento', $("#documento_upload")[0].files[0]);
	dataForm.append('id-usuario', '<?php echo $_SESSION['id-usuario']; ?>');
	dataForm.append('id-frente', id_frente_obra);
	dataForm.append('id-contrato', id_contrato);
	
	$.ajax({
		beforeSend: function(){
				ob.children().html('<img src="../images/loading.gif" title="Cargando Imagen" />');
			},
			url:"sources/insert/crear-foto.php?rand=" + Math.random() * 9999999,
			type: "POST",
			cache: false,
            contentType: false,
            processData: false,
			data: dataForm,
			/*xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('progress',function(ev) {
						if (ev.lengthComputable) {
							var percentUploaded = Math.floor(ev.loaded * 100 / ev.total);
							$("#progreso").css('width', percentUploaded + '%');
							$("#porcentaje").text(percentUploaded + '%');
						}
				   }, false);
				}
				return myXhr;
            },*/
			error: function(jqXHR, textStatus, errorThrown){
				$("#resultado").html(jqXHR.responseText);
				},
			success: function(resultado){
				},
			complete: function(){
				cargarFrentes(id_contrato);
				id_frente_obra = "";
				id_contrato = "";
				$("#documento_upload").val('');
				ob.children().text('Agregar nueva imagen');
				},
		});
	return false;
	});
function cargarFrentes(id_contrato){
	$.post('sources/listas/lista-frentes-fotos.php',{id_contrato: id_contrato}, function(resultado){
		$("#contenedor-frentes").html(resultado);
		});
	}
$(document).on('click', '.add-pie-fig', function(){
	var ob = $(this);
	ob.removeClass('add-pie-fig');
	ob.addClass('to-add-pie');
	if(!$("#contenedor-pie-imgen").length){
		ob.html('<div id="contenedor-pie-imgen"><input type="text" name="fecha" size="10" class="campo-fecha" placeholder="Fecha"><br/><textarea name="pie-imagen" class="pie-imagen" autofocus="autofocus" placeholder="Descripción"></textarea><br/><a href="javascript:;" id="guardar-pie">Guardar</a> <a href="javascript:;" id="cancelar-pie">Cancelar</a></div>');
	}
	});
$(document).on('click', '#cancelar-pie', function(){
	var ob = $(this).parent().parent();
	ob.removeClass('to-add-pie');
	ob.html('Agregar Pie Imagen/Descripción');
	ob.addClass('add-pie-fig');
	});
$(document).on('click', '#guardar-pie', function(){
	var ob = $(this).parent().parent();
	var id_img = ob.parent().attr('data-id');
	var pie = ob.find('textarea:first').val();
	var fecha = ob.find('input:first').val();
	if(pie !== ''){
		var pregunta = confirm("¿Seguro que desea guardar los cambios?");
		if(pregunta){
			$.post('sources/insert/crear-pie-imagen.php',{id_imagen: id_img, desc: pie, fecha: fecha}, function(data){
				ob.removeClass('to-add-pie');
				ob.html(data);
				});
			}
		}
	});
 $(document).on('focusin', '.campo-fecha', function(e) {
    $(this).datepicker({dayNames: ['Domingo','Lunes','Martes','Miercoles','Juevez','Viernes','Sabado'], dayNamesMin: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],nextText:'Siguiente',prevText: 'Atras', dateFormat: 'dd/mm/yy', autoSize: true, changeYear: true, monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']});
});
</script>
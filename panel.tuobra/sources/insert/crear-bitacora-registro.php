<?php
session_start();
require('../../funciones/query.class.inc.php');


/*checar si es post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*crear objeto query*/
	$query = new querys();
	/*ini resultado*/
	$resultado = array();
	/*recibir las varibles*/
	$id_obra = $_POST['obra'];
	$fecha = $_POST['fecha'];
	$descripcion = $_POST['descripcion'];
	/*ini and set fecha y hora*/
	$fecha_hora = date('Y-m-d H:i:s');
	
	/*set status to false*/
	$resultado['status'] = false;
	
	/*comprobar que venga id de la obra*/
	if(!empty($id_obra)){
		/*comprobar que venga mas de 10 letras*/
		if(strlen($descripcion) <= 10){
			/*mensaje*/
			$resultado['msg'] = "<b style=\"color:red;\">Debe de introducir más de 10 caracteres.</b>";
			
			}else{
		
			/*ini and set sql query string*/
			$query_string = "INSERT INTO `bitacora_obra` (`id_obra`, `fecha`, `descripcion`, `fecha_hora`, `id_creador`) VALUES (:id_obra, :fecha, :descripcion, :fecha_hora, :id_creador)";
			/*ini array bind*/
			$array_bind = array(':id_obra' => $id_obra, ':fecha' => $fecha, ':descripcion' => $descripcion, ':fecha_hora' => $fecha_hora, ':id_creador' => $_SESSION['id-usuario']);
			
			
			/*guardar registro*/
			if($query->ejecutarQuery($query_string, $array_bind)){
				/*set status to true*/
				$resultado['status'] = true;
				/*msg*/
				$resultado['msg'] = "Los datos de han guardado correctamente.";
				}
			}
		}
		
	/*enviar resultados*/
	echo json_encode($resultado);
	}
?>
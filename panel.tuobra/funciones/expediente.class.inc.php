<?php
class expediente{
	
	var $db;//varibles de conexion a la base de datos
	private $id_expediente;//id del expediente
	private $archivo;//ruta al documento
	private $descripcion;//nombre/descripcion del otros archivos
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		$this->db = $conn;//conexion a la base de datos
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			$this->id_expediente = $id;//set el id de la obra
			}
		}
	/*crear un objeto expediente*/
	public function crearExpediente($id_contrato, $id_usuario, $expediente, $descripcion, $archivo){
		/*query*/
		$sql_query = "INSERT INTO `expediente` (ID_CONTRATO, ID_USUARIO, ID_PLANTILLA, NOMBRE, ARCHIVO) VALUES (?, ?, ?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('iiiss',$id_contrato, $id_usuario, $expediente, $descripcion, $archivo);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*borrar expediente*/
	public function borrarExpediente(){
		/*comprobar que venga el id del expediente*/
		if(!empty($this->id_expediente) and strlen($this->id_expediente) > 0){
			/*query*/
			$sql_query = "DELETE FROM `expediente` WHERE ID = ?;";
			/*preparar query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $id_exp);
			/*ejecutar query*/
			$query->execute();
			/*comprobar query*/
			if($query->affected_rows > 0){
				return true;
				}else{
					return false;
					}
			}
	}
}
?>
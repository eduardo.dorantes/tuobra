<?php
require("../../config/conn.php");
require("../../funciones/upload.files.inc.php");
require("../../funciones/estudios.proyectos.inc.php");

/*path donde se guardaran los archivos*/
$path = "../../files/";
/*resivir las variables*/
$id_contrato = $_POST['id-contrato'];//id del contrato
$id_usuario = $_POST['id-usuario'];//id del usuario que realizar el registro
$descripcion = $_POST['descripcion'];

if(!empty($_POST) and !empty($id_contrato) and $_SESSION['login'] == true){
	/*crear un nuevo estudio*/
	$estudio_proyecto = new estudiosProyectos($conn);
	$estudio_proyecto->crearEstudioProyecto($id_contrato, $id_usuario, $descripcion);
	
	/*combinar el path*/
	$path .= $id_contrato.'/ESTUDIOS_PROYECTOS/';
	
	/*crear un objeto subir el archivo*/
	$upload = new upload();
	$se_subio = $upload->uploadFile($path, $_FILES['documento']['name'], $_FILES['documento']['tmp_name']);
	
		/*comprobar que se aya subido*/
		if($se_subio !== false){
			/*se subio actulizar el registros con el path*/
			if($estudio_proyecto->actulizarEstudioProyecto('ARCHIVO_FTP', substr($se_subio, 6)) == true){
				echo "<script>window.history.back();</script>";
				}
			}
	}
?>
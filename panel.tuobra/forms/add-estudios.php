<?php
session_start();
if($_SESSION['login'] != true){
	exit;
	}
?>
<div id="resultado"></div>
<div style="width:20%; height:5px; margin-top:10px; margin-bottom:10px;">
	<div id="progreso" style="height:5px; background-color:green; width:0px; text-align:center; position:relative;"><span id="porcentaje" style=" position:absolute; left:0;"></span></div>
</div>
<form name="agregar-estudio" id="agregar-estudio" method="post" enctype="multipart/form-data">
	<table cellpadding="5" cellspacing="0" border="0" id="table-form-add-estudio">
    	<tbody valign="top">
        	<tr>
            	<td><label for="descripcion">Descripción:</label></td>
                <td><textarea name="descripcion" cols="45" rows="3" id="descripcion" required></textarea></td>
            </tr>
            <tr>
            	<td><label for="documento">Archivo (pdf)</label></td>
                <td valign="bottom"><input type="file" name="documento" id="documento" accept="application/pdf" required></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                </td>
                <td><input type="hidden" name="id-usuario" id="id-usuario" value="<?php echo $_SESSION['id-usuario']; ?>"><input type="hidden" name="id-contrato" id="id-contrato" value="<?php echo $_POST['id']; ?>"></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$("#agregar-estudio").submit(function(e){
	e.preventDefault();
	
	if(window.FormData == undefined){
		alert('Necesita un navegador que soporte HTML5');
		return false;
		}
	var dataForm = new FormData($("#agregar-estudio")[0]);
	
	$.ajax({
		beforeSend: function(){},
			url:"sources/insert/crear-estudio-proyecto.php?rand=" + Math.random() * 9999999,
			type: "POST",
			cache: false,
            contentType: false,
            processData: false,
			data: dataForm,
			xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('progress',function(ev) {
						if (ev.lengthComputable) {
							var percentUploaded = Math.floor(ev.loaded * 100 / ev.total);
							$("#progreso").css('width', percentUploaded + '%');
							$("#porcentaje").text(percentUploaded + '%');
						}
				   }, false);
				}
				return myXhr;
            },
			error: function(jqXHR, textStatus, errorThrown){
				$("#resultado").html(jqXHR.responseText);
				},
			success: function(resultado){
				$("#resultado").html(resultado);
				},
			complete: function(){
				$("#agregar-estudio")[0].reset();
				},
		});
	return false;
	});
</script>
<?php
session_start();
header("cache-control: no-cache");
include("../../config/conn.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");
require("../../funciones/avance.class.inc.php");


/*comprobar el post*/
if(!empty($_POST) and !empty($_POST['id-contrato']) and $_SESSION['login'] == true){
	/*crear un nuevo objeto*/
	$avance = new avance($conn);
	/*recibir las varibles*/
	$fecha = $_POST['fecha-avance'];
	$por_avance = validar::onlyNumbers($_POST['porcentaje-avance']);
	$id_contrato = $_POST['id-contrato'];
	$id_usuario = $_POST['id-usuario'];
	/*objeto*/
	$objeto = false;
	
	/*validar los campos requeridos*/
	if(validar::notEmpty($fecha) == true){//comprobar la fecha
		echo "<div class=\"warning\">".$mensaje['1001']."1</div>";
		exit;
		}
	if(validar::notEmpty($por_avance) == true){//comprobar que venga un porcentaje de avance
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit;
		}
		
	/*comprobar que el avance introducirdo sea mayor al ultimo avance*/
	if($avance->comprobarAvance($id_contrato, $fecha, $por_avance) == true){
		echo "<div class=\"warning\">El avance debe ser mayor al ultimo registrado.</div>";
		exit;
		}

	/*crear el objeto avance*/
	$objeto = $avance->crearAvance($id_contrato, $id_usuario, $fecha, $por_avance);
	
	/*comprobar que se aya creado el objeto*/
	if($objeto == true){
		echo "<script>window.history.back();</script>";
		}
	}
?>
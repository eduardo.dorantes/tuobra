<?php
session_start();
require('../funciones/query.class.inc.php');
require('../funciones/config.php');

if($_SESSION['login'] != true){
	exit;
	}
/*crear nuevo objeto query*/
$query = new querys();
/*obtener el id de edicion*/
$id_evento = $_POST['selected-item'][0];
if(!empty($id_evento)){
	$row = $query->traerSoloResultado('SELECT `tipo`, `fecha`, `hora`, `url_trasmision` FROM `licitaciones_eventos` WHERE id = :id', $array_bind = array(':id' => $id_evento));
	}
/*query de eventos*/
$eventos = $query->traerMultiplesResultados('SELECT `tipo` FROM `licitaciones_eventos` WHERE id_licitacion = :id', $array_bind = array(':id' => $_POST['id']));
?>
<div id="resultado"></div>
<form name="add-evento-licitacion" id="add-evento-licitacion" method="post">
	<table cellpadding="5" cellspacing="0" border="0" id="table-form-add-obras" width="30%" class="table-forms">
    	<tbody valign="top">
        	<tr>
            	<td><label>Tipo de evento</label></td>
                <td><select name="tipo-evento" id="tipo-evento"><option value="">--Seleccione--</option>
                	<?php
					if(empty($id_evento)){
						foreach($eventos as $eve){
							unset($array_eventos[$eve['tipo']]);
							}
						}
					
                    foreach($array_eventos as $index => $valor){
						if($row['tipo'] == $index){
							echo "<option value=\"".$index."\" selected>".$valor."</option>";
							}else{
								echo "<option value=\"".$index."\">".$valor."</option>";
								}
						
						}
					?>
                   </select>
                </td>
            </tr>
            <tr>
            	<td><label>Fecha</label></td>
                <td><input type="text" name="fecha" id="fecha" class="campo-fecha" required readonly size="10" value="<?php echo !empty($id_evento) ? $row['fecha'] : ''; ?>"></td>
            </tr>
            <tr>
            	<td><label>Hora</label></td>
                <td><input type="text" name="hora" id="hora" size="10" required value="<?php echo !empty($id_evento) ? $row['hora'] : ''; ?>"> Eje: 17:15</td>
            </tr>
            <tr>
            	<td><label>URL trasmisión</label></td>
                <td><input type="url" name="url-trasmision" id="url-trasmision" size="70" value="<?php echo !empty($id_evento) ? $row['url_trasmision'] : ''; ?>"><br>
                Eje:<br>
                <ul style="list-style:none;">
                	<li>https://www.youtube.com/embed/3U6L3jbbt1U</li>
                    <li>https://www.youtube.com/watch?v=3U6L3jbbt1U</li>
                </ul></td>
            </tr>
            <tr>
            	<td><label>Documento</label></td>
                <td><input type="file" name="archivo" id="arcgivo"></td>
            </tr>
            <tr>
            	<td><input type="hidden" name="id-evento" id="id-evento" value="<?php echo $_POST['selected-item'][0]; ?>"><input type="hidden" name="id-licitacion" id="id-licitacion" value="<?php echo $_POST['id']; ?>"></td>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn-green">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                </td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$('#add-evento-licitacion').submit(function(){
	var formData = new FormData($('#add-evento-licitacion')[0]);
	$.ajax({
		beforeSend: function(){
			$("#resultado").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url: 'sources/insert/crear-evento.php',
		data: formData,
		dataType:"json",
		async: false,
		contentType: false,
		processData: false,
		cache: false,
		type:"POST",
		success: function(res){
			if(res.status == true){
				$("#resultado").html('Los datos se han guardado correctamente.');
				$("#add-evento-licitacion")[0].reset();
				setTimeout(function(){
					window.history.back();
					}, 2000);
				}
			},
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			}
		});
	return false;
	});
$(function() {
    $(".campo-fecha").datepicker({dayNames: ['Domingo','Lunes','Martes','Miercoles','Juevez','Viernes','Sabado'], dayNamesMin: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],nextText:'Siguiente',prevText: 'Atras', dateFormat: 'dd/mm/yy', autoSize: true, changeYear: true, monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']});
});
</script>
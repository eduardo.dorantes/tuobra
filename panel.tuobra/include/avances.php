<?php
/************************/
/* AVANCES			    */
/************************/
session_start();
require("../config/conn.php");
require("../sources/msg-file.php");
require("../funciones/avance.class.inc.php");
require("../funciones/contratos.class.inc.php");

/*recibir el id del contrato*/
$id_contrato = !empty($_POST['id']) ? $_POST['id'] : NULL;
/*nuevo contrato*/
$contrato = new contratos($conn, $id_contrato);
/*nuevo objeto avance*/
$avance = new avance($conn, NULL);
$fechas_avances = $avance->generarFechasAvance($id_contrato);

/*comprobar el post y que no venga vacio el id del contrato*/
if(!empty($_POST) and $id_contrato != NULL and $_SESSION['login'] == true){
		/*query*/
		$sql_query = "SELECT ID, FECHA, AVANCE FROM `avances` A WHERE A.id_contrato = ?;";
		/*preparar el query*/
		$query = $conn->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $query->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('i', $id_contrato);
		/*executar query*/
		$query->execute();
		/*store resultados*/
		$query->store_result();
		/*resultados*/
		$query->bind_result($id, $fecha, $avance);
		/*comprobar que aya resultados*/
			if($query->num_rows > 0){
				/*empezar la tabla*/
				echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\" width=\"20%\" style=\"float:left;\">";
				echo "<thead>";
				echo "<tr>";
				echo "<th></th>";
				echo "<th>Fecha</th>";
				echo "<th>Avance</th>";
				echo "</tr>";
				echo "</thead><tbody>";
				/*extraer los resultados*/
				while($query->fetch()){
					echo "<tr>";
					echo "<td align=\"center\"><input type=\"checkbox\" name=\"selected-item[]\" id=\"check_".$id."\" class=\"check_opciones\" value=\"".$id."\"></td>";
					echo "<td align=\"center\">".$fecha."</td>";
					echo "<td align=\"center\">".$avance."%</td>";
					echo "</tr>";
					}
				/*cerrar la tabla*/
				echo "</tbody></table>";
				/*inicio del contenedor de grafica del avance*/
				echo "<div class=\"contenedor-avance-grafica\">";
					/*grafica*/
					include("../sources/grafica-avance.php");
				/*fin del contenedor de grafica de avance*/
				echo "</div>";
				}else{
					echo $mensaje['1003'];
					}
		/*cerrar el query*/
		$query->close();
		/*cerrar la conexcion*/
		$conn->close();
	}
?>
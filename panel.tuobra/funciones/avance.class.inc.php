<?php
class avance{
	
	var $db;//conexion a la base de datos
	private $id_avance;//id del avance
	private $fecha_avance;//fecha del avance
	private $por_avance;//porcentaje de avance
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		$this->db = $conn;//conexion a la base de datos
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			$this->id_avance = $id;//set el id de la obra
			}
		}
	/*crear nuevo objeto avance*/
	public function crearAvance($id_contrato, $id_usuario, $fecha, $avance){
		/*query*/
		$sql_query = "INSERT INTO `avances` (ID_CONTRATO, ID_USUARIO, FECHA, AVANCE) VALUES (?, ?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('iisd',$id_contrato, $id_usuario, $fecha, $avance);
		/*ejecutar query*/
		$query->execute();
		/*el ultimo id registrado*/
		$this->id_avance = $query->insert_id;
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*borrar el avance*/
	public function borrarAvance(){
		/*query*/
		$sql_query = "DELETE FROM `avances` WHERE ID = ?;";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('i', $this->id_avance);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se aya borrado el registor*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*extraer el maximo avance del contrato*/
	private function getUltimoAvance($id_contrato){
		if(!empty($id_contrato) and strlen($id_contrato) > 0){
			/*query*/
			$sql_query = "SELECT FECHA, IFNULL(AVANCE, 0) AS AVANCE FROM `avances` WHERE ID_CONTRATO = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $id_contrato);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($fecha, $avance);
			/*fetch datos*/
			$query->fetch();
			/*datos del avance*/
			$this->fecha_avance = $fecha;
			$this->por_avance = $avance;
			
			/*cerrar conexiones*/
			$query->close();
			}
		}
	/*comparar los avances depediendo de la fecha*/
	public function comprobarAvance($id_contrato, $fecha, $avance){
		/*cargar los datos del ultimo avance*/
		$this->getUltimoAvance($id_contrato);
		/*comprobar si hay un iltomo registro*/
		if($this->por_avance > 0){
			/*convertir las fechas a time tamp*/
			$fechaUltimoAvance = DateTime::createFromFormat('!d/m/Y', $this->fecha_avance)->getTimestamp();//fecha del ultimo avance
			$fechaNueva = DateTime::createFromFormat('!d/m/Y', $fecha)->getTimestamp();//fecha del nuevo avance
			/*compara las fecha*/
			/*comparar si la fecha nueva es menor a la del ultimo avance*/
				if($fechaNueva > $fechaUltimoAvance){
					/*si es menor comparar los avances*/
					/*si el nuevo avance es mayor al ultimo avance regresar true*/
					if($avance < $this->por_avance){
						return true;
						}else{
							/*si es menor regresar false*/
							return false;
							}
					}
			}
		//return false;
		}
	/*funcion para traer las fecha del avance*/
	public function generarFechasAvance($id_contrato){
		/*declarar el array*/
		$fecha_avance = array();
		if(!empty($id_contrato) and strlen($id_contrato) > 0){
			/*query*/
			$sql_query = "SELECT FECHA, AVANCE FROM `avances` WHERE ID_CONTRATO = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $id_contrato);
			/*ejecutar query*/
			$query->execute();
			/*store resultados*/
			$query->store_result();
			/*bind los resultados*/
			$query->bind_result($fecha, $avance);
			/*fetch de los resultados*/
				while($query->fetch()){
					$fecha_avance[$fecha] = $avance;
				}
			/*cerrar conexiones*/
			$query->close();
			/*regresar resultados*/
			return $fecha_avance;
			}
		}
	/*get la fecha del avance*/
	public function getFechaAvance(){
		return $this->fecha_avance;
		}
	/*get el porcentaje de avance*/
	public function getAvance(){
		$this->por_avance;
		}
	}
?>
<?php
/***********************************/
/* 	CONTRATOS  */
session_start();
require("../config/conn.php");
require('../funciones/config.php');
require('../funciones/query.class.inc.php');
require("../sources/msg-file.php");
/*nuevo objequet query*/
$query = new querys();
/*sql aux*/
$sql_aux = "";
/*set array bind*/
/*recibir el ejercicio*/
$ejercicio = $_POST['eje'];
/*eje array bind*/
$array_bind[':eje'] = $ejercicio;
/*search string*/
$search = '%' . $_POST['dato-buscar'] . '%';
/*search array bind*/
$array_bind[':search'] = $search;
/*set array bind id cliente*/
$array_bind[':id_cliente'] = $_SESSION['id-cliente'];
/*modalidad*/
$modalidad = $_POST['modalidad'];
/*tipo de obra*/
$tipo_obra = $_POST['tipo-obra'];
/*municipio*/
$municipio = $_POST['municipio'];

$tipoContrato = $_POST['tipoContrato'];

/*estatus*/
$estatus = $_POST['estatus'];
/*municipi*/
if (!empty($municipio)) {
    $sql_aux .= " AND V.ID_MUNICIPIO = :municipio";
    $array_bind[':municipio'] = $municipio;
}
/*modalidad*/
if (!empty($modalidad)) {
    $sql_aux .= " AND V.TIPO_MODALIDAD = :modalidad";
    $array_bind[':modalidad'] = $modalidad;
}
/*tipo de obra*/
if (!empty($tipo_obra)) {
    $sql_aux .= " AND V.TIPO_OBRA = :tipo_obra";
    $array_bind[':tipo_obra'] = $tipo_obra;
}

if (!empty($tipoContrato)) {
    $sql_aux .= " AND V.TIPO_CONTRATO = :tipoContrato";
    $array_bind[':tipoContrato'] = $tipoContrato;
}



/*estatus*/
if (isset($estatus)) {
    switch ($estatus) {
        case 1:
            $sql_aux .= " AND (V.por_avance IS NULL OR V.por_avance < 100)";
            break;
        case 2:
            $sql_aux .= " AND V.por_avance >= 100";
            break;
        case 'all':
            $sql_aux .= " ";
            break;
        default:
            $sql_aux .= " AND (V.por_avance IS NULL OR V.por_avance <= 100)";
            break;
    }
} else {
    $sql_aux .= " AND (V.por_avance IS NULL OR V.por_avance <= 100)";
}
?>
<div class="lista-ventana">
    <?php
    /*query*/
    $sql_query = "SELECT V.ID, V.NUM_CONTRATO, V.NUM_LICITACION, V.OBJETO, V.IMPORTE_CONTRATO, V.IVA_CONTRATO, V.TOTAL_CONTRATO, 
V.nombre_municipio, V.LOCALIDAD, V.RAZON_SOCIAL, V.RESIDENTE, V.SUPERVEX, V.FECHA_CONTRATO, V.FECHA_INICIO, V.ID_MUNICIPIO, V.FECHA_TERMINO, 
(SELECT IFNULL(MAX(A.AVANCE), 0) FROM `avances` A WHERE A.ID_CONTRATO = V.ID) AS AVANCE, 
(SELECT COUNT(*) FROM `expediente` E WHERE E.ID_CONTRATO = V.ID) AS expediente, 
(SELECT COUNT(*) FROM `avance_fotografico` AF WHERE AF.ID_CONTRATO = V.ID) AS fotos, 
(SELECT COUNT(*) FROM `convenios_modificatorios` CM WHERE CM.ID_CONTRATO = V.ID) AS convenios, 
(SELECT COUNT(*) FROM `bitacora_obra` BO WHERE BO.id_obra = V.ID) AS bitacoras, 
(SELECT COUNT(*) FROM `estudios` E WHERE E.ID_CONTRATO = V.ID) AS estudios, 
(SELECT COUNT(*) FROM `municipiosContratos` MC WHERE MC.idContrato = V.ID) AS municipiosEstudios, V.ONLINE, V.por_avance 
FROM `vistacontratos` V WHERE V.EJERCICIO = :eje AND V.ID_CLIENTE = :id_cliente 
AND (V.NUM_CONTRATO LIKE :search OR V.OBJETO LIKE :search OR V.RAZON_SOCIAL LIKE :search OR V.LOCALIDAD LIKE :search)".$sql_aux." ORDER BY V.NUM_CONTRATO ASC";

    //print_r($array_bind);

    $i = 0;

    /*comprobar que aya resultados*/
    if ($rows = $query->traerMultiplesResultados($sql_query, $array_bind)) {
        /*empezar la tabla*/
       // echo "<p class='alert alert-danger'><strong>Aviso Importante:</strong> A todos los organimos encargados de capturar en el portal TUOBRA.MX se les informa que el día 26 de enero de 2017, este portal estará en proceso de mantenimiento de 8 a 11 horas. Por su atención, muchas gracias. </p>";
        echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" id=\"lista-contratos\">";
        echo "<thead>";
        echo "<tr>";
        echo "<th></th>";
        echo "<th width=\"1%\">&nbsp;</th>";

        echo "<th align=\"left\">Numero contrato</th>";
        echo "<th align=\"left\" width=\"50%\">Objeto</th>";
        echo "<th width=\"5%\">Monto</th>";
        echo "<th align=\"left\">Contratista</th>";
        echo "<th align=\"center\">Avance</th>";
        echo "<th align=\"center\" width=\"5%\">Fecha Contrato</th>";
        echo "<th align=\"center\" width=\"7%\">Fecha inicio</th>";
        echo "<th align=\"center\" width=\"7%\">Fecha termino</th>";
        echo "<th width=\"12%\"></th>";
        echo "</tr>";
        echo "</thead><tbody>";
        /*extraer los resultados*/
        foreach ($rows as $row) {

            $i++;
            $tr = "";

            if ($row['ID_MUNICIPIO'] == 103 && $row['municipiosEstudios'] == 0) {
                $tr = "resaltar";
            }

            echo "<tr class='" . $tr . "'>";
            echo "<td>" . $i . "</td>";

            echo "<td><input type=\"checkbox\" name=\"selected-item[]\" id=\"check_" . $row['ID'] . "\" class=\"check_opciones\" value=\"" . $row['ID'] . "\"> </td>";

            echo "<td>" . $row['NUM_CONTRATO'] . "</td>";
            echo "<td>" . $row['OBJETO'] . "</td>";
            echo "<td align=\"right\">" . number_format($row['TOTAL_CONTRATO'], 2) . "</td>";
            echo "<td>" . $row['RAZON_SOCIAL'] . "</td>";
            echo "<td align=\"center\">";
            echo !empty($row['por_avance']) ? $row['por_avance'] : 0;
            echo "%</td>";
            echo "<td align=\"center\">" . $row['FECHA_CONTRATO'] . "</td>";
            echo "<td align=\"center\">" . $row['FECHA_INICIO'] . "</td>";
            echo "<td align=\"center\">" . $row['FECHA_TERMINO'] . "</td>";
            echo "<td align=\"right\" class=\"over-flow-td\">";
            /*lista de opciones*/
            echo "<ul class=\"opciones-contrato\">";
            //echo "<li data-tooltip=\"Estudios y proyectos\"><a href=\"home/#estudios-".$row['ID']."\" class=\"icon-clipboard-pencil"; if($row['estudios'] > 0){ echo " has-datos"; } echo "\" onClick=\"getRecursoSelect('estudios');\"></a></li>";
            echo "<li data-tooltip=\"Avance\"><a href=\"home/#avances-" . $row['ID'] . "\" class=\"";
            if ($row['por_avance'] > 0) {
                echo " has-datos";
            }
            echo "\" onClick=\"getRecursoSelect('avances');\"> <i class=\"fa fa-bar-chart\" aria-hidden=\"true\"></i></a></li>";
            echo "<li data-tooltip=\"Fotografías\"><a href=\"home/#fotos-" . $row['ID'] . "\" class=\" ";
            if ($row['fotos'] > 0) {
                echo " has-datos";
            }
            echo "\" onClick=\"getRecursoSelect('fotos');\"> <i class=\"fa fa-camera\" aria-hidden=\"true\"></i></a></li>";
            //echo "<li data-tooltip=\"Convenios modificatorios\"><a href=\"home/#convenios-".$row['ID']."\" class=\"icon-conveniosmodificatorios"; if($row['convenios'] > 0){ echo " has-datos"; } echo "\" onClick=\"getRecursoSelect('convenios');\"></a></li>";
            echo "<li data-tooltip=\"Expediente\"><a href=\"home/#expediente-" . $row['ID'] . "\" class=\"";
            if ($row['expediente'] > 0) {
                echo " has-datos";
            }
            echo "\" onClick=\"getRecursoSelect('expediente');\"><i class=\"fa fa-archive\" aria-hidden=\"true\"></i></a></li>";
            /*bitacora*/

            /*
            echo "<li data-tooltip=\"Bitácora de Obra\"><a href=\"home/#bitacora-" . $row['ID'] . "\" class=\"";
            if ($row['bitacoras'] > 0) {
                echo " has-datos";
            }
            echo "\" onClick=\"getRecursoSelect('bitacora');\">
            <i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>
            </a></li>";

            */
            /*publicar*/


            if ($row['ONLINE'] == 0) {
                $pOnline = "Publicar contrato";
                $cOnline = "despublicado";
            } else {
                $pOnline = "Contrato Publicado";
                $cOnline = "publicado";

            }

/*
            echo '<li data-tooltip="'.$pOnline.'">';
            echo  '<a href="javascript:void(0);" data-id="'. $row['ID'] .'" class="publicar-contrato '.$cOnline.'">';
            if ($row['ONLINE'] == 1) {
                echo '<i class="fa fa-cloud" aria-hidden="true"></i>';
            } else {
                echo '<i class="fa fa-cloud-upload" aria-hidden="true"></i>';
            }
            echo '</a></li>';*/

            echo "</ul>";
            echo "</td>";
            echo "</tr>";
        }
        /*cerrar la tabla*/
        echo "</tbody></table>";
    } else {
        echo "<div class=\"info\" id=\"lista-contratos\">" . $mensaje['1003'] . "</div>";
    }
    ?>
</div>
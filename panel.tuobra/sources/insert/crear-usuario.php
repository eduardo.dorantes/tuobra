<?php
session_start();
require("../../config/conn.php");
require("../../funciones/usuarios.class.inc.php");
require("../../sources/msg-file.php");

/*crear un nuevo objeto usuario*/
$usuario = new usuario($conn, NULL);

if(!empty($_POST) && $_SESSION['login'] == true && $_SESSION['tipo'] ==1){
/*obtener los datos*/
$cliente = $_POST['cliente'];
$user = $_POST['usuario'];
$password = $_POST['password_uno'];
$repassword = $_POST['repassword_dos'];
$nombre = $_POST['nombre-usuario'];
$email = $_POST['email-usuario'];


	$r = array();
	$r['status'] = "";
	$r['msj'] = "";
	$r['msjTipo'] = "";

	$valid = true;


	/*comprobar los campos requerdos*/
	if(empty($cliente) || empty($user) || empty($password) || empty($repassword)|| empty($nombre)  || empty($email)){
		$r['status'] = "error";
		$r['msj'] = $mensaje['1001'];
		$r['msjTipo'] = "warning";

		$valid = false;
	}


	if(usuario::comprobarPasswords($password, $repassword) == false){
		$r['status'] = "error";
		$r['msj'] = "Las contraseñas no coinciden";
		$r['msjTipo'] = "warning";

		$valid = false;
	}


	if($valid){

		/*crear usuario*/
		if($usuario->crearUsuario($cliente, $user, $password, $nombre, $email) == true){
			$r['status'] = "success";
			$r['msj'] = $mensaje['1002'];
			$r['msjTipo'] = "success";

		}else{
			$r['status'] = "error";
			$r['msj'] = $mensaje['1007'];
			$r['msjTipo'] = "warning";

		}

	}

	echo json_encode($r);
	exit();



}
?>
<?php
$mensaje = array(
'1001' => "Algún campo requerido se encuentra vacío. Por favor verifique su información.",
'1002' => "Los  datos se han guardado correctamente.",
'1003' => "No encontraron registros.",
'1004' => "Por favor introduzca un correo electrónico valido.",
'1005' => "La dirección de correo electrónico o la contraseña que has introducido no son correctas.",
'1006' => "el documento seleccionado debe ser: ",
'1007' => "Ocurrio un error al guardar el registro."
);
?>
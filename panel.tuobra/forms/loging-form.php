<div class="login">
    <div class="login-cont">
        <img src="images/logo_tu-obra.png" />
    	<h2>Inicia sesión para acceder</h2>
        <div class="error-login">
        	<?php 
			if(!empty($_GET) and isset($_GET['login'])){
				echo $mensaje['1005'];
				}
			?>
        </div>
        <form name="login-form" id="login-form" method="post" action="login.php">
            <input type="text" name="nombre-usuario" id="nombre-usuario" placeholder="E-mail" required spellcheck="false">
            <input type="password" name="password-usuario" id="password-usuario" placeholder="Contraseña" required spellcheck="false">
            <input type="submit" name="enviar-formulario" id="enviar-formulario" class="btn btn-green" value="Iniciar sesión">
        </form>
        <div class="forgot-usuario"><a href="recovery.php">¿Olvidaste tu contraseña?</a></div>
    </div>
</div>
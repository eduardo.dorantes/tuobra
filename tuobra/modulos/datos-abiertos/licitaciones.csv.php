<?php
require('../../librerias/query.class.inc.php');
require('../../librerias/config.php');





// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=licitaciones-'.date("Y-m-d-H-i-s").'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');







// fetch the data
$q = "SELECT LICITACION,ORGANISMO,TIPO,NORMATIVIDAD, descripcion as DESCRIPCION,	FECHA_PUBLICACION, FECHA_VISITA_OBRA, HORA_VO,URL_VO,FECHA_JUNTA_ACLARACIONES,HORA_JA,URL_JA,FECHA_APERTURA_PROPUESTAS,HORA_AP,URL_AP,FECHA_FALLO,HORA_F,URL_F
 FROM vlicitaciones";


$a = $t= array();
$query = new querys();
$r = $query->traerMultiplesResultados($q, $a);


function encode_items(&$item, $key)
{
    $item = utf8_decode($item);
}

array_walk_recursive($r, 'encode_items');

$t = array_keys($r[0]);

// output the column headings
fputcsv($output, $t);


foreach($r as $v){
    fputcsv($output, $v);
}


?>
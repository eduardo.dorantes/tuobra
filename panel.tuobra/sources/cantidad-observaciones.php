<?php
session_start();
require('../funciones/query.class.inc.php');
require('../funciones/config.php');
require("../sources/msg-file.php");

if($_SESSION['login'] != true){
	exit;
	}

/*crear nuevo objeto query*/
$query = new querys();
/*ini set id del cliente*/
$id_cliente = $_SESSION['id-cliente'];
/*resultado array*/
$resultado = array();
/*cantidad de observaciones*/
$resultado['obs'] = 0;

/*query string*/
$query_string = "SELECT o.`id`, o.`tipo`, o.`referencia`, o.`nombre`, o.`email`, o.`comentario`, o.`path_file`, o.`visto`, o.`activo`, o.`fecha_creacion` FROM `observaciones` o INNER JOIN `contratos` c ON c.NUM_CONTRATO = o.referencia WHERE o.tipo = 'obra' AND c.ID_CLIENTE = :id_cliente AND o.visto = 0
UNION ALL
SELECT o.`id`, o.`tipo`, o.`referencia`, o.`nombre`, o.`email`, o.`comentario`, o.`path_file`, o.`visto`, o.`activo`, o.`fecha_creacion` FROM `observaciones` o INNER JOIN `licitaciones` l ON l.num_licitacion = o.referencia WHERE o.tipo = 'licitacion' AND l.id_dependencia = :id_cliente AND o.visto = 0
ORDER BY fecha_creacion DESC";
/*array bind*/
$array_bind = array(':id_cliente' => $id_cliente);

/*filtro*/
//echo "<select name=\"filtro-comentarios\" id=\"filtro-comentarios\"><option value=\"\">--Seleccione--</option><option value=\"1\">No Vistos</option></select>";

/*ejecutar y comprobar query*/
if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
	$resultado['obs'] = count($rows);
	}

echo json_encode($resultado);
?>
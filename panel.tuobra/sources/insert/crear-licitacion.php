<?php
session_start();
require('../../funciones/query.class.inc.php');
require('../../funciones/upload.files.inc.php');

/*checar si es post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*path*/
	$path = "../../files/";
	/*crear nuevo objeto query*/
	$query = new querys();
	/*crear un nuevo objeto upload*/
	$upload = new upload();
	/*resultado*/
	$resultado = array();
	$resultado['status'] = false;
	/*recibir las varibles*/
	$num_licitacion = TRIM($_POST['num_licitacion']);
	$tipo = $_POST['tipo'];
	$normatividad = $_POST['normatividad'];
	$objeto = $_POST['descripcion'];
	$fecha = $_POST['fecha-publicacion'];
	$url = $_POST['url-compranet'];
	/*id de la litacion*/
	$id_licitacion = $_POST['id-licitacion'];

    $desierta = isset($_POST['desierta']) ? $_POST['desierta'] : 0;


	/*extraer la extencion del archivo*/
	$extencion_archvio = upload::getExtencionArchivo($_FILES['archivo']['name']);

	/*checar si viene el archivo*/
	if(!empty($_FILES['archivo']['name'])){
		/*comprobar que sea jpg el archivo*/
		if($extencion_archvio == "jpg"){
			/*upload el archivo*/
			if(!$documento = $upload->uploadFile($path.$num_licitacion.'/', $_FILES['archivo']['name'], $_FILES['archivo']['tmp_name'])){
				echo "Error al subir el archivo.";
				exit;
				}
			}
		}
	/*determinar el query*/
	if(empty($id_licitacion)){
		$query_string = "INSERT INTO `licitaciones` (id_dependencia, num_licitacion, tipo, normatividad, descripcion, fecha_publiacion, url_compranet, foto_proyecto, fecha_creacion, desierta) VALUES (:id_dependencia, :num, :tipo, :norm, :desc, :fecha, :url, :foto, :fecha_creacion, :desierta)";
		$array_bind = array(':id_dependencia' => $_SESSION['id-cliente'],':num' => $num_licitacion,':tipo' => $tipo,':norm' => $normatividad,':desc' => $objeto,':fecha' => $fecha,':url' => $url,':foto' => substr($documento, 6), ':fecha_creacion' => date('Y-m-d H:i:s'), ':desierta' => $desierta);
		}else{
			$query_string = "UPDATE `licitaciones` SET num_licitacion = :num, tipo = :tipo, normatividad = :norm, descripcion = :desc, fecha_publiacion = :fecha, url_compranet = :url, foto_proyecto = :foto, desierta = :desierta WHERE id = :id_licitaciones";
			$array_bind = array(':num' => $num_licitacion,':tipo' => $tipo,':norm' => $normatividad,':desc' => $objeto,':fecha' => $fecha,':url' => $url,':foto' => substr($documento, 6), ':id_licitaciones' => $id_licitacion, ':desierta' => $desierta);
			}
	/*ejecutar y comprobar query*/
	if($query->ejecutarQuery($query_string, $array_bind)){
			$resultado['status'] = true;
		}
	
	echo json_encode($resultado);
	}
?>
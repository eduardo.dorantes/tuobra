<?php
class upload{
	
	private $archivo;//ruta y nombre del archivo con el que se guardo
	
	/*sube al rachivo*/
	public static function uploadFile($path, $file, $tmp_file){
		/*comprobar que la extencion del archivo sea pdf*/
		if(self::getExtencionArchivo($file) !== 'exe'){
			/*comprobar si existe el directorio*/
			if(!file_exists($path)){
				/*si no existe creamos el directorio*/
				mkdir($path, 0777, true);
				}
			/*alistar el path incluyendo el nombre del archivo*/
			$fileName_with_path = $path . self::getNameFile($file) . '_' . (rand() * time()) . '.' . self::getExtencionArchivo($file);
			/*subir el archvio*/
			if(move_uploaded_file($tmp_file, $fileName_with_path)){
				/*si el archivo se subio regresamos true*/
				return $fileName_with_path;
				}else{
					/*si ocurrio un error al subir el archivo*/
					return false;
					}
			}
		}
	/*get path hacia el archivo*/
	public function getPathToFile(){
		return $this->archivo;
		}
	/*get extencion del archivo*/
	public static function getExtencionArchivo($archivo){
		$documento = pathinfo($archivo);
		return strtolower($documento['extension']);
		}
	/*get nombre del documento sin extencion*/
	public static function getNameFile($archivo){
		$documento = pathinfo($archivo);
		return strtolower($documento['filename']);
		}
	}
?>
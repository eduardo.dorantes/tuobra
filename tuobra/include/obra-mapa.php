<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAB4Mdh2n0AKYk-uRPpbXP93VuboQTgy8s"></script>
<script type="text/javascript">


  function getTipoIcono(tipo_){


    var $markerImage = document.querySelector('.markerImage'),
        markerImageSvg = $markerImage.innerHTML || '';


    var color ="";


    switch(tipo_){
      case '1':
        //Educación
        color = "#fb3448";
        //var image = new google.maps.MarkerImage('images/flag_03.png', null, null, null, new google.maps.Size(42, 42));
        break;

      case '2':
        //infraestructura
        //var image = new google.maps.MarkerImage('images/flag_02.png', null, null, null, new google.maps.Size(42, 42));
        color ="#34C08E";
        break;

      case '3':
        // Salud
        //var image = new google.maps.MarkerImage('images/flag_02.png', null, null, null, new google.maps.Size(42, 42));
        color ="#7fcccc";
        break;

      case '4':
        //Carreteras
        color = "#F4B12A";
        //var image = new google.maps.MarkerImage('images/flag_04.png', null, null, null, new google.maps.Size(42, 42));
        break;
      default:
        //var image = new google.maps.MarkerImage('images/flag_05.png', null, null, null, new google.maps.Size(42, 42));
        color ="#b5263b";
        break;
    }


    var image = {
      anchor: new google.maps.Point(36, 26),
      url: 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(markerImageSvg.replace('{{colorPin}}', color))
    }



    return image;
  }






function initMap() {

  var $tipo = '<?php echo $_GET['tipo'] ?>';


  var myLatLng = {lat: <?php echo $_GET['lat'] ?>, lng: <?php echo $_GET['long'] ?>};



  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 9,
    center: myLatLng,
    scrollwheel: false,
    panControl: true,
    panControlOptions: {
      position: google.maps.ControlPosition.RIGHT_TOP
    },
    mapTypeControl: false,

    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.LARGE,
      position: google.maps.ControlPosition.RIGHT_BOTTOM
    },
    streetViewControl: false,
    scrollwheel: false,
    styles: [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}]

  });

  /*var marker = new google.maps.Marker({
    position: myLatLng,
    map: map
  });*/


  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: getTipoIcono($tipo)
  });


}

google.maps.event.addDomListener(window, 'load', initMap);
</script>
<style>
#map-canvas, #contenedor-mapa { height: 100%; margin: 0px; padding: 0px; position:relative; overflow:hidden;}
</style>

<div id="map-canvas"></div>


<div class="markerImage" style="display: none">
  <svg width="34" height="26" viewBox="0 0 34.911 26" xmlns="http://www.w3.org/2000/svg">
    <g opacity="0.25">

      <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="15.6815" y1="47.3321" x2="27.0614" y2="38.7109" gradientTransform="matrix(0.9691 -0.2468 0.2468 0.9691 -7.7202 -18.2274)">
        <stop  offset="0" style="stop-color:#000000"/>
        <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
      </linearGradient>
      <path fill="url(#SVGID_2_)" d="M17.392,26l11.438-4.727c2.055-0.846,3.716-2.018,4.809-3.391c2.656-3.336,1.018-6.792-3.651-7.704
		c-4.669-0.912-10.628,1.06-13.284,4.396c-1.093,1.372-1.493,2.79-1.158,4.1L17.392,26z M26.607,14.425
		c1.392,0.272,1.88,1.302,1.088,2.297s-2.568,1.582-3.96,1.31c-1.392-0.272-1.88-1.302-1.088-2.297S25.215,14.153,26.607,14.425z"/>
    </g>
    <g opacity="0">

      <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-171.8311" y1="-0.4306" x2="-160.4512" y2="-9.0517" gradientTransform="matrix(-0.9691 -0.2468 -0.2468 0.9691 -150.8687 -18.2274)">
        <stop  offset="0" style="stop-color:#000000"/>
        <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
      </linearGradient>
      <path fill="url(#SVGID_3_)" d="M19.366,18.675c0.335-1.31-0.066-2.728-1.158-4.1c-2.656-3.336-8.615-5.308-13.284-4.396
		c-4.669,0.912-6.307,4.368-3.651,7.704c1.093,1.372,2.754,2.544,4.809,3.391L17.519,26L19.366,18.675z M12.264,15.735
		c0.792,0.994,0.304,2.025-1.088,2.297s-3.168-0.316-3.96-1.31s-0.304-2.025,1.088-2.297C9.696,14.153,11.472,14.741,12.264,15.735z
		"/>
    </g>
    <g>
      <g>
        <path fill="{{colorPin}}" d="M17.448,25.988l7.795-9.896c1.403-1.774,2.142-3.9,2.142-6.155c0-5.48-4.458-9.937-9.937-9.937
			S7.511,4.458,7.511,9.937c0,2.254,0.74,4.381,2.139,6.15L17.448,25.988z M17.448,6.975c1.634,0,2.962,1.329,2.962,2.962
			S19.081,12.9,17.448,12.9s-2.962-1.329-2.962-2.962C14.486,8.304,15.814,6.975,17.448,6.975z"/>
      </g>
    </g>
  </svg>
</div>

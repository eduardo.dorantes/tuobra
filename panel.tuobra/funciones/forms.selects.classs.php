<?php
class Selects{
	/* Bind parameters. TYpes: s = string, i = integer, d = double,  b = blob */
	var $db;//conexciona la base de datos
	
	function __construct($conn){
		$this->db = $conn;
		}


    public function selectOrganismos(){
        /*queery*/
        $sql_query = "SELECT c.ID as id, c.NOMBRE as nombre FROM `clientes` c ORDER BY c.NOMBRE ASC;";
        /*preparar el query*/
        $query = $this->db->prepare($sql_query);
        /*comprobar el query*/
        if($query === false){
            trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
        }
        /*ejecutar el query*/
        $query->execute();
        /*bind los resultados*/
        $query->bind_result($id, $nombre);
        /*loop atravez de los resultados*/
        while($query->fetch()){
            echo "<option value=\"".$id."\">".$nombre."</option>";
        }
        /*cerrar la conexion*/
        $query->close();
    }






	public function selectMunicipios(){
		/*queery*/
		$sql_query = "SELECT M.id, M.nombre_municipio FROM `municipios` M ORDER BY M.nombre_municipio;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $nombre);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$nombre."</option>";
			}
		/*cerrar la conexion*/
		$query->close();
		}


	public function selectMunicipiosVarios(){
		/*queery*/
		$sql_query = "SELECT M.id, M.nombre_municipio FROM `municipios` M WHERE M.id !=103 ORDER BY M.nombre_municipio;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
		}
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $nombre);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$nombre."</option>";
		}
		/*cerrar la conexion*/
		$query->close();
	}



	function selectUnidadesAdministrativas($tipo, $cliente){
		/*queery*/
		$sql_query = "SELECT UA.ID, UA.NOMBRE FROM `unidades_administrativas` UA WHERE UA.TIPO = ? AND UA.ID_CLIENTE = ? ORDER BY UA.NOMBRE;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('ii', $tipo, $cliente);
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $nombre);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$nombre."</option>";
			}
		/*cerrar la conexion*/
		$query->close();
		
		}
	function selectResidentes($cliente){
		/*queery*/
		$sql_query = "SELECT RE.ID, RE.NOMBRE FROM `residentes` RE WHERE ID_CLIENTE = ? ORDER BY RE.NOMBRE;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('i', $cliente);
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $nombre);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$nombre."</option>";
			}
		/*cerrar la conexion*/
		$query->close();
		}
	function selectContratistas($cliente){
		/*queery*/
		$sql_query = "SELECT CT.ID, CT.RAZON_SOCIAL FROM `contratistas` CT WHERE CT.ID_CLIENTE = ? ORDER BY CT.RAZON_SOCIAL;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('i', $cliente);
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $nombre);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$nombre."</option>";
			}
		/*cerrar la conexion*/
		$query->close();
		
		}
	function selectSupervisionExterna($cliente){
		/*queery*/
		$sql_query = "SELECT SE.ID, SE.NOMBRE FROM `supervision_externa` SE WHERE SE.ID_CLIENTE = ? ORDER BY SE.NOMBRE;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind parametros*/
		$query->bind_param('i', $cliente);
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $nombre);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$nombre."</option>";
			}
		/*cerrar la conexion*/
		$query->close();
		}
	/*select para la lista de plantillas*/
	public function selectPlantillas($tipo){
		/*queery*/
		$sql_query = "SELECT ID, DOCUMENTO FROM `plantillas` ORDER BY ORDEN;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind parametros*/
		//$query->bind_param('i', $tipo);
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $nombre);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$nombre."</option>";
			}
		/*cerrar la conexion*/
		$query->close();
		}
	
	/*select para la lista de plantillas*/
	public function selectTipoObra(){
		/*queery*/
		$sql_query = "SELECT id, descripcion FROM `tipo_obra` ORDER BY descripcion;";
		/*preparar el query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar el query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*ejecutar el query*/
		$query->execute();
		/*bind los resultados*/
		$query->bind_result($id, $descripcion);
		/*loop atravez de los resultados*/
		while($query->fetch()){
			echo "<option value=\"".$id."\">".$descripcion."</option>";
			}
		/*cerrar la conexion*/
		$query->close();
		}
	}
?>
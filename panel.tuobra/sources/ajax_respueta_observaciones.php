<?php

    session_start();


error_reporting(E_ALL);
ini_set("display_errors", 1);


    require("../config/conn.php");
    require('../funciones/query.class.inc.php');
    require("../funciones/validar.formularios.class.inc.php");
    require("../require/phpmailer/PHPMailerAutoload.php");


    if(!empty($_POST) and $_SESSION['login'] == true){

        $respueta = $_POST['respuesta'];
        $observacionid = $_POST['observacionid'];
        $email = $_POST['email'];
        $tipo = $_POST['tipo'];
        $usuarioid = $_SESSION['id-usuario'];
        $id_cliente = $_SESSION['id-cliente'];
        if(validar::notEmpty($respueta) == true){
            $r['type'] = 'error';
            $r['title'] = 'Debe capturar una respuesta';
        }

        $query = new querys();
        $data = array(':usuarioid' =>$usuarioid, ':fecha' => date('Y-m-d H:i:s'), ':respuesta' =>$respueta, ':observacionid' =>$observacionid);
        $ins ="insert into respuesta_observaciones ( usuarioid, fecha, respuesta, observacionid) values ( :usuarioid, :fecha, :respuesta, :observacionid)";
        $result = $query -> insertarRegistro($ins, $data);

        if($result){


            if($tipo == "obra"){
                $query_string= "SELECT o.id, o.tipo, o.referencia, o.nombre, o.email, o.comentario, o.fecha_creacion  AS fecha, c.NUM_CONTRATO as folio FROM observaciones o INNER JOIN contratos c ON c.NUM_CONTRATO = o.referencia WHERE o.tipo = 'obra' AND o.id = :id_comentario";

            }else if($tipo == "licitacion"){
                $query_string = "SELECT o.id, o.tipo, o.referencia, o.nombre, o.email, o.comentario, o.fecha_creacion AS fecha, l.num_licitacion as folio FROM observaciones o INNER JOIN licitaciones l ON l.num_licitacion = o.referencia WHERE o.tipo = 'licitacion' AND o.id = :id_comentario ";
            }else{
                exit;
            }

            $bind_array = array(':id_comentario' => $observacionid);
            $p = $query->traerSoloResultado($query_string, $bind_array);




            $html = '<div style="margin: 30px auto; width: 70%; min-width: 320px;  font-family: Helvetica Neue, Helvetica, Arial, sans-serif; line-height:1.4em; text-align: justify; color:#2B2922; " >';
            $html .= '<div style="background:#2B2922;padding:15px 30px;text-align: center;">';
            $html .= '<img src="http://www.sonora.gob.mx/images/zo2_zone/logo_tu-obra.png" height="60" style="display: block; margin: auto;">';
            $html .= '</div>';
            $html .= '<div style="padding:30px; font-size:14px; background:#eee; color: #000;">';
            $html .= '<h2 style="line-height: 1.4em">Respuesta a observación en el portal #TuObraSonora</h2>';
            $html .= '<p><strong>Su comentario u observación en <a href="http://tuobra.sonora.gob.mx/' . $p['tipo'] . '/' .htmlentities($p['folio']).'" target="_blank" title="Ir al portal #TuObraSonora">#TuObraSonora</a>:</strong></p>';
            $html .= '<p><em style="font-style: italic; color: #666; font-size: 16px; line-height: 1.4em;;">'.htmlentities($p['comentario']).'</em></p>';
            $html .= '<p>ha sido leido y respondido por el operador del portal, el cual ha dado la siguiente respuesta: </p>';
            $html .= '<p><em style="font-style: italic; color: #666; font-size: 16px; line-height: 1.4em;;">'.$respueta.'</em></p>';
            $html .= '<p>En la fecha '.date('Y-m-d H:i:s').'</p><hr />';
            $html .= '<p>Aviso Importante: Este correo es solo de respuesta. Si tiene otra consulta favor de hacerla directamente en el portal <a href="http://tuobra.sonora.gob.mx" target="_blank" title="Ir al portal #TuObraSonora">#TuObraSonora</a></p>';
            $html .= '</div>';
            $html .= '<div style="background:#ddd;padding:15px 30px;text-align: center; color: #333; font-size: 12px;">';
            $html .= '<img src="http://www.sonora.gob.mx/images/zo2_zone/logo-sonora-unidos-logramos-mas-01.png" height="40" style="display: block; margin: auto;">';
            $html .= '<p> '.date("Y").' Gobierno del Estado de Sonora. Todos los derechos reservados. </p>';
            $html .= '</div>';
            $html .= '</div>';


            //$email = 'miguel.romero@sonora.gob.mx';

            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPDebug 	= 0;
            $mail->SMTPAuth 	= true;
            $mail->SMTPSecure 	= 'ssl';
            $mail->Host 		= 'smtp.gmail.com';
            $mail->Port 		= 465;
            $mail->Username   	= "reply-contacto@sonora.gob.mx";  // GMAIL username (cuenta de correo v?lida)
            $mail->Password 	= "3nv14rm41L";
            $mail->From			= "tuobra@sonora.gob.mx";
            $mail->FromName		= "Portal #TuObraSonora";
            //$mail->AddBCC("silvia.lomeli@sonora.gob.mx", "Silvia Cristina Lomeli Acevedo");
            $mail->AddBCC("dhernandez@sonora.gob.mx", "Diana Michel Hernandez Morales");
            $mail->setLanguage("es", '../require/phpmailer/language/');
            $mail->Subject      = utf8_decode("Respuesta a su solicitud de información");
            $mail->addAddress($email);
            $mail->msgHTML(utf8_decode($html));

            if($mail->send()){
                $r['type'] = 'success';
                $r['title'] = 'El mensaje ha sido enviado';
            }else{
                $r['type'] = 'error';
                $r['title'] = 'Ocurrio un error al enviar el correo electrónico';
            }
        }else{
            $r['type'] = 'error';
            $r['title'] = 'Ocurrio un error al guardar la respuesta';
        }




    }


echo json_encode($r);
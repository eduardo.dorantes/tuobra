<?php
//require('../librerias/query.class.inc.php');
/*crear nuevo objeto query*/
$query = new querys();

/*query string*/
$query_string = "SELECT `ID`, `NOMBRE`, `NOMBRE_CORTO`, `CUSTOMURLMD5` FROM `clientes`";

/*ejecutar query y comprobar*/
if($rows = $query->traerMultiplesResultados($query_string, NULL)){
	/*ini set contador*/
	$cont = 0;
	/*si existen resultados entonces loop para extraerlos*/
	foreach($rows as $raw){



?>

        <div class="panel panel-default" id="<?php echo strtolower($raw['NOMBRE_CORTO']); ?>">
            <div class="panel-heading" role="tab" id="heading<?php echo strtolower($raw['NOMBRE_CORTO']); ?>">
                <h4 class="panel-title text-uppercase">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#acoordion<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" aria-expanded="false" aria-controls="collapse<?php echo strtolower($raw['NOMBRE_CORTO']); ?>">
                        <?php echo $raw['NOMBRE']; ?> <span class="pull-right"><i class="fa fa-play-circle-o fa-2x" aria-hidden="true"></i></span>
                    </a>
                </h4>
            </div>
            <div id="acoordion<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" class="panel-collapse collapse <?php if($cont == 0){ echo 'in'; } ?>" role="tabpanel" aria-labelledby="heading<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" data-id="<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" >
                <div class="panel-body ">


                    <div class="row ">
                        <div class="col-sm-6">
<!--                            <h3>Próximos Eventos</h3>-->
                            <?php include('../include/proximos-eventos-transimision.php'); ?>
                        </div>
                        <div class="col-sm-6">
                            <h3 id="titulo-video_<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" class="titulo-video"></h3>
                            <iframe style="width:100%;" height="400" id="video_cliente_<?php echo strtolower($raw['NOMBRE_CORTO']); ?>" class="video_cliente" name="" src="" frameborder="0" allowfullscreen></iframe>
                            <!-- contenedor redes sociales -->
                            <ul class="social-icons share" data-title="Portal TuObra.mx / Transmisión de <?php echo $raw['NOMBRE']; ?>" data-url="http://tuobra.sonora.gob.mx/trasmisiones/#acoordion-<?php echo strtolower($raw['NOMBRE_CORTO']); ?>">
                        </div>
                    </div>



                </div>
            </div>
        </div>



<?php
	/*comntador ++*/
	$cont++;
	}
}
?>
<?php
session_start();

error_reporting(E_ALL);
ini_set("display_errors", 1);

require('../funciones/query.class.inc.php');
require('../funciones/config.php');
require("../sources/msg-file.php");


if ($_SESSION['login'] != true) {
    exit;
}
/*crear nuevo objeto query*/
$query = new querys();
/*ini set id del cliente*/
$id_cliente = $_SESSION['id-cliente'];


if ($_SESSION['tipo'] != 1) {
    $operadoresC = 'AND c.ID_CLIENTE = :id_cliente';
    $operadoresL = 'AND l.id_dependencia = :id_cliente';
    $array_bind = array(':id_cliente' => $id_cliente);
} else {
    $operadoresC = $operadoresL = '';
    $array_bind = NULL;
}


/*query string*/
$query_string = "SELECT o.`id`, o.`tipo`, o.`referencia`, o.`nombre`, o.`email`, o.`comentario`, o.`path_file`, o.`visto`, o.`activo`, o.`fecha_creacion`, cl.NOMBRE AS organismo, count(respuestaid) as respuestas FROM `observaciones` o INNER JOIN `contratos` c ON c.NUM_CONTRATO = o.referencia LEFT JOIN clientes cl on (c.ID_CLIENTE=cl.ID) LEFT JOIN respuesta_observaciones ro on (o.id=ro.observacionid) WHERE o.tipo = 'obra' " . $operadoresC . " GROUP BY o.id UNION ALL SELECT o.`id`, o.`tipo`, o.`referencia`, o.`nombre`, o.`email`, o.`comentario`, o.`path_file`, o.`visto`, o.`activo`, o.`fecha_creacion`, cl.NOMBRE AS organismo, count(respuestaid) as respuestas FROM `observaciones` o INNER JOIN `licitaciones` l ON l.num_licitacion = o.referencia LEFT JOIN clientes cl on (l.id_dependencia=cl.ID) LEFT JOIN respuesta_observaciones ro on (o.id=ro.observacionid) WHERE o.tipo = 'licitacion' " . $operadoresL . " GROUP BY o.id ORDER BY fecha_creacion DESC";

//print_r($array_bind );

$rows = $query->traerMultiplesResultados($query_string, $array_bind);


/*ejecutar y comprobar query*/
if ($rows) {



    /*display table de resultados*/
    echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" id=\"lista-contratos\">";
    echo "<thead>";
    echo "<tr>";


    if ($_SESSION['tipo'] == 1) {
        echo "<th>Dependencia</th>";
    }


    echo "<th align=\"center\">Fecha</th>";
    echo "<th>Tipo</th>";
    //echo "<th>Referencia</th>";
    echo "<th>Enviado por</th>";
    //echo "<th>Correo Ciudadano</th>";
    echo "<th align=\"left\" width=\"30%\">Comentario</th>";
    //echo "<th>Archivo</th>";

    echo "<th></th>";
    echo "</tr>";
    echo "</thead><tbody>";

    //$contador = 1;

    /*loop para mostrar los resultados*/
    foreach ($rows as $row) {

        $evidencia = '';


        echo "<tr>";


        if ($_SESSION['tipo'] == 1) {
            echo "<td> <br/><strong>" . $row['organismo'] . "</strong></td>";
        }

        //$contador++;

        echo "<td style='width:120px'>" . $row['fecha_creacion'] . "</td>";


        echo "<td><strong>" . $row['tipo'] . "</strong>";

        if ($row['tipo'] == "licitacion") {
            $tipo = "licitacion";
        } else {
            $tipo = "obra";
        }
        echo "<br /> <a data-tooltip=\"Ver en el portal\" style = 'color: #666; ' href='http://tuobra.sonora.gob.mx/" . $tipo . "/" . htmlentities($row['referencia']) . "' target='_blank' title='Ver detalles'>" . $row['referencia'] . "</a></td>";
        echo "<td><strong>" . htmlentities($row['nombre']) . "</strong><br />" . htmlentities($row['email']) . "</td>";
        //echo "<td></td>";

        if (!empty($row['path_file'])) {
            $evidencia = '<hr /> <strong>Evidencia:</strong> <a href="http://tuobra.sonora.gob.mx/' . $row["path_file"] . '" target="_blank"><br/><img src="http://tuobra.sonora.gob.mx/' . $row["path_file"] . '" width="180" title="Evidencia"></a>';
        }

        echo "<td style='width: 380px'>" . htmlentities($row['comentario']) . $evidencia . " </td>";

        $has = '';

        if($row['respuestas']>0){
            $has = 'has-datos';
        }

        echo "<td><ul class='opciones-contrato'><li data-tooltip=\"Respuesta\"></li><li data-tooltip=\"Respuesta\"><a href='javascript:void(0);' class='respuestas ".$has."' data-id='" . $row['id'] . "' data-type='" . $row['tipo'] . "'><span style='display:block'>" . $row['respuestas'] . "</span><i class=\"fa fa-comments\" aria-hidden=\"true\"></i></a></li></ul></td>";

        /*echo "<td>";
        if($row['activo'] == 0){
            echo "<a href=\"javascript:;\" id=\"".$row['id']."\" class=\"publica-obs\" data-type=\"0\"><img src=\"images/icons/1444781395_cloud_cloud-upload.png\" title=\"Publicar Comentario/Observación\"></a>";
            }elseif($row['activo'] == 1){
                echo "<a href=\"javascript:;\" id=\"".$row['id']."\" class=\"publica-obs\" data-type=\"1\"><img src=\"images/icons/1444781395_cloud_cloud-upload-online.png\" title=\"Comentario/Observación publicada\"></a>";
                }
        echo "</td>";*/
        echo "</tr>";
    }
    /*cerrar la tabla*/


    echo "</tbody><tbody></tbody></table>";
}

/*marcar las observaciones como vistas*/
/*ejecutar query y comprobar*/



if(!$query->ejecutarQuery("UPDATE `observaciones` o, `contratos` c SET o.visto = 1 WHERE o.referencia = c.NUM_CONTRATO AND o.tipo = 'obra' AND o.visto = 0 AND c.ID_CLIENTE = :cliente", $array_bind = array(':cliente' => $id_cliente))){
	echo "Error.";
	}
if(!$query->ejecutarQuery("UPDATE `observaciones` o, `licitaciones` l SET o.visto = 1 WHERE o.referencia = l.num_licitacion AND o.tipo = 'licitacion' AND o.visto = 0 AND l.id_dependencia = :cliente", $array_bind = array(':cliente' => $id_cliente))){
	echo "Error.";
	}




?>





<script type="text/javascript">

    $(document).ready(function(){
        $('#cantidad-observaciones').text('0');



        var oTable = $('.zebra').DataTable({
            "lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "Todos"]],
            "order": [[ 1, "desc" ]],
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json"
            }
        } );




        $(".respuestas").click(function (e) {
            e.preventDefault();
            var $id = $(this).data('id');
            var $type = $(this).data('type');
            //PopupCenter('sources/respuesta_observaciones.php?obs=' + $id + '&type=' + $type, 'Responder Observaciones', 900, 400);
            window.open('sources/respuesta_observaciones.php?obs='+$id+ '&type=' + $type, 'Responder Observaciones', 'location=1,status=1,scrollbars=1,width=1000,height=400, fullscreen=yes').focus();
            return false;
        });



    });


</script>
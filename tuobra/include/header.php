<?php
$domain = "http://tuzobra.cf/tuobra/";
?>
<!doctype html>
<html>
<head>
    <base href="<?php echo $domain; ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=no,minimum-scale=1.0,maximum-scale=1.0">
    <?php echo $meta ?>
    <meta content="">
    <meta http-equiv="cache-control" content="no-cache, must-revalidate">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="apple-mobile-web-app-title" content="tuObra">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.png"/>
    <title>Tu Obra | Testigo Virtual Ciudadano</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.1/fullcalendar.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <?php
    if ($patir_url[$baseCalc] == 'licitaciones'){
        echo '<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.1/fullcalendar.min.css" type="text/css" rel="stylesheet">';
    }
    ?>
    <link type="text/css" rel="stylesheet" href="assets/css/custom.css">
</head>
<body <?php echo $body ?> >

<!-- Intro -->



<!-- Top Bar -->
<section id="top-bar">
    <div class="container">
        <div class="row">
            <div id="top1-bar" class="col-sm-6">
                <div class="sp-column ">
                    <ul class="sp-contact-info">
                        <li class="sp-contact-phone"><i class="fa fa-phone"></i> <a href="tel:018004663786"> 01 800 HONESTO</a></li>
                        <li class="sp-contact-email"><i class="fa fa-envelope"></i> <a
                                href="mailto:digeres@hidalgo.gob.mx">digeres@hidalgo.gob.mx</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="top2-bar" class="col-sm-6">
                <div class="sp-column">
                    <ul class="social-icons">
                        <li><a target="_blank" href="https://www.facebook.com/contraloriaHGO"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://twitter.com/contraloria_hgo"><i class="fa fa-twitter"></i></a></li>
                        <li><a target="_blank" href="https://www.youtube.com/user/ccsocialgobhgo"><i class="fa fa-youtube"></i></a></li>
                    </ul>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
    <!-- header -->
<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#menuPrincipal" aria-expanded="false">
                    <span class="sr-only">Menú Principal</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="./">
                    <img class="hidden-xs" src="assets/img/logo_tu-obra.png">
                    <img class="hidden-xs" src="assets/img/Logo-TuzObra.png">
                    <img class="visible-xs" src="assets/img/logo_tu-obra-mobil.png">
                </a>

            </div>

            <div class="collapse navbar-collapse" id="menuPrincipal">
                <ul class="nav navbar-nav pull-right">
                    <li <?php if ($ultima <= 1) { echo "class=\"active\""; } ?>><a href=".">Inicio</a></li>
                    <li <?php if ($patir_url[$baseCalc] == 'acerca') { echo "class=\"active\""; } ?>><a href="#acerca">Acerca de</a></li>
                    <li <?php if ($patir_url[$baseCalc] == 'licitaciones' or $patir_url[$baseCalc] == 'licitacion' or $patir_url[$baseCalc] == 'trasmisiones'){ echo "class=\"active\"";} ?>>
                        <a href="licitaciones/">Licitaciones</a>
                    </li>

                    <li><a href="tuobra/" <?php if ($patir_url[$baseCalc] == 'tuobra' or $patir_url[$baseCalc] == 'obra') {
                            echo "class=\"active\"";
                        } ?>>Obra Pública</a></li>
                    <li><a href="datos-abiertos/" <?php if ($patir_url[$baseCalc] == 'datos-abiertos') {
                            echo "class=\"active\"";
                        } ?>>Datos Abiertos</a></li>
                    <li><a href="contacto/" <?php if ($patir_url[$baseCalc] == 'contacto') {
                            echo "class=\"active\"";
                        } ?>>Contacto</a></li>
                </ul>
            </div>
    </nav>
</header>


<!--style>
#mantenimiento{
    position: relative;
    overflow: hidden;
    z-index: 0;
}

#mantenimiento::before{
    content: '\f071';
    font-family: 'FontAwesome';
    font-size: 300px;
    line-height: 300px;
    position: absolute;
    top: -40px;
    left: 10%;
    opacity: 0.3;
    width: 300px;
    overflow: hidden;
}


</style>

<section id="mantenimiento" style="font-size: 16px; color: #fff; padding: 30px 0; background-color: #f8b133;">

<div class="container">
<div class="row">
    <div class="col-sm-12">
    <h3 style="font-size: 20px; color: #fff;">Atención usuarios del portal TuObra.mx</h3>
    <p>Se les informa que el día 26 de enero de 2017, este portal estará en proceso de mantenimiento de 8 a 11 horas. Disculpe las molestias.</p>
    </div>

</div>
</div>
</section -->


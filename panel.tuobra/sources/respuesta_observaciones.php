<?php
session_start();

error_reporting(E_ALL);
ini_set("display_errors", 1);

require("../funciones/query.class.inc.php");
require("../funciones/config.php");
require("../sources/msg-file.php");

if (empty($_SESSION['id-usuario']) or !isset($_SESSION['login']) or $_SESSION['login'] != true) {
    session_destroy();
    echo "<script>window.close();</script>";
}


$query = new querys();
$id_cliente = $_SESSION['id-cliente'];
$id_comentario = $_GET['obs'];
$tipo = $_GET['type'];

$query_string = '';

if($_SESSION['tipo']!=1){
    $operadoresC = 'AND c.ID_CLIENTE = :id_cliente';
    $operadoresL = 'AND l.id_dependencia = :id_cliente';
    $bind_array = array(':id_comentario' => $id_comentario, ':id_cliente' => $id_cliente);
}else{
    $operadoresC = $operadoresL = '';
    $bind_array = array(':id_comentario' => $id_comentario);
}


if($tipo == "obra"){
    $query_string= "SELECT o.id, o.tipo, o.referencia, o.nombre, o.email, o.comentario, o.fecha_creacion  AS fecha, c.NUM_CONTRATO as folio FROM observaciones o INNER JOIN contratos c ON c.NUM_CONTRATO = o.referencia WHERE o.tipo = 'obra' AND o.id = :id_comentario ".$operadoresC;

}else if($tipo == "licitacion"){
    $query_string = "SELECT o.id, o.tipo, o.referencia, o.nombre, o.email, o.comentario, o.fecha_creacion AS fecha, l.num_licitacion as folio FROM observaciones o INNER JOIN licitaciones l ON l.num_licitacion = o.referencia WHERE o.tipo = 'licitacion' AND o.id = :id_comentario ".$operadoresL;
}else{
 exit;
}

//$bind_array = array(':id_comentario' => $id_comentario, ':id_cliente' => $id_cliente);
//print_r($bind_array);
$p = $query->traerSoloResultado($query_string, $bind_array);
//print_r($p);



?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Respuesta a observaciones</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="../css/custom.bs.css" rel="stylesheet">
</head>
<body>


<div class="wrapper">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><img height="50" src="http://tuobra.sonora.gob.mx/assets/img/logo_tu-obra.png"></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:void(0);" onclick="window.close();">Cerrar</a></li>
            </ul>
        </div><!-- /.container-fluid -->
    </nav>



    <section id="formulario">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-4">
                    <h2>Comentario</h2>
                    <div class="">

                        <div class="form-group">
                            <label class="" for="">Fecha</label><br/>
                            <span class="form-control-static"><?php echo $p['fecha']; ?></span>
                        </div>


                        <div class="form-group">
                            <label class="" for="">Usuario</label><br/>
                            <span class="form-control-static"><?php echo strtoupper(htmlentities($p['nombre'])); ?></span>
                        </div>

                        <div class="form-group">
                            <label class="" for="">Correo Electrónico</label> <br/>
                            <span class="form-control-static"><?php echo strtoupper(htmlentities($p['email'])); ?></span>
                        </div>


                        <div class="form-group">
                            <label class="" for="">Comentario</label><br/>
                            <span class="form-control-static"><?php echo strtoupper(htmlentities($p['comentario'])); ?></span>
                        </div>

                        <div class="form-group">
                            <label class="" for="">Referencia</label><br/>
                            <p class="form-control-static"><a class="btn btn-primary"  target="_blank" href="<?php echo 'http://tuobra.sonora.gob.mx/' . $tipo . '/' .htmlentities($p['folio']); ?>"><?php echo htmlentities($p['folio']); ?></a></p>
                        </div>

                    </div>

                </div>


                <div class="col-sm-8">

                    <h2>Enviar Respuesta</h2>
                    <form id="formRespuesta" name="formRespuesta">
                        <div class="form-group">
                            <textarea id="respuesta" rows="5" class="form-control" name="respuesta" placeholder="Escriba la respuesta aquí"></textarea>
                            <input type="hidden" value="<?php echo $p['email']; ?>" name="email">
                            <input type="hidden" value="<?php echo $p['id']; ?>" name="observacionid">
                            <input type="hidden" value="<?php echo $p['tipo']; ?>" name="tipo">
                            <input type="hidden" value="<?php echo $_SESSION['id-usuario']; ?>" name="usuarioid">
                        </div>
                        <button type="button" id="enviarRespuesta" class="btn btn-default">Enviar</button>
                        <div id="errorAjax"></div>
                        <div id="loadingAjax"><i class="fa fa-cog fa-spin fa-2x fa-fw" aria-hidden="true"></i></div>
                    </form>

                    <hr />

                    <?php

                        $respuestas = new querys();
                        $html = '';
                        $sql = "select rc.*, u.NOMBRE as usuario from respuesta_observaciones as rc left join usuarios as u on(u.ID=rc.usuarioid) where rc.observacionid=:observacionid order by rc.fecha desc";
                        $bind = array(':observacionid'=>$p['id']);
                        $res = $respuestas->traerMultiplesResultados($sql,$bind);

                        if(count($res)>0){

                            foreach($res as $k => $v){
                                $html .= '<tr>';
                                $html .= '<td>'.$v['usuario'].'</td>';
                                $html .= '<td>'.$v['respuesta'].'</td>';
                                $html .= '<td>'.$v['fecha'].'</td>';
                                $html .= '</tr>';
                            }

                        }


                    ?>

                    <h5>Respuestas anteriores</h5>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Respuesta</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php echo $html; ?>
                        </tbody>
                    </table>






                </div>
            </div>
        </div>
    </section>

    <section id="respuestas">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">

                </div>
            </div>
        </div>
    </section>



</div>



<script type="text/javascript">

    $(document).ready(function () {

        $('#enviarRespuesta').click(function(){

            var data = $('#formRespuesta').serializeArray();

            if($('#respuesta').val() != ''){

                $.ajax({

                    url: 'ajax_respueta_observaciones.php?rand=' + Math.random() * 99999,
                    cache: false,
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    error: function(jqXHR, textStatus, errorThrown){
                        $("#errorAjax").html(jqXHR.responseText);
                    },
                    success: function(result){
                        if(result.type == "success"){
                            alert("Mensaje enviado correctamente.");
                            window.location.href = window.location;
                        }
                    },
                    beforeSend: function(){
                        $("#formRespuesta").addClass('loading');
                    },
                    complete: function(){
                        $("#formRespuesta").removeClass('loading');
                    },
                });

            }else{
                alert('Debe escribir una respuesta antes de enviar');
            }




        });



    });

</script>



</body>
</html>
<?php
session_start();
require("../config/conn.php");
require("../funciones/forms.selects.classs.php");
require("../funciones/contratos.class.inc.php");
require("../funciones/parametros.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}
/*crear objeto select*/
$select = new Selects($conn);
/*crear el objeto contrato*/
$contrato = new contratos($conn, $_POST['selected-item'][0]);
/*crear objeto parametros*/
$parametro = new parametros($conn, $_SESSION['id-cliente']);



$mC = $contrato->municipiosContrato();

$mC2 = json_encode($mC);
?>
<div id="resultado" tabindex="-1"></div>
<form name="agregar-nuevo-contrato" id="agregar-nuevo-contrato" method="post">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-form-add-obras" class="table-forms">
    	<caption class="form-caption" style="display:none;"><span>Agregar nuevo contrato</span></caption>
    	<tbody valign="top">
            <tr>
              <td><label for="tipo-contrato">Tipo de contrato</label></td>
              <td><label for="obra-complementaria-uno">¿Es obra complementaria?</label></td>
              <td><label for="obra-cabecera">Obra cabecera</label></td>
            </tr>
            <tr class="separador-form-bottom">
              <td><select name="tipo-contrato" required id="tipo-contrato"><option value="">--Seleccione--</option><option value="1">OBRA</option><option value="2">EQUIPAMIENTO</option><option value="3">SERVICIOS</option></select></td>
              <td><input type="checkbox" name="obra-complementaria" id="obra-complementaria" value="1"> <label for="cabecera-contrato" style="display:inline-block;">Si</label><p style="margin-top:15px; display:block"> <label for="recursos_inifed">¿Recursos INIFED?</label> <input type="checkbox" name="recursos_inifed" id="recursos_inifed" value="1"> <label for="recursos_inifed" style="display:inline-block;">Si</label></p></td>
              <td><select name="obra-cabecera" id="obra-cabecera" disabled><option value="0">--Seleccione--</option></select></td>
            </tr>
            <tr class="separador-form-up">
                <td><label for="tipo-modalidad">Tipo de modalidad</label></td>
                <td><label for="num_contrato">Numero de contrato</label></td>
                <td><label for="num_licitacion">Numero de licitación</label></td>
            </tr>
            <tr>
                <td><select name="tipo-modalidad" required id="tipo-modalidad"><option value="0">--Seleccione--</option><option value="1">LICITACIÓN PÚBLICA</option><option value="2">LICITACIÓN SIMPLIFICADA/INVITACIÓN RESTRINGIDA</option><option value="3">ADJUDICACIÓN DIRECTA</option><option value="4">ADMINISTRACIÓN DIRECTA</option></select></td>
                <td><input name="num_contrato" type="text" required id="num_contrato" value="<?php echo !empty($_POST) ? $contrato->getNumeroContrato() : ''; ?>" size="50"></td>
                <td><input name="num_licitacion" type="text" id="num_licitacion" autocomplete="off" value="<?php echo !empty($_POST) ? $contrato->getNumeroLicitacion() : ''; ?>" size="40">
                	<div id="lista-propuesta-licitaciones"></div>
                </td>
            </tr>
            <tr>
            	<td><label for="importe-contrato">Importe del contrato</label></td>
                <td><label for="porcentaje-iva">IVA</label></td>
                <td><label for="total-contrato">Total contratado</label></td>
            </tr>
            <tr>
            	<td><input name="importe-contrato" type="text" required class="only-numbers" id="importe-contrato" style="text-align:right;" onBlur="convertirCurrency(this);" onKeyUp="validarNumeros(this);" value="<?php echo !empty($_POST) ? number_format($contrato->getImporteContrato(), 2) : ''; ?>"></td>
                <td><input name="porcentaje-iva" type="text" required class="only-numbers" id="porcentaje-iva" onKeyUp="validarNumeros(this);" value="<?php echo !empty($contrato->getIvaContrato()) ? $contrato->getIvaContrato() : $parametro->getIva(); ?>" size="4" maxlength="2"> <span style="color:grey; font-weight:700;">%</span></td>
                <td><input name="total-contrato" type="text" required id="total-contrato" style="text-align:right;" value="<?php echo !empty($_POST) ? number_format($contrato->getTotalContratado(), 2) : ''; ?>" readonly></td>
            </tr>
            <tr>
                <td><label for="objeto_contrato">Objeto del contrato</label></td>
                <td><label for="municipio_prueba">Municipio</label></td>
                <td><label for="localidad">Ubicación/Localidad/Colonia</label></td>
            </tr>
            <tr>
                <td><textarea name="objeto_contrato" cols="50" rows="3" required id="objeto_contrato"><?php echo !empty($_POST) ? $contrato->getObjeto() : ''; ?></textarea></td>
                <td>
                    <select name="municipio" required id="municipio_prueba">
                    <?php
					$select->selectMunicipios();
					?>
                    </select>

					<p id="municipiosVariosC" style="margin: 15px 0; display: none">

						<label for="municipiosVarios" style="margin-bottom: 5px; display: block">Seleccione todos lo municipios:</label>

						<select style="display: block; height: 150px" name="municipioVarios[]" multiple="multiple" required id="municipiosVarios">
							<?php
							$select->selectMunicipiosVarios();
							?>
						</select><br>
						<small>Utilice CMD + Click o CTRL + Click para seleccionar varios</small>
					</p>


                </td>
                <td>
                	<textarea name="localidad" cols="45" rows="2" required id="localidad" style="margin-bottom:10px;"><?php echo !empty($_POST) ? $contrato->getLocalidad() : ''; ?></textarea><br />
                    <label for="latitud-obra" style="display:inline-block;">Latitud: </label> <input type="text" name="latitud-obra" id="latitud-obra" value="<?php echo !empty($_POST) ? $contrato->getLatitud() : ''; ?>" size="12">
                    <label for="longitud-obra" style="display:inline-block;">Longitud: </label> <input type="text" name="longitud-obra" id="longitud-obra" value="<?php echo !empty($_POST) ? $contrato->getLongitud() : ''; ?>" size="12">
                    <a href="javascript:;" title="Seleccione la latitud y longitud desde google maps" id="ventana-mapa-new"><img src="images/icons/1434070383_map.png" alt="mapIcon"></a>
               </td>
            </tr>
            <tr>
                <td><label>Fecha del contrato</label></td>
                <td><label>Fecha de inicio</label></td>
                <td><label>Fecha de término</label></td>
            </tr>
            <tr>
                <td><input name="fecha_contrato" type="text" required class="campo-fecha" id="fecha_contrato" value="<?php echo !empty($_POST) ? $contrato->getFechaContrato() : ''; ?>" size="10"></td>
                <td><input name="fecha_inicio" type="text" required class="campo-fecha" id="fecha_inicio" value="<?php echo !empty($_POST) ? $contrato->getFechaInicio() : ''; ?>" size="10"></td>
                <td><input name="fecha_termino" type="text" required class="campo-fecha" id="fecha_termino" value="<?php echo !empty($_POST) ? $contrato->getFechaTermino() : ''; ?>" size="10"></td>
            </tr>
            <tr>
                <td><label for="contratista">Contratista</label></td>
                <td><label for="residente">Residente</label></td>
                <td><label for="supervision-externa">Supervisión externa</label></td>
            </tr>
            <tr>
                <td><select name="contratista" required id="contratista"><option value="">--Seleccione--</option><?php $select->selectContratistas($_SESSION['id-cliente']); ?></select> <span class="agregar-catalogo" data-tooltip="Agregar nuevo contratista"><a href="javascript:;" class="icon-agregar" id="add-contratista"></a></span></td>
                <td><select name="residente" required id="residente"><option value="0">--Seleccione--</option><?php $select->selectResidentes($_SESSION['id-cliente']); ?></select> <span class="agregar-catalogo" data-tooltip="Agregar nuevo residente"><a href="javascript:;" class="icon-agregar" id="add-residente"></a></span></td>
                <td><select name="supervision-externa" id="supervision-externa"><option value="0">--Seleccione--</option><?php $select->selectSupervisionExterna($_SESSION['id-cliente']); ?></select> <span class="agregar-catalogo" data-tooltip="Agregar supervisión externa"><a href="javascript:;" class="icon-agregar" id="add-supervision"></a></span></td>
            </tr>
            <tr>
                <td><label for="unidad-a-solicitante">Unidad administrativa solicitante</label></td>
                <td><label for="unidad-a-responsable">Unidad administrativa responsable</label></td>
                <td><label for="">Beneficiados</label></td>
            </tr>
            <tr>
                <td><select name="unidad-a-solicitante" id="unidad-a-solicitante" class="unidades-administrativas"><option value="">--Seleccione--</option><?php $select->selectUnidadesAdministrativas(1, $_SESSION['id-cliente']); ?></select></td>
                <td><select name="unidad-a-responsable" id="unidad-a-responsable" class="unidades-administrativas"><option value="">--Seleccione--</option><?php $select->selectUnidadesAdministrativas(2, $_SESSION['id-cliente']); ?></select></td>
                <td><input type="text" name="beneficiados" id="beneficiados" size="5" value="<?php echo !empty($_POST) ? $contrato->getBeneficiados() : ''; ?>"></td>
            </tr>
            <tr>
                <td><label for="fecha-real-termino">Fecha de Inicio de Operación</label></td>
                <td><label for="observaciones-contrato">Observaciones</label></td>
                <td><label>Tipo de Obra</label></td>
            </tr>
            <tr>
                <td valign="top"><input name="fecha-real-termino" type="text" id="fecha-real-termino" class="campo-fecha" autocomplete="off" value="<?php echo !empty($_POST) ? $contrato->getFechaRealTerminacion() : ''; ?>" size="10"></td>
                <td><textarea name="observaciones-contrato" cols="40" rows="3" id="observaciones-contrato"><?php echo !empty($_POST) ? $contrato->getObservaciones() : ''; ?></textarea></td>
                <td><select name="tipo-obra" id="tipo-obra-prueba" required>
                <option value="">--Seleccione--</option>
                <?php $select->selectTipoObra(); ?>
                </select></td>
            </tr>
            <tr>
                <td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn-green">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                </td>
                <td><input type="hidden" name="id-contrato" id="id-contrato" value="<?php echo $_POST['selected-item'][0]; ?>"></td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$(function(){
	$("#importe-contrato, #porcentaje-iva").keyup(function(){
		var importe = parseFloat($("#importe-contrato").val().replace(/[^\d.-]/g, ''));
		var iva_contrato = (parseFloat($("#porcentaje-iva").val().replace(/[^\d.-]/g, '')) / 100 + 1);
		var total = 0;
		if(isNaN(iva_contrato) == false && iva_contrato > 0){
			total = importe * iva_contrato;
			}
			/*regresar el valor ya con formato*/
			$("#total-contrato").val(total.formatoDinero(2, ',', '.'));
		});
		
	$('#agregar-nuevo-contrato').submit(function(){
		var datos = $(this).serialize();
		$.ajax({
			beforeSend: function(){
				$("#resultado").html("Cargado....");
				$("#enviar-formulario").prop("disabled", true);
			},
			url:"sources/insert/crear-contrato.php?rand=" + Math.random() * 9999999,
			type:"POST",
			data: datos,
			dataType:"json",
			error: function(jqXHR, textStatus, errorThrown){
				$("#resultado").html(jqXHR.responseText);
				},
			success: function(res){
				$("#resultado").html(res.msg).focus();
				if(res.status == true){
					$("#enviar-formulario").prop("disabled", false);
					$("#agregar-nuevo-contrato")[0].reset();
					setTimeout(function(){
						window.location = '#';
						}, 3000);
					}
				
				},
			});
		
		return false;
		});
});
	$(document).ready(function(e) {


		$('#municipio_prueba').change(function(){
			var munid = $(this).val();
			if(munid == 103){
				$('p#municipiosVariosC').show();
				$('select#municipiosVarios').prop('required', true);
			}else{
				$('p#municipiosVariosC').hide();
				$('select#municipiosVarios').prop('required', false).val('').trigger('liszt:updated');
			}
		});




        <?php
		if(!empty($_POST) and !empty($_POST['selected-item'][0])){
			/*tipo de contrato*/
			$tipo_contrato = $contrato->getTipoObra();
			/*****************/
			echo "$(\"#tipo-modalidad\").val(\"".$contrato->getModalidad()."\"); \n";
			echo "$(\"#municipio_prueba\").val(\"".$contrato->getIdMunicipio()."\");\n";
			echo "$(\"#contratista\").val(\"".$contrato->getIdContratista()."\");\n";
			echo "$(\"#residente\").val(\"".$contrato->getIdResidente()."\");\n";
			echo "$(\"#supervision-externa\").val(\"".$contrato->getIdSupervisionExterna()."\");\n";
			echo "$(\"#unidad-a-solicitante\").val(\"".$contrato->getIdUnidadResponsable()."\");\n";
			echo "$(\"#unidad-a-responsable\").val(\"".$contrato->getIdUnidadSolicitante()."\");\n";
			echo "$(\"#tipo-contrato\").val(\"".$tipo_contrato[1]."\");\n";
			echo "$(\"#tipo-obra-prueba\").val(".$contrato->getTipoObraNew().");";
			/*obra complementaria*/
			if($contrato->getObraComplementaria() == 1){
				echo "$(\"#obra-complementaria\").prop(\"checked\", true);\t\n";
				echo "$(\"#obra-cabecera\").append('<option selected value=\"".$contrato->getIdObraCabecera()."\">".$contrato->getNumContratoCabcera()."-</option>').val(".$contrato->getIdObraCabecera().").prop(\"disabled\", false);";
			}

			if($contrato->getRecursosINIFED() == 1){
				echo "$(\"#recursos_inifed\").prop(\"checked\", true);\t\n";
			}


			if($contrato->getIdMunicipio() == 103){
				echo "var _mC3js = ".$mC2.";\t\n";

				echo "$(_mC3js).each(function(x,y){\t\n
					$('select#municipiosVarios option[value=' + y + ']').attr('selected', true);\t\n
				});\t\n";

			}




		}else{
			echo "$(\"#municipio_prueba\").val(\"".$parametro->getMunicipio()."\");\t\n";
		}

		echo "$(\"#municipio_prueba\").trigger('change');\t\n";



		?>
    });

$(document).on("click","#ventana-mapa-new", function(){
	window.open('sources/mapa-location.php?lat=<?php echo $contrato->getLatitud(); ?>&log=<?php echo $contrato->getLongitud(); ?>','window','location=1,status=1,scrollbars=1,width=1000,height=900, fullscreen=yes').focus();
	});
$(document).on("click", "#obra-complementaria", function(){
	if($(this).is(":checked")){
		$("#obra-cabecera").prop("disabled", false);
		}else{
			$("#obra-cabecera").prop("disabled", true);
			}
	});
$('#num_licitacion').keyup(function(){
	var q = $(this).val();
	if(q != ''){
		$.ajax({
			url: 'sources/serch-licitaciones-proponer.php',
			type:'POST',
			data: 'q=' + q,
			error: function(jqXHR, textStatus, errorThrown){
				$('#lista-propuesta-licitaciones').html(jqXHR.responseText).show();
				},
			success: function(res){
				$('#lista-propuesta-licitaciones').html(res);
				if($('.licitacion-sugerida').length > 0){
					$('#lista-propuesta-licitaciones').show();
					}
				},
			});
		}
	});
$(document).on('click', '.licitacion-sugerida', function(){
	var num_lic = $(this).attr('id');
	$('#num_licitacion').val(num_lic);
	$('#lista-propuesta-licitaciones').hide();
	});
$("#obra-cabecera").click(function(e){
	var num_elementos = $("#obra-cabecera option").length;
	/*comprobar que no exista ya un registros*/
	if(num_elementos > 1){
		var pregunta = confirm("¿Seguro que desea cambiar la obra ya seleccionada?");
		if(pregunta){
			window.open('sources/obras-cabecera.php?c=<?php echo $_POST['selected-item'][0]; ?>','window','location=1,status=1,scrollbars=1,width=1000,height=900, fullscreen=yes').focus();
			}
		}else{
			window.open('sources/obras-cabecera.php?c=<?php echo $_POST['selected-item'][0]; ?>','window','location=1,status=1,scrollbars=1,width=1000,height=900, fullscreen=yes').focus();
		}
	});
	  $(function() {
    $(".campo-fecha").datepicker({dayNames: ['Domingo','Lunes','Martes','Miercoles','Juevez','Viernes','Sabado'], dayNamesMin: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],nextText:'Siguiente',prevText: 'Atras', dateFormat: 'dd/mm/yy', autoSize: true, changeYear: true, monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']});
  });
</script>
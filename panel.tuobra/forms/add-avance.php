<?php
session_start();
if($_SESSION['login'] != true){
	exit;
	}
?>
<div id="resultado"></div>
<form name="agregar-avance" id="agregar-avance" method="post">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-form-add-avance">
    	<caption class="form-caption" style="display:none;">Agregar nuevo avance</caption>
    	<tbody valign="top">
        	<tr>
            	<td width="11%"><label for="fecha-avance">Fecha</label></td>
                <td width="89%"><input name="fecha-avance" type="text" required id="fecha-avance" class="datepicker" size="10"></td>
            </tr>
            <tr>
            	<td><label for="porcentaje-avance">Porcentaje de avance</label></td>
                <td><input name="porcentaje-avance" type="text" required id="porcentaje-avance" onKeyUp="validarNumeros(this);" size="4" maxlength="4"> <b>%</b></td>
            </tr>
            <tr>
            	<td class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                </td>
                <td><input type="hidden" name="id-usuario" id="id-usuario" value="<?php echo $_SESSION['id-usuario']; ?>"><input type="hidden" name="id-contrato" id="id-contrato" value="<?php echo $_POST['id']; ?>"></td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
 $(function(){$( ".datepicker" ).datepicker({ dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Juevez', 'Viernes', 'Sabado'], dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'], nextText: 'Siguiente', prevText: 'Atras', dateFormat: 'dd/mm/yy', autoSize: true, changeYear: true, monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'] });});
$("#agregar-avance").submit(function(){
	var datos = $(this).serialize();
	$.ajax({
		beforeSend: function(){
			$("#resultado").html("Cargado....");
			$("#enviar-formulario").prop("disabled", true);
			},
		url:"sources/insert/crear-avance.php?rand=" + (new Date()).getTime(),
		type:"POST",
		data: datos,
		dataType:"html",
		cache:false,
		error: function(jqXHR, textStatus, errorThrown){
			$("#resultado").html(jqXHR.responseText);
			},
		success: function(resultados){
			$("#resultado").html(resultados);
			},
		complete: function(){
			$("#enviar-formulario").prop("disabled", false);
			},
		});
	return false;
	});
</script>
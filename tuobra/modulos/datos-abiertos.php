<?php include('../include/header.php'); ?>

<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Datos Abiertos</h2>
                <p>Inicio | Datos Abiertos</p>
            </div>
        </div>
    </div>

</section>

<section id="contentForm">
    <div class="container">
        <div class="row">
            <div id="contenedor-clientes-eventos" class="col-sm-12">

                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr><th>Datos</th><th class="col-sm-1 text-center">CSV</th><th class="col-sm-1 text-center">JSON</th></tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Obra Pública</td>
                        <td class="col-sm-1 text-center"><a class="" href="datos-abiertos/obras/csv"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a></td>
                        <td class="col-sm-1 text-center"><a class="" href="datos-abiertos/obras/json"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a></td>
                    </tr>

                    <tr>
                        <td>Licitaciones</td>
                        <td class="col-sm-1 text-center"><a class="" href="datos-abiertos/licitaciones/csv"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a></td>
                        <td class="col-sm-1 text-center"><a class="" href="datos-abiertos/licitaciones/json"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a></td>
                    </tr>

                    </tbody>


                </table>


            </div>
        </div>
    </div>
</section>

<?php include('../include/footer.php'); ?>


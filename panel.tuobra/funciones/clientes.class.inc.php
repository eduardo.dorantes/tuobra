<?php
class clientes{
	
	var $db;//conexion a la base de datos
	private $id_cliente;//id del cliente
	private $nombre;//nombre del cliente
	private $nombre_corto;
	private $direccion;//direccion del cliente
	private $municipio;//municipio del cliente
	private $telefono;//telefono del cliente
	private $email;
	private $nombre_responsable;//nombre del responsable
	private $custom_url;//url personalizada del cliente
	private $logo;//logo tipo del cliente
	private $mensaje;//mensaje personalizado del cliente
	
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		$this->db = $conn;//conexion a la base de datos
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			$this->id_cliente = $id;
			/*cargar los datos del cliente*/
			$this->datosCliente();
			}
		}
	
	/*actulizar datos del cliente*/
	public function updateCliente($nombre, $nombre_corto, $direccion, $telefono, $municipio, $responsable, $email, $custom_url, $mensaje, $logo){
		if(!empty($this->id_cliente) and strlen($this->id_cliente) > 0){
			$fecha = date("Y-m-d H:i:s");
			/*convertir la url custom en md5*/
			$custom_url_md5 = md5($custom_url);
			/*query*/
			$sql_query = "UPDATE `clientes` SET NOMBRE = ?, NOMBRE_CORTO =?, DIRECCCION = ?, TELEFONO = ?, MUNICIPIO = ?, RESPONSABLE = ?, EMAIL = ?, CUSTOMURL = ?, MENSAJE = ?, LOGO = ?, CUSTOMURLMD5 = ? WHERE ID = ?;";
			/*preparar queery*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('ssssissssssi',$nombre, $nombre_corto, $direccion, $telefono, $municipio, $responsable, $email, $custom_url, $mensaje, $logo, $custom_url_md5, $this->id_cliente);
			/*ejecutar query*/
			$query->execute();
			/*comprobar que se ejecutar bien el query*/
			if($query->affected_rows > 0){
				return true;
				}else{
					return false;
					}
			}
		}
	
	/*datos del cliente*/
	public function datosCliente(){
		if(!empty($this->id_cliente) and strlen($this->id_cliente) > 0){
			/*query*/
			$sql_query = "SELECT C.NOMBRE, C.NOMBRE_CORTO, C.DIRECCCION, C.TELEFONO, C.MUNICIPIO, M.nombre_municipio, C.RESPONSABLE, C.EMAIL, C.CUSTOMURL, C.LOGO, C.MENSAJE FROM `clientes` C LEFT JOIN `municipios` M ON M.id = C.MUNICIPIO WHERE C.ID = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_cliente);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($nombre, $nombre_corto, $direccion, $telefono, $id_municipio, $municipio, $responsable, $email, $custom_url, $logo, $mensaje);
			/*fetch datos*/
			$query->fetch();
			/*datos del cliente*/
			$this->nombre = $nombre;
			$this->direccion = $direccion;
			$this->telefono = $telefono;
			$this->municipio = $municipio;
			$this->nombre_responsable = $responsable;
			$this->email = $email;
			$this->custom_url = $custom_url;
			$this->logo = $logo;
			$this->mensaje = $mensaje;
			$this->nombre_corto = $nombre_corto;
			
			/*cerrar conexiones*/
			$query->close();
			}
		}
		
	
	/*get nombre del cliente*/
	public function getNombre(){
		return $this->nombre;
		}
	/*get nombre corto*/
	public function getNombreCorto(){
		return $this->nombre_corto;
		}
	/*get nombre de la direccion*/
	public function getDireccion(){ 
		return $this->direccion;
		}
	/*get telefono del cliente*/
	public function getTelefono(){
		return $this->telefono;
		}
	/*get nombre del municipio*/
	public function getNombreMunicipio(){ 
		return $this->municipio;		
		}
	/*get nombre del responsable*/
	public function getNombreResponsable(){ 
		return $this->nombre_responsable;
		}
	/*get email del cliente*/
	public function getEmail(){ 
		return $this->email;
		}
	/*get custom url del cliente*/
	public function getCustomUrl(){ 
		return $this->custom_url;
		}
	/*get logo del cliente*/
	public function getLogo(){ 
		return $this->logo;
		}
	/*get el mensaje custom del cliente*/
	public function getMensaje(){
		return $this->mensaje;
		}
	
	}
?>
<?php
session_start();
/********************************/
//LISTA DE FRENTES
/********************************/
header("cache-control: no-cache");
include("../../config/conn.php");


if(!empty($_POST) and $_SESSION['login'] == true){
	/*array de tios*/
	$tipos = array(1 => 'Unidad administrativa solicitante', 2 => 'Unidad administrativa responsable');
	/*conexcion a la db*/
	$db = $conn;
	
	/*query string*/
	$query = $db->prepare("SELECT ID, NOMBRE, TIPO FROM unidades_administrativas WHERE ID_CLIENTE = ?");
	if($query === false){
		trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
		}
	/*preparar query*/
	$query->bind_param('i', $_SESSION['id-cliente']);
	/*ejecutar query*/
	$query->execute();
	/*almacenar los resultados*/
	$query->store_result();
	/*bind resultados*/
	$query->bind_result($id, $nombre, $tipo);
	/*comprobar que aya frentes*/
	if($query->num_rows > 0){
		/*loop para sacar los resultados*/
		echo "<table class=\"zebra\" width=\"90%\"><thead><tr><th>Tipo</th><th>Nombre</th><th></th></tr></thead><tbody>";
		while($query->fetch()){
			echo "<tr>";
				echo "<td>".$tipos[$tipo]."</td>";
				echo "<td>".$nombre."</td>";
				echo "<td><a href=\"javascript:;\" class=\"editar-unidad\" data-id=\"".$id."\">Editar</a></td>";
			echo "</tr>";
		}
		/*cerrar tabla*/
		echo "</tbody></table>";
	}
	
	/*cerrar query*/
	$query->close();
	/*cerrar conexion*/
	$db->close();
	}
?>
<?php
require("../config/conn.php");
require("../sources/msg-file.php");
?>
<!-- contenedor de documentos -->
<div class="contenedor-documento-expediente">
<?php
/*recibir el id del contrato*/
$id_contrato = !empty($_POST['id']) ? $_POST['id'] : NULL;

/*comprobar el post y que venga el id del contrato*/
if(!empty($_POST) and !empty($id_contrato)){
	/*queery*/
	$sql_query = "SELECT E.ID, IFNULL(P.DOCUMENTO, E.NOMBRE) AS DOCUMENTO, E.ARCHIVO FROM `expediente` E LEFT JOIN `plantillas` P ON E.ID_PLANTILLA = P.ID WHERE E.ID_CONTRATO = ?;";
	/*preparar el query*/
	$query = $conn->prepare($sql_query);
	/*comprobar el query*/
	if($query === false){
		trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
		}
	/*bind parametros*/
	$query->bind_param('i', $id_contrato);
	/*ejecutar el query*/
	$query->execute();
	/*store resultados*/
	$query->store_result();
	/*bind los resultados*/
	$query->bind_result($id, $documento, $archivo);
		/*comprobar que aya resultados*/
		if($query->num_rows > 0){
			/*empezar la tabla*/
			echo "<table class=\"zebra\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
			echo "<thead>";
			echo "<tr>";
			echo "<th width=\"5%\"></th>";
			echo "<th align=\"left\">Documento</th>";
			echo "<th></th>";
			echo "</tr>";
			echo "</thead><tbody>";
			/*extraer los resultados*/
			while($query->fetch()){
				echo "<tr>";
				echo "<td align=\"center\"><input type=\"checkbox\" name=\"selected-item[]\" id=\"check_".$id."\" class=\"check_opciones\" value=\"".$id."\"></td>";
				echo "<td>".$documento."</td>";
				echo "<td align=\"center\"><a href=\"#".$archivo."\" class=\"documento-expediente\" id=\"".$id."\"><img src=\"images/icons/pdf_big.png\" title=\"".$documento."\" /></a></td>";
				echo "</tr>";
				}
			/*cerrar la tabla*/
			echo "</tbody></table>";
			}else{
				echo $mensaje[1003];
				}
	/*cerrar la conexion*/
	$query->close();
	}
?>
</div>
<!-- fin del contenedor de documentos -->
<!-- contenedor vista previa del documento -->
<div class="contenedor-documento-vista-previa">
	<div class="inner-contenedor-vista-previa" id="contenedor-vista-previa">
		<div class="titulo-vista-previa" id="titulo-vista-previa"><h3>Vista previa del documento </h3></div>
    </div>
</div>
<!-- fin del contenedor de vista previa -->
<?php
session_start();
require('../../funciones/query.class.inc.php');
require('../../funciones/upload.files.inc.php');
/*checar si es post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*set path*/
	$path= "../../files/licitacion/";
	/*crear nuevo objeto query*/
	$query = new querys();
	$upload = new upload();
	/*crear un nuevo objeto upload*/
	/*resultado*/
	$resultado = array();
	$resultado['status'] = false;
	/*recibir las varibles*/
	$id_licitacion = $_POST['id-licitacion'];
	$tipo = $_POST['tipo-evento'];
	$fecha = $_POST['fecha'];
	$hora = $_POST['hora'];
	$url = $_POST['url-trasmision'];
	/*id de la litacion*/
	$id_evento = $_POST['id-evento'];
	$name_file = $_FILES['archivo']['name'];
	$tmp_name = $_FILES['archivo']['tmp_name'];
	
	/*extraer la extencion del archivo*/
	$extencion_archvio = upload::getExtencionArchivo($_FILES['archivo']['name']);
	
	/*comprobar que venga el documento*/
	if(!empty($name_file)){
		/*comprobar extencion del documento*/
		if($extencion_archvio == "pdf"){
			/*subir el documento*/
			  if(!$documento = $upload->uploadFile($path.$id_licitacion.'/eventos/', $name_file, $tmp_name)){
				  echo "Error no se pudo subir el documento.";
				  }
			  }
		}
	
	/*determinar el query*/
	if(empty($id_evento)){
		$query_string = "INSERT INTO `licitaciones_eventos` (`id_licitacion`, `tipo`, `fecha`, `hora`, `url_trasmision`, `documento`) VALUES (:id_licitacion, :tipo, :fecha, :hora, :url, :documento)";
		$array_bind = array(':id_licitacion' => $id_licitacion, ':tipo' => $tipo, ':fecha' => $fecha, ':hora' => $hora, ':url' => $url, ':documento' => substr($documento, 6));
		}else{
			if(empty($name_file)){
				$query_string = "UPDATE `licitaciones_eventos` SET `tipo` = :tipo, `fecha` = :fecha, `hora` = :hora, `url_trasmision` = :url WHERE id = :id";
				$array_bind = array(':tipo' => $tipo, ':fecha' => $fecha, ':hora' => $hora, ':url' => $url, ':id' => $id_evento);
				}else{
					$query_string = "UPDATE `licitaciones_eventos` SET `tipo` = :tipo, `fecha` = :fecha, `hora` = :hora, `url_trasmision` = :url, `documento` = :documento WHERE id = :id";
					$array_bind = array(':tipo' => $tipo, ':fecha' => $fecha, ':hora' => $hora, ':url' => $url, ':documento' => substr($documento, 6), ':id' => $id_evento);
					}
			}
	/*ejecutar y comprobar query*/
	if($query->ejecutarQuery($query_string, $array_bind)){
			$resultado['status'] = true;
		}
	
	echo json_encode($resultado);
	}
?>
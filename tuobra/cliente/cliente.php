<?php
require('../librerias/query.class.inc.php');
/*crear nuevo objeto query*/
$query = new querys();
/*recibir el id del cliente*/
$id_cliente = $_GET['cliente'];
/*comprobar que venga*/
if(!isset($_GET['cliente']) or empty($id_cliente)){
	exit;
	}
/*query para extraer la informacion del cliente*/
if(!$row = $query->traerSoloResultado('SELECT `ID`, `NOMBRE`, `NOMBRE_CORTO` FROM `clientes` WHERE `CUSTOMURLMD5` = :id_cliente', $array_bind = array(':id_cliente' => $id_cliente))){
	echo 'Error: 404!';
	exit;
	}else{
		$nombre_corto = strtolower($row['NOMBRE_CORTO']);
		}
echo $_SERVER['HTTP_REFERER'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $row['NOMBRE']; ?></title>
<link type="text/css" rel="stylesheet" href="http://tuobra.sonora.gob.mx/css/tuobra_css.css">
<link type="text/css" rel="stylesheet" href="http://tuobra.sonora.gob.mx/css/cliente.css">
<link type="text/css" rel="stylesheet" href="http://tuobra.sonora.gob.mx/css/styles.css">
<script type="text/javascript" src="http://tuobra.sonora.gob.mx/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="http://tuobra.sonora.gob.mx/js/youtube.js"></script>
<script type="text/javascript" src="http://tuobra.sonora.gob.mx/js/cliente.js"></script>
</head>

<body>
<!-- contenedor cliente -->
<div id="contenedor-cliente">
    <!-- contenedor inside acoordion -->
    <div class="contenedor-transmision-inside-acoordion show-acoordion" id="acoordion-<?php echo $nombre_corto; ?>">
      <!-- proximos eventos -->
      <div class="contenedor-transmisiones-proximos-eventos">
          <h2>Próximos Eventos</h2>
          <?php include('../include/proximos-eventos-transimision.php'); ?>
      </div>
      <!-- embed video live -->
      <div class="contenedor-transmisiones-video">
        <h3 id="titulo-video_<?php echo $nombre_corto; ?>" class="titulo-video"></h3>
        <iframe style="width:100%;" height="600" id="video_cliente_<?php echo $nombre_corto; ?>" class="video_cliente" name="" src="" frameborder="0" allowfullscreen></iframe>
          <!-- contenedor redes sociales -->
          <div class="contenedor-transmision-redes-sociales">
            <!-- <ul>
                <li><a href="#" class="tu-obra-twitter-circled"></a></li>
                <li><a href="#" class="tu-obra-facebook-circled"></a></li>
            </ul> -->
          </div>
      </div>
    <!-- fin contenedor acoordion -->
    </div>
    <!-- contenedor header -->
    <div class="header">
        <!-- logo -->
        <figure>
            <a href="http://tuobra.sonora.gob.mx/trasmisiones/#<?php echo $row['NOMBRE_CORTO']; ?>" target="_blank" title="Testigo Virtual Ciudadano"><img src="../images/logo.png" title="Testigo Virtual Ciudadano - Logo"></a>
        </figure>
    </div>
</div>
</body>
</html>
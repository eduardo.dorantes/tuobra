<?php
session_start();
require("../config/conn.php");
require("../funciones/convenios.modificatorios.class.inc.php");

if($_SESSION['login'] != true){
	exit;
	}

/*crear objeto convenio*/
$convenio = new conveniosModificatorios($conn, $_POST['selected-item'][0]);

?>
<div id="resultado"></div>
<div style="width:20%; height:5px; margin-top:10px; margin-bottom:10px;">
	<div id="progreso" style="height:5px; background-color:green; width:0px; text-align:center; position:relative;"><span id="porcentaje" style=" position:absolute; left:0;"></span></div>
</div>
<form name="agregar-convenio-modificatorio" id="agregar-convenio-modificatorio" method="post" enctype="multipart/form-data">
	<table width="100%" cellpadding="5" cellspacing="0" border="0" id="table-form-add-convenios-modificatorios">
    	<tbody valign="top">
        	<tr>
            	<td width="21%"><label>Fecha:</label></td>
                <td colspan="3"><input name="fecha" type="text" required id="fecha" class="campo-fecha" size="10" value="<?php echo $convenio->getFecha(); ?>"></td>
            </tr>
            <tr>
            	<td><label>Tipo:</label></td>
                <td colspan="3" class="radios">
                	<input name="tipo" type="radio" required id="tipo" value="1"><label for="tipo">Plazo</label>
                    <input name="tipo" type="radio" required id="monto" value="3"><label for="monto">Monto</label>
                    <input name="tipo" type="radio" required id="monto-plazo" value="4"><label for="monto-plazo">Plazo/Monto</label>
                    <input name="tipo" type="radio" required id="objeto" value="2"><label for="objeto">Objeto</label>
                </td>
            </tr>
            <tr>
            	<td><label for="fechaInicio">Fecha Inicio:</label></td>
                <td width="24%"><input name="fechaInicio" type="text" disabled="disabled" required id="fechaInicio" class="campo-fecha" size="10" value="<?php echo $convenio->getFechaInicio(); ?>"></td>
                <td width="7%"><label for="fechaTermino">Fecha Termino:</label></td>
                <td width="48%"><input name="fechaTermino" type="text" disabled="disabled" required id="fechaTermino" class="campo-fecha" size="10" value="<?php echo $convenio->getFechaTermino(); ?>"></td>
            </tr>
            <tr>
            	<td><label for="monto_">Monto:</label></td>
                <td colspan="3"><input name="monto" type="text" disabled="disabled" required class="montos" id="monto_" style="text-align:right;" onBlur="convertirCurrency(this);" onKeyUp="validarNumeros(this);" value="<?php echo number_format($convenio->getMonto(), 2); ?>"></td>
            </tr>
            <tr>
            	<td><label for="objeto_">Nuevo objeto</label></td>
                <td colspan="3"><textarea name="objeto" cols="45" rows="3" disabled="disabled" required id="objeto_"><?php echo $convenio->getObejto(); ?></textarea></td>
            </tr>
            <tr>
            	<td><label for="documento_convenio">Documento</label></td>
                <td colspan="3"><input type="file" name="documento_convenio" id="documento_convenio" accept="application/pdf"></td>
            </tr>
            <tr>
            	<td colspan="4" class="content-form-buttons">
                	<input type="submit" name="enviar-formulario" id="enviar-formulario" value="Guardar" class="btn">
                    <input type="button" name="cancelar-formulario" id="cancelar-formulario" value="Cancelar" class="btn" onClick="window.history.back();">
                    <input type="hidden" name="id-convenio" id="id-convenio" value="<?php echo $_POST['selected-item'][0]; ?>">
                    <input type="hidden" name="id-usuario" id="id-usuario" value="<?php echo $_SESSION['id-usuario']; ?>">
                    <input type="hidden" name="id-contrato" id="id-contrato" value="<?php echo $_POST['id']; ?>">
                </td>
            </tr>
        </tbody>
    </table>
</form>
<script type="text/javascript">
$(document).ready(function(e) {
	var tipo = '<?php echo $convenio->getTipo(); ?>';
    $("input[name=tipo][value=" + tipo + "]").prop('checked', true);
	validarTipo(tipo);
});
$(document).on("click", "input[name=tipo]", function(){
	var tipo = $(this).val();
	$("#fechaInicio, #fechaTermino, #monto_, #objeto_").attr("disabled", true);
	validarTipo(tipo);
	});

function validarTipo(tipo){
	switch(tipo){
		case '1':
			$("#fechaInicio, #fechaTermino").attr("disabled", false);
		break;
		case '2':
			$("#objeto_").attr("disabled", false);
		break;
		case '3':
			$("#monto_").attr("disabled", false);
		break;
		case '4':
		$("#fechaInicio, #fechaTermino, #monto_").attr("disabled", false);
		break;
		}
	}

$("#agregar-convenio-modificatorio").submit(function(e){
	e.preventDefault();
	
	if(window.FormData == undefined){
		alert('Necesita un navegador que soporte HTML5');
		return false;
		}
	var dataForm = new FormData($("#agregar-convenio-modificatorio")[0]);
	
	$.ajax({
		beforeSend: function(){},
			url:"sources/insert/crear-convenio-modificatorio.php?rand=" + Math.random() * 9999999,
			type: "POST",
			cache: false,
            contentType: false,
            processData: false,
			data: dataForm,
			xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('progress',function(ev) {
						if (ev.lengthComputable) {
							var percentUploaded = Math.floor(ev.loaded * 100 / ev.total);
							$("#progreso").css('width', percentUploaded + '%');
							$("#porcentaje").text(percentUploaded + '%');
						}
				   }, false);
				}
				return myXhr;
            },
			error: function(jqXHR, textStatus, errorThrown){
				$("#resultado").html(jqXHR.responseText).show();
				},
			success: function(resultado){
				$("#resultado").html(resultado).show();
				},
			complete: function(){
				},
		});
	return false;
	});
	$(function() {
    $(".campo-fecha").datepicker({dayNames: ['Domingo','Lunes','Martes','Miercoles','Juevez','Viernes','Sabado'], dayNamesMin: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],nextText:'Siguiente',prevText: 'Atras', dateFormat: 'dd/mm/yy', autoSize: true, changeYear: true, monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']});
  });
</script>
<?php
class fotos{
	
	var $db;//conexcion a la base de datos
	private $id_fotos;//id del registro de la foto
	private $fecha_foto;//fecha de la imagen
	private $pie_imagen;//descripcion de la imagen
	private $archivo;//path al documento imagen
	private $thub_nail;//thubnail de  la imagen
	private $thub_nail_large;//thubnail de la imagen large
	
	/*constructor*/
	function __construct($conn, $id = NULL){
		//conexion a la base de datos
		$this->db = $conn;
		/*checar si el id de la obra bien vacio o no*/
		if(!is_null($id) and !empty($id)){
			/* set el id de la obra */
			$this->id_fotos = $id;
			/*si biene el id de la imagen entonces creamos la imgen del id*/
			$this->datosDeImagen();
			}
		}
	/*crear una nueva foto*/
	public function crearFoto($id_contrato, $id_frente, $id_usuario, $fecha, $pie_foto){
		/*query*/
		$sql_query = "INSERT INTO `avance_fotografico` (ID_CONTRATO, ID_FRENTE, ID_USUARIO, FECHA, PIE_FOTO) VALUES (?, ?, ?, ?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('iiiss',$id_contrato, $id_frente, $id_usuario, $fecha, $pie_foto);
		/*ejecutar query*/
		$query->execute();
		/*el ultimo id registrado*/
		$this->id_fotos = $query->insert_id;
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*datos de la imagen*/
	private function datosDeImagen(){
		/*comprobar que venga el id de la foto*/
		if(!empty($this->id_fotos) and strlen($this->id_fotos) > 0){
			/*query*/
			$sql_query = "SELECT FECHA, PIE_FOTO, IMAGEN, THUMB, THUMB_LARGE FROM `avance_fotografico` WHERE ID = ?;";
			/*preparar el query*/
			$query = $this->db->prepare($sql_query);
			/*comprobar el query*/
			if($query === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind los resultados*/
			$query->bind_param('i', $this->id_fotos);
			/*ejecutar query*/
			$query->execute();
			/*bind los resultados*/
			$query->bind_result($fecha, $pie, $imagen_original, $thub, $thub_large);
			/*fetch datos*/
			$query->fetch();
			/*datos de la imagen*/
			$this->fecha_foto = $fecha;
			$this->pie_imagen = $pie;
			$this->archivo = $imagen_original;
			$this->thub_nail = $thub;
			$this->thub_nail_large = $thub_large;
			
			/*cerrar conexiones*/
			$query->close();
			}
		}
	/*actulizar foto*/
	public function actulizarFoto($campo, $nuevoValor){
		/*query*/
		$sql_query = "UPDATE `avance_fotografico` SET $campo = ? WHERE ID = ?;";
		/*preparar queery*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('si', $nuevoValor, $this->id_fotos);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*borrar la foto*/
	public function borrarFoto(){
		/*query*/
		$sql_query = "DELETE FROM `avance_fotografico` WHERE ID = ?;";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('i', $this->id_fotos);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se aya borrado el registor*/
		if($query->affected_rows > 0){
			/*si todos salio bien entonces borramos los documentos */
			$this->deleteImagenes($this->archivo);//borramos el documento original
			$this->deleteImagenes($this->thub_nail);//borramos el documento thumb
			$this->deleteImagenes($this->thub_nail_large);//borramos el documento thumb large
			/*todo bien regresamos true*/
			return true;
			}else{
				/*si no se borra regresamos false*/
				return false;
				}
		}
	/*borrar las imagenes del servidor*/
	private function deleteImagenes($archivo_imagen){
		/*path*/
		$path = "../../";
		/*comprobar que venga el id de la foto*/
		if($this->id_fotos){
			/*set path hacia el documento*/
			$path .= $archivo_imagen;
			/*comprobar que el archivo exista*/
			if(file_exists($path)){
				/*si el archivo existe entonces lo borramos*/
				if(unlink($path)){
					return true;
					}else{
						return false;
						}
				}
			}
		}
	/*crear frente*/
	public function crearFrente($id_contrato, $descripcion){
		/*query*/
		$sql_query = "INSERT INTO `avance_fotografico_frentes` (ID_CONTRATO, DESCRIPCION) VALUES (?, ?);";
		/*preparar query*/
		$query = $this->db->prepare($sql_query);
		/*comprobar query*/
		if($query === false){
			trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
			}
		/*bind los resultados*/
		$query->bind_param('is', $id_contrato, $descripcion);
		/*ejecutar query*/
		$query->execute();
		/*comprobar que se ejecutar bien el query*/
		if($query->affected_rows > 0){
			return true;
			}else{
				return false;
				}
		}
	/*get el id del foto*/
	public function getIdFoto(){
		return $this->id_fotos;
		}
	/*get la fecha del foto*/
	public function getFechaFoto(){
		return $this->fecha_foto;
		}
	/*get el pie de pagina*/
	public function getPieFoto(){
	   return $this->pie_imagen;
		}
	/*get el archivo*/
	public function getArchivo(){
		return $this->archivo;
		}
	/*get el thumb nail*/
	public function getThumbNail(){
		return $this->thub_nail;
		}
	/*get el thumb nail large*/
	public function getThumbNailLarge(){
		return $this->thub_nail_large;
		}
	}
?>
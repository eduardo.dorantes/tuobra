<?php
require('../librerias/query.class.inc.php');
require('../librerias/config.php');
/*nuevo objeto query*/
$query = new querys();
/*query municipios*/
$municipios = $query->traerMultiplesResultados('SELECT M.id, M.nombre_municipio FROM `municipios` M');
$clientes = $query->traerMultiplesResultados('SELECT `ID`, `NOMBRE` FROM `clientes` ORDER BY `NOMBRE`', NULL);
$tipos_obra = $query->traerMultiplesResultados('SELECT id, descripcion FROM `tipo_obra` GROUP BY id, descripcion ORDER BY descripcion', NULL);
?>
<form name="buscar-obras" id="buscar-obras" action="obras/" method="get" class="form-horizontal">


    <div class="form-group">

        <label for="ejercicio-del" class="col-sm-1 text-left control-label">Ejercicio</label>
        <div class="col-sm-1">
            <select name="ejercicio-del" id="ejercicio-del" class="form-control">
                <?php for($i=2015; $i<=date('Y'); $i++ ){
                    echo '<option value="'.$i.'">'.$i.'</option>';
                } ?>
            </select>
        </div>
        <label for="ejercicio-al" class="col-sm-1 text-center control-label ">al</label>
        <div class="col-sm-1">
            <select name="ejercicio-al" id="ejercicio-al" class="form-control">
                <?php for($i=2016; $i<=date('Y'); $i++ ){
                    echo '<option value="'.$i.'">'.$i.'</option>';
                } ?>
            </select>
        </div>


        <label for="contrato-al" class="col-sm-2 text-center control-label ">Tipo de Contrato</label>
        <div class="col-sm-2">
            <select name="contrato-al" id="contrato-al" class="form-control">
                <option value="">--Seleccione--</option>
                <option value="1">OBRA</option>
                <option value="2">EQUIPAMIENTO</option>
                <option value="3">SERVICIOS</option>
            </select>
        </div>


        <label for="estatus" class="col-sm-1 text-left control-label">Estatus</label>
        <div class="col-sm-3">
            <select name="estatus" id="estatus" class="form-control">
                <option value="">Todas</option>
                <option value="1">En Proceso</option>
                <option value="2">Finalizadas</option>
            </select>
        </div>


    </div>


    <div class="form-group">

        <label for="dependencia-licitacion" class="col-sm-2 text-left control-label">Dependencia / Organismo</label>
        <div class="col-sm-4">
            <select name="ejecutora" id="ejecutora" class="form-control">
                <option value="">Todas</option>
                <?php
                foreach ($clientes as $value) {
                    echo "<option value=\"" . $value['ID'] . "\">" . $value['NOMBRE'] . "</option>";
                }
                ?>
            </select>
        </div>


        <label for="tipo" class="col-sm-1 text-left control-label">Modalidad</label>
        <div class="col-sm-5">
            <select name="modalidad" id="modalidad" class="form-control">
                <option value="">Todas</option>
                <?php
                foreach ($array_modalidad as $index => $mod) {
                    echo "<option value=\"" . $index . "\">" . $mod . "</option>";
                }
                ?>
            </select>
        </div>

    </div>


    <div class="form-group">

        <label for="dependencia-licitacion" class="col-sm-2 text-left control-label">Municipio</label>
        <div class="col-sm-4">
            <select name="municipio" id="municipio" class="form-control">
                <option value="">Todas</option>
                <?php
                foreach ($municipios as $mun) {
                    echo "<option value=\"" . $mun['id'] . "\">" . $mun['nombre_municipio'] . "</option>";
                }
                ?>
            </select>
        </div>

        <label for="tipo" class="col-sm-1 text-left control-label">Tipo</label>
        <div class="col-sm-3">
            <select name="tipo" id="tipo" class="form-control">
                <option value="">Todas</option>
                <?php
                foreach ($tipos_obra as $tipo) {
                    echo "<option value=\"" . $tipo['id'] . "\">" . $tipo['descripcion'] . "</option>";
                }
                ?>
            </select>
        </div>

    </div>


    <div class="form-group">

        <div class="col-sm-5">
            <input type="text" class="form-control" name="localidad-colonia" id="localidad-colonia" autocomplete="off"
                   placeholder="Localidad, colonia">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="descripcion" id="descripcion" autocomplete="off"
                   placeholder="Buscar por palabras clave"></td>
        </div>
        <div class="col-sm-2 text-right">
            <button name="enviar" id="enviar" type="submit" class="btn btn-success">Buscar</button>
        </div>


    </div>

    <hr/>

</form>



<!--<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" type="text/css" rel="stylesheet">
<script src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.0/js/buttons.flash.min.js"></script>-->



<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.js"></script>



<script type="text/javascript">
    var dataTable = {
        "lengthMenu": [[30, 50, 100,-1], [ 30, 50, 100, "Todo"]],
        responsive: true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        },
        "order": [[2, "des"]],
        dom: 'lBfrtip',
        buttons: {
            name: 'primary',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        }

    }
    $(document).ready(function (e) {
        $('#enviar').trigger('click');
    });

    $('#buscar-obras').submit(function () {
        $('#listado-licitaciones').html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
        $.post('search-obras/', $(this).serialize(), function (res) {
            $('#listado-licitaciones').html(res);


        }).done(function() {
            $('#listado-licitaciones .table').DataTable(dataTable);
        });


        return false;
    });
</script>
/**
 * Created by miguelromero on 16/05/16.
 */



$(document).ready(function(){

    /*
    $.ajax({
        type: "POST",
        url: 'ajax/get_information_works.php',
        data: {'opcion':'get_inversion_works'},
        success: function(data){
            $(data).each(function(x,y){
                $('#information-public-works').find('h2').html(function(index, text) {
                    return text.replace('{{year}}', y.year).replace('{{inversion}}', accounting.formatMoney(y.inversion, "$", 0, ",", "."));
                });
            });
        },
        dataType: 'json'
    });

    */


    function informationWorks(i){
        var data = '';
        if(i!=""){
            data = {'opcion':'get_information_works','year':i};
        }else{
            data = {'opcion':'get_information_works'};
        }

        $.ajax({
        type: "POST",
        url: 'ajax/get_information_works.php',
        data: data,
        success: function(data){

            $(data).each(function(x,y){


                //alert('asdasdasd');

                $('#information-public-works h2 span#inversion').html(accounting.formatMoney(y.inversion, "$", 0, ",", "."));

                $('#totalWorks').html('<h2>'+ y.works+'</h2><p>Total de Obras</p>');
                $('#totalContractors').html('<h2>'+ y.contractors+'</h2><p>Empresas contratistas <br/> ejecutando Obra Pública</p>');
                $('#benefited').html('<i class="fa fa-map-o" aria-hidden="true"></i> <h2>'+accounting.formatMoney(y.benefited, "", 0, ",", ".")+'</h2><p>Municipios Beneficiados</p>');



                $('#health').html(' <h2>'+accounting.formatMoney(y.health[2], "", 0, ",", ".")+'</h2><p>'+y.health[0]+'</p><h4>'+accounting.formatMoney(y.health[1], "$", 0, ",", ".")+'</h4>');

                $('#education').html(' <h2>'+accounting.formatMoney(y.education[2], "", 0, ",", ".")+'</h2><p>'+y.education[0]+'</p><h4>'+accounting.formatMoney(y.education[1], "$", 0, ",", ".")+'</h4>');

                $('#roads').html(' <h2>'+accounting.formatMoney(y.roads[2], "", 0, ",", ".")+'</h2><p>'+y.roads[0]+'</p><h4>'+accounting.formatMoney(y.roads[1], "$", 0, ",", ".")+'</h4>');

                $('#infraestructure').html(' <h2>'+accounting.formatMoney(y.infraestructure[2], "", 0, ",", ".")+'</h2><p>'+y.infraestructure[0]+'</p><h4>'+accounting.formatMoney(y.infraestructure[1], "$", 0, ",", ".")+'</h4>');



                var chartStatusWorks = c3.generate({
                    bindto: '#chartStatusWorks',
                    data: {
                        columns: y.s,
                        type : 'pie'
                    },
                    color: {
                        pattern: ['#EFC98B', '#F29F0C']
                    },
                    tooltip: {
                        format: {
                            value: function (value, ratio, id) {
                                var format = d3.format(',');
                                return format(value);
                            }
                        }
                    }

                });



                var chartByContract = c3.generate({
                    bindto: '#byContract',
                    data: {
                        columns: y.tc,
                        type : 'pie'
                    },
                    color: {
                        pattern: ['#7FCCCC', '#5C918F', '#3DB2AC']
                    },
                    tooltip: {
                        format: {
                            value: function (value, ratio, id) {
                                var format = d3.format(',');
                                return format(value);
                            }
                        }
                    }

                });


                var chartByModality = c3.generate({
                    bindto: '#byModality',
                    data: {
                        columns: y.tm,
                        type : 'pie'
                    },
                    color: {
                        pattern: ['#FB3448', '#D82E46', '#BC0A34', '#821D30', '#4C121E']
                    },
                    tooltip: {
                        format: {
                            value: function (value, ratio, id) {
                                var format = d3.format(',.2f');
                                return "$" +format(value);



                            }
                        }
                    }

                });





            });



        },
        dataType: 'json'
    });
    }

    $.ajax({
        type: "POST",
        url: 'ajax/get_information_works.php',
        data: {'opcion':'get_lastest_finished_works'},
        success: function(data){

                var tipoObra = ['','educacion','infraestructura','salud','carreteras'];

                $(data).each(function(k,v){


                var $html = $('<div>').addClass('thumbnail');
                    $html.append($('<div>').addClass('image'));
                    $html.find('.image').append($('<span>').text(v.tipo).addClass('tipo-obra tipo-'+tipoObra[v.idTipo]));
                    $html.find('.image').append($('<img />').attr('src','http://paneltuobra.sonora.gob.mx/'+ v.thumbnail));
                    $html.append($('<div>').addClass('caption'));
                    $html.find('.caption').append($('<h3>').html(v.contrato));
                    $html.find('.caption').append($('<p>').addClass('municipio').html(v.municipio+', SONORA'));
                    $html.find('.caption').append($('<a>').attr('href','obra/'+ v.contrato).attr('title','Ver Obra: '+ v.contrato).html('<i class="fa fa-chevron-right" aria-hidden="true"></i>').addClass('btn btn-success btn-circle'));
                    $html.find('.caption').append($('<p>').addClass('objeto').html(v.objeto));
                    $html.find('.caption').append($('<p>').addClass('monto').html('Inversión: '+accounting.formatMoney(v.monto, "$", 0, ",", ".")));




                $('#lastestWorks .row').append($('<div>').addClass('col-sm-4').html($html));

            });


        },
        dataType: 'json'
    });


    informationWorks();


    $('#information-public-works .filters ul li a').on('click',function(){
        var year = parseInt($(this).attr('data-year'));
        if(year > 2000 && year < 3000){ ///:)
            informationWorks(year);

            $('#information-public-works .fecha').html('EN EL AÑO '+year);


        }else{
            informationWorks();
            $('#information-public-works .fecha').html('A LA FECHA');
        }

    });

});
<?php
if(!empty($_POST)){ $query = new querys();
	/*set error var*/
	$error = false;
	/*set path*/
	$path = "../files/";
	/*recibir las variables*/
	$nombre = $_POST['nombre'];
	$correo = $_POST['email'];
	$comentario = $_POST['comentario'];
	$file = $_FILES['foto']['name'];
	$tmp_file = $_FILES['foto']['tmp_name'];
	$tipo = $_POST['tipo'];
	$referencia = $_POST['referencia'];
	/*completar path*/
	$path .= $referencia.'/';
	
	/*comprobar que venga los campos requeridos*/
	if(empty($comentario)){
		/*set error to true*/
		$error = true;
		/*mostrar mensaje de comentario vacio*/
		echo 'Debe de introducir un comentario u observación.';
		}
	/*comprobar que tenga mas de 5 palabras*/
	if(str_word_count($comentario) <= 5){
		/*set error to true*/
		$error = true;
		/*mostrar comentario*/
		echo 'Debe de escribir más de 5 palabras.';
		}
	/*comprobar si biene alguna foto*/
	if(!empty($file)){
		/*si biene entonces subirla*/
		if(!$documento = upload::uploadFile($path, $file, $tmp_file)){
			/*set error to true*/
			$error = true;
			/*si no se pudo subir el documento entonces mostrar mensaje*/
			echo "Ocurrió un error la subir la imagen.";
			}
		}
	
	/*comprobar si todo salio bien*/
	if($error == false){
		/*si todo esta bien guardar los datos*/
		if($query->ejecutarQuery('INSERT INTO `observaciones` (`tipo`, `referencia`, `nombre`, `email`, `comentario`, `path_file`, `fecha_creacion`) VALUES (:tipo, :referencia, :nombre, :email, :comentario, :path, :fecha)', $array_bind = array(':tipo' => $tipo, ':referencia' => $referencia, ':nombre' => $nombre, ':email' => $correo, ':comentario' => $comentario, ':path' => $documento, ':fecha' => date('Y-m-d H:i:s')))){
			/*re-enviar usuario*/
			echo "Mensaje enviado.<br>";
			echo "<a href=\"./".$_GET['evento']."/".$_GET['num']."\" class=\"btn-general grey\">Regresar</a>";
			exit;
			}
		}
	}
?>
<form name="add-reporte-evento" id="add-reporte-evento" class="form-horizontal" method="post" enctype="multipart/form-data">


	<div class="form-group">

		<label for="nombre" class="col-sm-2 text-left control-label">Nombre</label>
		<div class="col-sm-10">
			<input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo $nombre; ?>">
		</div>

	</div>

	<hr />

	<div class="form-group">

		<label for="email" class="col-sm-2 text-left control-label">Correo electrónico</label>
		<div class="col-sm-10">
			<input type="email" name="email" id="email" class="form-control" value="<?php echo $correo; ?>">
		</div>

	</div>

	<hr />

	<div class="form-group">

		<label for="comentario" class="col-sm-2 text-left control-label">Comentario / Observación</label>
		<div class="col-sm-10">
			<textarea name="comentario" id="comentario" rows="5" class="form-control" required placeholder="Al menos 5 palabras"><?php echo $comentario; ?></textarea>
		</div>

	</div>

	<hr />


	<div class="form-group">

		<label for="comentario" class="col-sm-2 text-left control-label">Agregar Foto</label>
		<div class="col-sm-6">
			<input type="file" name="foto" id="foto" accept="image/*" id="capture" capture="camera">
		</div>
		<div class="col-sm-4"><div id="contenedor-thumb-img"></div></div>

	</div>

	<hr />

	<div class="form-group">

		<div class="col-sm-12 text-right">
			<input type="hidden" name="tipo" id="tipo" value="<?php echo $_GET['evento']; ?>">
			<input type="hidden" name="referencia" id="referencia" value="<?php echo $_GET['num']; ?>">
			<button name="enviar" id="enviar" class="btn btn-success" type="submit"><i class="fa fa-comments" aria-hidden="true"></i> Comentar</button>
			<input type="reset" name="cancelar" class="btn btn-default" id="cancelar" value="Cancelar" onClick="window.history.back();">
		</div>

	</div>


</form>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			$('#contenedor-thumb-img').html();
            $('#contenedor-thumb-img').html('<img src="'+e.target.result+'" name="'+e.target.name+'" width="300" height="200">');
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$('#foto').change(function(){
	readURL(this);
	});
</script>
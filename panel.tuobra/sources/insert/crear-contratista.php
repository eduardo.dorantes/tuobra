<?php
session_start();
require("../../config/conn.php");
require("../../funciones/contratistas.class.inc.php");
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");

/*comprobar que se aya hecho post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*crea un objeto contratista*/
	$contratista = new contratista($conn, $_POST['id-contratista']);
	/*recibir las varibles*/
	$id_contratista = $_POST['id-contratista'];
	$razon = $_POST['nombre-razon'];
	$rfc = $_POST['rfc'];
	$direccion = $_POST['direccion'];
	$telefono = $_POST['telefono'];
	$email = $_POST['email'];
	/*comprobar si el objeto se creo*/
	$comprobar_objeto = false;
	
	/*comprobar los campos requeridos*/
	if(validar::notEmpty($razon) == true){//comprobar que venga el nombre del contratista
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
	if(validar::notEmpty($rfc) == true){//comprobar que venga el rfc del contratista
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
	if(validar::notEmpty($direccion) == true){//comporbar que venga la direccion del contratista
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
	if(validar::notEmpty($telefono) == true){//comprobar que venga el telfono del contratista
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}
	/*if(validar::notEmpty($email) == true){//comprobar que venga el email del contratista
		echo "<div class=\"warning\">".$mensaje['1001']."</div>";
		exit();
		}*/
		
	/*comprobar si se crear o se actuliza el objeto*/
	if(empty($id_contratista) and strlen($id_contratista) == 0){
		$comprobar_objeto = $contratista->crearContratista($_SESSION['id-cliente'], $razon, $rfc, $direccion, $telefono, $email);
		}else{
			$comprobar_objeto = $contratista->updateContratista($razon, $rfc, $direccion, $telefono, $email);
			}
			
	/*comprobar si se creo o se actulizo el objeto*/
	if($comprobar_objeto == true){
		echo "<script type=\"text/javascript\">alert('".$mensaje['1002']."');$(\"#contenedor-datos-catalogo\").remove(); $(\"#titulos h1\").text(\"Agregar nuevo contrato\");</script>";
		}
	}
?>
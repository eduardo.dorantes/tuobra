<?php
session_start();
require('../../funciones/query.class.inc.php');

/*comprobar que sea post*/
if(!empty($_POST) and $_SESSION['login'] == true){
	/*crear objeto query*/
	$query = new querys();
	/*set resultado array*/
	$resultado = array();
	/*recibir las varibles*/
	$descripcion = $_POST['descripcion-tipo'];
	$id_tipo = $_POST['id-tipo'];
	
	/*comprobar que venga la descripcion*/
	if(empty($descripcion)){
		$resultado['status'] = false;
		$resultado['msg'] = "Debe de introducir una descripción.";
		}else{
			/*determinar query*/
			if(empty($id_tipo)){
				$sql_query = "INSERT INTO `tipo_obra` (id_cliente, descripcion) VALUES (:id_cliente, :desc)";
				$array_bind = array(':id_cliente' => $_SESSION['id-cliente'], ':desc' => $descripcion);
				}else{
					$sql_query = "UPDATE `tipo_obra` SET descripcion = :desc WHERE id = :id_tipo";
					$array_bind = array(':desc' => $descripcion, ':id_tipo' => $id_tipo);
					}
			/*ejecutar query*/
			if($query->ejecutarQuery($sql_query, $array_bind)){
				$resultado['status'] = true;
				$resultado['msg'] = "<div class=\"success\">Los datos se han guardado correctamente.</div>";
				}
			}
		
	echo json_encode($resultado);
	}
?>
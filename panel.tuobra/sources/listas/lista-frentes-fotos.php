<?php
session_start();
/********************************/
//LISTA DE FRENTES
/********************************/
header("cache-control: no-cache");
include("../../config/conn.php");


if(!empty($_POST) and $_SESSION['login'] == true){
	/*conexcion a la db*/
	$db = $conn;
	/*get id del contrato*/
	$id_contrato = $_POST['id_contrato'];
	
	/*query string*/
	$query = $db->prepare("SELECT ID, DESCRIPCION FROM avance_fotografico_frentes WHERE ID_CONTRATO = ?");
	if($query === false){
		trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
		}
	/*preparar query*/
	$query->bind_param('i', $id_contrato);
	/*ejecutar query*/
	$query->execute();
	/*almacenar los resultados*/
	$query->store_result();
	/*bind resultados*/
	$query->bind_result($id_frente, $descripcion);
	/*comprobar que aya frentes*/
	if($query->num_rows > 0){
		/*loop para sacar los resultados*/
		while($query->fetch()){
			echo "<div class=\"titulo-fecha\" id=\"".$id_frente."\"><span><h1>".$descripcion."</h1></span></div><div class=\"contenedor-imagenes\"><div class=\"contenedor-imagenes-inner\">";
			/*query para sacar las imagenes*/
			$query_img = $db->prepare("SELECT AF.ID, AF.FECHA, AF.IMAGEN, AF.THUMB, str_to_date(AF.FECHA, '%d/%m/%Y'), AF.PIE_FOTO FROM `avance_fotografico` AF WHERE AF.ID_FRENTE = ? ORDER BY FECHA");
			/*comprobar el query*/
			if($query_img === false){
				trigger_error('Ocurrio un problema con el query: ' . $sql_query . ' Error: ' . $this->db->error, E_USER_ERROR);
				}
			/*bind parametros*/
			$query_img->bind_param('i', $id_frente);
			/*ejecutar el query*/
			$query_img->execute();
			/*almacenar los resultados*/
			$query_img->store_result();
			/*bind resultados*/
			$query_img->bind_result($id, $fecha, $imagen, $thumb, $fecha_orden, $pie);
			/*comprobar que vengan resultados*/
			if($query_img->num_rows > 0){
				/*loop para sacar los resultados*/
				while($query_img->fetch()){
					/*resultados*/
					echo "<figure id=\"foto_".$id."\" data-id=\"".$id."\"><img src=\"".$imagen."\" alt=\"\" width=\"240\" height=\"175\"><div class=\"borrar-imagen\"><span><a href=\"javascript:;\" id=\"".$id."\" date=\"".str_replace('/', '', $fecha)."\" class=\"borrar-imagen-click\">&#10006;</a></span></div><figcaption"; echo empty($pie) ? " class=\"add-pie-fig\"": ''; echo ">"; echo !empty($pie) ? "<p>".$fecha." - ".$pie."</p>" : 'Agregar Pie Imagen/Descripción'; echo "</figcaption></figure>";
					}
				}
			/*opcion de agregar nueva imagen*/
			echo "<div class=\"contenedor-add-frente-imagen-nueva\" data-id=\"".$id_contrato."\" data-rel=\"".$id_frente."\"><a href=\"javascript:;\">Agregar nueva imagen</a>";
			echo "</div></div>";
			}
		}
	
	/*cerrar query*/
	$query->close();
	/*cerrar conexion*/
	$db->close();
	}
?>
<?php
session_start();
require('../../funciones/query.class.inc.php');
require("../../funciones/validar.formularios.class.inc.php");
require("../../sources/msg-file.php");

/*comprobar que sea post*/
if(!empty($_POST)){
	/*crear un nuevo objeto query*/
	$query = new querys();
	/*reicibr las varibles*/
	$nombre = $_POST['nombre'];
	$nombre_corto = $_POST['nombre_corto'];
	$direccion = $_POST['direccion'];
	$municipio = $_POST['municipio'];
	$telefono = $_POST['telefono'];
	$email = $_POST['email'];
	$responsable = $_POST['nombre-responsable'];
	$custom_url = $_POST['url-cliente'];
	
	/*comprobar los campos requeridos*/
	/*validar que venga el nombre*/
	if(validar::notEmpty($nombre) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."1</div>";
		exit;
		}
	/*validar el municipio*/
	if(validar::notEmpty($municipio) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."2</div>";
		exit;
		}
	/*validar el responsable*/
	if(validar::notEmpty($responsable) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."3</div>";
		exit;
		}
	/*validar el correo electronico*/
	if(validar::notEmpty($email) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."4</div>";
		exit;
		}
	/*validar la custom url*/
	if(validar::notEmpty($custom_url) == true){
		echo "<div class=\"warning\">".$mensaje['1001']."5</div>";
		exit;
		}
	
	/*ini sql query string*/
	$query_string = "INSERT INTO `clientes` (`NOMBRE`, `NOMBRE_CORTO`, `DIRECCCION`, `TELEFONO`, `MUNICIPIO`, `RESPONSABLE`, `EMAIL`, `CUSTOMURL`, `CUSTOMURLMD5`) VALUES (:nombre, :nombre_corto, :direccion, :telefono, :municipio, :responsable, :email, :custom_url, :custom_md5)";
	/*ini array bind*/
	$array_bind = array();
	$array_bind[':nombre'] = $nombre;
	$array_bind[':nombre_corto'] = $nombre_corto;
	$array_bind[':direccion'] = $direccion;
	$array_bind[':telefono'] = $telefono;
	$array_bind[':municipio'] = $municipio;
	$array_bind[':responsable'] = $responsable;
	$array_bind[':email'] = $email;
	$array_bind[':custom_url'] = $custom_url;
	$array_bind[':custom_md5'] = md5($custom_url);
	/*ejecutar y comprobar query*/
	if($query->ejecutarQuery($query_string, $array_bind)){
		echo "<div class=\"success\">".$mensaje['1002']."</div>";
		}
	}
?>
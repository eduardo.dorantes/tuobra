
<?php

$meta = '<meta property="og:site_name" content="tuObra" />
<meta property="og:site" content="tuobra.sonora.gob.mx" />
<meta property="og:title" content="Portal TuObra.mx | Testigo Virtual Ciudadano" />
<meta property="og:description" content="El portal TuObra.mx es una herramienta tecnológica diseña para la rendición de cuentas y el fácil acceso del ciudadano a la información de las Licitaciones y Obra Pública en el Estado de Sonora." />
<meta property="og:image" content="http://pruebastuobra.sonora.gob.mx/assets/img/share-tu-obra-01.png" />
<meta property="og:url" content="http://tuobra.sonora.gob.mx/" />
<meta property="og:type" content="article" />
<meta name="twitter:card" content=\'summary\' />
<meta name="twitter:image" content="http://pruebastuobra.sonora.gob.mx/assets/img/share-tu-obra-01.png" />
<meta name="twitter:site" content="@gobiernosonora" />
<meta name="twitter:url" content="http://tuobra.sonora.gob.mx/" />
<meta name="twitter:description" content="El portal TuObra.mx es una herramienta tecnológica diseña para la rendición de cuentas y el fácil acceso del ciudadano a la información de las Licitaciones y Obra Pública en el Estado de Sonora." />
<meta name="twitter:title" content="Portal TuObra.mx | Testigo Virtual Ciudadano" />
<title>Portal TuObra.mx | Testigo Virtual Ciudadano</title>
<meta name="description" content="Portal TuObra.mx | Testigo Virtual Ciudadano">';

$body = "";
include('../include/header.php');

?>


<?php

$include = '';
$includeT = 1;
$mbn = "";
switch($_GET['t']){
	case 'buscar':
		$include = '../include/forms/buscar-obras.php';
		break;
	case 'mapa':
		$include = '../include/obras-mapa.php';
		$includeT = 2;
		$mbn = "m-b-n";
		break;
	default:
		$include = '../include/forms/buscar-obras.php';
		break;
}
?>



<section id="breadcrumbs" class="<?php echo $mbn; ?>">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-7">
				<h2>Obra Pública</h2>
				<p>Inicio | Obra Pública | <?php if($_GET['t'] == 'buscar'){ echo 'Listado';}elseif(!isset($_GET['t'])){ echo 'Listado'; }elseif($_GET['t'] == 'mapa'){ echo 'Mapa';} ?></p>
			</div>

			<div class="col-sm-6 col-xs-5">
				<ul id="filterTypeWorks" class="nav navbar-nav navbar-right">
					<li><a href="tuobra/?t=buscar" class="tu-obra-magnifying-glass <?php if($_GET['t'] == 'buscar'){ echo 'selected-obra-opcion';}elseif(!isset($_GET['t'])){ echo 'selected-obra-opcion'; } ?>" title="Listado de obras"><i class="fa fa-list" aria-hidden="true"></i></a></li>
					<li><a href="tuobra/?t=mapa" class="tu-obra-thumb-tack <?php if($_GET['t'] == 'mapa'){ echo 'selected-obra-opcion';} ?>" title="Obras en el mapa"><i class="fa fa-map" aria-hidden="true"></i></a></li>
				</ul>
			</div>


		</div>
	</div>

</section>







<?php if($includeT==1){?>
<section id="contentForm">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php include($include); ?>
				<div id="listado-licitaciones"></div>

			</div>
		</div>
	</div>
</section>
<?php }else{
	include($include);
} ?>






<?php include('../include/footer.php'); ?>

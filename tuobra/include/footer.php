<section id="transparencyFooter">

    <div class="container">
        <div class="row">


            <div class="col-sm-3">
                <a class="m-t-b banner" href="http://www.plataformadetransparencia.org.mx/" target="_blank" title="Plataforma Nacional de Transparencia">
                    <img src="http://www.sonora.gob.mx/images/pnt-01.png" alt="Plataforma Nacional de Transparencia" height="72" width="262">
                </a>
            </div>


            <div class="col-sm-3">
                <a class="m-t-b btn btn-danger btn-lg btn-block" href="http://transparencia.hidalgo.gob.mx/" target="_blank">Portal de transparencia <br />del Estado de Hidalgo</a>
            </div>


            <div class="col-sm-3">
                <a class="m-t-b banner" href="http://www.gobiernosabiertos.org.mx" target="_blank">
                    <img src="assets/img/banner-gobiernos-abiertos-01.png" alt="Plataforma Nacional de Transparencia" height="72" width="262">

                </a>
            </div>


            <div class="col-sm-3">
                <a class="m-t-b banner" href="http://www.mxabierto.org/" target="_blank">
                    <img src="assets/img/banner__mexico-abierto.png" alt="Red México Abierto" height="72" width="262">

                </a>
            </div>


        </div>
    </div>

</section>


<section id="subfooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">

                <ul class="menu-inferior">
                    <li class="item-234"><a href="http://www.sonora.gob.mx/acerca-del-portal/">Acerca del Portal</a></li>
                    <li class="item-244"><a href="http://www.sonora.gob.mx/denuncia-ciudadana/">Denuncia Ciudadana</a></li>
                    <li class="item-399"><a href="http://www.sonora.gob.mx/politicas-uso/">Políticas de uso y privacidad</a></li>
                    <li class="item-400"><a href="http://www.sonora.gob.mx/mapa-del-sitio/">Mapa del Sitio</a></li>
                </ul>

            </div>

            <div class="col-sm-4">

                <ul class="social-icons">
                    <li><a target="_blank" href="https://www.facebook.com/contraloriaHGO"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" href="https://twitter.com/Contraloria_Hgo"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="https://www.youtube.com/user/ccsocialgobhgo"><i class="fa fa-youtube"></i></a></li>
                </ul>


            </div>


        </div>
    </div>

</section>
<!-- footer -->
<footer class="text-center">
    <!-- inner -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class=" " style=" text-align: center; clear: both;"><img src="http://tuzobra.cf/tuobra/images/hidalgo-crece.png" height="50"
                       style="max-height:50px; height:50px"/></p>
                <p> Secretaría de la Contraloría del Estado de Hidalgol, Allende #901, esq. Belisario Domínguez, CP 42000, Pachuca de Soto, Hidalgo.<br>Teléfonos: +52 (771) 717-60-00 ext. 1834, +52 (771) 718-45-89, +52 (771) 713-22-65.
                    <br><a href="www.s-contraloria.hidalgo.gob.mx">www.s-contraloria.hidalgo.gob.mx</a>
                    <br><a href="www.hidalgo.gob.mx">www.hidalgo.gob.mx</a></p>

                <p>
                    <small>2017-2021 Portal del Gobierno del Estado de Hidalgo. Secretaría de la Contraloría.
                        Subdirección de Sistemas y Soporte Técnico. Todos los derechos reservados
                    </small>
                </p>
            </div>

        </div>

    </div>

</footer>


<script>
    $(document).ready(function () {


        $('.btnTopPage > .btn').on("click", function () {
            $('html, body').animate({scrollTop: 0}, 'slow', function () {
                //alert("reached top");
            });
        });

    });
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74793739-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-79480008-1', 'auto');
 ga('send', 'pageview');

</script>


</body>
</html>
<?php
require('../librerias/query.class.inc.php');
require('../librerias/config.php');
require_once('../requires/Mobile_Detect.php');

/*comprobar que sea post*/
if(!empty($_POST)){
	/*crear nuevo objeto query*/
	$query = new querys();
	/*nuevo objeto dispositivo*/
	$detectar = new Mobile_Detect();
	/*query aux*/
	$sql_aux = "";
	/*recibir las varibles*/
	$ejercicio_del = $_POST['ejercicio-del'];
	$ejercicio_al = $_POST['ejercicio-al'];
	$tipo = $_POST['tipo'];
	$dependencia = $_POST['dependencia'];
	$estatus = $_POST['estatus'];
	$search = '%'.$_POST['q'].'%';
	
	/*array bind*/
	$array_bind = array(':del' => $ejercicio_del, ':al' => $ejercicio_al, ':search' => $search);
	
	/*determinar tipo*/
	if(!empty($tipo)){
		$sql_aux .= " AND l.tipo = :tipo";
		$array_bind[':tipo'] = $tipo;
		}
	/*determinar dependencia*/
	if(!empty($dependencia)){
		$sql_aux .= " AND l.id_dependencia = :cliente";
		$array_bind[':cliente'] = $dependencia;
		}
	/*determinar estatus*/
	if(!empty($estatus)){
		/*fecha de hoy*/
		$hoy = date('Y-m-d');
		switch($estatus){
			case 1:
				$sql_aux .= " AND (str_to_date(le.fecha, '%d/%m/%Y') IS NULL OR str_to_date(le.fecha, '%d/%m/%Y') >= '$hoy')";
			break;
			case 2:
				$sql_aux .= " AND (str_to_date(le.fecha, '%d/%m/%Y') < '$hoy')";
			break;
			}
		}
	
	/*query string*/
	$query_string = "SELECT l.num_licitacion, l.descripcion, str_to_date(l.fecha_publiacion, '%d/%m/%Y') as fecha_publiacion, l.url_compranet, l.normatividad, c.NOMBRE, c.NOMBRE_CORTO FROM `licitaciones` l INNER JOIN `clientes` c ON l.id_dependencia = c.ID LEFT JOIN `licitaciones_eventos` le ON le.id_licitacion = l.id AND le.tipo = 4 WHERE YEAR(str_to_date(l.fecha_publiacion, '%d/%m/%Y')) BETWEEN :del AND :al AND (l.num_licitacion LIKE :search OR l.descripcion LIKE :search OR c.NOMBRE LIKE :search) $sql_aux ORDER BY str_to_date(l.fecha_publiacion, '%d/%m/%Y') DESC";
	
	/*ejecutar y comprobar query*/
	if($rows = $query->traerMultiplesResultados($query_string, $array_bind)){
		echo "<table class=\"table table-bordered table-hover table-striped\" border=\"0\" width=\"100%\">";
		echo "<thead><tr>";
			//echo "<th></th>";
			echo "<th class='col-sm-2 col-xs-12'>No.</th>";
			echo "<th class='col-sm-1'>Fecha</th>";
			echo "<th class='col-sm-6'>Descripción</th>";
			echo "<th class='col-sm-3'>Dependencia/Organismo</th>";
		echo "</tr></thead><tbody valing=\"top\">";
		/*loop*/
		foreach($rows as $row){
			echo "<tr>";
				//7echo "<td><a title='Ver detalle: ".$row['num_licitacion']."' href=\"licitacion/".$row['num_licitacion']."\" class=\"detalles detalles-licitacion\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i></a></td>";
				echo "<td><a title='Ver detalle: ".$row['num_licitacion']."' href=\"licitacion/".$row['num_licitacion']."\" class=\"detalles detalles-licitacion\">".$row['num_licitacion']."</a></td>";
				echo "<td><a title='Ver detalle: ".$row['num_licitacion']."' href=\"licitacion/".$row['num_licitacion']."\" class=\"detalles detalles-licitacion\">".$row['fecha_publiacion']."</a></td>";
				echo "<td><a title='Ver detalle: ".$row['num_licitacion']."' href=\"licitacion/".$row['num_licitacion']."\" class=\"detalles detalles-licitacion\">".$row['descripcion']."</a></td>";
				if(!$detectar->isMobile()){
					echo "<td><a href=\"licitacion/".$row['num_licitacion']."\" class=\"tu-obra-info-circle detalles detalles-licitacion\">".$row['NOMBRE']."</a></td>";
					}else{
						echo "<td><a href=\"licitacion/".$row['num_licitacion']."\" class=\"tu-obra-info-circle detalles detalles-licitacion\">".strtoupper($row['NOMBRE_CORTO'])."</a></td>";
						}
			echo "</tr>";
			}
		
		/*cerrar tabla*/
		echo "</tbody></table>";
		}
	}
?>